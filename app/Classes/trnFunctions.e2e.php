<?php
   session_start();
   include_once "constant.e2e.php";
   include_once pathClass.'0620functions.e2e.php';


   class dbTRN {
      function insert() {

      }
      function AddSaveRecord($table,$fld,$val) {

      }
      function EditSaveRecord($table,$fld,$val) {

      }

   }

   if (isset($_FILES['imageComment']) && $_POST["hPostNewComment"] == "new") {
      if (!empty($_FILES['imageComment']['tmp_name'])) {
         $t = time();
         $filename = base64_encode(date("Ymd",$t).date("His",$t)."_".$_POST["char_CommentBy"]."_".$_POST["sint_issuesRefId"]);
         $target_dir = path."uploads/";
         $old_filename = basename($_FILES['imageComment']['name']);
         $imageFileType = pathinfo($target_dir.$old_filename,PATHINFO_EXTENSION);
         $new_filename = $filename.".".$imageFileType;
         if(move_uploaded_file($_FILES['imageComment']['tmp_name'], pathPublic."images/Comments/" . $new_filename)){
            $comments = $_POST["char_Comments"];
            $commentby = $_POST["char_CommentBy"];
            $issuesRefId = $_POST["sint_issuesRefId"];
            $Fields = "`issuesRefId`, `Comments`, `CommentBy`, `PictureComments`,";
            $Values = "$issuesRefId, '$comments', '$commentby', '$new_filename',";
            $SaveSuccessfull = f_SaveRecord("NEWSAVE","issuescomments",$Fields,$Values);
            if (!is_numeric($SaveSuccessfull)) {
               echo $SaveSuccessfull;
            }
         } else {
           echo $_FILES['imageComment']['name']. " KO";
         }
      } else {
         $comments = $_POST["char_Comments"];
         $commentby = $_POST["char_CommentBy"];
         $issuesRefId = $_POST["sint_issuesRefId"];
         $Fields = "`issuesRefId`, `Comments`, `CommentBy`,";
         $Values = "$issuesRefId, '$comments', '$commentby',";
         $SaveSuccessfull = f_SaveRecord("NEWSAVE","issuescomments",$Fields,$Values);
         if (!is_numeric($SaveSuccessfull)) {
            echo $SaveSuccessfull;
         }
      }
      return;
   }


   if (isset($_FILES['postImage']) && $_POST["hPostNewIssue"] == "new") {
      if (
            $_POST["char_Issue"] != "" &&
            $_POST["char_Description"] != ""
         )
      {
         if (!empty($_FILES['postImage']['tmp_name'])) {
            $t = time();
            $filename = base64_encode(date("Ymd",$t).date("His",$t)."_".$_POST["char_CreatedBy"]);
            $target_dir = path."uploads/";
            $old_filename = basename($_FILES['postImage']['name']);
            $imageFileType = pathinfo($target_dir.$old_filename,PATHINFO_EXTENSION);
            $new_filename = $filename.".".$imageFileType;
            if(move_uploaded_file($_FILES['postImage']['tmp_name'], pathPublic."images/Comments/" . $new_filename)){
               $issue = $_POST["char_Issue"];
               $desc = $_POST["char_Description"];
               $createdby = $_POST["char_CreatedBy"];
               $status = $_POST["char_Status"];
               $Assigned = $_POST["drpAssigned"];
               $Fields = "`Issue`, `Description`, `CreatedBy`, `ImageFile`, `Status`, `AssignedTo`,";
               $Values = "'$issue', '$desc', '$createdby', '$new_filename', '$status', '$AssignedTo',";
               $SaveSuccessfull = f_SaveRecord("NEWSAVE","issues",$Fields,$Values);
               if (!is_numeric($SaveSuccessfull)) {
                  echo $SaveSuccessfull;
               } else {
                  header("Location:scrnIssues.e2e.php?id=".$SaveSuccessfull);
               }
            } else {
              echo $_FILES['postImage']['name']. " KO";
            }
         } else {
            $issue = $_POST["char_Issue"];
            $desc = $_POST["char_Description"];
            $createdby = $_POST["char_CreatedBy"];
            $status = $_POST["char_Status"];
            $Assigned = $_POST["drpAssigned"];
            $Fields = "`Issue`, `Description`, `CreatedBy`, `Status`, `AssignedTo`,";
            $Values = "'$issue', '$desc', '$createdby', '$status', '$AssignedTo',";
            $SaveSuccessfull = f_SaveRecord("NEWSAVE","issues",$Fields,$Values);
            echo $SaveSuccessfull;
            if (!is_numeric($SaveSuccessfull)) {
               echo $SaveSuccessfull;
            } else {
               header("Location:scrnIssues.e2e.php?id=".$SaveSuccessfull);
            }
         }
      }
   }
?>