<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LoanInfo extends Model
{
    protected $table = 'pms_loansinfo';
    protected $fillable = [
    	'employeeinfo_id',
        'employee_id',
    	'loan_id',
    	'loan_totalamount',
    	'loan_totalbalance',
    	'loan_amortization',
    	'loan_pay_period',
    	'loan_date_granted',
    	'loan_date_started',
    	'loan_date_end',
    	'loan_date_terminated'
    ];

    public function loans(){
        return $this->belongsTo('App\Loan','loan_id');
    }

    public function employeeinfo(){
        return $this->belongsTo('App\EmployeeInfo','employeeinfo_id');
    }

    public function employees(){
        return $this->belongsTo('App\Employee','employee_id');
    }

}
