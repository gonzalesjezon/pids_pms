<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Adjustment extends Model
{
    protected $table = 'pms_adjustments';
    protected $fillable = [
    	'code',
    	'name',
    	'tax_type',
    	'amount',
    	'payroll_group',
    	'itr_classification',
    	'alpahlist_classification',
    	'remarks',
    	
    ];
}
