<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmployeeStatus extends Model
{
    // protected $table = 'pms_employee_status';
    // protected $fillable = [
    // 	'code',
    // 	'name',
    // 	'category',
    // 	'created_by',
    // 	'updated_by'
    // ];
    protected $primaryKey = 'RefId';
    protected $table = 'empstatus';
    protected $fillable = [
    	'Code',
    	'Name',
    	'category',
    	'created_by',
    	'updated_by'
    ];

}
