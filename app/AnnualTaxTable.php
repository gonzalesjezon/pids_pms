<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AnnualTaxTable extends Model
{
    protected $table = 'pms_taxannualtable';
    protected $fillable = [
    	'below',
    	'above',
    	'rate',
    	'effectivity_date',
    	'remarks',
    ];
}
