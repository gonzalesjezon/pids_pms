<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ResponsibilityCenter extends Model
{
    protected $table = 'responsibilitycenter';
    protected $fillable = [
    	'code',
    	'name',
    	'description',
    	'remarks'
    ];
}
