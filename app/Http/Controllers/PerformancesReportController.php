<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input;
use App\Transaction;
use App\BenefitInfo;
use App\Benefit;
class PerformancesReportController extends Controller
{
    function __construct(){
		$this->title = 'PERFORMANCE ENHANCEMENT INCENTIVE';
    	$this->module = 'performancesreport';
        $this->module_prefix = 'payrolls/reports';
    	$this->controller = $this;
	}

	public function index(){


    	$response = array(
    					'module'        => $this->module,
    					'controller'    => $this->controller,
                        'module_prefix' => $this->module_prefix,
    					'title'		    => $this->title
    					);

    	return view($this->module_prefix.'.'.$this->module,$response);
    }


    public function getEmployeeinfo(){

        $q = Input::all();

        $year = $q['year'];
        $month = $q['month'];
        $employee_id = Transaction::where('year',$year)->where('month',$month)->select('employee_id')->get()->toArray();
        $benefit_id = Benefit::where('code','PEI')->select('id')->first();
        $query = BenefitInfo::whereIn('employee_id',$employee_id)
                            ->where('benefit_id',$benefit_id->id)
                            ->with(['employees','employeeinformation'=>function($qry){
                            $qry->with(['offices','positionitems'=>function($qry){
                                    $qry->with('positions'); }]);
                            }])->get();

        if(count($query) > 0){
            foreach ($query as $key => $value) {

                $data[$value->employeeinformation->offices->name][$key] = $value;
            }

        }else{
            $data = [];
        }


        return json_encode($data);
    }
}
