<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Input;
use App\JobGrade;


class JobGradesController extends Controller
{
    function __construct(){
        $this->controller = $this;
        $this->title = 'JOB GRADE';
        $this->module_prefix = 'payrolls/admin/filemanagers';
        $this->module = 'jobgrades';
        $this->table = 'pms_jobgrade';
    }

    public function index(){

        $jg_data = JobGrade::get();

        $response = array(
            'jg_data'       => $jg_data,
            'controller'    => $this->controller,
            'title'         => $this->title,
            'module'        => $this->module,
            'module_prefix' => $this->module_prefix,
        );

        return view($this->module_prefix.'.'.$this->module.'.index',$response);
    }

    public function store(Request $request){

        $this->validate(request(),[
            'job_grade'         => 'required',
            'step_inc'             => 'required',
            'amount'               => 'required',
            // 'effectivity_date'     => 'required',
        ]);

        $jobgrade = new JobGrade;
        $amount = str_replace(',', '', $request->amount);
        $step = "";
        switch ($request->step_inc){
            case 'step1':
                $step = 'step1';
                $jobgrade->step1 = $amount;
                break;
            case 'step2':
                $step = 'step2';
                $jobgrade->step2 = $amount;
                break;
            case 'step3':
                $step = 'step3';
                $jobgrade->step3 = $amount;
                break;
            case 'step4':
                $step = 'step4';
                $jobgrade->step4 = $amount;
                break;
            case 'step5':
                $step = 'step5';
                $jobgrade->step5 = $amount;
                break;
            case 'step6':
                $step = 'step6';
                $jobgrade->step6 = $amount;
                break;
            case 'step7':
                $step = 'step7';
                $jobgrade->step7 = $amount;
                break;
            case 'step8':
                $step = 'step8';
                $jobgrade->step8 = $amount;
                break;
            default:
                # code...
                break;
        }

        $check = JobGrade::where('job_grade',$request->job_grade)->first();

        if(isset($check)){

            $jobgrade->where('id',$check->id)->update([$step => $amount]);

            $response = json_encode(['status'=>true,'response' => 'Save Successfully!']);

        }else{

            $jobgrade->fill($request->all())->save();

            $response = json_encode(['status'=>true,'response' => 'Save Successfully!']);

        }

        return $response;
        
    }

    public function show(){

        $q = Input::get('q');
        // $limit = Input::get('limit');
        // if(empty($limit)){ $limit  =  10; } 

        $data = $this->get_records($q);

        $response = array(
                        'data'          => $data,
                        'controller'    => $this->controller,
                        'module'        => $this->module,
                        'module_prefix' => $this->module_prefix,
                        'cols'          => $this->table_columns($this->table)

                        );
        return view($this->module_prefix.'.'.$this->module.'.datatable',$response);

    }


    private function get_records($q){

        $cols = ['job_grade'];

        $query = JobGrade::where(function($query) use($cols,$q){

                $query = $query->where(function($qry) use($q, $cols){
                    foreach ($cols as $key => $value) {
                        $qry->orWhere($value,'like','%'.$q.'%');
                    }
                });
        });
        $response = $query->get();

        return $response;

    }

    public function getJgstep(){
        $id = Input::get('id');
        $step_no = Input::get('step_no');

        $amount = JobGrade::where('id',$id)->first([$step_no])->toArray();

        return $amount;
    }
}
