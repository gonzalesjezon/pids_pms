<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\EmployeeInfo;
use App\Transaction;
use App\Employee;
use App\LoanInfo;
use Input;
class PhilhealthReportsController extends Controller
{
    function __construct(){
    	$this->title = 'Philhealth Reports';
    	$this->module = 'philhealthreports';
        $this->module_prefix = 'payrolls/reports';
    	$this->controller = $this;

    }

    public function index(){


    	$response = array(
    					'module'        => $this->module,
    					'controller'    => $this->controller,
                        'module_prefix' => $this->module_prefix,
    					'title'		    => $this->title
    					);

    	return view($this->module_prefix.'.'.$this->module,$response);
    }

    public function show(){

        $q = Input::all();

        $transaction  = new Transaction;

        $year = $q['year'];
        $month = $q['month'];

        $query = $transaction
        ->with('employees','employeeinfo')
        ->where('year',$year)
        ->where('month',$month)
        ->get();

        $data = [];
        if(count($query) > 0){
            foreach ($query as $key => $value) {
                if(isset($value->department_id)){
                    $data[$value->departments->name][$key] = $value;
                }
            }

        }else{
            $data = [];
        }

        return json_encode($data);
    }
}
