<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Input;
use App\Adjustment;

class AdjustmentsController extends Controller
{
    function __construct(){
    	$this->controller = $this;
    	$this->title = 'ADJUSTMENTS';
    	$this->module_prefix = 'payrolls/admin/filemanagers';
        $this->module = 'adjustments';
        $this->table = 'adjustments';

    }

    public function index(){
    	$response = array(
    		'controller'	=> $this->controller,
    		'title'			=> $this->title,
    		'module'		=> $this->module,
    		'module_prefix'	=> $this->module_prefix,
    	);

    	return view($this->module_prefix.'.'.$this->module.'.index',$response);
    }

     public function store(Request $request){

        $this->validate(request(),[
            'code'                  => 'required',
            'name'                  => 'required',
            // 'payroll_group'         => 'required',
            'tax_type'              => 'required',
            // 'itr_classification'    => 'required',
            // 'alphalist_classification' => 'required',
            'amount'                => 'required',
        ]);

        $adjustment = new Adjustment;
        
        $amount = str_replace(',', '', $request->amount);
        
        if(isset($request->for_update)){

            $adjustment = Adjustment::find((int)$request->for_update);

            $adjustment->amount = $amount;

            $adjustment->save();

            $response =  json_encode(['status'=>true,'response' => 'Update Successfully!']);

        }else{

            $check = $adjustment->where('code',$request->code)->get()->toArray();

            if(count($check) > 0){

                $response = json_encode(['status'=>false,'response' => 'Code already exists!']);

            }else{

                $request->merge(['amount' => str_replace(',', '', $amount)]);
                
                $adjustment->fill($request->all())->save();

                $response =  json_encode(['status'=>true,'response' => 'Save Successfully!']);
            }
        }

        return $response;
        
    }


    public function show(){

        $q = Input::get('q');
        // $limit = Input::get('limit');
        // if(empty($limit)){ $limit  =  10; } 

        $data = $this->get_records($q);

        $response = array(
                        'data'          => $data,
                        'controller'    => $this->controller,
                        'module'        => $this->module,
                        'module_prefix' => $this->module_prefix,
                        'cols'          => $this->table_columns($this->table)

                        );
        return view($this->module_prefix.'.'.$this->module.'.datatable',$response);

    }


    private function get_records($q){

        $cols = ['code','name','tax_type','payroll_group'];


        $query = Adjustment::where(function($query) use($cols,$q){

                $query = $query->where(function($qry) use($q, $cols){
                    foreach ($cols as $key => $value) {
                        $qry->orWhere($value,'like','%'.$q.'%');
                    }
                });
        });
        $response = $query->get();

        return $response;

    }

    public function getItem(){
        $id = Input::get('id');

        $query = Adjustment::where('id',$id)->first();

        return json_encode($query);
    }
}
