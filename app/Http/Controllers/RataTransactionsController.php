<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Employee;
use App\EmployeeStatus;
use App\EmployeeInformation;
use App\Benefit;
use App\EmployeeInfo;
use App\BenefitInfo;
use App\Company;
use App\Division;
use App\Position;
use App\Department;
use App\Office;
use App\Rata;
use App\Leave;
use App\Transaction;
use Input;

class RataTransactionsController extends Controller
{
    function __construct(){
        $this->title = 'RATA TRANSACTIONS';
        $this->controller = $this;
        $this->module = 'ratatransactions';
        $this->module_prefix = 'payrolls/specialpayrolls';
    }

    public function index(){


        $response = array(
           'title'          => $this->title,
           'controller'        => $this->controller,
           'module'         => $this->module,
           'module_prefix'     => $this->module_prefix,

       );

        return view($this->module_prefix.'.'.$this->module.'.index',$response);
    }

    public function storeRata(Request $request){

        $check = Leave::where('employee_id',$request['employee_id'])
                        ->where('leave_date',$request['leave']['inclusive_leave_date'])
                        ->first();

        if(!isset($check)){

            $rata = new Rata;

            $representation_amount = ($request['rata']['representation_amount'] * $request['rata']['percentage_of_rata_value']);
            $transportation_amount = ($request['rata']['transportation_amount'] * $request['rata']['percentage_of_rata_value']);


            $rata = Rata::find($request['rata_id']);

            $rata->number_of_actual_work = $request['rata']['no_of_actual_work'];
            $rata->number_of_work_days = $request['rata']['no_of_work_days'];
            $rata->number_of_used_vehicles = $request['rata']['no_of_used_vehicles'];
            $rata->percentage_of_rata = $request['rata']['percentage_of_rata'];
            $rata->percentage_of_rata_value = $request['rata']['percentage_of_rata_value'];
            $rata->year = $request['rata']['year'];
            $rata->month = $request['rata']['month'];

            $rata->representation_amount = $representation_amount;
            $rata->transportation_amount = $transportation_amount;

            $rata->save();

            if($request['leave']){
                $leave = new Leave;

                $leave->rata_id = $request['rata_id'];
                $leave->employee_id = $request['employee_id'];
                $leave->leave_type = $request['leave']['leave_type'];
                $leave->leave_date = $request['leave']['inclusive_leave_date'];
                $leave->number_of_leave_field = $request['leave']['no_of_leave_field'];

                $leave->save();
            }

            $query['rata'] = Rata::where('employee_id',$request['employee_id'])
                        ->where('year',$request['rata']['year'])
                        ->where('month',$request['rata']['month'])
                        ->first();

            $query['leave'] = Leave::where('rata_id',$request['rata_id'])
                                ->where('employee_id',$request['employee_id'])
                                ->select('leave_type','leave_date','number_of_leave_field')
                                ->get();

            $leave_list = [];
            foreach ($query['leave'] as $key => $value) {
                $leave_list[$value->leave_type][$key] = date('d',strtotime($value->leave_date));

            }

            $query['leave_list'] = $leave_list;

            $response = json_encode(['status'=>true,'response'=>'Save Successfully!','rata'=>$query]);
        }else{
            $response = json_encode(['status'=>false,'response'=>'Date already taken!']);
        }

        return $response;

    }

    public function show(){

        $q = Input::get('q');
        $check_rata = Input::get('check_rata');
        $empstatus = Input::get('empstatus');
        $category = Input::get('category');
        $emp_type = Input::get('emp_type');
        $searchby = Input::get('searchby');
        $year = Input::get('year');
        $month = Input::get('month');

        $data = "";

        $data = $this->searchName($q,$check_rata,$year,$month);

        if(isset($empstatus) || isset($category)){
            $data = $this->filter($empstatus,$category,$emp_type,$searchby);

        }

        $response = array(
            'data'          => $data,
            'title'         => $this->title,
            'controller'    => $this->controller,
            'module'        => $this->module,
            'module_prefix' => $this->module_prefix
        );

        return view($this->module_prefix.'.'.$this->module.'.namelist',$response);

    }

    public function searchName($q,$check_rata,$year,$month){

        $cols = ['lastname','firstname'];

        $benefit_id = Benefit::whereIn('code',['TRANSPO','REP'])
        ->select('id')->get()->toArray();

        $employee_id = BenefitInfo::whereIn('benefit_id',$benefit_id)->select('employee_id')->groupBy('employee_id')->get()->toArray();

        $rata_employee_id = Rata::whereIn('employee_id',$employee_id)
                                ->where('year',$year)
                                ->where('month',$month)
                                ->select('employee_id')
                                ->get()->toArray();

        $arr_employee_id = array();

        switch ($check_rata) {
            case 'wrata':
                $arr_employee_id = $rata_employee_id;
            break;

            default:
                $_employee_id = $rata_employee_id;

                if(count($employee_id) > 0){

                    foreach ($employee_id as $key => $value) {
                        $new_employee_id[] =  $value['employee_id'];
                    }

                    foreach ($_employee_id as $key => $value) {
                        $new_rata_employee_id[] =  $value['employee_id'];
                    }

                    if(isset($new_employee_id) && isset($new_rata_employee_id)) {

                        $arr_employee_id = array_diff($new_employee_id, $new_rata_employee_id);

                        if(!isset($arr_employee_id)){
                            $arr_employee_id = [];
                        }
                    }else{
                        $arr_employee_id = $new_employee_id;
                    }
                }

        }

        $query = Employee::whereIn('id',@$arr_employee_id)
                    ->where(function($query) use($cols,$q){
                    $query = $query->where(function($qry) use($q, $cols){
                        foreach ($cols as $key => $value) {
                            $qry->orWhere($value,'like','%'.$q.'%');
                        }
                    });
                });

        $response = $query->where('active','!=',0)->orderBy('lastname','asc')->get();
        return $response;
    }

    public function showRataDatatable(){

        $response = array(
            'title'             => $this->title,
            'controller'        => $this->controller,
            'module'            => $this->module,
            'module_prefix'     => $this->module_prefix,

        );

        return view($this->module_prefix.'.'.$this->module.'.ratadatatable',$response);
    }

    public function getRataInfo(){

        $data = Input::all();

        $query['rata'] = Rata::where('employee_id',@$data['id'])
                        ->where('year',$data['year'])
                        ->where('month',$data['month'])
                        ->first();

        if(isset($query['rata'])){
            $query['leave'] = Leave::where('rata_id',$query['rata']->id)
                                ->where('employee_id',@$data['id'])
                                ->select('leave_type','leave_date','number_of_leave_field')
                                ->get();

            $leave_list = [];
            foreach ($query['leave'] as $key => $value) {
                $leave_list[$value->leave_type][$key] = date('d',strtotime($value->leave_date));

            }

            $query['leave_list'] = $leave_list;
        }

        // $query['employeeinfo'] = $this->getRataInfo($data['year'],$data['month'],@$data['id']);



        return json_encode($query);

    }

    public function processRata(){

        $data = Input::all();

        $benefit             = new Benefit;
        $rata                = new Rata;
        $benefitInfo         = new BenefitInfo;
        $employeeinformation = new EmployeeInformation;
        $payroll_info        = new EmployeeInfo;

        $benefit_id = $benefit->whereIn('code',['REP','TRANSPO'])
                            ->select('id')->get()->toArray();

        if(isset($data['rata_id'])){

            $rata = $rata->find($data['rata_id']);

            $benefit_info = $benefitInfo->whereIn('benefit_id',$benefit_id)
                    ->where('employee_id',$data['employee_id'])
                    ->select('benefit_amount','benefit_id')
                    ->get()->toArray();

            $employeeinfo = $employeeinformation->where('employee_id',$data['employee_id'])
                                            ->select('office_id','department_id')->first();

            $payrollinfo = $payroll_info->where('employee_id',$data['employee_id'])->first();

            $representation_amount = (isset($benefit_info[0]['benefit_amount'])) ? $benefit_info[0]['benefit_amount'] : NULL;
            $transportation_amount = (isset($benefit_info[1]['benefit_amount'])) ? $benefit_info[1]['benefit_amount'] : NULL;

            $rata->office_id             = (isset($employeeinfo->office_id)) ? $employeeinfo->office_id : NULL;

            // $rata->position_item_id      = (isset($employeeinfo->position_item_id)) ? $employeeinfo->position_item_id : NULL;
            $rata->department_id         = (isset($employeeinfo->department_id)) ? $employeeinfo->department_id : NULL;
            $rata->employee_id           = $data['employee_id'];
            $rata->representation_amount = $representation_amount;
            $rata->transportation_amount = $transportation_amount;
            $rata->number_of_work_days   = $payrollinfo->no_ofdays_inamonth;
            $rata->number_of_actual_work = $payrollinfo->no_ofdays_inamonth;
            $rata->percentage_of_rata_value = 1;
            $rata->year                  = $data['year'];
            $rata->month                 = $data['month'];
            $rata->save();
            $response = json_encode(['status'=>true,'response'=>'Update Successfully!']);
        }else{

            foreach ($data['list_id'] as $key => $value) {
                if(isset($value)){

                    $benefit_info = $benefitInfo->whereIn('benefit_id',$benefit_id)
                     ->where('employee_id',$value)
                     ->select('benefit_amount','benefit_id')
                     ->get()->toArray();

                     $employeeinfo = $employeeinformation->where('employee_id',$value)
                                                            ->select('office_id','department_id')->first();

                     $payrollinfo = $payroll_info->where('employee_id',$value)->first();

                     $representation_amount = (isset($benefit_info[0]['benefit_amount'])) ? $benefit_info[0]['benefit_amount'] : NULL;
                     $transportation_amount = (isset($benefit_info[1]['benefit_amount'])) ? $benefit_info[1]['benefit_amount'] : NULL;

                     $rata = new Rata;
                     $rata->office_id             = (isset($employeeinfo->office_id)) ? $employeeinfo->office_id : NULL;

                     // $rata->position_item_id      = (isset($employeeinfo->position_item_id)) ? $employeeinfo->position_item_id : NULL;
                     $rata->department_id         = (isset($employeeinfo->department_id)) ? $employeeinfo->department_id : NULL;
                     $rata->employee_id           = $value;
                     $rata->representation_amount = $representation_amount;
                     $rata->transportation_amount = $transportation_amount;
                     $rata->number_of_work_days   = $payrollinfo->no_ofdays_inamonth;
                     $rata->number_of_actual_work = $payrollinfo->no_ofdays_inamonth;
                     $rata->percentage_of_rata_value = 1;
                     $rata->year                  = $data['year'];
                     $rata->month                 = $data['month'];

                     $rata->save();
                }
            }
            $response = json_encode(['status'=>true,'response'=>'Process Successfully!']);
        }


       return $response;
   }

    public function deleteRata(){
        $data = Input::all();

        $rata = new Rata;

        $leave = new Leave;

        foreach ($data['empid'] as $key => $value) {

            if(isset($value)){
                $id =  $rata->where('employee_id',$value)
                                    ->where('month',$data['month'])
                                    ->where('year',$data['year'])
                                    ->delete();

                 $leave->where('employee_id',$value)
                                    ->where('rata_id',$id)
                                    ->delete();
            }


        }

        return json_encode(['status'=>true,'response'=>'Delete Successfully']);
   }

}
