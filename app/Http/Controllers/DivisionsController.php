<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Input;
use Auth;
use App\Division;
class DivisionsController extends Controller
{
    function __construct(){
    	$this->title 		 = 'DIVISION SETUP';
    	$this->module_prefix = 'payrolls/admin/filemanagers';
    	$this->module 		 = 'divisions';
    	$this->controller 	 = $this;
    }

    public function index(){

    	$response = array(
						'module'        => $this->module,
						'controller'    => $this->controller,
		                'module_prefix' => $this->module_prefix,
						'title'		    => $this->title
					);

    	return view($this->module_prefix.'.'.$this->module.'.index',$response);
    }

    public function show(){

        $q = Input::get('q');

        $data = $this->get_records($q);

        $response = array(
                        'data'          => $data,
                        'controller'    => $this->controller,
                        'module'        => $this->module,
                        'module_prefix' => $this->module_prefix,
                    );

        return view($this->module_prefix.'.'.$this->module.'.datatable',$response);

    }

    private function get_records($q){

        $cols = ['code','name'];

        $query = Division::where(function($query) use($cols,$q){

                $query = $query->where(function($qry) use($q, $cols){
                    foreach ($cols as $key => $value) {
                        $qry->orWhere($value,'like','%'.$q.'%');
                    }
                });
        });
        $response = $query->orderBy('name','asc')->get();

        return $response;

    }

    public function store(Request $request){

    	$this->validate($request,[
    		'code' 	=> 'required|unique:pms_divisions',
    		'name' 	=> 'required'
		]);

    	$divisions = new Division;

    	if(isset($request->division_id)){

    		$divisions = Division::find($request->division_id);

	    	$divisions->code 	   = $request->code;
	    	$divisions->name 	   = $request->name;
	    	$divisions->updated_by = Auth::User()->id;

	    	$divisions->save();

	    	$response = json_encode(['status'=>true,'response'=>'Update Successfully!']);
    	}else{

    		$divisions->code 	   = $request->code;
	    	$divisions->name 	   = $request->name;
	    	$divisions->created_by = Auth::User()->id;

	    	$divisions->save();

	    	$response = json_encode(['status'=>true,'response'=>'Save Successfully!']);
    	}


    	return $response;
    }
}
