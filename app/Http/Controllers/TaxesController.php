<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Input;
use App\TaxTable;
use App\AnnualTaxTable;
use App\TaxStatusTable;
use App\TaxPolicy;

class TaxesController extends Controller
{

    function __construct(){
        $this->controller = $this;
        $this->title = 'TAX TABLE & POLICES';
        $this->module_prefix = 'payrolls/admin/filemanagers';
        $this->module = 'taxes';
    }

    public function index(){



        $response = array(

            'controller'    => $this->controller,
            'title'         => $this->title,
            'module'        => $this->module,
            'module_prefix' => $this->module_prefix,
        );

        return view($this->module_prefix.'.'.$this->module.'.index',$response);
    }

    public function store(Request $request){

        $this->validate(request(),[
            'effectivity_date'         => 'required|unique:pms_taxtable',
            // 'payperiod'                => 'required'

        ]);
        if(isset($request->daily)){

            $daily = new TaxTable;
            $daily->payperiod = 'Daily';

            $daily->salary_bracket_level1 = str_replace(',', '', $request->daily[0]);
            $daily->salary_bracket_level2 = str_replace(',', '', $request->daily[1]);
            $daily->salary_bracket_level3 = str_replace(',', '', $request->daily[2]);
            $daily->salary_bracket_level4 = str_replace(',', '', $request->daily[3]);
            $daily->salary_bracket_level5 = str_replace(',', '', $request->daily[4]);
            $daily->salary_bracket_level6 = str_replace(',', '', $request->daily[5]);
            $daily->effectivity_date = $request->effectivity_date;

            $daily->save();
        }

        if(isset($request->monthly)){
            $monthly = new TaxTable;
            $monthly->payperiod = 'Monthly';
            $monthly->salary_bracket_level1 = str_replace(',', '', $request->monthly[0]);
            $monthly->salary_bracket_level2 = str_replace(',', '', $request->monthly[1]);
            $monthly->salary_bracket_level3 = str_replace(',', '', $request->monthly[2]);
            $monthly->salary_bracket_level4 = str_replace(',', '', $request->monthly[3]);
            $monthly->salary_bracket_level5 = str_replace(',', '', $request->monthly[4]);
            $monthly->salary_bracket_level6 = str_replace(',', '', $request->monthly[5]);
            $monthly->effectivity_date = $request->effectivity_date;

            $monthly->save();
        }

        if(isset($request->semimonthly)){
            $semimonthly = new TaxTable;
            $semimonthly->payperiod = 'Semi Monthly';
            $semimonthly->salary_bracket_level1 = str_replace(',', '', $request->semimonthly[0]);
            $semimonthly->salary_bracket_level2 = str_replace(',', '', $request->semimonthly[1]);
            $semimonthly->salary_bracket_level3 = str_replace(',', '', $request->semimonthly[2]);
            $semimonthly->salary_bracket_level4 = str_replace(',', '', $request->semimonthly[3]);
            $semimonthly->salary_bracket_level5 = str_replace(',', '', $request->semimonthly[4]);
            $semimonthly->salary_bracket_level6 = str_replace(',', '', $request->semimonthly[5]);
            $semimonthly->effectivity_date = $request->effectivity_date;

            $semimonthly->save();
        }

        if(isset($request->weekly)){
            $weekly = new TaxTable;
            $weekly->payperiod = 'Weekly';
            $weekly->salary_bracket_level1 = str_replace(',', '', $request->weekly[0]);
            $weekly->salary_bracket_level2 = str_replace(',', '', $request->weekly[1]);
            $weekly->salary_bracket_level3 = str_replace(',', '', $request->weekly[2]);
            $weekly->salary_bracket_level4 = str_replace(',', '', $request->weekly[3]);
            $weekly->salary_bracket_level5 = str_replace(',', '', $request->weekly[4]);
            $weekly->salary_bracket_level6 = str_replace(',', '', $request->weekly[5]);
            $weekly->effectivity_date = $request->effectivity_date;

            $weekly->save();
        }


        return json_encode(['status'=>true,'response' => 'Save Successfully!']);

    }

    public function show(){

        $q = Input::get('q');
        $limit = Input::get('limit');
        if(empty($limit)){ $limit  =  10; }

        $data = $this->get_records($limit,$q);

        $taxtable = TaxTable::get();

        $response = array(
                        'taxtable'      => json_encode($taxtable),
                        'data'          => $data,
                        'controller'    => $this->controller,
                        'module'        => $this->module,
                        'module_prefix' => $this->module_prefix,

                        );
        return view($this->module_prefix.'.'.$this->module.'.datatable',$response);

    }


    private function get_records($limit,$q){

        $cols = ['status'];


        $query = TaxTable::where(function($query) use($cols,$q){

                $query = $query->where(function($qry) use($q, $cols){
                    foreach ($cols as $key => $value) {
                        $qry->orWhere($value,'like','%'.$q.'%');
                    }
                });
        });
        $response = $query->paginate($limit);

        return $response;

    }


    public function storeTaxstatusannual(Request $request){

        $this->validate(request(),[
            'below'               => 'required',
            'above'               => 'required',
            'rate'                => 'required',
            'effectivity_date'    => 'required',


        ]);

        $taxannual = new AnnualTaxTable;

        if(isset($request->update_taxannual)){

            $taxannual = AnnualTaxTable::find((int)$request->update_taxannual);

            $taxannual->fill($request->all())->save();

            $response = json_encode(['status'=>true,'response' => 'Update Successfully!']);

        }else{


            $taxannual->fill($request->all())->save();

            $response = json_encode(['status'=>true,'response' => 'Save Successfully!']);


        }

        return $response;

    }

    public function showTaxstatusannual(){

        $q = Input::get('q');
        $limit = Input::get('limit');
        if(empty($limit)){ $limit  =  10; }

        $data = $this->get_records_statusannual($limit,$q);

        $response = array(
                        'data'          => $data,
                        'controller'    => $this->controller,
                        'module'        => $this->module,
                        'module_prefix' => $this->module_prefix,

                        );
        return view($this->module_prefix.'.'.$this->module.'.datatabletaxannual',$response);

    }


    private function get_records_statusannual($limit,$q){

        $cols = ['below','above','rate'];

        $query = AnnualTaxTable::where(function($query) use($cols,$q){

                $query = $query->where(function($qry) use($q, $cols){
                    foreach ($cols as $key => $value) {
                        $qry->orWhere($value,'like','%'.$q.'%');
                    }
                });
        });
        $response = $query->paginate($limit);

        return $response;

    }

    public function storeTaxpolicy(Request $request){

        $this->validate(request(),[
            'pay_period'        => 'required',
            'policy_name'        => 'required',
            'deduction_period'  => 'required',
            'policy_type'       => 'required',
            'based_on'          => 'required',
            'computation'       => 'required',
        ]);

        $taxpolicy = new TaxPolicy;

        if($request->is_withholding == 'Monthly'){
            $request->is_withholding    = 'Monthly';
        }else{
            $request->is_withholding    = 'Annual';
        }

        if(isset($request->update_taxpolicy)){

            $taxpolicy = TaxPolicy::find((int)$request->update_taxpolicy);

            $taxpolicy->fill($request->all())->save();

            $response = json_encode(['status'=>true,'response' => 'Update Successfully!']);

        }else{

            $check = $taxpolicy->where('policy_name',$request->policy_name)->get()->toArray();

            if(count($check) > 0){

                $response = json_encode(['status'=>false,'response' => 'Policy name already taken!']);

            }else{

                $taxpolicy->fill($request->all())->save();

                $response = json_encode(['status'=>true,'response' => 'Save Successfully!']);
            }

        }

        return $response;

    }

    public function showTaxpolicy(){

        $q = Input::get('q');
        // $limit = Input::get('limit');
        // if(empty($limit)){ $limit  =  10; }

        $data = $this->get_records_policy($q);

        $response = array(
                        'data'          => $data,
                        'controller'    => $this->controller,
                        'module'        => $this->module,
                        'module_prefix' => $this->module_prefix,

                        );

        return view($this->module_prefix.'.'.$this->module.'.datatabletaxpolicy',$response);

    }


    private function get_records_policy($q){

        $cols = ['policy_type','policy_name','deduction_period','pay_period'];

        $query = TaxPolicy::where(function($query) use($cols,$q){

                $query = $query->where(function($qry) use($q, $cols){
                    foreach ($cols as $key => $value) {
                        $qry->orWhere($value,'like','%'.$q.'%');
                    }
                });
        });
        $response = $query->get();

        return $response;

    }

    public function getTaxtable(){
        $id = Input::get('id');

        $query = TaxTable::where('id',$id)->first();

        return json_encode($query);
    }

    public function getTaxannual(){
        $id = Input::get('id');

        $query = AnnualTaxTable::where('id',$id)->first();

        return json_encode($query);
    }

    public function getTaxpolicy(){
        $id = Input::get('id');

        $query = TaxPolicy::where('id',$id)->first();

        return json_encode($query);
    }

    public function getTaxstatus(){
        $id = Input::get('id');

        $query = TaxStatusTable::where('id',$id)->first();

        return json_encode($query);
    }
}
