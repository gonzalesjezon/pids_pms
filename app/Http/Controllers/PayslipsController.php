<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Employee;
use App\LoanInfo;
use App\Transaction;
use App\EmployeeInfo;
use App\BenefitInfo;
use App\DeductionInfo;
use App\SalaryInfo;
use App\Company;
use App\Office;
use App\Division;
use App\Department;
use App\Position;
use App\AttendanceInfo;
use App\Rata;
use Input;
class PayslipsController extends Controller
{
    function __construct(){
    	$this->title = 'PAYSLIP';
    	$this->module = 'payslips';
        $this->module_prefix = 'payrolls/reports';
    	$this->controller = $this;

    }

    public function index(){

        $employeeinfo = Employee::where('active',1)->orderBy('lastname','asc')->get();

    	$response = array(
                        'employeeinfo'  => $employeeinfo,
    					'module'        => $this->module,
    					'controller'    => $this->controller,
                        'module_prefix' => $this->module_prefix,
    					'title'		    => $this->title
    					);

    	return view($this->module_prefix.'.'.$this->module,$response);
    }

    public function show(){
        $data = Input::all();

        $transaction    = new Transaction;
        $employeeinfo   = new EmployeeInfo;
        $loaninfo       = new LoanInfo;
        $benefitinfo    = new BenefitInfo;
        $deductioninfo  = new DeductionInfo;
        $salaryinfo     = new SalaryInfo;

        if(isset($data['id'])){
            $query['transaction'] = $transaction
            ->with(['employees',
                'positions',
                'offices',
                'benefitTransaction' => function($qry){ $qry->where('status','pera'); }
            ])
            ->where('employee_id',$data['id'])
            ->where('year',$data['year'])
            ->where('month',$data['month'])
            ->first();

            $query['rata']          = Rata::where('employee_id',$data['id'])
                                        ->where('year',$data['year'])
                                        ->where('month',$data['month'])
                                        ->first();

            $query['employeeinfo'] = $employeeinfo
            ->with('employees')
            ->where('employee_id',$data['id'])
            ->first();

            $query['loaninfo'] = $loaninfo
            ->with('loans')
            ->where('employee_id',$data['id'])
            ->get();

            $query['deductioninfo'] = $deductioninfo
            ->where('employee_id',$data['id'])
            ->get();

            $query['salaryinfo'] = $salaryinfo
            ->where('employee_id',$data['id'])
            ->first();

            return json_encode($query);

        }
    }


}
