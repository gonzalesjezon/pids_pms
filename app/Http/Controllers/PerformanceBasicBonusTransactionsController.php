<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input;
use Auth;
use App\Employee;
use App\EmployeeStatus;
use App\Benefit;
use App\BenefitInfo;
use App\SpecialPayrollTransaction;
use App\EmployeeInformation;
use App\PositionItem;
class PerformanceBasicBonusTransactionsController extends Controller
{
     function __construct(){
    	$this->title = 'PERFORMANCE BASIC BONUS TRANSACTIONS';
    	$this->controller = $this;
    	$this->module = 'pbbtransactions';
        $this->module_prefix = 'payrolls/specialpayrolls';
    }

    public function index(){


    	$response = array(
           'title' 	        	=> $this->title,
           'controller'         => $this->controller,
           'module'	        	=> $this->module,
           'module_prefix'      => $this->module_prefix,

       );

    	return view($this->module_prefix.'.'.$this->module.'.index',$response);
    }

    public function show(){

        $q 			= Input::get('q');
        $year 		= Input::get('year');
        $month 		= Input::get('month');
        $check_pbb 	= Input::get('check_pbb');
        $checkpbb 	= Input::get('checkpbb');
        $data = "";

        $data = $this->searchName($q,$check_pbb);

        if(isset($year) || isset($month) || isset($checkpbb)){
            $data = $this->filter($year,$month,$checkpbb);
        }


        $response = array(
            'data'          => $data,
            'title'         => $this->title,
            'controller'    => $this->controller,
            'module'        => $this->module,
            'module_prefix' => $this->module_prefix
        );

        return view($this->module_prefix.'.'.$this->module.'.namelist',$response);

    }

    public function searchName($q,$checkpbb){

        $employee 	 	  = new Employee;
        $benefit  	 	  = new Benefit;
        $benefitinfo 	  = new BenefitInfo;
        $transaction 	  = new SpecialPayrollTransaction;

        $cols = ['lastname','firstname','id'];

        // $arrBenefit  = $benefit->where('code',['PBB'])->select('id')->get()->toArray();
        // $arrEmployee = $benefitinfo->whereIn('benefit_id',$arrBenefit)->select('employee_id')->get()->toArray();
        // $employee_id = $transaction->whereIn('employee_id',$arrEmployee)->select('employee_id')->get()->toArray();

        // $query = [];
        // switch ($checkpbb) {
        //     case 'wpbb':
        //         $query = $employee->whereIn('id',$employee_id);
        //         break;

        //     default:
        //         $query = $employee->whereNotIn('id',$employee_id)->whereIn('id',$arrEmployee);
        //         break;
        // }

      	$query = $employee->where(function($query) use($cols,$q){
            $query = $query->where(function($qry) use($q, $cols){
                foreach ($cols as $key => $value) {
                    $qry->orWhere($value,'like','%'.$q.'%');
                }
            });

        });

        $response = $query->orderBy('lastname','asc')->get();

        return $response;
    }

    public function filter($year,$month,$checkpbb){


        $employee           = new Employee;
        $benefit  	 	  	= new Benefit;
        $benefitinfo 	  	= new BenefitInfo;
        $transaction        = new SpecialPayrollTransaction;

        $arrBenefit 	 = $benefit->whereIn('code',['PBB'])->select('id')->get()->toArray();
        $arrEmployee 	 = $benefitinfo->whereIn('benefit_id',$arrBenefit)->select('employee_id')->get()->toArray();

        $query = [];
        $response = "";

        switch ($checkpbb) {
            case 'wpbb':

                $query =  $transaction->select('employee_id');

                    if(isset($year)){
                        $query = $query->where('year',$year);

                    }
                    if(isset($month)){
                        $query = $query->where('month',$month);
                    }

                    $query = $query->get()->toArray();

                    $response = $employee->whereIn('id',$query)
                                        ->orderBy('lastname','asc')->get();


                break;

            case 'wopbb':

                 $query =  $transaction->select('employee_id');


                    if(isset($year)){
                        $query = $query->where('year',$year);

                    }
                    if(isset($month)){
                        $query = $query->where('month',$month);
                    }

                    $query = $query->get()->toArray();

                    $response = $employee->orderBy('lastname','asc')->get();

                break;
        }
        return $response;
    }


    public function processPbb(Request $request){
    	$data = Input::all();

    	$employeeinformation = new EmployeeInformation;
        $positionitem 		 = new PositionItem;
        $benefit 			 = new Benefit;

        $benefit_id =  $benefit->where('code','PBB')->select('id','amount')->first()->toArray();

    	foreach ($data['list_id'] as $key => $value) {

	    	if(isset($value)){
                $data_pbb   = new SpecialPayrollTransaction;
                $data_pbb = $data_pbb->where('employee_id',$value)->where('year',$data['year'])->where('month',$data['month'])->first();

                if(!isset($data_pbb)){
            		$benefitinfo       = new BenefitInfo;
                    $benefitinfo->employee_id = $value;
                    $benefitinfo->benefit_id  = $benefit_id['id'];
                    $benefitinfo->benefit_amount = $benefit_id['amount'];
                    $benefitinfo->save();

                    $benefit_info_id = $benefitinfo->id;

            		$employeeinfo 	= $employeeinformation->where('employee_id',$value)->select('office_id','position_item_id')->get();

                    $pbb = new SpecialPayrollTransaction;

                    $pbb->employee_id       = $value;
                    $pbb->office_id         = @$employeeinfo[0]->office_id;
                    $pbb->position_item_id  = @$employeeinfo[1]->position_item_id;
                    $pbb->benefit_info_id   = $benefit_info_id;
                    $pbb->year              = $data['year'];
                    $pbb->month             = $data['month'];
                    $pbb->created_by        = Auth::User()->id;
                    $pbb->save();


                }

	    	}
    	}

    	$response = json_encode(['status'=>true,'response'=>'Process Successfully!']);

    	return $response;
    }

    public function showPbbDatatable(){

    	$response = array(
            'title'             => $this->title,
            'controller'        => $this->controller,
            'module'            => $this->module,
            'module_prefix'     => $this->module_prefix,

        );

        return view($this->module_prefix.'.'.$this->module.'.datatable',$response);
    }

    public function getPbbInfo(){
    	$data = Input::all();

    	$pbb =  new SpecialPayrollTransaction;

    	$query = $pbb->with(['positionitems'=>function($qry){
		    		$qry->with('positions');
			    	},'offices','benefitinfo'=>function($qry){
			    		$qry->with('benefits');
			    	}])->where('year',$data['year'])
			    	->where('month',$data['month'])
			    	->where('employee_id',@$data['employee_id'])->get();

    	return json_encode($query);
    }

    public function deletePbb(){
        $data = Input::all();

        $rata = new SpecialPayrollTransaction;

        foreach ($data['empid'] as $key => $value) {

            $rata->where('employee_id',$data['empid'][$key])
            ->where('month',$data['month'])
            ->where('year',$data['year'])
            ->delete();

        }

        return json_encode(['status'=>true,'response'=>'Delete Successfully']);
    }
}
