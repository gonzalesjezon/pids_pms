<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\EmployeeInfo;
use App\Transaction;
use App\Employee;
use App\LoanInfo;
use App\Deduction;
use App\DeductionInfoTransaction;
use Input;
class ProvidentFundReportsController extends Controller
{
    function __construct(){
    	$this->title = 'Provident Fund Reports';
    	$this->module = 'providentfundreports';
        $this->module_prefix = 'payrolls/reports';
    	$this->controller = $this;

    }

    public function index(){


    	$response = array(
    					'module'        => $this->module,
    					'controller'    => $this->controller,
                        'module_prefix' => $this->module_prefix,
    					'title'		    => $this->title
    					);

    	return view($this->module_prefix.'.'.$this->module,$response);
    }

    public function show(){

        $q = Input::all();

        $transaction  = new Transaction;
        $deduction 	  = new Deduction;
        $deductionTransaction =  new DeductionInfoTransaction;

        $year = $q['year'];
        $month = $q['month'];

        $deduction = $deduction
        ->where('effective_year',$year)
        ->where('code','PF')->first();

        $employee_id = $deductionTransaction
        ->where('deduction_id',$deduction->id)
        ->where('year',$year)
        ->where('month',$month)
        ->select('employee_id')
        ->get()
        ->toArray();

        $query = $transaction
        ->with([
        	'employees',
        	'employeeinfo',
        	'getProvidentFund',
        	'deductioninfo' => function($qry) use($deduction){
        		$qry = $qry->where('deduction_id',$deduction->id);
        	}
        ])
        ->whereIn('employee_id',$employee_id)
        ->where('year',$year)
        ->where('month',$month)
        ->get();

        $data = [];
        if(count($query) > 0){
            foreach ($query as $key => $value) {
                if(isset($value->department_id)){
                    $data[$value->departments->name][$key] = $value;
                }
            }
        }else{
            $data = [];
        }

        return json_encode(['providentfund'=>$data,'rate'=>$deduction->deduction_rate]);
    }
}
