<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Input;
use App\ProvidentFundPolicy;
use App\Rate;

class ProvidentFundPoliciesController extends Controller
{

    function __construct(){
        $this->controller = $this;
        $this->title = 'PROVIDENT FUND';
        $this->module_prefix = 'payrolls/admin/filemanagers';
        $this->module = 'providentfundpolicies';
        $this->table = 'providentfund_policies';
    }

    public function index(){

        $policies = Rate::where('status','pf')->orderBy('rate','asc')->get();

        $response = array(
            'policies'      => $policies,
            'controller'    => $this->controller,
            'title'         => $this->title,
            'module'        => $this->module,
            'module_prefix' => $this->module_prefix,
        );

        return view($this->module_prefix.'.'.$this->module.'.index',$response);
    }

    public function store(Request $request){

        $this->validate(request(),[
            'policy_name'           => 'required',
            'pay_period'            => 'required',
            'deduction_period'      => 'required',
            'policy_type'           => 'required',
            'based_on'              => 'required',
            'ee_share'              => 'required',
            // 'er_share'              => 'required',
        ]);

        // $ee_share = ((int)$request->ee_share/100);
// dd($request->all());
        $providentfund = new ProvidentFundPolicy;

        if(isset($request->for_update)){

            $providentfund = ProvidentFundPolicy::find((int)$request->for_update);

            $request->offsetSet('ee_share',$request->ee_share);
            $providentfund->fill($request->all())->save();

            $response =  json_encode(['status'=>true,'response' => 'Updated Successfully!']);

        }else{

            $check = $providentfund->where('policy_name',$request->policy_name)->get()->toArray();

            if(count($check) > 0){

                $response = json_encode(['status'=>false,'response' => 'Code already exists!']);

            }else{

                $request->offsetSet('ee_share',$request->ee_share);
                $providentfund->fill($request->all())->save();

                $response =  json_encode(['status'=>true,'response' => 'Save Successfully!']);

            }
        }


        return $response;

    }

    public function show(){

        $q = Input::get('q');
        $limit = Input::get('limit');
        if(empty($limit)){ $limit  =  10; }

        $data = $this->get_records($limit,$q);

        $response = array(
                        'data'          => $data,
                        'controller'    => $this->controller,
                        'module'        => $this->module,
                        'module_prefix' => $this->module_prefix,
                        'cols'          => $this->table_columns($this->table)

                        );
        return view($this->module_prefix.'.'.$this->module.'.datatable',$response);

    }


    private function get_records($limit,$q){

        $cols = ['policy_name','pay_period','deduction_period','policy_type'];


        $query = ProvidentFundPolicy::where(function($query) use($cols,$q){

                $query = $query->where(function($qry) use($q, $cols){
                    foreach ($cols as $key => $value) {
                        $qry->orWhere($value,'like','%'.$q.'%');
                    }
                });
        });
        $response = $query->paginate($limit);

        return $response;

    }

    public function getItem(){
        $id = Input::get('id');

        $query = ProvidentFundPolicy::where('id',$id)->first();

        return json_encode($query);
    }
}
