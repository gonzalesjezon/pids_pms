<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Rata;
use Input;
class RATAGeneralPayrollReportsController extends Controller
{
    function __construct(){
    	$this->title = 'RATA GENERAL PAYROLL';
    	$this->module = 'ratageneralpayroll';
        $this->module_prefix = 'payrolls/reports';
    	$this->controller = $this;

    }

    public function index(){

    	$response = array(
    					'module'        => $this->module,
    					'controller'    => $this->controller,
                        'module_prefix' => $this->module_prefix,
    					'title'		    => $this->title
    					);

    	return view($this->module_prefix.'.'.$this->module,$response);
    }

    public function show(){
        $data = Input::all();

        $year = $data['year'];
        $month = $data['month'];

        $transaction    = new Rata;

        $query = $transaction
        ->with('employees')
        ->where('year',$year)
        ->where('month',$month)
        ->get();



        return json_encode($query);
    }
}
