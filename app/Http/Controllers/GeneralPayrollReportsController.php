<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\EmployeeInfo;
use App\Transaction;
use App\Employee;
use App\LoanInfo;
use Input;
class GeneralPayrollReportsController extends Controller
{
    function __construct(){
    	$this->title = 'GENERAL PAYROLL';
    	$this->module = 'generalpayroll';
        $this->module_prefix = 'payrolls/reports';
    	$this->controller = $this;

    }

    public function index(){


    	$response = array(
    					'module'        => $this->module,
    					'controller'    => $this->controller,
                        'module_prefix' => $this->module_prefix,
    					'title'		    => $this->title
    					);

    	return view($this->module_prefix.'.'.$this->module,$response);
    }

    public function show(){

        $q = Input::all();

        $transaction  = new Transaction;

        $year = $q['year'];
        $month = $q['month'];

        $query = $transaction
        ->with(['benefitTransaction' =>function($qry){ $qry->where('status','pera'); },
            'employees',
            'positions',
            'employeeinfo',
            'employeeinformation',
            'deductionTransaction' => function($qry) use($year,$month){
                $qry = $qry->where('year',$year)
                ->where('month',$month);
            },
            'rata' => function($qry) use($year,$month){
                $qry = $qry->where('year',$year)
                ->where('month',$month);
            },
            'loaninfo',
            'salaryinfo',
            'offices'=>function($qry){
                $qry->orderBy('name','asc');
            },
        ])
        ->where('year',$year)
        ->where('month',$month)
        ->get();

        $data = [];
        if(count($query) > 0){
            foreach ($query as $key => $value) {
                if(isset($value->department_id)){
                    $data[$value->departments->name][$key] = $value;
                }
            }

        }else{
            $data = [];
        }

        return json_encode($data);
    }
}
