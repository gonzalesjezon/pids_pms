<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input;
use App\Employee;
use App\SalaryInfo;
use App\PhilhealthPolicy;
use App\DeductionInfo;
use App\Deduction;
class SalaryAdjustmentsController extends Controller
{
    function __construct(){
    	$this->title = 'SALARY ADJUSTMENTS';
    	$this->module = 'salaryadjustments';
        $this->module_prefix = 'payrolls/otherpayrolls';
    	$this->controller = $this;

    }

    public function index(){


    	$response = array(
    					'module'        => $this->module,
    					'controller'    => $this->controller,
                        'module_prefix' => $this->module_prefix,
    					'title'		    => $this->title
    					);

    	return view($this->module_prefix.'.'.$this->module.'.index',$response);
    }

    public function show(){

        $q = Input::get('q');
        $empstatus = Input::get('empstatus');
        $category = Input::get('category');
        $emp_type = Input::get('emp_type');
        $searchby = Input::get('searchby');

        $data = "";

        $data = $this->searchName($q);

        if(isset($empstatus) || isset($category)){
            $data = $this->filter($empstatus,$category,$emp_type,$searchby);

        }

        $response = array(
                        'data'          => $data,
                        'title'         => $this->title,
                        'controller'    => $this->controller,
                        'module'        => $this->module,
                        'module_prefix' => $this->module_prefix
                    );

        return view($this->module_prefix.'.'.$this->module.'.namelist',$response);

    }

    public function searchName($q){
        $cols = ['lastname','firstname','id'];

        $query = Employee::where(function($query) use($cols,$q){

                        $query = $query->where(function($qry) use($q, $cols){
                            foreach ($cols as $key => $value) {
                                $qry->orWhere($value,'like','%'.$q.'%');
                            }
                        });
                    });


        $response = $query->where('active',1)->orderBy('lastname','asc')->get();

        return $response;

    }


    public function store(Request $request){

        dd($request->all());

    }

    public function showSalaryAdjustment(){

         $response = array(
            'title'             => $this->title,
            'controller'        => $this->controller,
            'module'            => $this->module,
            'module_prefix'     => $this->module_prefix,

        );

        return view($this->module_prefix.'.'.$this->module.'.datatable',$response);


    }

    public function getSalaryAdjustment(){

        $employee_id = Input::get('id');

        $salaryinfo            = new SalaryInfo;
        $philhealth_policy     = new PhilhealthPolicy;
        $deductioninfo         = new DeductionInfo;
        $deduction             = new Deduction;

        $deduction = $deduction->where('code','PF')->first();

        $deductioninfo = $deductioninfo
        ->where('deduction_id',@$deduction->id)
        ->where('employee_id',@$employee_id)
        ->orWhereNotNull('deduct_date_terminated')
        ->orderBy('created_at','desc')
        ->first();

        $salaryinfo = $salaryinfo
                    ->with('positions')
                    ->where('employee_id',@$employee_id)
                    ->orderBy('salary_effectivity_date','desc')
                    ->take(2)
                    ->get();

        $query['old_position']    = (isset($salaryinfo[1]->positions)) ? @$salaryinfo[1]->positions.Name : '';
        $query['new_position']    = (isset($salaryinfo[0]->positions)) ? @$salaryinfo[0]->positions.Name : '';
        $query['old_rate_amount'] = @$salaryinfo[0]->salary_old_rate;
        $query['new_rate_amount'] = @$salaryinfo[0]->salary_new_rate;
        $query['philhealth_policy'] = $philhealth_policy->orderBy('created_at','desc')->first();
        $query['pf_rate'] = @$deductioninfo->deduction_rate;

        return $query;

    }

    public function getDaysInAMonth(){
        $data = Input::all();

        $transact_date = $data['transaction_date'];

        $year = date('Y', strtotime($transact_date));
        $month = date('m', strtotime($transact_date));

        $workdays = $this->countDays($year,$month,array(0,6));

        return json_encode(['workdays'=>$workdays]);

    }

    public function getCountedDays(){
        $data = Input::all();

        $from         = @$data['date_from'];
        $to           = @$data['date_to'];

        if(isset($from) && isset($to)){
            $start_date   = date('Y-m-d', strtotime($from));
            $end_date     = date('Y-m-d', strtotime($to));
            $counted_days = $this->getWorkingDays($start_date,$end_date);

            return json_encode(['counted_days'=>$counted_days]);
        }

    }
}
