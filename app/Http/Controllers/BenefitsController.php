<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Input;
use App\Benefit;

class BenefitsController extends Controller
{
    function __construct(){
    	$this->controller = $this;
    	$this->title = 'BENEFITS';
    	$this->module_prefix = 'payrolls/admin/filemanagers';
    	$this->module = 'benefits';
        $this->table = 'benefits';
    }

    public function index(){
    	$response = array(
    		'controller'	=> $this->controller,
    		'title'			=> $this->title,
    		'module'		=> $this->module,
    		'module_prefix'	=> $this->module_prefix,
    	);

    	return view($this->module_prefix.'.'.$this->module.'.index',$response);
    }

    public function store(Request $request){

        $this->validate(request(),[
            'code'                  => 'required',
            'name'                  => 'required',
            // 'de_minimis_type'       => 'required',
            'computation_type'      => 'required',
            // 'payroll_group'         => 'required',
            'tax_type'              => 'required',
            'remarks'               => 'required'
            // 'itr_classification'    => 'required',
            // 'include_computation'   => 'required',
            // 'alphalist_classification' => 'required',
        ]);

        $benefit = new Benefit;
        if(isset($request->for_update)){

            $benefit = Benefit::find((int)$request->for_update);

            $benefit->code                      = $request->code;
            $benefit->name                      = $request->name;
            // $benefit->de_minimis_type           = $request->de_minimis_type;
            $benefit->computation_type          = $request->computation_type;
            // $benefit->payroll_group             = $request->payroll_group;
            $benefit->tax_type                  = $request->tax_type;
            $benefit->remarks                   = $request->remarks;
            $benefit->amount                    = str_replace(',', '', $request->amount);
            // $benefit->itr_classification        = $request->itr_classification;
            // $benefit->alphalist_classification  = $request->alphalist_classification;

            $benefit->save();

            $response =  json_encode(['status'=>true,'response' => 'Update Successfully!']);

        }else{

            $check = $benefit->where('code',$request->code)->get()->toArray();

            if(count($check) > 0){

                $response = json_encode(['status'=>false,'response' => 'Code already exists!']);

            }else{

                $benefit->code                      = $request->code;
                $benefit->name                      = $request->name;
                // $benefit->de_minimis_type           = $request->de_minimis_type;
                $benefit->computation_type          = $request->computation_type;
                // $benefit->payroll_group             = $request->payroll_group;
                $benefit->tax_type                  = $request->tax_type;
                $benefit->remarks                   = $request->remarks;
                $benefit->amount                    = str_replace(',', '', $request->amount);
                // $benefit->itr_classification        = $request->itr_classification;
                // $benefit->alphalist_classification  = $request->alphalist_classification;

                $benefit->save();

                $response =  json_encode(['status'=>true,'response' => 'Save Successfully!']);
            }
        }

        return $response;

    }


    public function show(){

        $q = Input::get('q');
        $limit = Input::get('limit');
        // if(empty($limit)){ $limit  =  10; }

        $data = $this->get_records($q);

        $response = array(
                        'data'          => $data,
                        'controller'    => $this->controller,
                        'module'        => $this->module,
                        'module_prefix' => $this->module_prefix,
                        'cols'          => $this->table_columns($this->table)

                        );
        return view($this->module_prefix.'.'.$this->module.'.datatable',$response);

    }


    private function get_records($q){

        $cols = ['code','name','tax_type','payroll_group'];


        $query = Benefit::where(function($query) use($cols,$q){

                $query = $query->where(function($qry) use($q, $cols){
                    foreach ($cols as $key => $value) {
                        $qry->orWhere($value,'like','%'.$q.'%');
                    }
                });
        });
        $response = $query->get();

        return $response;

    }

    public function getItem(){
        $id = Input::get('id');

        $query = Benefit::where('id',$id)->first();

        return json_encode($query);
    }
}
