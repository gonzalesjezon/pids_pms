<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input;
use App\EmployeeInfo;
use App\Transaction;
class HDMFReportsController extends Controller
{
    function __construct(){
    	$this->title = 'HDMF REPORTS';
    	$this->module = 'hdmfreports';
        $this->module_prefix = 'payrolls/reports';
    	$this->controller = $this;

    }

    public function index(){

    	$response = array(
    					'module'        => $this->module,
    					'controller'    => $this->controller,
                        'module_prefix' => $this->module_prefix,
    					'title'		    => $this->title
    					);

    	return view($this->module_prefix.'.'.$this->module,$response);
    }

    public function show(){
        $data = Input::all();

        $transaction    = new Transaction;
        $employeeinfo   = new EmployeeInfo;

        $query = $transaction
        ->with(['employees',
            'positions',
            'employeeinfo'])
        ->where('year',$data['year'])
        ->where('month',$data['month'])
        ->get();


        return json_encode($query);

    }
}
