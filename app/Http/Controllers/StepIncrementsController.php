<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input;
use App\Employee;
use App\SalaryInfo;
use App\EmployeeInfo;
use App\DeductionInfo;
use App\LoanInfo;
use Carbon\Carbon;
class StepIncrementsController extends Controller
{
    function __construct(){
    	$this->title = 'STEP INCREMENT';
    	$this->module = 'stepincrements';
        $this->module_prefix = 'payrolls/otherpayrolls';
    	$this->controller = $this;

    }

    public function index(){


    	$response = array(
    					'module'        => $this->module,
    					'controller'    => $this->controller,
                        'module_prefix' => $this->module_prefix,
    					'title'		    => $this->title
    					);

    	return view($this->module_prefix.'.'.$this->module.'.index',$response);
    }

    public function show(){

        $q = Input::get('q');
        $empstatus = Input::get('empstatus');
        $category = Input::get('category');
        $emp_type = Input::get('emp_type');
        $searchby = Input::get('searchby');

        $data = "";

        $data = $this->searchName($q);

        if(isset($empstatus) || isset($category)){
            $data = $this->filter($empstatus,$category,$emp_type,$searchby);

        }

        $response = array(
                        'data'          => $data,
                        'title'         => $this->title,
                        'controller'    => $this->controller,
                        'module'        => $this->module,
                        'module_prefix' => $this->module_prefix
                    );

        return view($this->module_prefix.'.'.$this->module.'.namelist',$response);

    }

    public function searchName($q){
        $cols = ['lastname','firstname','id'];

        $query = Employee::where(function($query) use($cols,$q){

                        $query = $query->where(function($qry) use($q, $cols){
                            foreach ($cols as $key => $value) {
                                $qry->orWhere($value,'like','%'.$q.'%');
                            }
                        });
                    });


        $response = $query->where('active',1)->orderBy('lastname','asc')->get();

        return $response;

    }


    public function store(Request $request){



    }

    public function showStepIncrement(){

         $response = array(
            'title'             => $this->title,
            'controller'        => $this->controller,
            'module'            => $this->module,
            'module_prefix'     => $this->module_prefix,

        );

        return view($this->module_prefix.'.'.$this->module.'.datatable',$response);


    }

    public function getStepIncrement(){

        $data = Input::all();

        $employee_id = $data['id'];

        $salaryinfo     = new SalaryInfo;
        $employeeinfo   = new EmployeeInfo;
        $deductioninfo  = new DeductionInfo;
        $loaninfo       = new LoanInfo;

        $now = Carbon::now();

        $year = $now->year;
        $month = $now->month;

        $no_of_days = $this->countDays($year,$month,array(0,6));

        $query['salaryinfo'] = $salaryinfo
        ->where('employee_id',$employee_id)
        ->orderBy('salary_effectivity_date','desc')
        ->first();

        $query['employeeinfo'] = $employeeinfo
        ->where('employee_id',$employee_id)
        ->first();

        $query['days_in_month'] = $no_of_days;

        return json_encode($query);

    }
}
