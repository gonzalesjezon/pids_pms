<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use DB;
class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;


     public function table_columns($table){

        $res = array();
        $data = DB::select('SELECT column_name FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = "default" AND TABLE_NAME="'.$table.'"');
        foreach ($data as $key => $value) {
        	$notInArray = array('id','created_at','updated_at');
        	// echo
            if(!in_array($value->column_name,$notInArray)){
                array_push($res,$value->column_name);
            }
        }
        return $res;

    }

    public function countDays($year, $month, $ignore) {
        $count = 0;
        $counter = mktime(0, 0, 0, $month, 1, $year);
        while (date("n", $counter) == $month) {
            if (in_array(date("w", $counter), $ignore) == false) {
                $count++;
            }
            $counter = strtotime("+1 day", $counter);
        }
        // return $count-1;
        return $count;
    }

    public function getWorkingDays($startDate,$endDate){
        $start = strtotime($startDate);
        $end = strtotime($endDate);

        if($start > $end){
            $working_days = 0;
        }else{
            $no_days = 0;
            $weekends = 0;
            while ($start <= $end) {
                $no_days++; // no of days in given interval
                $what_day = date('N',$start);
                if($what_day>6){ // 6 and 7 are weekend
                    $weekends++;
                }
                $start+=86400; // +1 day
            }
        }
        $working_days = $no_days - $weekends;

        return $working_days;

    }
}
