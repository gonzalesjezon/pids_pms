<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input;
use App\Rata;
use App\Employee;
class RataAdjustmentsController extends Controller
{
    function __construct(){
        $this->title = 'RATA ADJUSTMENTS';
        $this->controller = $this;
        $this->module = 'rataadjustments';
        $this->module_prefix = 'payrolls/otherpayrolls';
    }

    public function index(){


        $response = array(
           'title'          => $this->title,
           'controller'        => $this->controller,
           'module'         => $this->module,
           'module_prefix'     => $this->module_prefix,

       );

        return view($this->module_prefix.'.'.$this->module.'.index',$response);
    }

    public function show(){

        $q = Input::get('q');
        $year = Input::get('year');
        $month = Input::get('month');
        $Year = Input::get('Year');
        $Month = Input::get('Month');

        $data = "";

        $data = $this->searchName($q,$Year,$Month);

        if(isset($year) || isset($month)){
            $data = $this->filter($year,$month);

        }

        $response = array(
            'data'          => $data,
            'title'         => $this->title,
            'controller'    => $this->controller,
            'module'        => $this->module,
            'module_prefix' => $this->module_prefix
        );

        return view($this->module_prefix.'.'.$this->module.'.namelist',$response);

    }

    public function searchName($q,$year,$month){

        $cols = ['lastname','firstname'];

        $employee = new Employee;
        $transaction = new Rata;

        $query = $employee->where(function($query) use($cols,$q){
                    $query = $query->where(function($qry) use($q, $cols){
                        foreach ($cols as $key => $value) {
                            $qry->orWhere($value,'like','%'.$q.'%');
                        }
                    });
                });

        $response = $query->where('active',1)->orderBy('lastname','asc')->get();
        return $response;
    }

    public function filter($year,$month){

        $transaction            = new Rata;
        $employee               = new Employee;

        $transaction = $transaction
        ->where('year',$year)
        ->where('month',$month)
        ->select('employee_id')
        ->get()
        ->toArray();


        $response = $employee
        ->whereIn('id',$transaction)
        ->orderBy('lastname','asc')
        ->get();

        return $response;
    }

    public function showRataDatatable(){

        $response = array(
            'title'             => $this->title,
            'controller'        => $this->controller,
            'module'            => $this->module,
            'module_prefix'     => $this->module_prefix,

        );

        return view($this->module_prefix.'.'.$this->module.'.ratadatatable',$response);
    }

    public function getRataAdjustment(){
    	$data = Input::all();

 		$year = $data['year'];
 		$month = $data['month'];
 		$employee_id = $data['id'];

 		$transaction = new Rata;

 		$transaction = $transaction
 		->where('year',$year)
 		->where('month',$month)
 		->where('employee_id',$employee_id)
 		->first();

 		return json_encode($transaction);
    }

    public function store(Request $request){

    	$this->validate($request,[
    		'adjustment_number_of_workdays' => 'required',
    		'adjustment_deduction_period' 	=> 'required'
    	]);

    	$rep_amount = (isset($request->rep_amount)) ? str_replace(',', '',$request->rep_amount) : 0;
    	$transpo_amount = (isset($request->transpo_amount)) ? str_replace(',', '', $request->transpo_amount)  : 0;

    	$number_of_workdays = $request->adjustment_number_of_workdays;
    	$percentage_rate 	= str_replace(' %', '', $request->adjustment_rata_percentage/100);
    	$new_rep_amount 	= ($percentage_rate * $rep_amount);
    	$new_transpo_amount = ($percentage_rate * $transpo_amount);
    	$deduction_period 	= $request->adjustment_deduction_period;

    	$transaction = new Rata;

    	$transaction = $transaction->find($request->transaction_id);

    	$transaction->adjustment_number_work_days 	= $number_of_workdays;
    	$transaction->adjustment_transpo_amount 	= $new_transpo_amount;
        $transaction->adjustment_rep_amount 		= $new_rep_amount;
        $transaction->adjustment_percentage 		= $percentage_rate;
        $transaction->adjustment_deduction_period 	= $deduction_period;
        $transaction->adjustment_percentage_amount 	= ($new_transpo_amount) ? $new_transpo_amount : $new_rep_amount;

        $transaction->save();

        return json_encode(['status'=>true, 'response'=>'Save Successfully!']);
    }


}
