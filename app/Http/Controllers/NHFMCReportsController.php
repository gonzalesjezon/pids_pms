<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input;
use App\Employee;
use App\Transaction;
use App\LoanInfo;
use App\Loan;
use App\EmployeeStatus;
use App\EmployeeInformation;
class NHFMCReportsController extends Controller
{
    function __construct(){
    	$this->title = 'NHFMC Reports';
    	$this->module = 'nhfmcreports';
        $this->module_prefix = 'payrolls/reports';
    	$this->controller = $this;

    }

    public function index(){

    	$employee 	= new Employee;
    	$empstatus 	= new EmployeeStatus;
    	$employeeinfo = new EmployeeInformation;

    	$empstatus = $empstatus
    	->where('status',1)
    	->select('RefId')
    	->get()
    	->toArray();

    	$employeeinfo = $employeeinfo
    	->whereIn('employee_status_id',$empstatus)
    	->select('employee_id')
    	->get()
    	->toArray();

    	$employee = $employee
    	->whereIn('id',$employeeinfo)
    	->where('active',1)
    	->orderBy('lastname','asc')
    	->get();

    	$response = array(
    					'employee'		=> $employee,
    					'module'        => $this->module,
    					'controller'    => $this->controller,
                        'module_prefix' => $this->module_prefix,
    					'title'		    => $this->title
    					);

    	return view($this->module_prefix.'.'.$this->module,$response);
    }

    public function show(){
        $data = Input::all();

        $year = $data['year'];
        $month = $data['month'];
        $empid = $data['id'];

        // $transaction    = new Transaction;
        // $employeeinfo   = new EmployeeInfo;
        // $loaninfo       = new LoanInfo;

    }
}
