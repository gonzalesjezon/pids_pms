<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input;
use App\EmployeeInfo;
use App\Transaction;
use App\LoanInfo;
use App\Loan;
class HDMFMPLReportsController extends Controller
{
    function __construct(){
    	$this->title = 'HDMF MPL REPORTS';
    	$this->module = 'hdmfmplreports';
        $this->module_prefix = 'payrolls/reports';
    	$this->controller = $this;

    }

    public function index(){

    	$response = array(
    					'module'        => $this->module,
    					'controller'    => $this->controller,
                        'module_prefix' => $this->module_prefix,
    					'title'		    => $this->title
    					);

    	return view($this->module_prefix.'.'.$this->module,$response);
    }

    public function show(){
        $data = Input::all();

        $transaction    = new Transaction;
        $loaninfo       = new LoanInfo;
        $loan           = new Loan;

        $loan = $loan
        ->where('code','MPL')
        ->first();

        $loaninfo = $loaninfo
        ->select('employee_id')
        ->where('loan_id',$loan->id)
        ->get()
        ->toArray();

        $transaction = $transaction
        ->whereIn('employee_id',$loaninfo)
        ->where('year',$data['year'])
        ->where('month',$data['month'])
        ->select('employee_id')
        ->get()
        ->toArray();

        $loanInfo = new LoanInfo;

        $query = $loanInfo
        ->with('employees')
        ->where('loan_id',$loan->id)
        ->whereIn('employee_id',$transaction)
        ->get();


        return json_encode($query);

    }
}
