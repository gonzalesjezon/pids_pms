<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TravelRate;
use App\TravelRateTransaction;
use Input;
class GeneralPayrollTravelAllowanceReportsController extends Controller
{
    function __construct(){
    	$this->title = 'GENERAL PAYROLL TRAVEL ALLOWANCE';
    	$this->module = 'generalpayrolltravelallowance';
        $this->module_prefix = 'payrolls/reports';
    	$this->controller = $this;

    }

    public function index(){

    	$transaction = new TravelRateTransaction;

    	$transaction = $transaction->groupBy('remarks')->get();

    	$response = array(
    					'transaction' 	=> $transaction,
    					'module'        => $this->module,
    					'controller'    => $this->controller,
                        'module_prefix' => $this->module_prefix,
    					'title'		    => $this->title
    					);

    	return view($this->module_prefix.'.'.$this->module,$response);
    }

    public function show(){

    	$transaction = new TravelRateTransaction;

        $data = Input::all();

        $created_at = $data['created_at'];

        $query['transaction'] = $transaction
        ->with(['rates','travelrates','positions','employees'])
        ->where('created_at','like','%'.$created_at.'%')
        ->get();

       return json_encode($query);
    }
}
