<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class LWOPAdjustmentReportsController extends Controller
{
    function __construct(){
    	$this->title = 'SCHEDULE SALARY ADJUSTMENT';
    	$this->module = 'lwopadjustments';
        $this->module_prefix = 'payrolls/reports';
    	$this->controller = $this;

    }

    public function index(){

    	$response = array(
    					'module'        => $this->module,
    					'controller'    => $this->controller,
                        'module_prefix' => $this->module_prefix,
    					'title'		    => $this->title
    					);

    	return view($this->module_prefix.'.'.$this->module,$response);
    }

    public function show(){
        // $data = Input::all();

        // $transaction    = new Transaction;
        // $employeeinfo   = new EmployeeInfo;
        // $loaninfo       = new LoanInfo;
        // $benefitinfo    = new BenefitInfo;
        // $deductioninfo  = new DeductionInfo;
        // $salaryinfo     = new SalaryInfo;

        // if(isset($data['id'])){
        //     $query['transaction'] = $transaction
        //     ->with(['employees',
        //         'positions',
        //         'offices',
        //         'benefitTransaction' => function($qry){ $qry->where('status','pera'); }
        //     ])
        //     ->where('employee_id',$data['id'])
        //     ->where('year',$data['year'])
        //     ->where('month',$data['month'])
        //     ->first();

        //     // $query['rata']          = Rata::where('employee_id',$data['id'])
        //     //                             ->where('year',$data['year'])
        //     //                             ->where('month',$data['month'])
        //     //                             ->first();

        //     $query['employeeinfo'] = $employeeinfo
        //     ->with('employees')
        //     ->where('employee_id',$data['id'])
        //     ->first();

        //     $query['loaninfo'] = $loaninfo
        //     ->with('loans')
        //     ->where('employee_id',$data['id'])
        //     ->get();

        //     $query['deductioninfo'] = $deductioninfo
        //     ->where('employee_id',$data['id'])
        //     ->get();

        //     $query['salaryinfo'] = $salaryinfo
        //     ->where('employee_id',$data['id'])
        //     ->first();

        //     return json_encode($query);

        // }
    }
}
