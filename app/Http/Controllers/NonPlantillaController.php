<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Input;
use Crypt;
use App\Employee;
use App\GsisPolicy;
use App\PhilhealthPolicy;
use App\PagibigPolicy;
use App\TaxPolicy;
use App\Bank;
use App\BankBranch;
use App\Department;
use App\Company;
use App\Position;
use App\Office;
use App\Division;
use App\EmployeeStatus;
use App\EmployeeInformation;
use App\Benefit;
use App\PositionItemSetup;
use App\TaxTable;
use App\WageRate;
use App\BenefitInfo;
use App\SalaryInfo;
use App\LoanInfo;
use App\DeductionInfo;
use App\Loan;
use App\Deduction;
use App\NonPlantillaEmployeeInfo;
use App\NonPlantillaTransaction;
use Carbon\Carbon;
use App\AttendanceInfo;
use Session;

class NonPlantillaController extends Controller
{

    function __construct(){
        $this->title = 'NON PLANTILLA';
        $this->module = 'nonplantilla';
        $this->module_prefix = 'payrolls';
        $this->controller = $this;

    }

    public function index(){

        $benefit        = Benefit::orderBy('Name','asc')->get();
        $loans          = Loan::orderBy('Name','asc')->get();
        $deduction      = Deduction::orderBy('Name','asc')->get();
        $empstatus      = EmployeeStatus::where('status',0)->get();


        $response = array(
                        'empstatus'     => $empstatus,
                        'benefit'       => $benefit,
                        'loans'         => $loans,
                        'deductions'    => $deduction,
                        'module'        => $this->module,
                        'controller'    => $this->controller,
                        'module_prefix' => $this->module_prefix,
                        'title'         => $this->title
                        );

        return view($this->module_prefix.'.'.$this->module.'.index',$response);
    }

    public function showBenefitinfo(){


        $response       = array(
                            'controller'    => $this->controller,
                            'module'        => $this->module,
                            'module_prefix' => $this->module_prefix,
                        );

        return view($this->module_prefix.'.'.$this->module.'.benefitdatatable',$response);
    }

    public function showLoaninfo(){


        $response       = array(
                            'controller'    => $this->controller,
                            'module'        => $this->module,
                            'module_prefix' => $this->module_prefix,
                        );

        return view($this->module_prefix.'.'.$this->module.'.loandatatable',$response);
    }

     public function showDeductioninfo(){


        $response       = array(
                            'controller'    => $this->controller,
                            'module'        => $this->module,
                            'module_prefix' => $this->module_prefix,
                        );

        return view($this->module_prefix.'.'.$this->module.'.deductdatatable',$response);
    }

    public function show(){
        $q                  = Input::get('q');
        $year               = Input::get('year');
        $month              = Input::get('month');
        $checkpayroll       = Input::get('checkpayroll');
        $check_payroll      = Input::get('check_payroll');
        $employee_status    = Input::get('employee_status');

        $data = $this->searchName($q,$check_payroll);

        if(isset($year) || isset($month) || isset($checkpayroll) || isset($employee_status)){
            $data = $this->filter($year,$month,$checkpayroll,$employee_status);
        }
        $response = array(
                        'data'          => $data,
                        'title'         => $this->title,
                        'controller'    => $this->controller,
                        'module'        => $this->module,
                        'module_prefix' => $this->module_prefix
                    );

        return view($this->module_prefix.'.'.$this->module.'.datatable',$response);
    }

    public function searchName($q,$checkpayroll){

        $employeestatus              = new EmployeeStatus;
        $employee_information        = new EmployeeInformation;
        $employee                    = new Employee;
        $nonplantilla_employee_info  = new NonPlantillaEmployeeInfo;
        $transaction                 = new NonPlantillaTransaction;

        $cols         = ['lastname','firstname','middlename'];

        $empstatus_id = $employeestatus->where('status',0)->select('RefId')->get()->toArray();
        $employee_info_id  = $employee_information->whereIn('employee_status_id',$empstatus_id)->select('employee_id')->get()->toArray();
        $nonplantilla_employee_id =  $nonplantilla_employee_info->select('employee_id')->whereNotNull('daily_rate_amount')->get()->toArray();


        $query = [];
        switch ($checkpayroll) {
            case 'wpayroll':
               $employee_id = $transaction->whereIn('employee_id',$employee_info_id)->select('employee_id')->get()->toArray();
               $query = $employee->whereIn('id',$employee_id)->whereIn('id',$nonplantilla_employee_id);
                break;

            default:
                $employee_id = $transaction->whereIn('employee_id',$employee_info_id)->select('employee_id')->get()->toArray();
                $query = $employee->whereNotIn('id',$employee_id)->whereIn('id',$nonplantilla_employee_id);
                break;
        }

      $query = $query->where(function($query) use($cols,$q){
            $query = $query->where(function($qry) use($q, $cols){
                foreach ($cols as $key => $value) {
                    $qry->orWhere($value,'like','%'.$q.'%');
                }
            });

        });

        $response = $query->orderBy('lastname','asc')->get();
        return $response;
    }

    public function filter($year,$month,$checkpayroll,$employee_status){



        $employeestatus              = new EmployeeStatus;
        $employee_information        = new EmployeeInformation;
        $nonplantilla                = new NonPlantillaTransaction;
        $employee                    = new Employee;
        $nonplantilla_employee_info  = new NonPlantillaEmployeeInfo;

        // --GET EMPLOYEE STATUS ID EX(JOB ORDER)
        $empstatus_id = $employeestatus
        ->where('status',0)
        ->where('RefId',$employee_status)
        ->select('RefId')
        ->get()
        ->toArray();

        $employee_id  = $employee_information
        ->whereIn('employee_status_id',$empstatus_id)
        ->select('employee_id')
        ->get()
        ->toArray();

        $nonplantilla_employee_id =  $nonplantilla_employee_info
        ->select('employee_id')
        ->whereNotNull('daily_rate_amount')
        ->get()
        ->toArray();

        $query = [];
        $response = "";

        switch ($checkpayroll) {
            case 'wpayroll':
                    $query = $nonplantilla->select('employee_id');
                    if(count($empstatus_id) > 0){
                        $query = $query->whereIn('employee_status_id',$empstatus_id);
                    }
                    if(isset($year)){
                        $query = $query->where('year',$year);
                    }
                    if(isset($month)){
                        $query = $query->where('month',$month);
                    }
                    $query = $query->get()->toArray();

                    $response = $employee->whereIn('id',$query)->orderBy('lastname','asc')->get();
                break;

            case 'wopayroll':
                    $query = $nonplantilla->select('employee_id');
                    if(count($empstatus_id) > 0){
                        $query = $query->whereIn('employee_status_id',$empstatus_id);
                    }
                    if(isset($year)){
                        $query = $query->where('year',$year);
                    }
                    if(isset($month)){
                        $query = $query->where('month',$month);
                    }
                    $query = $query->get()->toArray();

                    $response = $employee->whereIn('id',$employee_id)
                                        ->whereIn('id',$nonplantilla_employee_id)
                                        ->whereNotIn('id',$query)
                                        ->orderBy('lastname','asc')->get();

                break;
        }

        return $response;
    }

     public function getSearchby(){
        $q = Input::get('q');

        $query = "";
        switch ($q) {
            case 'company':
                $query = Company::orderBy('name','asc')->get();
                break;
            case 'department':
                $query = Department::orderBy('name','asc')->get();
                break;
            case 'office':
                $query = Office::orderBy('name','asc')->get();
                break;
            case 'division':
                $query = Division::orderBy('name','asc')->get();
                break;
            case 'position':
                $query = Position::orderBy('name','asc')->get();
                break;

            default:
                # code...
                break;
        }

        return json_encode($query);
    }

    public function getEmployeesinfo(){

        $data = Input::all();

        $query['transaction'] = NonPlantillaTransaction::with('employees')
                            ->where('year',@$data['year'])
                            ->where('month',@$data['month'])
                            ->where('employee_id',@$data['id'])->first();

        $query['attendanceinfo'] = AttendanceInfo::where('year',@$data['year'])
                            ->where('month',@$data['month'])
                            ->where('employee_id',@$data['id'])->first();

        $query['employeeinfo'] = NonPlantillaEmployeeInfo::with('taxpolicyOne','taxpolicyTwo')
                                            ->where('employee_id',@$data['id'])
                                            ->first();


        // $query['benefitinfo'] = BenefitInfo::with(['benefits' => function($qry) {$qry->where('name','PERA');} ])->where('employeeinfo_id',$query['employeeinfo']['id'])
        //                                 ->get();

        // $query['deductioninfo'] = DeductionInfo::with('deductions')->where('employeeinfo_id',$query['employeeinfo']['id'])
        //                                 ->get();

        // $query['loaninfo'] = LoanInfo::with('loans')->where('employeeinfo_id',$query['employeeinfo']['id'])
                                        // ->get();

        return json_encode($query);
    }

    public function processPayroll() {
        // getting values from form (like $record_num)
        $data = Input::all();
        // Session::put('progress', 0);
        // Session::save(); // Remember to call save()

        // for ($i = 1; $i < $record_num; $i++) {
            // $record = new Record();

            // // adding attributes...

            // $record->save();
        //     Session::put('progress', $i);
        //     Session::save(); // Remember to call save()
        // }

        // $response = Response::make();
        // $response->header('Content-Type', 'application/json');
        // return $response;

        $transactions =  new NonPlantillaTransaction;

        if(isset($data['transaction_id'])){

            //ATTENDACE

            $this->postAttendanceinfo($data['attendance'],$data['transaction_id'],$data['empid'],$data['attendance_id']);

            // TRANSACTIONS
            $actual_basicpay     = ($data['summary']['actual_basicpay']) ? str_replace(',', '', $data['summary']['actual_basicpay']) : 0;
            $adjust_basicpay     = ($data['summary']['adjust_basicpay']) ? str_replace(',', '', $data['summary']['adjust_basicpay']) : 0;
            $total_basicpay      = ($data['summary']['total_basicpay']) ? str_replace(',', '', $data['summary']['total_basicpay']) : 0;

            $actual_absences     = ($data['summary']['actual_absences']) ? str_replace(',', '', $data['summary']['actual_absences']) : 0;
            $adjust_absences     = ($data['summary']['adjust_absences']) ? str_replace(',', '', $data['summary']['adjust_absences']) : 0;
            $total_absences      = ($data['summary']['total_absences']) ? str_replace(',', '', $data['summary']['total_absences']) : 0;

            $actual_tardines     = ($data['summary']['actual_tardines']) ? str_replace(',', '', $data['summary']['actual_tardines']) : 0;
            $adjust_tardines     = ($data['summary']['total_tardines']) ? str_replace(',', '', $data['summary']['adjust_tardines']) : 0;
            $total_tardines      = ($data['summary']['total_tardines']) ? str_replace(',', '', $data['summary']['total_tardines']) : 0;

            $actual_undertime     = ($data['summary']['actual_undertime']) ? str_replace(',', '', $data['summary']['actual_undertime']) : 0;
            $adjust_undertime     = ($data['summary']['adjust_undertime']) ? str_replace(',', '', $data['summary']['adjust_undertime']) : 0;
            $total_undertime      = ($data['summary']['total_undertime']) ? str_replace(',', '', $data['summary']['total_undertime']) : 0;

            // $actual_contribution     = ($data['summary']['actual_contribution']) ? str_replace(',', '', $data['summary']['actual_contribution']) : 0;
            // $adjust_contribution     = ($data['summary']['adjust_contribution']) ? str_replace(',', '', $data['summary']['adjust_contribution']) : 0;
            // $total_contribution      = ($data['summary']['total_contribution']) ? str_replace(',', '', $data['summary']['total_contribution']) : 0;

            // $actual_loan     = ($data['summary']['actual_loan']) ? str_replace(',', '', $data['summary']['actual_loan']) : 0;
            // $adjust_loan     = ($data['summary']['adjust_loan']) ? str_replace(',', '', $data['summary']['adjust_loan']) : 0;
            // $total_loan      = ($data['summary']['total_loan']) ? str_replace(',', '', $data['summary']['total_loan']) : 0;

            // $actual_otherdeduct     = ($data['summary']['actual_otherdeduct']) ? str_replace(',', '', $data['summary']['actual_otherdeduct']) : 0;
            // $adjust_otherdeduct     = ($data['summary']['adjust_otherdeduct']) ? str_replace(',', '', $data['summary']['adjust_otherdeduct']) : 0;
            // $total_otherdeduct      = ($data['summary']['total_otherdeduct']) ? str_replace(',', '', $data['summary']['total_otherdeduct']) : 0;

            $basic_net_pay      = ($data['summary']['basic_net_pay']) ? str_replace(',', '', $data['summary']['basic_net_pay']) : 0;
            $net_deduction      = ($data['summary']['net_deduction']) ? str_replace(',', '', $data['summary']['net_deduction']) : 0;
            $gross_pay      = ($data['summary']['gross_pay']) ? str_replace(',', '', $data['summary']['gross_pay']) : 0;
            $gross_taxable_pay      = ($data['summary']['gross_taxable_pay'])? str_replace(',', '', $data['summary']['gross_taxable_pay']) : 0;
            $net_pay      = ($data['summary']['net_pay']) ? str_replace(',', '', $data['summary']['net_pay']) : 0;
            $tax_rate_amount_one      = ($data['summary']['tax_rate_amount_one']) ? str_replace(',', '', $data['summary']['tax_rate_amount_one']) : 0;
            $tax_rate_amount_two      = ($data['summary']['tax_rate_amount_two']) ? str_replace(',', '', $data['summary']['tax_rate_amount_two']) : 0;


            $transactions = NonPlantillaTransaction::find($data['transaction_id']);

            $transactions->actual_basicpay     = $actual_basicpay;
            $transactions->adjust_basicpay     = $adjust_basicpay;
            $transactions->total_basicpay      = $total_basicpay;
            $transactions->actual_absences     = $actual_absences;
            $transactions->adjust_absences     = $adjust_absences;
            $transactions->total_absences      = $total_absences;
            $transactions->actual_tardines     = $actual_tardines;
            $transactions->adjust_tardines     = $adjust_tardines;
            $transactions->total_tardines      = $total_tardines;
            $transactions->actual_undertime    = $actual_undertime;
            $transactions->adjust_undertime    = $adjust_undertime;
            $transactions->total_undertime     = $total_undertime;
            $transactions->basic_net_pay       = $basic_net_pay;
            $transactions->pay_period          = $data['summary']['pay_period'];
            $transactions->sub_pay_period      = @$data['summary']['semi_pay_period'];
            $transactions->tax_rate_amount_one = $tax_rate_amount_one;
            $transactions->tax_rate_amount_two = $tax_rate_amount_two;
            $transactions->net_deduction       = $net_deduction;
            $transactions->gross_pay           = $gross_pay;
            $transactions->gross_taxable_pay   = $gross_taxable_pay;
            $transactions->net_pay             = $net_pay;

            $transactions->save();


            $response = json_encode(['status'=>true,'response'=>'Payroll Updated Successfully!']);

        }else{

            $ctr = 0;
            $error = 0;
            $error_id = [];
            foreach ($data['empid'] as $key => $_id) {

                if(isset($_id)){
                    $check = NonPlantillaEmployeeInfo::where('employee_id',$_id)->whereNotNull('daily_rate_amount')->first();
                    if($check === null){
                        $error++;
                        $error_id[] = $_id;
                    }
                }
            }
            if($error == 0){
            $this->postAttendanceinfo($data['attendance'],$data['transaction_id'],$data['empid'],$data['attendance_id']);
                foreach ($data['empid'] as $key => $_id) {
                    if(isset($_id)){

                        $employeeinfo = NonPlantillaEmployeeInfo::where('employee_id',$_id)->first();
                        $employeeinformation = EmployeeInformation::where('employee_id',$_id)->first();
                        $effectivity_date = Carbon::now()->toDateTimeString();
                        $daily_rate_amount  = $employeeinfo->daily_rate_amount;

                        $transactions->employee_id        = $_id;
                        $transactions->employeeinfo_id    = $employeeinfo->id;
                        $transactions->division_id        = $employeeinformation->division_id;
                        $transactions->company_id         = $employeeinformation->company_id;
                        $transactions->position_item_id   = $employeeinformation->position_item_id;
                        $transactions->office_id          = $employeeinformation->office_id;
                        $transactions->department_id      = $employeeinformation->department_id;
                        $transactions->employee_status_id = $employeeinformation->employee_status_id;
                        $transactions->position_id        = $employeeinformation->position_id;
                        $transactions->pay_period         = $data['summary']['pay_period'];
                        $transactions->sub_pay_period     = @$data['summary']['semi_pay_period'];
                        $transactions->year               = $data['year'];
                        $transactions->month              = $data['month'];

                        NonPlantillaTransaction::create($transactions->toArray());
                        $ctr++;
                    }
                }
                $response = json_encode(['status'=>true,'response'=>'Processed Successfully! <br>'.$ctr.' Records Saved']);
            }else{
                $error_id = implode(',', $error_id);
                $response = json_encode(['status'=>false,'response'=>'Save Failed! <br>'.$error.' Records <br>No Employee Setup <br>Employee ID <br>['.$error_id.']']);
            }
        }

        return $response;
    }

    public function postAttendanceinfo($data,$transaction_id,$employee_id,$attendance_id){

        $total_workdays = ($data['total_workdays']) ? str_replace(',', '',  $data['total_workdays']) : 0;
        $total_absence = ($data['total_absences']) ? str_replace(',', '',  $data['total_absences']) : 0;
        $total_tardines = ($data['total_tardines']) ? str_replace(',', '',  $data['total_tardines']) : 0;
        $total_undertime = ($data['total_undertime']) ? str_replace(',', '',  $data['total_undertime']) : 0;

        $attendance = new AttendanceInfo;

        foreach ($employee_id as $key => $value) {

            if(isset($value)){

                if(isset($attendance_id)){

                    $attendance = AttendanceInfo::find($attendance_id);

                    $attendance->actual_workdays         = $data['actual_workdays'];
                    $attendance->adjust_workdays         = $data['adjust_workdays'];
                    $attendance->total_workdays          = $total_workdays;
                    $attendance->actual_absence          = $data['actual_absences'];
                    $attendance->adjust_absence          = $data['adjust_absences'];
                    $attendance->total_absence           = $total_absence;
                    $attendance->actual_tardines         = $data['actual_tardines'];
                    $attendance->adjust_tardines         = $data['adjust_tardines'];
                    $attendance->total_tardines          = $total_tardines;
                    $attendance->actual_undertime        = $data['actual_undertime'];
                    $attendance->adjust_undertime        = $data['adjust_undertime'];
                    $attendance->total_undertime         = $total_undertime;
                    $attendance->year                    = $data['year'];
                    $attendance->month                   = $data['month'];
                    $attendance->employee_status         = 'nonplantilla';

                    $attendance->save();

                }else{

                    $data = array_merge($data,[
                        'employee_status'        => 'nonplantilla',
                        'total_workdays'         => $total_workdays,
                        'total_absence'          => $total_absence,
                        'total_tardines'         => $total_tardines,
                        'total_undertime'        => $total_undertime,
                        'employee_id'            => $value,
                        'transaction_id'         => $transaction_id,
                        'actual_absence'         => (int)$data['actual_absences'],
                    ]);

                    $attendance->create($data);
                }
            }
        }

    }


    public function destroy(){
        $data = Input::all();

        $transactions = new NonPlantillaTransaction;

        $attendance = new AttendanceInfo;

        foreach ($data['empid'] as $key => $value) {

            $transactions->where('employee_id',$data['empid'][$key])
                                ->where('month',$data['month'])
                                ->where('year',$data['year'])
                                ->delete();
             $attendance->where('employee_id',$data['empid'][$key])
                                ->where('month',$data['month'])
                                ->where('year',$data['year'])
                                ->delete();

        }

        return json_encode(['status'=>true,'response'=>'Delete Successfully']);
    }

}
