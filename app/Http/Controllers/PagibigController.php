<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Input;
use App\PagibigTable;
use App\PagibigPolicy;

class PagibigController extends Controller
{
    
    function __construct(){
        $this->controller = $this;
        $this->title = 'PAGIBIG POLICY';
        $this->module_prefix = 'payrolls/admin/filemanagers';
        $this->module = 'pagibig';
        
    }

    public function index(){
        $response = array(
            'controller'    => $this->controller,
            'title'         => $this->title,
            'module'        => $this->module,
            'module_prefix' => $this->module_prefix,
        );

        return view($this->module_prefix.'.'.$this->module.'.index',$response);
    }

    public function storePolicy(Request $request){

        $this->validate(request(),[
            'policy_name'           => 'required',
            // 'pay_period'            => 'required',
            'deduction_period'      => 'required',
            'policy_type'           => 'required',
            'based_on'              => 'required',
        ]);

        $pagibig_policy = new PagibigPolicy;

        if(isset($request->for_update)){

            $pagibig_policy = PagibigPolicy::find((int)$request->for_update);

            $pagibig_policy->fill($request->all())->save();

            $pagibig_policy->save();

            $response =  json_encode(['status'=>true,'response' => 'Update Successfully!']);


        }else{

            $check = $pagibig_policy->where('policy_name',$request->policy_name)->get()->toArray();

            if(count($check) > 0){

                $response = json_encode(['status'=>false,'response' => 'Saving Failed!']);

            }else{

                $pagibig_policy->fill($request->all())->save();

                $pagibig_policy->save();

                $response =  json_encode(['status'=>true,'response' => 'Save Successfully!']);

            }
        }


        return $response;

    }

    public function showPolicy(){

        $q = Input::get('q');
        // $limit = Input::get('limit');
        // if(empty($limit)){ $limit  =  10; } 

        $data = $this->get_pagibigpolicy_records($q);

        $response = array(
                        'data'          => $data,
                        'controller'    => $this->controller,
                        'module'        => $this->module,
                        'module_prefix' => $this->module_prefix,

                        );

        return view($this->module_prefix.'.'.$this->module.'.datatablepolicy',$response);

    }


    private function get_pagibigpolicy_records($q){

        $cols = ['policy_name','pay_period','deduction_period','policy_type'];

        $query = PagibigPolicy::where(function($query) use($cols,$q){

                $query = $query->where(function($qry) use($q, $cols){
                    foreach ($cols as $key => $value) {
                        $qry->orWhere($value,'like','%'.$q.'%');
                    }
                });
        });
        $response = $query->get();

        return $response;

    }

    public function getItem(){
        $id = Input::get('id');

        $query = PagibigPolicy::where('id',$id)->first();

        return json_encode($query);
    }
}
