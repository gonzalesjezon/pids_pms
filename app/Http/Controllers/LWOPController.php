<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input;
use App\Employee;
use App\Transaction;
use App\BenefitInfoTransaction;
use App\LWOPTransaction;
use App\Benefit;
use Auth;
class LWOPController extends Controller
{
    function __construct(){
    	$this->title = 'LWOP';
    	$this->module = 'lwop';
        $this->module_prefix = 'payrolls/otherpayrolls';
    	$this->controller = $this;

    }

    public function index(){


    	$response = array(
    					'module'        => $this->module,
    					'controller'    => $this->controller,
                        'module_prefix' => $this->module_prefix,
    					'title'		    => $this->title
    					);

    	return view($this->module_prefix.'.'.$this->module.'.index',$response);
    }

    public function show(){

        $q = Input::get('q');
        $empstatus = Input::get('empstatus');
        $category = Input::get('category');
        $emp_type = Input::get('emp_type');
        $searchby = Input::get('searchby');

        $data = "";

        $data = $this->searchName($q);

        if(isset($empstatus) || isset($category)){
            $data = $this->filter($empstatus,$category,$emp_type,$searchby);

        }

        $response = array(
                        'data'          => $data,
                        'title'         => $this->title,
                        'controller'    => $this->controller,
                        'module'        => $this->module,
                        'module_prefix' => $this->module_prefix
                    );

        return view($this->module_prefix.'.'.$this->module.'.namelist',$response);

    }

    public function searchName($q){
        $cols = ['lastname','firstname','id'];

        $query = Employee::where(function($query) use($cols,$q){

                        $query = $query->where(function($qry) use($q, $cols){
                            foreach ($cols as $key => $value) {
                                $qry->orWhere($value,'like','%'.$q.'%');
                            }
                        });
                    });


        $response = $query->where('active',1)->orderBy('lastname','asc')->get();

        return $response;

    }


    public function store(Request $request){

        $transaction = new LWOPTransaction;

        $basic_amount = (isset($request->net_basic_pay)) ? str_replace(',', '', $request->net_basic_pay) : 0;
        $pera_amount = (isset($request->net_pera)) ? str_replace(',', '', $request->net_pera) : 0;
        $lwop_amount = (isset($request->net_total)) ? str_replace(',', '', $request->net_total) : 0;


        if(isset($request->transaction_id)){

            $transaction = $transaction->find($request->transaction_id);

            $transaction->employee_id               = $request->employee_id;
            $transaction->employee_number           = $request->employee_number;
            $transaction->number_of_lwop            = $request->lwop_adjustment;
            $transaction->number_of_undertime       = $request->undertime_adjustment;
            $transaction->number_of_tardiness       = $request->tardiness_adjustment;
            $transaction->basic_adjustment_amount   = $basic_amount;
            $transaction->pera_adjustment_amount    = $pera_amount;
            $transaction->lwop_adjustment_amount    = $lwop_amount;
            $transaction->updated_by                = Auth::User()->id;

            $transaction->save();

            $response = json_encode(['status'=>true,'response'=>'Update Successfully']);

        }else{

            $request->validate([
                'lwop_adjustment' => 'required',
                'employee_id'     => 'required'
            ]);


            $transaction->employee_id               = $request->employee_id;
            $transaction->employee_number           = $request->employee_number;
            $transaction->number_of_lwop            = $request->lwop_adjustment;
            $transaction->number_of_undertime       = $request->undertime_adjustment;
            $transaction->number_of_tardiness       = $request->tardiness_adjustment;
            $transaction->basic_adjustment_amount   = $basic_amount;
            $transaction->pera_adjustment_amount    = $pera_amount;
            $transaction->lwop_adjustment_amount    = $lwop_amount;
            $transaction->year                      = $request->year;
            $transaction->month                     = $request->month;
            $transaction->created_by                = Auth::User()->id;

            $transaction->save();

            $response = json_encode(['status'=>true,'response'=>'Save Successfully']);
        }

        return $response;



    }

    public function showLWOP(){

         $response = array(
            'title'             => $this->title,
            'controller'        => $this->controller,
            'module'            => $this->module,
            'module_prefix'     => $this->module_prefix,

        );

        return view($this->module_prefix.'.'.$this->module.'.datatable',$response);


    }

    public function getLWOP(){

        $data = Input::all();

        $year        = $data['year'];
        $month       = $data['month'];
        $employee_id = $data['id'];

        $month_number = date('m',strtotime($month));

        $transaction        = new Transaction;
        $benefit            = new Benefit;
        $benefitTransaction = new BenefitInfoTransaction;
        $lwopTransaction    = new LWOPTransaction;

        $benefit =  $benefit
        ->where('code','PERA')
        ->first();

        $query['transaction'] = $transaction
        ->with([
            'benefitTransaction' => function($qry) use($benefit){
                $qry = $qry->where('benefit_id',$benefit->id);
            }
        ])
        ->where('employee_id',$employee_id)
        ->where('year',$year)
        ->where('month',$month)
        ->first();

        $query['lwoptransaction'] = $lwopTransaction
        ->where('employee_id',$employee_id)
        ->get();

        return $query;


    }
}
