<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Office extends Model
{

    // protected $table = 'pms_offices';
    // protected $fillable = [
    // 	'code',
    // 	'name',
    //     'created_by',
    //     'updated_by'

    // ];

    protected $primaryKey = 'RefId';
    protected $table = 'office';
    protected $fillable = [
    	'Code',
    	'Name',
        'created_by',
        'updated_by'

    ];

}
