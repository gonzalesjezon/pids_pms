<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Deduction extends Model
{
    protected $table = 'pms_deductions';
    protected $fillable = [
    	'code',
    	'name',
    	'tax_type',
    	'amount',
    	'payroll_group',
    	'itr_classification',
    	'alphalist_classification',
    	'remarks'
    ];
}
