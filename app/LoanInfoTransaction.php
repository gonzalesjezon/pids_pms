<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LoanInfoTransaction extends Model
{
    protected $table 	= 'pms_loaninfo_transactions';
    protected $fillable = [
		'employee_id',
		'transaction_id',
		'loan_id',
		'loan_info_id',
		'amount',
        'year',
        'month',
		'created_by',
		'updated_by',
    ];

    public function loanInfo(){
    	return $this->belongsTo('App\LoanInfo','loan_info_id')->with('loans');
    }
    public function loans(){
    	return $this->belongsTo('App\Loan','loan_id');
    }
}
