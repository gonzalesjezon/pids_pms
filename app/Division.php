<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Division extends Model
{

    // protected $table = 'pms_divisions';
    // protected $fillable = [
    // 	'code',
    // 	'name',
    //     'created_by',
    //     'updated_by'

    // ];
    protected $primaryKey = 'RefId';
    protected $table = 'division';
    protected $fillable = [
    	'Code',
    	'Name',
        'created_by',
        'updated_by'

    ];

}
