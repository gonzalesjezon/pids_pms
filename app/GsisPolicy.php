<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GsisPolicy extends Model
{
    protected $table = 'pms_gsispolicy';
    protected $fillable = [
    	'policy_name',
    	'pay_period',
    	'deduction_period',
    	'policy_type',
    	'based_on',
    	'computation',
    	'remarks',
        'ee_percentage',
        'er_percentage'
    ];
}
