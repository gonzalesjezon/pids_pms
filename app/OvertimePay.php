<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OvertimePay extends Model
{
    protected $table = 'pms_overtime';
    protected $fillable = [
		'employee_id',
		'previous_balance',
		'used_amount',
		'available_balance',
		'actual_regular_overtime',
		'adjust_regular_overtime',
		'total_regular_amount',
		'actual_special_overtime',
		'adjust_special_overtime',
		'total_special_amount',
		'actual_regular_holiday_overtime',
		'adjust_regular_holiday_overtime',
		'total_regular_holiday_amount',
		'total_overtime_amount',
		'year',
		'month',
		'pay_period',
		'sub_pay_period',
		'status',
		'created_by',
		'updated_by'
    ];

    public function employeeinformation(){
    	return $this->belongsTo('App\EmployeeInformation','employee_id')->with('positions');
    }
    public function salaryinfo(){
    	return $this->belongsTo('App\SalaryInfo','employee_id')->with('salarygrade');
    }
    public function employees(){
    	return $this->belongsTo('App\Employee','employee_id');
    }
    public function employeeinfo(){
    	return $this->belongsTo('App\EmployeeInfo','employee_id');
    }
}
