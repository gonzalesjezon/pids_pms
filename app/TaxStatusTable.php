<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TaxStatusTable extends Model
{
    protected $table = 'taxes_status_table';
    protected $fillable = [
    	'tax_status',
    	'description',
    	'exemption',
    	'remarks',
    ];
}
