<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmployeeInformation extends Model
{

    protected $table = 'pms_employee_information';
    protected $fillable = [

        'employee_id',
        'company_id',
        'division_id',
        'department_id',
        'office_id',
        'employee_status_id',
        'position_item_id',
        'position_id',
        'hired_date',
        'assumption_date',
        'resigned_date',
        'rehired_date',
        'start_date',
        'end_date',
        'pay_period',
        'pay_rate_id',
        'work_schedule_id',
        'appointment_status_id',
        'designation_id',
        'created_by',
        'updated_by',
        'deleted_at'

    ];

   	public function employees(){
    	return $this->belongsTo('App\Employee','employee_id');
    }

    public function companies(){
    	return $this->belongsTo('App\Company','company_id');
    }

    public function divisions(){
    	return $this->belongsTo('App\Division','division_id');
    }

    public function employeestatus(){
    	return $this->belongsTo('App\EmployeeStatus','employee_status_id');
    }

    public function departments(){
    	return $this->belongsTo('App\Department','department_id');
    }

    public function offices(){
    	return $this->belongsTo('App\Office','office_id');
    }

    public function positionitems(){
    	return $this->belongsTo('App\PositionItem','position_item_id');
    }

    public function positions(){
        return $this->belongsTo('App\Position','position_id');
    }


}

