<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SalaryGrade extends Model
{
	// protected $table = 'pms_salarygrade';
	// protected $fillable = [
	// 	'salary_grade',
	// 	'step1',
	// 	'step2',
	// 	'step3',
	// 	'step4',
	// 	'step5',
	// 	'step6',
	// 	'step7',
	// 	'step8',
	// 	'remarks',
	// ];

	protected $primaryKey = 'RefId';
	protected $table = 'salarygrade';
	protected $fillable = [
		'Code',
		'Name',
		'Step1',
		'Step2',
		'Step3',
		'Step4',
		'Step5',
		'Step6',
		'Step7',
		'Step8',
		'remarks',
	];

}
