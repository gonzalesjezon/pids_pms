<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Position extends Model
{

    // protected $table = 'pms_positions';
    // protected $fillable = [
    // 	'code',
    // 	'name',
    //     'created_by',
    //     'updated_by'
    // ];

	protected $primaryKey = 'RefId';
    protected $table = 'position';
    protected $fillable = [
    	'Code',
    	'Name',
        'created_by',
        'updated_by'
    ];

}
