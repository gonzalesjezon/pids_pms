<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LWOPTransaction extends Model
{
    protected $table = 'pms_lwop_transactions';
    protected $fillable = [

		'employee_id',
		'employee_number',
		'number_of_lwop',
		'number_of_undertime',
		'number_of_tardiness',
		'basic_adjustment_amount',
		'pera_adjustment_amount',
		'lwop_adjustment_amount',
		'created_by',
		'updated_by',
		'year',
		'month',
		'deleted_at'

    ];
}
