<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LeaveMonetizationTransaction extends Model
{
    protected $table = 'pms_leave_monetization_transactions';
    protected $fillable = [
    	'employee_id',
    	'salary_grade_id',
    	'position_id',
    	'salary_amount',
    	'number_of_days',
    	'net_amount',
    	'factor_rate',
        'remarks',
    	'created_by',
    	'updated_by',

    ];
    public function employees(){
        return $this->belongsTo('App\Employee','employee_id');
    }

    public function salaryinfo(){
    	return $this->belongsTo('App\SalaryInfo','employee_id');
    }
    public function positions(){
    	return $this->belongsTo('App\Position','position_id');
    }
    public function salarygrade(){
     return $this->belongsTo('App\SalaryGrade','salary_grade_id');
    }
}
