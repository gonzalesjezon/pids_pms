<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    protected $table = 'pms_employees';
    protected $fillable = [
        'company_id',
        'branch_id',
        'lastname',
        'firstname',
        'middlename',
        'extension_name',
        'nickname',
        'contact_number',
        'mobile_number',
        'telephone_number',
        'birthday' ,
        'birth_place',
        'email_address',
        'gender',
        'civil_status',
        'citizenship_id',
        'citizenship_country_id',
        'filipino',
        'naturalized',
        'height' ,
        'weight' ,
        'blood_type',
        'pagibig',
        'gsis',
        'philhealth' ,
        'tin',
        'sss',
        'govt_issued_id',
        'govt_issued_id_number',
        'govt_issued_id_place',
        'govt_issued_id_date_issued' ,
        'govt_issued_valid_until' ,
        'agency_employee_number',
        'biometrics',
        'house_number',
        'street',
        'subdivision',
        'brgy',
        'city_id',
        'province_id',
        'country_id',
        'permanent_house_number',
        'permanent_street',
        'permanent_subdivision',
        'permanent_brgy',
        'permanent_city_id',
        'permanent_province_id',
        'permanent_country_id',
        'permanent_telephone_number',
        'image_path',
        'active',
        'remarks',
        'created_by',
        'updated_by',
        'deleted_at'
    ];
    public function employeeinformation(){
        return $this->belongsTo('App\EmployeeInformation','id','employee_id')->with('employeestatus');
    }
    public function salaryinfo(){
        return $this->belongsTo('App\SalaryInfo','id','employee_id')->with('salarygrade');
    }


}
