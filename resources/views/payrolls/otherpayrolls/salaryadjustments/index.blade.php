@extends('app-front')

@section('content')
<style type="text/css">
.noborder{
    border:none !important;
}
.noborder-top{
    border-top:none !important;
}
.panel{
	padding: 25px;
}
</style>
<div class="row" style="padding: 40px 10px 0px 10px;">
	<div class="col-md-12">
		{!! $controller->showSalaryAdjustment() !!}
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="col-md-3">
			<input type="text" name="filter_search" class="form-control search1" placeholder="Search here">
			<div style="height: 5px;"></div>
			<div class="sub-panelnamelist ">
				{!! $controller->show() !!}
			</div>
		</div>

		<div class="col-md-9">
			<div class="row" style="margin-left: 2px;">
				<div class="col-md-12">
					<div class="button-wrapper" style="position: relative;top: 10px;left: 5px;" >
						<a class="btn btn-xs btn-info btn-savebg btn_new" id="newSummary" data-btnnew="newSummary" data-btncancel="cancelSummary" data-btnedit="editSummary" data-btnsave="saveSummary"><i class="fa fa-save"></i> New</a>

						<a class="btn btn-xs btn-info btn-editbg btn_edit hidden" id="editSummary" data-btnnew="newSummary" data-btncancel="cancelSummary" data-btnedit="editSummary" data-btnsave="saveSummary"><i class="fa fa-save"></i> Edit</a>

						<a class="btn btn-xs btn-info btn-savebg btn_save submitme hidden" data-form="form" data-btnnew="newSummary" data-btncancel="cancelSummary" data-btnedit="editSummary" data-btnsave="saveSummary" id="saveSummary"><i class="fa fa-save"></i> Save</a>
						<a class="btn btn-xs btn-danger btn_cancel hidden" data-btnnew="newSummary" data-btncancel="cancelSummary" data-form="myform" data-btnedit="editSummary" data-btnsave="saveSummary"id="cancelSummary"> Cancel</a>
					</div>
				</div>
			</div>
			<div class="row" style="padding: 20px;">
				<div class="col-md-12">
				<label id="employee_name" style="margin-left:5px; "></label>
				<form method="POST" action="{{ url($module_prefix.'/'.$module)}}" onsubmit="return false" id="form" class="myform">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<input type="hidden" name="employee_id" id="employee_id">
					<input type="hidden" name="year" id="year">
					<input type="hidden" name="month" id="month">
					<input type="hidden" name="salaryadjustment_id" id="salaryadjustment_id">
					<div class="col-md-5">
						<div class="panel">
							<table class="table noborder borderless">
								<tbody class="noborder-top">
									<tr>
										<td colspan="2">
											<span>Transaction Date</span>
										</td>
									</tr>
									<tr class="newSummary">
										<td colspan="2">
											<input type="text" name="transaction_date" id="transaction_date" class="form-control datepicker font-style2" >
										</td>
									</tr>
									<tr>
										<td>From</td>
										<td>To</td>
									</tr>
									<tr class="newSummary">
										<td>
											<input type="text" name="date_from" id="date_from" class="form-control font-style2 datepicker">
										</td>
										<td>
											<input type="text" name="date_to" id="date_to" class="form-control font-style2 datepicker">
										</td>
									</tr>
									<tr>
										<td colspan="2">Basic Pay</td>
									</tr>
									<tr class="newSummary">
										<td>
											<input type="text" name="new_position" id="new_position" class="form-control font-style2" readonly>
										</td>
										<td>
											<input type="text" name="new_rate_amount" id="new_rate_amount" class="form-control font-style2" readonly>
										</td>
									</tr>
									<tr class="newSummary">
										<td>
											<input type="text" name="old_position" id="old_position" class="form-control font-style2" readonly>
										</td>
										<td>
											<input type="text" name="old_rate_amount" id="old_rate_amount" class="form-control font-style2" readonly>
										</td>
									</tr>
									<tr>
										<td>Salary Adjustment</td>
										<td>
											<input type="text" name="adjustment_amount" id="adjustment_amount" class="form-control font-style2" readonly>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					<div class="col-md-6">
						<div class="panel">
							<table class="table noborder borderless">
								<tbody class="noborder-top">
									<tr>
										<td colspan="2">
											<span>Less</span>
										</td>
									</tr>
									<tr>
										<td>Gsis Contribution</td>
										<td>
											<input type="text" name="gsis_cont_amount" id="gsis_cont_amount" class="form-control font-style2" readonly>
										</td>
									</tr>
									<tr>
										<td>Philhealth Contribution</td>
										<td>
											<input type="text" name="philhealth_cont_amount" id="philhealth_cont_amount" class="form-control font-style2" readonly>
										</td>
									</tr>
									<tr>
										<td>Provident Fud Contribution</td>
										<td>
											<input type="text" name="pf_cont_amount" id="pf_cont_amount" class="form-control font-style2" readonly>
										</td>
									</tr>
									<tr>
										<td>Withholding Tax</td>
										<td>
											<input type="text" name="tax_amount" id="tax_amount" class="form-control font-style2" readonly>
										</td>
									</tr>
									<tr>
										<td>Total Deductions</td>
										<td>
											<input type="text" name="total_deduction_amount" id="total_deduction_amount" class="form-control font-style2" readonly>
										</td>
									</tr>
									<tr>
										<td>Net Amount</td>
										<td>
											<input type="text" name="net_amount" id="net_amount" class="form-control font-style2" readonly>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>

				</form>
				</div>
			</div>
		</div>

	</div>


</div>
<!-- <div class="ajax-loader">
  <img src="{{ asset('images/ajax-loader1.gif') }}" class="img-responsive" />
</div> -->
<br>
@endsection

@section('js-logic1')
<script type="text/javascript">
$(document).ready(function(){
	// GENERATE YEAR
var year = [];
year += '<option ></option>';
for(y = 2018; y <= 2100; y++) {
    year += '<option value='+y+'>'+y+'</option>';
}
$('#select_year').html(year);

// GENERATE MONTH
month = ["January","February","March","April","May","June","July","August","September","October", "November","December"];
mArr = [];

mArr += '<option ></option>';
for ( m =  0; m <= month.length - 1; m++) {
	mArr += '<option '+month[m]+'>'+month[m]+'</option>';
}
$('#select_month').html(mArr);


// ************************************************
var _Year;
var _Month;
$(document).on('change','#select_year',function(){
	_Year = "";
	_Year = $(this).find(':selected').val();
	$('#year').val(_Year);

})
$(document).on('change','#select_month',function(){
	_Month = "";
	_Month = $(this).find(':selected').val();
	$('#month').val(_Month);
})

$('.select2').select2();

$('#select_month').select2({
    allowClear:true,
    placeholder: "Month",
});

$('#select_year').select2({
    allowClear:true,
    placeholder: "Year"
});

	var _monthlyRate;
	var taxAmountBR;
	var _taxDue;
	$('.newNonPlantilla :input').attr('disabled',true);
	$('.newNonPlantilla').attr('disabled',true);
	$('.newSummary :input').attr('disabled',true);
	$('.newSummary').attr('disabled',true);
	$('.newSalary :input').attr('disabled',true);
	$('.newSalary').attr('disabled',true);
	$('.newBenefit :input').attr('disabled',true);
	$('.newBenefit').attr('disabled',true);
	$('.newLoan :input').attr('disabled',true);
	$('.newLoan').attr('disabled',true);
	$('.newDeduct :input').attr('disabled',true);
	$('.newDeduct').attr('disabled',true);

	$('.btn_new').on('click',function(){
		$('#benefitinfo_id').val('');
		$('#deductinfo_id').val('');
		$('#loaninfo_id').val('');

		btnnew = $(this).data('btnnew');
		btnsave = $(this).data('btnsave');
		btncancel = $(this).data('btncancel');
		$('.'+btnnew+' :input').attr("disabled",false);
		$('.'+btnnew).attr('disabled',false);
		$('#select_taxspolicy').attr('disabled',true);
		$('#'+btnnew).addClass('hidden');
		$('#'+btnsave).removeClass('hidden');
		$('#'+btncancel).removeClass('hidden');
		_taxDue = 0;
	});

	$('.btn_edit').on('click',function(){
		btnnew = $(this).data('btnnew');
		btnsave = $(this).data('btnsave');
		btncancel = $(this).data('btncancel');
		btnedit = $(this).data('btnedit');
		btndelete = $(this).data('btndelete');
		$('.'+btnnew+' :input').attr("disabled",false);
		$('.'+btnnew).attr('disabled',false);
		$('#select_taxspolicy').attr('disabled',true);
		$('#'+btnnew).addClass('hidden');
		$('#'+btnedit).addClass('hidden');
		$('#'+btnsave).removeClass('hidden');
		$('#'+btndelete).removeClass('hidden');
		$('#'+btncancel).removeClass('hidden');
	});

	$('.btn_cancel').on('click',function(){
		$('#benefitinfo_id').val('');
		$('#deductinfo_id').val('');
		$('#loaninfo_id').val('');

		btnnew = $(this).data('btnnew');
		btnsave = $(this).data('btnsave');
		btncancel = $(this).data('btncancel');
		btnedit = $(this).data('btnedit');
		btndelete = $(this).data('btndelete');

		$('.'+btnnew+' :input').attr("disabled",true);
		$('.'+btnnew).attr('disabled',true);
		$('#'+btnnew).removeClass('hidden');
		$('#'+btnedit).addClass('hidden');
		$('#'+btnsave).addClass('hidden');
		$('#'+btncancel).addClass('hidden');
		$('#'+btndelete).addClass('hidden');
		$('.weekly').addClass('hidden');
		$('.semi-monthly').addClass('hidden');

		$('#employee_name').text('');
		$('#otherpayroll_id').val('');
		form = $(this).data('form');
		clear_form_elements(form);
		clear_form_elements('nonplantilla');
		$('.error-msg').remove();

	});

	$('.select2').select2();

	$('.onlyNumber').keypress(function (event) {
		return isNumber(event, this)
	});

	$(".onlyNumber").keyup(function(){
		amount  = $(this).val();
		if(amount == 0){
			$(this).val('');
		}else{
			plainAmount = amount.replace(/\,/g,'')
			$(this).val(commaSeparateNumber(plainAmount));
		}
	});

	// DATE PICKER
	$('.datepicker').datepicker({
		dateFormat:'yy-mm-dd'
	});


// =============================================== //
// ===== SALARY ADJUSTMENT COMPUTATION ========== //
// ============================================= //

var workDays;
var dateFrom;
var dateTo;
var countedDays;
var diffAmount;
var newPhContAmount;
var salaryAdjustment;
var gsisContribution;
var totalDeduction;
var netAmount;
var pfAmount;

$(document).on('change','#transaction_date',function(){
	transactDate = $(this).val();

	$.ajax({
		url:base_url+module_prefix+module+'/getDaysInAMonth',
		data:{
			'transaction_date':transactDate,
		},
		type:'get',
		dataType:'JSON',
		success:function(res){
			workDays = res.workdays;
		}
	})
});

$(document).on('change','#date_from',function(){
	dateFrom = $(this).val();
	$.ajax({
		url:base_url+module_prefix+module+'/getCountedDays',
		data:{
			'date_from':dateFrom,
			'date_to':dateTo,
		},
		type:'get',
		dataType:'JSON',
		success:function(res){
			countedDays = res.counted_days;

			salaryAdjustment = compute_salaryAdjustment(countedDays,workDays,diffAmount);
			gsisContribution = compute_gsisCont(countedDays,workDays,diffAmount);
			totalDeduction   = compute_totalDeduction(gsisContribution,newPhContAmount,pfAmount);
			netAmount 		 = compute_totalDeduction(salaryAdjustment,totalDeduction);

			gsis_cont_amount = (gsisContribution !== 0) ? commaSeparateNumber(parseFloat(gsisContribution).toFixed(2)) : '';
			ph_cont_amount   = (newPhContAmount !== 0) ? commaSeparateNumber(parseFloat(newPhContAmount).toFixed(2)) : '';
			deduction_amount   = (totalDeduction !== 0) ? commaSeparateNumber(parseFloat(totalDeduction).toFixed(2)) : '';
			net_amount   = (netAmount !== 0) ? commaSeparateNumber(parseFloat(netAmount).toFixed(2)) : '';

			$('#gsis_cont_amount').val(gsis_cont_amount);
			$('#philhealth_cont_amount').val(ph_cont_amount);
			$('#total_deduction_amount').val(deduction_amount);
			$('#net_amount').val(net_amount);

		}
	})
});

$(document).on('change','#date_to',function(){
	dateTo = $(this).val();

	$.ajax({
		url:base_url+module_prefix+module+'/getCountedDays',
		data:{
			'date_from':dateFrom,
			'date_to':dateTo,
		},
		type:'get',
		dataType:'JSON',
		success:function(res){
			countedDays = res.counted_days;

			salaryAdjustment = compute_salaryAdjustment(countedDays,workDays,diffAmount);
			gsisContribution = compute_gsisCont(countedDays,workDays,diffAmount);
			totalDeduction   = compute_totalDeduction(gsisContribution,newPhContAmount,pfAmount);
			netAmount 		 = compute_totalDeduction(salaryAdjustment,totalDeduction);

			gsis_cont_amount = (gsisContribution !== 0) ? commaSeparateNumber(parseFloat(gsisContribution).toFixed(2)) : '';
			ph_cont_amount   = (newPhContAmount !== 0) ? commaSeparateNumber(parseFloat(newPhContAmount).toFixed(2)) : '';
			deduction_amount   = (totalDeduction !== 0) ? commaSeparateNumber(parseFloat(totalDeduction).toFixed(2)) : '';
			net_amount   = (netAmount !== 0) ? commaSeparateNumber(parseFloat(netAmount).toFixed(2)) : '';

			$('#gsis_cont_amount').val(gsis_cont_amount);
			$('#philhealth_cont_amount').val(ph_cont_amount);
			$('#total_deduction_amount').val(deduction_amount);
			$('#net_amount').val(net_amount);
		}
	})
});


function compute_salaryAdjustment(count_days,work_days,diff_amount){
	days = (parseInt(count_days)/work_days)
	salary_adjustment = (parseFloat(diff_amount)*days);

	return salary_adjustment;
}

function compute_gsisCont(count_days,work_days,diff_amount){
	qou_amount = (parseFloat(diff_amount)/work_days);
	pro_amount = (parseFloat(qou_amount)*count_days);
	gsis_cont  = (parseFloat(pro_amount)*.09);

	return gsis_cont
}

function compute_totalDeduction(gsis_cont,ph_cont,pf_amount){
	gsis_cont = (gsis_cont) ? gsis_cont : 0;
	ph_cont = (ph_cont) ? ph_cont : 0;
	pf_amount = (pf_amount) ? pf_amount : 0;

	deduction_amount = (parseFloat(gsis_cont) + parseFloat(ph_cont) + parseFloat(pf_amount));

	return deduction_amount;
}

function compute_netAmount(adjustment,deductions){
	adjustment = (adjustment) ? adjustment : 0;
	deductions = (deductions) ? deductions : 0;

	net_amount = (parseFloat(adjustment) - parseFloat(deductions));

	return net_amount;
}


$(document).on('click','#namelist tr',function(){

	employee_id = $(this).data('empid');
	$('#employee_id').val(employee_id);
	fullname = $(this).data('fullname');
	$('#employee_name').text(fullname);

	$.ajax({
		url:base_url+module_prefix+module+'/getSalaryAdjustment',
		data:{
			'id':employee_id
		},
		type:'GET',
		dataType:'JSON',
		success:function(data){
			console.log(data);

			if(data !== null){

				oldRateAmount = (data.old_rate_amount) ? data.old_rate_amount : 0;
				newRateAmount = (data.new_rate_amount) ? data.new_rate_amount : 0;
				oldPosition   = data.old_position;
				newPosition   = data.new_position;

				$('#new_position').val(newPosition);
				$('#old_position').val(oldPosition);

				old_rate_amount = (oldRateAmount !== 0) ? commaSeparateNumber(parseFloat(oldRateAmount).toFixed(2)) : 0;
				new_rate_amount = (newRateAmount !== 0) ? commaSeparateNumber(parseFloat(newRateAmount).toFixed(2)) : 0;

				$('#old_rate_amount').val(old_rate_amount);
				$('#new_rate_amount').val(new_rate_amount);

				diffAmount = (parseFloat(newRateAmount) - parseFloat(oldRateAmount));

				adjustment_amount = (diffAmount !== 0) ? commaSeparateNumber(parseFloat(diffAmount).toFixed(2)) : 0;

				$('#adjustment_amount').val(adjustment_amount);

				// PHILHEALTH POLICY

				phPolicyBelow = data.philhealth_policy.below;
				phPolicyAbove = data.philhealth_policy.above;

				phOneAmount = (parseFloat(newRateAmount)*phPolicyBelow);
				phTwoAmount = (parseFloat(oldRateAmount)*phPolicyBelow);

				newPhContAmount = (parseFloat(phOneAmount) + parseFloat(phTwoAmount));

				pf_rate = (data.pf_rate) ? data.pf_rate : 0;
				pfAmount = (parseFloat(diffAmount)*pf_rate);

				pf_amount = (pfAmount !== 0) ? commaSeparateNumber(parseFloat(pfAmount).toFixed(2)) : 0;
				$('#pf_cont_amount').val(pf_amount);


				// GENERATE TR FOR SALARY INFO TAB
				// var tIntial = $('#tbl_initial_salary').DataTable();

				// tIntial.clear().draw();

				// $.each(data,function(k,v){

				// 	tIntial.row.add( [
			 //        	v.process_date,
			 //        	v.gross_salary_amount,
			 //        	v.total_deductions_amount,
			 //        	v.net_amount
			 //        ]).draw( false );

			 //        tIntial.rows(k).nodes().to$().attr("data-id", v.id);
			 //        tIntial.rows(k).nodes().to$().attr("data-employee_id", v.employee_id);
			 //        tIntial.rows(k).nodes().to$().attr("data-basic_salary_amount", v.basic_salary_amount);
			 //        tIntial.rows(k).nodes().to$().attr("data-pera_amount", v.pera_amount);
			 //        tIntial.rows(k).nodes().to$().attr("data-gsis_premium_amount", v.gsis_premium_amount);
			 //        tIntial.rows(k).nodes().to$().attr("data-philhealth_amount", v.philhealth_amount);
			 //        tIntial.rows(k).nodes().to$().attr("data-hdmf_amount", v.hdmf_amount);
			 //        tIntial.rows(k).nodes().to$().attr("data-withholding_amount", v.withholding_amount);
			 //        tIntial.rows(k).nodes().to$().attr("data-process_date", v.process_date);
			 //        tIntial.rows(k).nodes().to$().attr("data-gross_salary_amount", v.gross_salary_amount);
			 //        tIntial.rows(k).nodes().to$().attr("data-total_deductions_amount", v.total_deductions_amount);
			 //        tIntial.rows(k).nodes().to$().attr("data-net_amount", v.net_amount);
			 //        tIntial.rows(k).nodes().to$().attr("data-btnnew", "newSalary");
			 //        tIntial.rows(k).nodes().to$().attr("data-btnsave", "saveSalary");
			 //        tIntial.rows(k).nodes().to$().attr("data-btnedit", "editSalary");
			 //        tIntial.rows(k).nodes().to$().attr("data-btncancel", "cancelSalary");
				// });
			}


		}
	});
});

//SUBMIT FORM
$(document).off('click',".submitme").on('click',".submitme",function(){
	btn = $(this);
	form = $(this).data('form');

		$('#'+form).ajaxForm({
			beforeSend:function(){

			},
			success:function(data){
				par  =  JSON.parse(data);

				if(par.status){

					swal({  title: par.response,
							text: '',
							type: "success",
							icon: 'success',

						}).then(function(){

							// $('.btn_cancel').trigger('click');
							window.location.href = base_url+module_prefix+module;


						});// end swal

				}else{

					swal({  title: par.response,
							text: '',
							type: "error",
							icon: 'error',

						});

				}// end of main IF STATUS

				btn.button('reset');
			},
			error:function(data){
				$error = data.responseJSON;
				/*reset popover*/
				$('input[type="text"], select').popover('destroy');

				/*add popover*/
				block = 0;
				$(".error-msg").remove();
				$.each($error,function(k,v){
					var messages = v.join(', ');
					msg = '<div class="error-msg err-'+k+'" style="color:red;"><i class="fa fa-exclamation-triangle" style="color:rgb(255, 184, 0);"></i> '+messages+'</div>';
					$('input[name="'+k+'"], textarea[name="'+k+'"], select[name="'+k+'"]').after(msg).attr('data-content',messages);
					if(block == 0){
						$('html, body').animate({
					        scrollTop: $('.err-'+k).offset().top - 250
					    }, 500);
					    block++;
					}
				})
				$('.saving').replaceWith(btn);
			},
			always:function(){
				setTimeout(function(){
						$('.saving').replaceWith(btn);
					},300)
			}
		}).submit();

});


var timer;
$(document).on('click','.btnfilter',function(){
	// $('input.search').addClass('searchSpinner');
	tools  = $('#tools-form').serialize()

	category  	= $('#select_searchvalue :selected').val();
	empstatus   = $('#emp_status :selected').val();
	emp_type    = $('#emp_type :selected').val();
	searchby    = $('#searchby :selected').val();

	clearTimeout(timer);
	timer = setTimeout(
				function(){
					$.ajax({
					   type: "GET",
					   url: base_url+module_prefix+module+'/show',
					   data: {'category':category,'empstatus':empstatus,'emp_type':emp_type,'searchby':searchby },
					   beforeSend:function(){
					   		// $('#loading').removeClass('hidden');
					   },
					   complete:function(){
					   		// $('#loading').addClass('hidden');
					   },
					   success: function(res){
					      $(".sub-panelnamelist").html(res);
					      // $('input.search').removeClass('searchSpinner');
					   }
					});
				},500);
})


$(document).on('keyup','.search1',function(){
	clearTimeout(timer);
	timer = setTimeout(
		function(){
			$.ajax({
			   type: "GET",
			   url: base_url+module_prefix+module+'/show',
			   data: {"q":$('.search1').val(),'limit':$(".limit").val()},
			   beforeSend:function(){

			   },
			   success: function(res){
			      $(".sub-panelnamelist").html(res);

			   },
			   complete:function(){

			   }
			});
		},500);
});


})
</script>
@endsection
