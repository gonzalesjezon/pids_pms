@extends('app-front')

@section('content')
<style type="text/css">
.noborder{
    border:none !important;
}
.noborder-top{
    border-top:none !important;
}
.panel{
	padding: 25px;
}
</style>
<div class="row" style="padding: 40px 10px 0px 10px;">
	<div class="col-md-12">
		{!! $controller->showLWOP() !!}
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="col-md-3">
			<input type="text" name="filter_search" class="form-control search1" placeholder="Search here">
			<div style="height: 5px;"></div>
			<div class="sub-panelnamelist ">
				{!! $controller->show() !!}
			</div>
		</div>
		<div class="col-md-9">
			<div class="row" style="margin-left: 2px;">
				<div class="col-md-12">
					<div class="button-wrapper" style="position: relative;top: 10px;left: 5px;" >
						<a class="btn btn-xs btn-info btn-savebg btn_new" id="newLWOP" data-btnnew="newLWOP" data-btncancel="cancelLWOP" data-btnedit="editLWOP" data-btnsave="saveLWOP"><i class="fa fa-save"></i> New</a>

						<a class="btn btn-xs btn-info btn-editbg btn_edit hidden" id="editLWOP" data-btnnew="newLWOP" data-btncancel="cancelLWOP" data-btnedit="editLWOP" data-btnsave="saveLWOP"><i class="fa fa-save"></i> Edit</a>

						<a class="btn btn-xs btn-info btn-savebg btn_save submitme hidden" data-form="form" data-btnnew="newLWOP" data-btncancel="cancelLWOP" data-btnedit="editLWOP" data-btnsave="saveLWOP" id="saveLWOP"><i class="fa fa-save"></i> Save</a>
						<a class="btn btn-xs btn-danger btn_cancel hidden" data-btnnew="newLWOP" data-btncancel="cancelLWOP" data-form="myform" data-btnedit="editLWOP" data-btnsave="saveLWOP"id="cancelLWOP"> Cancel</a>
					</div>
				</div>
			</div>
			<div class="row" style="padding: 20px;">
				<div class="col-md-12">
				<label id="employee_name"></label>
				<form method="POST" action="{{ url($module_prefix.'/'.$module)}}" onsubmit="return false" id="form" class="myform">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<input type="hidden" name="employee_id" id="employee_id">
					<input type="hidden" name="employee_number" id="employee_number">
					<input type="hidden" name="transaction_id" id="transaction_id">
					<input type="hidden" name="year" id="year">
					<input type="hidden" name="month" id="month">
					<div class="panel">
						<table class="table borderless noborder">
							<tbody class="noborder-top">
								<tr>
									<td style="font-weight: bold;width: 130px;">
										<span>Transaction Period</span>
									</td>
									<td>
										<select class="form-control font-style2 select2" id="select_year" name="select_year">
											<option></option>
										</select>
									</td>
									<td>
										<select class="form-control font-style2 select2" id="select_month" name="select_month">
											<option></option>
										</select>
									</td>
								</tr>
								<tr class="text-center" style="font-weight: bold;padding-top: 10px;">
									<td></td>
									<td>Ajustments</td>
									<td>Monthly Salary</td>
									<td>Basic Pay</td>
									<td>Pera</td>
									<td>Total</td>
								</tr>
								<tr class="newLWOP">
									<td>Actual Workdays</td>
									<td>
										<input type="text" name="workdays_adjustment" id="workdays_adjustment" class="form-control font-style2" readonly>
									</td>
									<td>
										<input type="text" name="workdays_montly_salary" id="workdays_montly_salary" class="form-control salary_0 font-style2" readonly>
									</td>
									<td>
										<input type="text" name="workdays_basic_pay" id="workdays_basic_pay" class="form-control basic_0 font-style2" readonly>
									</td>
									<td>
										<input type="text" name="workdays_pera" id="workdays_pera" class="form-control pera_0 font-style2" readonly>
									</td>
									<td>
										<input type="text" name="workdays_total" id="workdays_total" class="form-control total_0 font-style2" readonly>
									</td>
								</tr>
								<tr class="newLWOP">
									<td>LWOP</td>
									<td>
										<input type="text" name="lwop_adjustment" id="lwop_adjustment" class="form-control font-style2" maxlength="2">
									</td>
									<td>
										<input type="text" name="lwop_montly_salary" id="lwop_montly_salary" class="form-control salary_1 font-style2" readonly>
									</td>
									<td>
										<input type="text" name="lwop_basic_pay" id="lwop_basic_pay" class="form-control basic_1 font-style2" readonly>
									</td>
									<td>
										<input type="text" name="lwop_pera" id="lwop_pera" class="form-control pera_1 font-style2" readonly>
									</td>
									<td>
										<input type="text" name="lwop_total" id="lwop_total" class="form-control total_1 font-style2" readonly>
									</td>
								</tr>
								<tr class="newLWOP">
									<td>Undertime</td>
									<td>
										<input type="text" name="undertime_adjustment" id="undertime_adjustment" class="form-control font-style2" maxlength="3">
									</td>
									<td>
										<input type="text" name="undertime_montly_salary" id="undertime_montly_salary" class="form-control salary_2 font-style2" readonly>
									</td>
									<td>
										<input type="text" name="undertime_basic_pay" id="undertime_basic_pay" class="form-control basic_2 font-style2" readonly>
									</td>
									<td>
										<input type="text" name="undertime_pera" id="undertime_pera" class="form-control pera_2 font-style2" readonly>
									</td>
									<td>
										<input type="text" name="undertime_total" id="undertime_total" class="form-control total_2 font-style2" readonly>
									</td>
								</tr>
								<tr class="newLWOP">
									<td>Tardiness</td>
									<td>
										<input type="text" name="tardiness_adjustment" id="tardiness_adjustment" class="form-control font-style2" maxlength="3">
									</td>
									<td>
										<input type="text" name="tardiness_montly_salary" id="tardiness_montly_salary" class="form-control salary_3 font-style2" readonly>
									</td>
									<td>
										<input type="text" name="tardiness_basic_pay" id="tardiness_basic_pay" class="form-control basic_3 font-style2" readonly>
									</td>
									<td>
										<input type="text" name="tardiness_pera" id="tardiness_pera" class="form-control pera_3 font-style2" readonly>
									</td>
									<td>
										<input type="text" name="tardiness_total" id="tardiness_total" class="form-control total_3 font-style2" readonly>
									</td>
								</tr>
								<tr>
									<td></td>
									<td></td>
									<td>
										Total
									</td>
									<td>
										<input type="text" name="net_basic_pay" id="net_basic_pay" class="form-control font-style2" readonly>
									</td>
									<td>
										<input type="text" name="net_pera" id="net_pera" class="form-control font-style2" readonly>
									</td>
									<td>
										<input type="text" name="net_total" id="net_total" class="form-control font-style2" readonly>
									</td>
								</tr>
							</tbody>
						</table>
					</div>

					<div class="panel">
					<label>Deduction Period</label>
					<table class="table borderless noborder">
						<tbody class="noborder-top">
							<tr>
								<td>First Week</td>
								<td>
									<input type="text" name="first_amount" id="first_amount" class="form-control font-style2 onlyNumber" placeholder="0.00">
								</td>
							</tr>
							<tr>
								<td>Second Week</td>
								<td>
									<input type="text" name="second_amount" id="second_amount" class="form-control font-style2 onlyNumber" placeholder="0.00">
								</td>
							</tr>
							<tr>
								<td>Third Week</td>
								<td>
									<input type="text" name="third_amount" id="third_amount" class="form-control font-style2 onlyNumber" placeholder="0.00">
								</td>
							</tr>
							<tr>
								<td>Fourth Week</td>
								<td>
									<input type="text" name="fourth_amount" id="fourth_amount" class="form-control font-style2 onlyNumber" placeholder="0.00">
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				</form>
				</div>
			</div>
		</div>

	</div>


</div>
<!-- <div class="ajax-loader">
  <img src="{{ asset('images/ajax-loader1.gif') }}" class="img-responsive" />
</div> -->
<br>
@endsection

@section('js-logic1')
<script type="text/javascript">
$(document).ready(function(){
	// GENERATE YEAR
var year = [];
year += '<option ></option>';
for(y = 2018; y <= 2100; y++) {
    year += '<option value='+y+'>'+y+'</option>';
}
$('#select_year').html(year);

// GENERATE MONTH
month = ["January","February","March","April","May","June","July","August","September","October", "November","December"];
mArr = [];

mArr += '<option ></option>';
for ( m =  0; m <= month.length - 1; m++) {
	mArr += '<option '+month[m]+'>'+month[m]+'</option>';
}
$('#select_month').html(mArr);


// ************************************************
var _Year;
var _Month;
$(document).on('change','#select_year',function(){
	_Year = "";
	_Year = $(this).find(':selected').val();
	$('#year').val(_Year);

})
$(document).on('change','#select_month',function(){
	_Month = "";
	_Month = $(this).find(':selected').val();
	$('#month').val(_Month);
})

$('.select2').select2();

$('#select_month').select2({
    allowClear:true,
    placeholder: "Month",
});

$('#select_year').select2({
    allowClear:true,
    placeholder: "Year"
});

	var _monthlyRate;
	var taxAmountBR;
	var _taxDue;
	$('.newLWOP :input').attr('disabled',true);
	$('.newLWOP').attr('disabled',true);

	$('.btn_new').on('click',function(){
		$('#benefitinfo_id').val('');
		$('#deductinfo_id').val('');
		$('#loaninfo_id').val('');

		btnnew = $(this).data('btnnew');
		btnsave = $(this).data('btnsave');
		btncancel = $(this).data('btncancel');
		$('.'+btnnew+' :input').attr("disabled",false);
		$('.'+btnnew).attr('disabled',false);
		$('#select_taxspolicy').attr('disabled',true);
		$('#'+btnnew).addClass('hidden');
		$('#'+btnsave).removeClass('hidden');
		$('#'+btncancel).removeClass('hidden');
		_taxDue = 0;
	});

	$('.btn_edit').on('click',function(){
		btnnew = $(this).data('btnnew');
		btnsave = $(this).data('btnsave');
		btncancel = $(this).data('btncancel');
		btnedit = $(this).data('btnedit');
		btndelete = $(this).data('btndelete');
		$('.'+btnnew+' :input').attr("disabled",false);
		$('.'+btnnew).attr('disabled',false);
		$('#select_taxspolicy').attr('disabled',true);
		$('#'+btnnew).addClass('hidden');
		$('#'+btnedit).addClass('hidden');
		$('#'+btnsave).removeClass('hidden');
		$('#'+btndelete).removeClass('hidden');
		$('#'+btncancel).removeClass('hidden');
	});

	$('.btn_cancel').on('click',function(){
		$('#benefitinfo_id').val('');
		$('#deductinfo_id').val('');
		$('#loaninfo_id').val('');

		btnnew = $(this).data('btnnew');
		btnsave = $(this).data('btnsave');
		btncancel = $(this).data('btncancel');
		btnedit = $(this).data('btnedit');
		btndelete = $(this).data('btndelete');

		$('.'+btnnew+' :input').attr("disabled",true);
		$('.'+btnnew).attr('disabled',true);
		$('#'+btnnew).removeClass('hidden');
		$('#'+btnedit).addClass('hidden');
		$('#'+btnsave).addClass('hidden');
		$('#'+btncancel).addClass('hidden');
		$('#'+btndelete).addClass('hidden');
		$('.weekly').addClass('hidden');
		$('.semi-monthly').addClass('hidden');

		$('#employee_name').text('');
		$('#otherpayroll_id').val('');
		clear_form_elements('myform');
		$('.error-msg').remove();

	});

	$('.select2').select2();

	$('.onlyNumber').keypress(function (event) {
		return isNumber(event, this)
	});

	$(".onlyNumber").keyup(function(){
		amount  = $(this).val();
		if(amount == 0){
			$(this).val('');
		}else{
			plainAmount = amount.replace(/\,/g,'')
			$(this).val(commaSeparateNumber(plainAmount));
		}
	});

	// DATE PICKER
	$('.datepicker').datepicker({
		dateFormat:'yy-mm-dd'
	});

var basicAmount;
var peraAmount;
var workDays;
var dailyBasicAmount;
var dailyPeraAmount;
var hourBasicAmount;
var hourPeraAmount;

$(document).on('click','#namelist tr',function(){

	employee_id = $(this).data('empid');
	employee_number = $(this).data('employee_number');
	$('#employee_id').val(employee_id);
	$('#employee_number').val(employee_number);
	fullname = $(this).data('fullname');
	$('#employee_name').text(fullname);


	_Year = (_Year) ? _Year : '';
	_Month = (_Month) ? _Month : '';

	$.ajax({
		url:base_url+module_prefix+module+'/getLWOP',
		data:{
			'id':employee_id,
			'year':_Year,
			'month':_Month
		},
		type:'GET',
		dataType:'JSON',
		success:function(data){
			console.log(data);

			clear_form_elements('myform');

			if(data.transaction !== null){

				basicAmount = data.transaction.total_basicpay_amount;
				peraAmount = data.transaction.benefit_transaction.amount;
				workDays = data.transaction.actual_workdays;

				dailyBasicAmount = parseFloat(basicAmount) / workDays;
				dailyPeraAmount = parseFloat(peraAmount) / workDays;
				hourBasicAmount = parseFloat(dailyBasicAmount) / 8 / 60;
				hourPeraAmount = parseFloat(dailyPeraAmount) / 8 / 60;

				totalAmount = parseFloat(basicAmount) + parseFloat(peraAmount);

				basic_amount = (basicAmount) ? commaSeparateNumber(parseFloat(basicAmount).toFixed(2)) : '';
				pera_amount = (peraAmount) ? commaSeparateNumber(parseFloat(peraAmount).toFixed(2)) : '';
				total_amount = (totalAmount) ? commaSeparateNumber(parseFloat(totalAmount).toFixed(2)) : '';

				$('#workdays_adjustment').val(workDays);
				$('.basic_0').val(basic_amount);
				$('.pera_0').val(pera_amount);
				$('.total_0').val(total_amount);
				for (var i = 0; i <= 4; i++) {
					$('.salary_'+i).val(basic_amount);
				}

				// GENERATE TR FOR SALARY INFO TAB
				var tIntial = $('#tbl_initial_salary').DataTable();

				tIntial.clear().draw();

				if(data.lwoptransaction.length !== 0){
					$.each(data.lwoptransaction,function(k,v){

						adjustmentAmount = (v.lwop_adjustment_amount) ? v.lwop_adjustment_amount : 0;
						basicAmount 	 = (v.basic_adjustment_amount) ? v.basic_adjustment_amount : 0;
						peraAmount 		 = (v.pera_adjustment_amount) ? v.pera_adjustment_amount : 0;

						adjustment_amount = (adjustmentAmount !== 0) ? commaSeparateNumber(parseFloat(adjustmentAmount).toFixed(2)) : '';
						basic_amount = (basicAmount !== 0) ? commaSeparateNumber(parseFloat(basicAmount).toFixed(2)) : '';
						pera_amount = (peraAmount !== 0) ? commaSeparateNumber(parseFloat(peraAmount).toFixed(2)) : '';

						tIntial.row.add( [
				        	adjustment_amount,
				        	basic_amount,
				        	pera_amount,
				        	''
				        ]).draw( false );

				        tIntial.rows(k).nodes().to$().attr("data-id", v.id);
				        tIntial.rows(k).nodes().to$().attr("data-employee_id", v.employee_id);
				        tIntial.rows(k).nodes().to$().attr("data-employee_number", v.employee_number);
				        tIntial.rows(k).nodes().to$().attr("data-number_of_lwop", v.number_of_lwop);
				        tIntial.rows(k).nodes().to$().attr("data-number_of_tardiness", v.number_of_tardiness);
				        tIntial.rows(k).nodes().to$().attr("data-number_of_undertime", v.number_of_undertime);
				        tIntial.rows(k).nodes().to$().attr("data-btnsave", "saveSalary");
				        tIntial.rows(k).nodes().to$().attr("data-btnedit", "editSalary");
				        tIntial.rows(k).nodes().to$().attr("data-btncancel", "cancelSalary");
					});
				}
			}
		}
	});

});


var basicOne;
var peraOne;
var totalOne;
$(document).on('keyup','#lwop_adjustment',function(){
	days = $(this).val();

	basicOne = parseFloat(dailyBasicAmount) * days;
	peraOne  = parseFloat(dailyPeraAmount) * days;

	totalOne = parseFloat(basicOne) + parseFloat(peraOne);

	basic_one = (basicOne) ? commaSeparateNumber(parseFloat(basicOne).toFixed(2)) : '';
	pera_one = (peraOne) ? commaSeparateNumber(parseFloat(peraOne).toFixed(2)) : '';
	total_one = (totalOne) ? commaSeparateNumber(parseFloat(totalOne).toFixed(2)) : '';

	$('.basic_1').val(basic_one);
	$('.pera_1').val(pera_one);
	$('.total_1').val(total_one);

	netBasic = compute_net_basic(basicOne,basicTwo,basicThree);
	netPera = compute_net_pera(peraOne,peraTwo,peraThree);
	netPay = compute_net_total(totalOne,totalTwo,totalThree);

	net_basic = (netBasic) ? commaSeparateNumber(parseFloat(netBasic).toFixed(2)) : '';
	net_pera = (netPera) ? commaSeparateNumber(parseFloat(netPera).toFixed(2)) : '';
	net_pay = (netPay) ? commaSeparateNumber(parseFloat(netPay).toFixed(2)) : '';

	$('#net_basic_pay').val(net_basic);
	$('#net_pera').val(net_pera)
	$('#net_total').val(net_pay)

});


var basicTwo;
var peraTwo;
var totalTwo;
$(document).on('keyup','#undertime_adjustment',function(){
	hour = $(this).val();

	basicTwo = parseFloat(hourBasicAmount) * hour;
	peraTwo  = parseFloat(hourPeraAmount) * hour;

	totalTwo = parseFloat(basicTwo) + parseFloat(peraTwo);

	basic_two = (basicTwo) ? commaSeparateNumber(parseFloat(basicTwo).toFixed(2)) : '';
	pera_two = (peraTwo) ? commaSeparateNumber(parseFloat(peraTwo).toFixed(2)) : '';
	total_two = (totalTwo) ? commaSeparateNumber(parseFloat(totalTwo).toFixed(2)) : '';

	$('.basic_2').val(basic_two);
	$('.pera_2').val(pera_two);
	$('.total_2').val(total_two);

	netBasic = compute_net_basic(basicOne,basicTwo,basicThree);
	netPera = compute_net_pera(peraOne,peraTwo,peraThree);
	netPay = compute_net_total(totalOne,totalTwo,totalThree);

	net_basic = (netBasic) ? commaSeparateNumber(parseFloat(netBasic).toFixed(2)) : '';
	net_pera = (netPera) ? commaSeparateNumber(parseFloat(netPera).toFixed(2)) : '';
	net_pay = (netPay) ? commaSeparateNumber(parseFloat(netPay).toFixed(2)) : '';

	$('#net_basic_pay').val(net_basic);
	$('#net_pera').val(net_pera)
	$('#net_total').val(net_pay)

});

var basicThree;
var peraThree;
var totalThree;
$(document).on('keyup','#tardiness_adjustment',function(){
	hour = $(this).val();

	basicThree = parseFloat(hourBasicAmount) * hour;
	peraThree  = parseFloat(hourPeraAmount) * hour;

	totalThree = parseFloat(basicThree) + parseFloat(peraThree);

	basic_three = (basicThree) ? commaSeparateNumber(parseFloat(basicThree).toFixed(2)) : '';
	pera_three = (peraThree) ? commaSeparateNumber(parseFloat(peraThree).toFixed(2)) : '';
	total_three = (totalThree) ? commaSeparateNumber(parseFloat(totalThree).toFixed(2)) : '';

	$('.basic_3').val(basic_three);
	$('.pera_3').val(pera_three);
	$('.total_3').val(total_three);

	netBasic = compute_net_basic(basicOne,basicTwo,basicThree);
	netPera = compute_net_pera(peraOne,peraTwo,peraThree);
	netPay = compute_net_total(totalOne,totalTwo,totalThree);

	net_basic = (netBasic) ? commaSeparateNumber(parseFloat(netBasic).toFixed(2)) : '';
	net_pera = (netPera) ? commaSeparateNumber(parseFloat(netPera).toFixed(2)) : '';
	net_pay = (netPay) ? commaSeparateNumber(parseFloat(netPay).toFixed(2)) : '';

	$('#net_basic_pay').val(net_basic);
	$('#net_pera').val(net_pera)
	$('#net_total').val(net_pay)

});

function compute_net_basic(basic1,basic2,basic3){
	basic1 = (basic1) ? basic1 : 0;
	basic2 = (basic2) ? basic2 : 0;
	basic3 = (basic3) ? basic3 : 0;

	total = parseFloat(basic1) + parseFloat(basic2) + parseFloat(basic3)

	return total;
}

function compute_net_pera(pera1,pera2,pera3){
	pera1 = (pera1) ? pera1 : 0;
	pera2 = (pera2) ? pera2 : 0;
	pera3 = (pera3) ? pera3 : 0;

	total = parseFloat(pera1) + parseFloat(pera2) + parseFloat(pera3)

	return total;
}

function compute_net_total(total1,total2,total3){
	total1 = (total1) ? total1 : 0;
	total2 = (total2) ? total2 : 0;
	total3 = (total3) ? total3 : 0;

	total = parseFloat(total1) + parseFloat(total2) + parseFloat(total3)

	return total;
}



//SUBMIT FORM
$(document).off('click',".submitme").on('click',".submitme",function(){
	btn = $(this);
	form = $(this).data('form');

		$('#'+form).ajaxForm({
			beforeSend:function(){

			},
			success:function(data){
				par  =  JSON.parse(data);

				if(par.status){

					swal({  title: par.response,
							text: '',
							type: "success",
							icon: 'success',

						}).then(function(){

							// $('.btn_cancel').trigger('click');
							window.location.href = base_url+module_prefix+module;


						});// end swal

				}else{

					swal({  title: par.response,
							text: '',
							type: "error",
							icon: 'error',

						});

				}// end of main IF STATUS

				btn.button('reset');
			},
			error:function(data){
				$error = data.responseJSON;
				/*reset popover*/
				$('input[type="text"], select').popover('destroy');

				/*add popover*/
				block = 0;
				$(".error-msg").remove();
				$.each($error.errors,function(k,v){
					var messages = v.join(', ');
					msg = '<div class="error-msg err-'+k+'" style="color:red;"><i class="fa fa-exclamation-triangle" style="color:rgb(255, 184, 0);"></i> '+messages+'</div>';
					$('input[name="'+k+'"], textarea[name="'+k+'"], select[name="'+k+'"]').after(msg).attr('data-content',messages);
					if(block == 0){
						$('html, body').animate({
					        scrollTop: $('.err-'+k).offset().top - 250
					    }, 500);
					    block++;
					}
				})
				$('.saving').replaceWith(btn);
			},
			always:function(){
				setTimeout(function(){
						$('.saving').replaceWith(btn);
					},300)
			}
		}).submit();

});


var timer;
$(document).on('click','.btnfilter',function(){
	// $('input.search').addClass('searchSpinner');
	tools  = $('#tools-form').serialize()

	category  	= $('#select_searchvalue :selected').val();
	empstatus   = $('#emp_status :selected').val();
	emp_type    = $('#emp_type :selected').val();
	searchby    = $('#searchby :selected').val();

	clearTimeout(timer);
	timer = setTimeout(
				function(){
					$.ajax({
					   type: "GET",
					   url: base_url+module_prefix+module+'/show',
					   data: {'category':category,'empstatus':empstatus,'emp_type':emp_type,'searchby':searchby },
					   beforeSend:function(){
					   		// $('#loading').removeClass('hidden');
					   },
					   complete:function(){
					   		// $('#loading').addClass('hidden');
					   },
					   success: function(res){
					      $(".sub-panelnamelist").html(res);
					      // $('input.search').removeClass('searchSpinner');
					   }
					});
				},500);
})


$(document).on('keyup','.search1',function(){
	clearTimeout(timer);
	timer = setTimeout(
		function(){
			$.ajax({
			   type: "GET",
			   url: base_url+module_prefix+module+'/show',
			   data: {"q":$('.search1').val(),'limit':$(".limit").val()},
			   beforeSend:function(){

			   },
			   success: function(res){
			      $(".sub-panelnamelist").html(res);

			   },
			   complete:function(){

			   }
			});
		},500);
});


function generateBenefitInfoTable(benefitinfo){
	var tBenefit = $('#tbl_benefitinfo').DataTable();

	tBenefit.clear().draw();

	$.each(benefitinfo,function(k,v){

		tBenefit.row.add( [
        	benefitinfo[k].benefit_effectivity_date,
        	benefitinfo[k].benefits.name,
			benefitinfo[k].benefit_description,
			benefitinfo[k].benefit_amount,
			benefitinfo[k].benefit_pay_period,
        ]).draw( false );

        tBenefit.rows(k).nodes().to$().attr("data-id", v.id);
        tBenefit.rows(k).nodes().to$().attr("data-employeeid", v.employee_id);
        tBenefit.rows(k).nodes().to$().attr("data-date", v.benefit_effectivity_date);
        tBenefit.rows(k).nodes().to$().attr("data-name", v.benefits.name);
        tBenefit.rows(k).nodes().to$().attr("data-description", v.benefit_description);
        tBenefit.rows(k).nodes().to$().attr("data-payperiod", v.benefit_pay_period);
        tBenefit.rows(k).nodes().to$().attr("data-paysub", v.benefit_pay_sub);
        tBenefit.rows(k).nodes().to$().attr("data-amount", v.benefit_amount);
        tBenefit.rows(k).nodes().to$().attr("data-benefitid", v.benefit_id);
        tBenefit.rows(k).nodes().to$().attr("data-datefrom", v.date_from);
        tBenefit.rows(k).nodes().to$().attr("data-dateto", v.date_to);
        tBenefit.rows(k).nodes().to$().attr("data-btnnew", "newBenefit");
        tBenefit.rows(k).nodes().to$().attr("data-btnsave", "saveBenefit");
        tBenefit.rows(k).nodes().to$().attr("data-btnedit", "editBenefit");
        tBenefit.rows(k).nodes().to$().attr("data-btndelete", "deleteBenefit");
        tBenefit.rows(k).nodes().to$().attr("data-btncancel", "cancelBenefit");


	});

	$('.newBenefit').attr('disabled',true);
	$('#newBenefit').removeClass('hidden');
	$('#saveBenefit').addClass('hidden');
	$('#editBenefit').addClass('hidden');
	$('#deleteBenefit').addClass('hidden');
	$('#cancelBenefit').addClass('hidden');
	$('.weekly').removeClass('show');
	$('.weekly').addClass('hidden');
	$('.semi-monthly').removeClass('show');
	$('.semi-monthly').addClass('hidden');
	clear_form_elements("myform3");
}

// # loan Info Table
function generateLoanInfoTable(loaninfo){
	var tLoan = $('#tbl_loaninfo').DataTable();

	tLoan.clear().draw();

	$.each(loaninfo,function(k,v){

		tLoan.row.add( [
        	loaninfo[k].loans.name,
        	loaninfo[k].loan_totalamount,
			loaninfo[k].loan_totalbalance,
			loaninfo[k].loan_amortization,
			loaninfo[k].loan_date_started,
			loaninfo[k].loan_date_end,
        ]).draw( false );

        tLoan.rows(k).nodes().to$().attr("data-id", v.id);
        tLoan.rows(k).nodes().to$().attr("data-employeeid", v.employee_id);
        tLoan.rows(k).nodes().to$().attr("data-loanamount", v.loan_totalamount);
        tLoan.rows(k).nodes().to$().attr("data-loanbalance", v.loan_totalbalance);
        tLoan.rows(k).nodes().to$().attr("data-amortization", v.loan_amortization);
        tLoan.rows(k).nodes().to$().attr("data-datestart", v.loan_date_started);
        tLoan.rows(k).nodes().to$().attr("data-dateend", v.loan_date_end);
        tLoan.rows(k).nodes().to$().attr("data-loanid", v.loan_id);
        tLoan.rows(k).nodes().to$().attr("data-payperiod", v.loan_pay_period);
        tLoan.rows(k).nodes().to$().attr("data-dategranted", v.loan_date_granted);
        tLoan.rows(k).nodes().to$().attr("data-dateterminated", v.loan_date_terminated);
        tLoan.rows(k).nodes().to$().attr("data-btnnew", "newLoan");
        tLoan.rows(k).nodes().to$().attr("data-btnsave", "saveLoan");
        tLoan.rows(k).nodes().to$().attr("data-btnedit", "editLoan");
        tLoan.rows(k).nodes().to$().attr("data-btncancel", "cancelLoan");
        tLoan.rows(k).nodes().to$().attr("data-btndelete", "deleteLoan");
	});

	$('.newLoan').attr('disabled',true);
	$('#newLoan').removeClass('hidden');
	$('#editLoan').addClass('hidden');
	$('#deleteLoan').addClass('hidden');
	$('#saveLoan').addClass('hidden');
	$('#cancelLoan').addClass('hidden');
	clear_form_elements("myform4");
}

function generateDeductionInfoTable(deductioninfo){
	var tDeduct = $('#tbl_deductioninfo').DataTable();

	tDeduct.clear().draw();

	$.each(deductioninfo,function(k,v){

		tDeduct.row.add( [
        	deductioninfo[k].deductions.name,
        	deductioninfo[k].deduct_amount,
			deductioninfo[k].deduct_date_start,
			deductioninfo[k].deduct_date_end,
			deductioninfo[k].deduct_pay_period,
        ]).draw( false );

        tDeduct.rows(k).nodes().to$().attr("data-id", v.id);
        tDeduct.rows(k).nodes().to$().attr("data-employeeid", v.employee_id);
        tDeduct.rows(k).nodes().to$().attr("data-deductionid", v.deduction_id);
        tDeduct.rows(k).nodes().to$().attr("data-amount", v.deduct_amount);
        tDeduct.rows(k).nodes().to$().attr("data-datestart", v.deduct_date_start);
        tDeduct.rows(k).nodes().to$().attr("data-dateend", v.deduct_date_end);
        tDeduct.rows(k).nodes().to$().attr("data-payperiod", v.deduct_pay_period);
        tDeduct.rows(k).nodes().to$().attr("data-dateterminated", v.deduct_date_terminated);
        tDeduct.rows(k).nodes().to$().attr("data-btnnew", "newDeduct");
        tDeduct.rows(k).nodes().to$().attr("data-btnsave", "saveDeduct");
        tDeduct.rows(k).nodes().to$().attr("data-btnedit", "editDeduct");
        tDeduct.rows(k).nodes().to$().attr("data-btncancel", "cancelDeduct");
        tDeduct.rows(k).nodes().to$().attr("data-btndelete", "deleteDeduct");
	});

	$('.newDeduct').attr('disabled',true);
	$('#newDeduct').removeClass('hidden');
	$('#saveDeduct').addClass('hidden');
	$('#editDeduct').addClass('hidden');
	$('#deleteDeduct').addClass('hidden');
	$('#cancelDeduct').addClass('hidden');
	clear_form_elements("myform5");
}



})
</script>
@endsection
