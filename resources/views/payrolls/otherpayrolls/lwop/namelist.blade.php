<table class="table scroll datatable" id="namelist">
	<thead>
		<tr>
			<th>ID</th>
			<th>Name</th>
		</tr>
	</thead>
	<tbody >

		@foreach($data as $key => $value)

			<tr data-empid="{{ $value->id }}" data-employee_number="{{ $value->employee_number }}" data-fullname="{{ $value->lastname }} {{ $value->firstname }} {{ $value->middlename }}" data-btnnew="newSalary" data-btnedit="editSalary" data-btnsave="saveSalary" data-btncancel="cancelSalary">
				<td>{{ ucwords(strtolower($value->lastname)) }} {{ ucwords(strtolower($value->firstname)) }} {{ ucwords(strtolower($value->middlename)) }}</td>
			</tr>

		@endforeach

	</tbody>
</table>
<script type="text/javascript">
	$(document).ready(function(){
});
</script>