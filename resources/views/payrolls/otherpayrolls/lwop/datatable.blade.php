
<table class="table table-responsive datatable" id="tbl_initial_salary">
	<thead>
		<tr>
			<th>Adjustment</th>
			<th>Basic</th>
			<th>Pera</th>
			<th>Period</th>
		</tr>
	</thead>
	<tbody class="text-right">
	</tbody>
</table>
<script type="text/javascript">
$(document).ready(function(){

	 var table = $('#tbl_initial_salary').DataTable({
	 	'dom':'<lf<t>pi>',
	 	"paging": false,
	 	"scrollY":"250px",
        "scrollCollapse": true,
	 });

	$('#tbl_initial_salary tbody').on( 'click', 'tr', function () {
	    if ( $(this).hasClass('selected') ) {
	        $(this).removeClass('selected');
	        clear_form_elements('myForm4');

	        id 						= $(this).data('id');
			employee_id 			= $(this).data('employee_id');
			employee_number 		= $(this).data('employee_number');
			number_of_lwop 			= $(this).data('number_of_lwop');
			number_of_tardiness 	= $(this).data('number_of_tardiness');
			number_of_undertime 	= $(this).data('number_of_undertime');

			$('#transaction_id').val(id);
			$('#employee_id').val(employee_id);
			$('#employee_number').val(employee_number);

			$('#lwop_adjustment').val(number_of_lwop).trigger('keyup');
			$('#undertime_adjustment').val(number_of_undertime).trigger('keyup');
			$('#tardiness_adjustment').val(number_of_tardiness).trigger('keyup');

			btnnew = $(this).data('btnnew');
			btnsave = $(this).data('btnsave');
			btnedit = $(this).data('btnedit');
			btncancel = $(this).data('btncancel');
			btndelete = $(this).data('btndelete');

			if(!$('#'+btnsave).is(':visible')){
				$('#'+btnedit).removeClass('hidden');
				$('#'+btndelete).removeClass('hidden');
				$('#'+btncancel).removeClass('hidden');
				$('#'+btnnew).addClass('hidden');
			}
	    }
	    else {
	        table.$('tr.selected').removeClass('selected');
	        $(this).addClass('selected');
	    }

	});

	$('#button').click( function () {
	    table.row('.selected').remove().draw( false );
	} );

})
</script>
