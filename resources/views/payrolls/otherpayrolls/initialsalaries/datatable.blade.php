
<table class="table table-responsive datatable" id="tbl_initial_salary">
	<thead>
		<tr>
			<th>Processed Date</th>
			<th>Gross Salary</th>
			<th>Total Deductions</th>
			<th>Net Amount</th>
		</tr>
	</thead>
	<tbody class="text-right">

	</tbody>
</table>
<script type="text/javascript">
$(document).ready(function(){

	 var table = $('#tbl_initial_salary').DataTable({
	 	'dom':'<lf<t>pi>',
	 	"paging": false,
	 	// "scrollY":"250px",
   //      "scrollCollapse": true,
	 });

	$('#tbl_initial_salary tbody').on( 'click', 'tr', function () {
	    if ( $(this).hasClass('selected') ) {
	        $(this).removeClass('selected');
	        clear_form_elements('myForm4');

	        id 						= $(this).data('id');				
			employee_id 			= $(this).data('employee_id');
			basic_salary_amount 	= $(this).data('basic_salary_amount');
			pera_amount 			= $(this).data('pera_amount');
			gsis_premium_amount 	= $(this).data('gsis_premium_amount');
			philhealth_amount 		= $(this).data('philhealth_amount');
			hdmf_amount 			= $(this). data('hdmf_amount');
			withholding_amount 		= $(this).data('withholding_amount');
			process_date 			= $(this).data('process_date');
			gross_salary_amount 	= $(this).data('gross_salary_amount');
			total_deductions_amount = $(this).data('total_deductions_amount');
			net_amount 				= $(this).data('net_amount');

			$('#otherpayroll_id').val(id);
			$('#employee_id').val(employee_id);
			$('#basic_salary_amount').val(basic_salary_amount);
			$('#pera_amount').val(pera_amount);
			$('#gsis_premium_amount').val(gsis_premium_amount);
			$('#philhealth_amount').val(philhealth_amount);
			$('#hdmf_amount').val(hdmf_amount);
			$('#witholding_tax_amount').val(withholding_amount);
			$('#process_date').val(process_date);
			$('#gross_salary_amount').val(gross_salary_amount);
			$('#total_deductions_amount').val(total_deductions_amount);
			$('#net_amount').val(net_amount);

			btnnew = $(this).data('btnnew');
			btnsave = $(this).data('btnsave');
			btnedit = $(this).data('btnedit');
			btncancel = $(this).data('btncancel');
			btndelete = $(this).data('btndelete');

			if(!$('#'+btnsave).is(':visible')){
				$('#'+btnedit).removeClass('hidden');
				$('#'+btndelete).removeClass('hidden');
				$('#'+btncancel).removeClass('hidden');
				$('#'+btnnew).addClass('hidden');
			}
	    }
	    else {
	        table.$('tr.selected').removeClass('selected');
	        $(this).addClass('selected');
	    }

	});

	$('#button').click( function () {
	    table.row('.selected').remove().draw( false );
	} );

})
</script>
