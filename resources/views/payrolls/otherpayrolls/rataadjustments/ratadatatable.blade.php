<div class="col-md-12">
	<table class="table table-responsive datatable" id="tbl_rata">
		<thead>
			<tr >
				<th rowspan="2">REPRESENTATION</th>
				<th rowspan="2">TRANSPORTATION</th>
				<th rowspan="2">ACTUAL PERFORMANCE <br> FOR THE MONTH</th>
				<th rowspan="2">ADJUSTMENT RATA <br> FOR THE MONTH</th>
				<th colspan="3">NET</th>
			</tr>
			<tr>
				<th>RA</th>
				<th>TA</th>
				<th>TOTAL</th>
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table>
</div>
<script type="text/javascript">
$(document).ready(function(){

	var table = $('#tbl_rata').DataTable({
	 	'dom':'<lf<t>pi>',
	 	"paging": false,
	 	'responsive':true
	});

})
</script>
