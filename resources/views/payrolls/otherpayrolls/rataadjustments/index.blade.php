@extends('app-front')

@section('content')
<div class="row">
	<div class="col-md-12">
		<hr>
		<div class="col-md-3">
			<div>
				<h5 ><b>Filter Employee By</b></h5>
				<table class="table borderless" style="border:none;font-weight: bold">
					<tr class="text-left">
						<td colspan="2"><span>Transaction Period</span></td>
					</tr>
					<tr>
						<td>
							<div class="col-md-6" style="padding-left: 0px;">
								<select class="employee-type form-control font-style2 select2" id="select_month" name="select_month" placeholder="Month">
									<option value=""></option>
								</select>
							</div>
							<div class="col-md-6" style="padding-right: 0px;">
								<select class="employee-type form-control font-style2 select2" id="select_year" name="select_year" placeholder="Year" >
									<option value=""></option>
								</select>
							</div>
						</td>

					</tr>
				</table>
				<div >
					<a class="btn btn-xs btn-info btnfilter hidden"style="background-color: #164c8a;" style="float: left;line-height: 16px;" ><i class="fa fa-filter"></i>Filter</a>
					<input type="text" name="filter_search" class="form-control _searchname" placeholder="Search Name">
				</div>
				<div style="height: 5px;"></div>
				<div class="sub-panelnamelist ">
					{!! $controller->show() !!}
				</div>

			</div>
		</div>

		<div class="col-md-9" id="rata">
			<div class="row" style="margin-top: 30px;margin-left: 5px;">
				<div class="col-md-12">
					<div class="sub-panel">
						{!! $controller->showRataDatatable() !!}
					</div>
				</div>
			</div>
			<div class="row" style="margin-left: 0px;">
				<div class="col-md-12">
					<div class="box-1 button-style-wrapper">
						<a class="btn btn-xs btn-info btn-savebg btn_new" id="btn_savebanks"><i class="fa fa-save"></i> New</a>
						<a class="btn btn-xs btn-info btn-savebg btn_save submit hidden" id="btn_savebenefits"><i class="fa fa-save"></i> Save</a>
						<!-- <a class="btn btn-xs btn-success btn-editbg btn_edit"><i class="fa fa-edit"></i> Edit</a> -->
						<a class="btn btn-xs btn-danger btn_cancel"> Cancel</a>
					</div>
				</div>
			</div>
			<form method="post" action="{{ url($module_prefix.'/'.$module) }}" onsubmit="return false" id="form">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<input type="hidden" name="transaction_id" id="transaction_id">
				<input type="hidden" name="transpo_amount" id="transpo_amount">
				<input type="hidden" name="rep_amount" id="rep_amount">
				<div class="row" style="margin-left: 8px;">
					<div class="col-md-3 benefits-content">
						<label>No of Actual Works</label>
						<input type="text" name="adjustment_number_of_workdays" id="adjustment_number_of_workdays" class="form-control font-style2">
					</div>
					<div class="col-md-3">
						<label>Rata Percentage</label>
						<input type="text" name="adjustment_rata_percentage" id="adjustment_rata_percentage" class="form-control font-style2" readonly>
					</div>
				</div>
				<div class="row" style="margin-left: 8px;">
					<div class="col-md-3 benefits-content">
						<label>Deduction Period</label>
						<select class="form-control font-style2" id="adjustment_deduction_period" name="adjustment_deduction_period">
							<option></option>
							<option value="secondweek">2nd Week</option>
							<option value="thirdweek">3rd Week</option>
							<option value="fourthweek">4th Week</option>
						</select>
					</div>
				</div>
			</form>
		</div>

		</div>
	</div>
	@endsection

@section('js-logic1')
<script type="text/javascript">
$(document).ready(function(){
	var tblRata = $('#tbl_rata').DataTable();
	number_of_actual_work = 0;
	// GENERATE YEAR
	var year = [];
	year += '<option ></option>';
	for(y = 2018; y <= 2100; y++) {
		year += '<option value='+y+'>'+y+'</option>';
	}
	$('#select_year').html(year);

    // GENERATE MONTH
    month = ["January","February","March","April","May","June","July","August","September","October", "November","December"];
    mArr = [];

    mArr += '<option ></option>';
    for ( m =  0; m <= month.length - 1; m++) {
    	mArr += '<option '+month[m]+'>'+month[m]+'</option>';
    }
    $('#select_month').html(mArr);

    $('#inclusive_leave_date').datepicker({
    	dateFormat:'yy-mm-dd'
    });

    var _Year;
    var _Month;
    $(document).on('change','#select_year',function(){
    	_Year = "";
    	_Year = $(this).find(':selected').val();
    	$('.btnfilter').trigger('click');

    });
    $(document).on('change','#select_month',function(){
    	_Month = "";
    	_Month = $(this).find(':selected').val();
    });

    $('.select2').select2();

    $('#worata').prop('checked','checked').trigger('keyup');

    $('.benefits-content :input').attr("disabled",true);
    $('.btn_new').on('click',function(){
    	$('.benefits-content :input').attr("disabled",false);
    	$('.btn_new').addClass('hidden');
    	$('.btn_edit').addClass('hidden');
    	$('.btn_save').removeClass('hidden');
    });
    $('.btn_edit').on('click',function(){
    	$('.leave-field').attr("disabled",false);
    	$('.btn_edit').addClass('hidden');
    	$('.btn_new').addClass('hidden');
    	$('.btn_save').removeClass('hidden');
    });
    $('.btn_cancel').on('click',function(){
    	$('.benefits-content :input').attr("disabled",true);
    	$('.btn_new').removeClass('hidden');
    	$('.btn_save').addClass('hidden');
    	$('.btn_edit').removeClass('hidden');

    	myform = "myform";
    	clear_form_elements(myform);
    	clear_form_elements('benefits-content');
    	$('#tbl_leave').html('');
    	$('.error-msg').remove();
    	$('#for_update').val('');
    });

    ;

    $('.onlyNumber').keypress(function (event) {
    	return isNumber(event, this)
    });

    $(document).on('keyup','#adjustment_number_of_workdays',function(){
    	num = 0;
    	num = $(this).val();
    	$('#adjustment_rata_percentage').val('');
    	if(num >= 17){
    		$('#adjustment_rata_percentage').val('100 %');
    	}else if(num <= 16 && num >= 12){
    		$('#adjustment_rata_percentage').val('75 %');
    	}else if(num <= 11 && num >= 6){
    		$('#adjustment_rata_percentage').val('50 %');
    	}else if(num <= 5 && num >= 1){
    		$('#adjustment_rata_percentage').val('25 %');
    	}
    });


    var _listId = [];
    $(document).on('click','#check_all',function(){

    	if(!_Year && !_Month){
    		swal({
    			title: "Select year and month first",
    			type: "warning",
    			showCancelButton: false,
    			confirmButtonClass: "btn-danger",
    			confirmButtonText: "Yes",
    			closeOnConfirm: false

    		});

    		$('#check_all').prop('checked',false);

    	}else{
    		if ($(this).is(':checked')) {
    			$('.emp_select').prop('checked', 'checked');

    			$('.emp_select:checked').each(function(){
    				_listId.push($(this).val())
    			});

    		} else {
    			$('.emp_select').prop('checked', false)
    			_listId = [];
    		}

    	}

    });

    _checkrata = ""
    $('input[type=radio][name=chk_wrata]').change(function() {

    	if(!_Year){
    		swal({
    			title: 'Select year and month first',
    			type: "warning",
    			showCancelButton: false,
    			confirmButtonClass: "btn-warning",
    			confirmButtonText: "OK",
    			closeOnConfirm: false
    		})

    		$(this).prop('checked',false);

    	}else{


    		if (this.value == 'wrata') {
    			$('#btn_process_rata').prop('disabled',true);
    			_checkrata = 'wrata';

    		}
    		else if (this.value == 'worata') {
    			$('#btn_process_rata').prop('disabled',false);
    			_checkrata = 'worata';

    		}
    		$('._searchname').trigger('keyup');

    	}

    });

    $(document).on('click','.emp_select',function(){
    	empid = $(this).val();
    	index = $(this).data('key');
    	if(!_Year && !_Month){

    		swal({
    			title: "Select year and month first",
    			type: "warning",
    			showCancelButton: false,
    			confirmButtonClass: "btn-danger",
    			confirmButtonText: "Yes",
    			closeOnConfirm: false

    		});

    		$('.emp_select').prop('checked',false);

    	}else{

    		if($(this).is(':checked')){
    			_listId[index] =  empid;

    		}else{
    			delete _listId[index];
    		}

    	}
    	console.log(_listId);
    });


$(document).on('click','#namelist tr',function(){
	employee_id = $(this).data('empid');

	if(_Year && _Month){

		$.ajax({
			url:base_url+module_prefix+module+'/getRataAdjustment',
			data:{
				'id':employee_id,
				'year':_Year,
				'month':_Month,
			},
			type:'GET',
			dataType:'JSON',
			success:function(data){
				console.log(data);
				clear_form_elements('benefits-content');

				tblRata.clear().draw();
				rata_id = data.id;
				actual_work = data.number_of_actual_work;
				rep_amount = (data.adjustment_rep_amount) ? data.adjustment_rep_amount : 0;
				transpo_amount = (data.adjustment_transpo_amount) ? data.adjustment_transpo_amount : 0;
				percentage_rate = (data.adjustment_percentage) ? parseFloat(data.adjustment_percentage)*100 +'%' : 0;
				number_of_days = (data.adjustment_number_work_days) ? data.adjustment_number_work_days : '';
				representation_amount = (data.representation_amount) ? data.representation_amount : 0;
				transportation_amount = (data.transportation_amount) ? data.transportation_amount : 0;

				net_ra_amount = parseFloat(representation_amount) - parseFloat(rep_amount);
				net_ta_amount = parseFloat(transportation_amount) - parseFloat(transpo_amount);

				net_rata_amount = parseFloat(net_ra_amount) + parseFloat(net_ta_amount);

				representation_amount = (representation_amount !== 0) ? commaSeparateNumber(parseFloat(representation_amount).toFixed(2))  : '';
				transportation_amount = (transportation_amount !== 0) ? commaSeparateNumber(parseFloat(transportation_amount).toFixed(2))  : '';
				net_ra_amount = (net_ra_amount !== 0) ? commaSeparateNumber(parseFloat(net_ra_amount).toFixed(2))  : '';
				net_ta_amount = (net_ta_amount !== 0) ? commaSeparateNumber(parseFloat(net_ta_amount).toFixed(2))  : '';
				net_rata_amount = (net_rata_amount !== 0) ? commaSeparateNumber(parseFloat(net_rata_amount).toFixed(2))  : '';

				tblRata.row.add( [
					representation_amount,
					transportation_amount,
					actual_work,
					percentage_rate,
					net_ra_amount,
					net_ta_amount,
					net_rata_amount,

					]).draw( false );

				$('#transaction_id').val(rata_id);
				$('#rep_amount').val(representation_amount);
				$('#transpo_amount').val(transportation_amount);
			}
		});
	}else{
		swal({
			title: 'Select year and month first',
			type: "warning",
			showCancelButton: false,
			confirmButtonClass: "btn-warning",
			confirmButtonText: "Yes",
			closeOnConfirm: false

		});
	}

})

$(document).on('keyup','._searchname',function(){
	clearTimeout(timer);
	timer = setTimeout(
		function(){
			$.ajax({
				type: "GET",
				url: base_url+module_prefix+module+'/show',
				data: {
					'q':$('._searchname').val(),
					'Year':_Year,
					'Month':_Month,
				},
			   	success: function(res){
			   		$(".sub-panelnamelist").html(res);

			   	},
			   	complete:function(){
			   		// $('.ajax-loader').css("visibility", "hidden");
			   	}
			 });
		},500);
});



$(document).on('click','#delete_rata',function(){
	if(_listId.length == 0){
		swal("Select employee first!", "", "warning");
	}else{
		swal({
			title: "Delete Rata?",
			type: "warning",
			showCancelButton: true,
			confirmButtonClass: "btn-warning",
			confirmButtonText: "Yes",
			cancelButtonText: "No",
		}).then(function(isConfirm){
			if(isConfirm.value == true){
				$.deleteRata();
			}else{
				return false;
			}
		});
	}
});

var timer;
$(document).on('click','.btnfilter',function(){
	// $('input.search').addClass('searchSpinner');
	tools  	  = $('#tools-form').serialize()
	year 	  = $('#select_year :selected').val();
	month 	  = $('#select_month :selected').val();
	category  = $('#select_searchvalue :selected').val();
	empstatus = $('#emp_status :selected').val();
	searchby  = $('#searchby :selected').val();
	subperiod = $('#select_subperiod :selected').val();
	period	  = $('#select_period :selected').val();

	clearTimeout(timer);
	timer = setTimeout(
		function(){
			$.ajax({
			   type: "GET",
			   url: base_url+module_prefix+module+'/show',
			   data: {
			   	'year':year,
			   	'month':month,
			   },
			   beforeSend:function(){
			   		// $('#loading').removeClass('hidden');
			   },
			   complete:function(){
			   		// $('#loading').addClass('hidden');
			   },
			   success: function(res){
			   	// console.log(res);
			      $(".sub-panelnamelist").html(res);
			   }
			});
		},500);
});

$.deleteRata = function(){
	$.ajax({
		url:base_url+module_prefix+module+'/deleteRata',
		data:{'empid':_listId,'year':_Year,'month':_Month,'_token':"{{ csrf_token() }}"},
		type:'post',
		beforeSend:function(){
			$('#delete_rata').html('<i class="fa fa-spinner fa-spin"></i> Deleting').prop('disabled',true);
		},
		success:function(response){
			par = JSON.parse(response)

			if(par.status){
				swal({
					title: par.response,
					type: "success",
					showCancelButton: false,
					confirmButtonClass: "btn-success",
					confirmButtonText: "OK",
					closeOnConfirm: false
				})

				_listId = [];
				$('#delete_rata').html('<i class="fas fa-minus-circle"></i> Delete').prop('disabled',false);
				$('._searchname').trigger('keyup');
			}
		}
	});
}


});


</script>
@endsection