<div class="col-md-12">
	<table class="table table-responsive datatable" id="tbl_pei">
		<thead>
			<tr >
				<th  >Designation</th>
				<th  >SG</th>
				<th  >Salary</th>
				<th  >No of Days</th>
				<th>Amount</th>
			</tr>
		</thead>
		<tbody class="text-right">
		</tbody>
	</table>
</div>
<script type="text/javascript">
$(document).ready(function(){

	 var table = $('#tbl_pei').DataTable({
	 	'dom':'<lf<t>pi>',
	 	"paging": false,
	 	// "scrollY":"250px",
   //      "scrollCollapse": true,
	 });

	$('#tbl_pei tbody').on( 'click', 'tr', function () {
	    if ( $(this).hasClass('selected') ) {

	        $(this).removeClass('selected');

	        id 					= $(this).data('id');
	        employee_id 		= $(this).data('employee_id');
	        position_id 		= $(this).data('position_id');
	        salary_grade_id 	= $(this).data('salary_grade_id');
	        number_of_days 		= $(this).data('number_of_days');
	        salary_amount 		= $(this).data('salary_amount');

	        $('#monetization_id').val(id);
	        $('#employee_id').val(employee_id);
	        $('#position_id').val(position_id);
	        $('#salary_grade_id').val(salary_grade_id);
	        $('#number_of_days').val(number_of_days);
	        $('#basic_amount').val(commaSeparateNumber(parseFloat(salary_amount).toFixed(2)));

	        btnnew = $(this).data('btnnew');
			btnsave = $(this).data('btnsave');
			btnedit = $(this).data('btnedit');
			btncancel = $(this).data('btncancel');

			if(!$('#'+btnsave).is(':visible')){
				$('#'+btnedit).removeClass('hidden');
				$('#'+btncancel).removeClass('hidden');
				$('#'+btnnew).addClass('hidden');
			}


	    }
	    else {
	        table.$('tr.selected').removeClass('selected');
	        $(this).addClass('selected');
	    }
	} );

	$('#button').click( function () {
	    table.row('.selected').remove().draw( false );
	} );

})
</script>
