<div class="col-md-12">
	<table class="table table-responsive datatable" id="tbl_pei" style="width: 809px;">
		<thead>
			<tr >
				<th  >Travel Allowance (Rate/Day)</th>
				<th  >Entitlement (%)</th>
				<th  >Travel Allowance</th>
				<th  >Amount</th>
			</tr>
		</thead>
		<tbody class="text-right">
		</tbody>
	</table>
</div>
<script type="text/javascript">
$(document).ready(function(){

	 var table = $('#tbl_pei').DataTable({
	 	'dom':'<lf<t>pi>',
	 	"paging": false,
	 	// "scrollY":"250px",
   //      "scrollCollapse": true,
	 });

	$('#tbl_pei tbody').on( 'click', 'tr', function () {
	    if ( $(this).hasClass('selected') ) {

	        $(this).removeClass('selected');

	        id 					= $(this).data('id');
	        employee_id 		= $(this).data('employee_id');
	        travel_rate_id 		= $(this).data('travel_rate_id');
	        rate_id 			= $(this).data('rate_id');
	        number_of_days 		= $(this).data('number_of_days');

	        $('#travel_allowance_id').val(id);
	        $('#employee_id').val(employee_id);
	        $('#travel_rate_id').val(travel_rate_id);
	        $('#rate_id').val(rate_id);
	        $('#number_of_days').val(number_of_days);

	        btnnew = $(this).data('btnnew');
			btnsave = $(this).data('btnsave');
			btnedit = $(this).data('btnedit');
			btncancel = $(this).data('btncancel');

			if(!$('#'+btnsave).is(':visible')){
				$('#'+btnedit).removeClass('hidden');
				$('#'+btncancel).removeClass('hidden');
				$('#'+btnnew).addClass('hidden');
			}


	    }
	    else {
	        table.$('tr.selected').removeClass('selected');
	        $(this).addClass('selected');
	    }
	} );

	$('#button').click( function () {
	    table.row('.selected').remove().draw( false );
	} );

})
</script>
