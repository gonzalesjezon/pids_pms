<table class="table scroll datatable" id="namelist">
	<thead>
		<tr>
			<th>#</th>
			<th>Name</th>
		</tr>
	</thead>
	<tbody >
		@foreach($data as $key => $value)
			<tr data-empid="{{ $value->id }}"   data-empname="{{ $value->lastname.' '.$value->firstname.' '.$value->middlename }}">
				<td>{{ ucwords(strtolower($value->lastname)) }} {{ ucwords(strtolower($value->firstname)) }} {{ ucwords(strtolower($value->middlename)) }}</td>
			</tr>
		@endforeach
	</tbody>
</table>
<script type="text/javascript">
$(document).ready(function(){


});
</script>