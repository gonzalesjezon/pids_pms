@extends('app-front')

@section('content')
<div class="row">
	<div class="col-md-12">
		<hr>
		<div class="col-md-3">
			<div>
				<h5 ><b>Filter Employee By</b></h5>
				<table class="table borderless" style="border:none;font-weight: bold">
					<tr class="text-left">
						<td colspan="2"><span>Transaction Period</span></td>
					</tr>
					<tr>
						<td>
							<div class="col-md-6" style="padding-left: 0px;">
								<select class="employee-type form-control font-style2 select2" id="select_month" name="select_month" placeholder="Month">
									<option value=""></option>
								</select>
							</div>
							<div class="col-md-6" style="padding-right: 0px;">
								<select class="employee-type form-control font-style2 select2" id="select_year" name="select_year" placeholder="Year" >
									<option value=""></option>
								</select>
							</div>
						</td>

					</tr>
				</table>
				<div class="col-md-12 text-right">
					<button class="btn btn-xs btn-info" id="btn_process_rata" style="background-color: #164c8a;"><i class="far fa-save"></i>&nbsp;Process</button>
					<a class="btn btn-xs btn-danger" id="delete_rata" ><i class="fas fa-minus-circle"></i>&nbsp;Delete</a>
				</div>
				<div class="search-btn">
					<div class="col-md-4">
						<span>Search</span>
					</div>
					<div class="col-md-8">
						<label class="radiobut-style radio-inline ">
							<input type="radio" name="chk_wrata" id="wrata" value="wrata">
							With
						</label>
						<label class="radiobut-style radio-inline ">
							<input type="radio" name="chk_wrata" id="worata" value="worata">
							W/Out
						</label>
					</div>
				</div>
				<div >
					<input type="text" name="filter_search" class="form-control _searchname">
				</div>
				<div style="height: 5px;"></div>
				<div class="sub-panelnamelist ">
					{!! $controller->show() !!}
				</div>

			</div>
		</div>

		<div class="col-md-9" id="rata">
			<label style="font-weight: 600;font-size: 15px;">&nbsp;&nbsp;RATA</label>
			<div class="sub-panel">
				{!! $controller->showRataDatatable() !!}
			</div>
			<div class="box-1 button-style-wrapper">
				<a class="btn btn-xs btn-info btn-savebg btn_new hidden" id="btn_savebanks"><i class="fa fa-save"></i> New</a>
				<a class="btn btn-xs btn-info btn-savebg btn_save hidden" id="btn_save_rata"><i class="fa fa-save"></i> Save</a>
				<a class="btn btn-xs btn-success btn-editbg btn_edit"><i class="fa fa-edit"></i> Edit</a>
				<a class="btn btn-xs btn-danger btn_cancel"> Cancel</a>
			</div>
			<div class="benefits-content style-box2">
				<div class="col-md-6">
					<table class="table borderless" style="border: none;">
						<tr>
							<td><label>No. of Actual Work</label></td>
							<td>
								@if ($errors->has('no_of_actual_work'))
								<span class="text-danger">{{ $errors->first('no_of_actual_work') }}</span>
								@endif
								<input type="text" name="no_of_actual_work" id="no_of_actual_work" class="form-control font-style2 onlyNumber" readonly>
							</td>
						</tr>
						<tr>
							<td><label>No. of Leave Field</label></td>
							<td>
								@if ($errors->has('number_of_leave_field'))
								<span class="text-danger">{{ $errors->first('number_of_leave_field') }}</span>
								@endif
								<input type="text" name="number_of_leave_field" id="no_of_leave_field" class="form-control font-style2 onlyNumber" maxlength="2" disabled>
							</td>
						</tr>
						<tr>
							<table class="table">
								<label>Inclusive Date</label>
								<thead>
									<tr>
										<th>Date Field</th>
										<th>Leave Type</th>
									</tr>
								</thead>
								<tbody id="tbl_leave">
								</tbody>
							</table>
						</tr>
						<tr>
							<td><label>Date</label></td>
							<td>
								@if ($errors->has('inclusive_date'))
								<span class="text-danger">{{ $errors->first('inclusive_date') }}</span>
								@endif
								<input type="text" name="inclusive_leave_date" id="inclusive_leave_date" class="form-control font-style2 leave-field">
							</td>
						</tr>
						<tr>
							<td><label>Leave Type</label></td>
							<td>
								@if ($errors->has('leave_type'))
								<span class="text-danger">{{ $errors->first('leave_type') }}</span>
								@endif
								<select  name="leave_type" id="leave_type" class="form-control font-style2  leave-field">
									<option value=""></option>
									<option value="VL">Vacation Leave</option>
									<option value="SL">Sick Leave</option>
									<option value="SPL">Special Priviledge Leave</option>
									<option value="PL">Paternity Leave</option>
									<option value="CTO">CTO</option>
									<option value="OB">O.B</option>
									<option value="SO">S.O</option>
									<option value="FTO">FTO</option>

								</select>
							</td>
						</tr>
					</table>
				</div>
				<div class="col-md-6">
					<table class="table borderless" style="border: none;">
						<tr>
							<td><label>No. of Work Days</label></td>
							<td>
								@if ($errors->has('no_of_work_days'))
								<span class="text-danger">{{ $errors->first('no_of_work_days') }}</span>
								@endif
								<input type="text" name="no_of_work_days" id="no_of_work_days" class="form-control font-style2 onlyNumber" readonly>
							</td>
						</tr>
						<tr>
							<td><label>No. of Used Vehicle</label></td>
							<td>
								@if ($errors->has('no_of_used_vehicles'))
								<span class="text-danger">{{ $errors->first('no_of_used_vehicles') }}</span>
								@endif
								<input type="text" name="no_of_used_vehicles" id="no_of_used_vehicles" class="form-control font-style2 onlyNumber" disabled>
							</td>
						</tr>
						<tr>
							<td><label>Percentage of RATA</label></td>
							<td>
								@if ($errors->has('percentage_of_rata'))
								<span class="text-danger">{{ $errors->first('percentage_of_rata') }}</span>
								@endif
								<select class="form-control font-style2" name="percentage_of_rata" id="percentage_of_rata" disabled>

									<option value=""></option>
									<option value=".25">1 - 5 25% of Monthly RATA</option>
									<option value=".50">6 - 11 50% of Monthly RATA</option>
									<option value=".75">12 - 16 75% of Monthly RATA</option>
									<option value="1">17 & more 100% of Monthly RATA</option>
								</select>
							</td>
						</tr>
					</table>
				</div>

			</div>

		</div>


		</div>
	</div>
	@endsection

@section('js-logic1')
<script type="text/javascript">
$(document).ready(function(){
	var tblRata = $('#tbl_rata').DataTable();
	var tblEoam = $('#tbl_eoam').DataTable();
	var tblCead = $('#tbl_cead').DataTable();
	var tblPei = $('#tbl_pe').DataTable();
	var tblCashGift = $('#tbl_cashgift').DataTable();
	number_of_actual_work = 0;
	// GENERATE YEAR
	var year = [];
	year += '<option ></option>';
	for(y = 2018; y <= 2100; y++) {
		year += '<option value='+y+'>'+y+'</option>';
	}
	$('#select_year').html(year);

    // GENERATE MONTH
    month = ["January","February","March","April","May","June","July","August","September","October", "November","December"];
    mArr = [];

    mArr += '<option ></option>';
    for ( m =  0; m <= month.length - 1; m++) {
    	mArr += '<option '+month[m]+'>'+month[m]+'</option>';
    }
    $('#select_month').html(mArr);

    $('#inclusive_leave_date').datepicker({
    	dateFormat:'yy-mm-dd'
    });

    var _Year;
    var _Month;
    $(document).on('change','#select_year',function(){
    	_Year = "";
    	_Year = $(this).find(':selected').val();

    });
    $(document).on('change','#select_month',function(){
    	_Month = "";
    	_Month = $(this).find(':selected').val();
    });

    $('.select2').select2();

    $('#worata').prop('checked','checked').trigger('keyup');

    $('.benefits-content :input').attr("disabled",true);
    $('.btn_new').on('click',function(){
    	$('.benefits-content :input').attr("disabled",false);
    	$('.btn_new').addClass('hidden');
    	$('.btn_edit').addClass('hidden');
    	$('.btn_save').removeClass('hidden');
    });
    $('.btn_edit').on('click',function(){
    	$('.leave-field').attr("disabled",false);
    	$('.btn_edit').addClass('hidden');
    	$('.btn_new').addClass('hidden');
    	$('.btn_save').removeClass('hidden');
    });
    $('.btn_cancel').on('click',function(){
    	$('.benefits-content :input').attr("disabled",true);
    	// $('.btn_new').removeClass('hidden');
    	$('.btn_save').addClass('hidden');
    	$('.btn_edit').removeClass('hidden');

    	myform = "myform";
    	clear_form_elements(myform);
    	clear_form_elements('benefits-content');
    	$('#tbl_leave').html('');
    	$('.error-msg').remove();
    	$('#for_update').val('');
    });

    ;

    $('.onlyNumber').keypress(function (event) {
    	return isNumber(event, this)
    });


    var _listId = [];
    $(document).on('click','#check_all',function(){

    	if(!_Year && !_Month){
    		swal({
    			title: "Select year and month first",
    			type: "warning",
    			showCancelButton: false,
    			confirmButtonClass: "btn-danger",
    			confirmButtonText: "Yes",
    			closeOnConfirm: false

    		});

    		$('#check_all').prop('checked',false);

    	}else{
    		if ($(this).is(':checked')) {
    			$('.emp_select').prop('checked', 'checked');

    			$('.emp_select:checked').each(function(){
    				_listId.push($(this).val())
    			});

    		} else {
    			$('.emp_select').prop('checked', false)
    			_listId = [];
    		}

    	}

    });

    _checkrata = ""
    $('input[type=radio][name=chk_wrata]').change(function() {

    	if(!_Year){
    		swal({
    			title: 'Select year and month first',
    			type: "warning",
    			showCancelButton: false,
    			confirmButtonClass: "btn-warning",
    			confirmButtonText: "OK",
    			closeOnConfirm: false
    		})

    		$(this).prop('checked',false);

    	}else{


    		if (this.value == 'wrata') {
    			$('#btn_process_rata').prop('disabled',true);
    			_checkrata = 'wrata';

    		}
    		else if (this.value == 'worata') {
    			$('#btn_process_rata').prop('disabled',false);
    			_checkrata = 'worata';

    		}
    		$('._searchname').trigger('keyup');

    	}

    });

    $(document).on('click','.emp_select',function(){
    	empid = $(this).val();
    	index = $(this).data('key');
    	if(!_Year && !_Month){

    		swal({
    			title: "Select year and month first",
    			type: "warning",
    			showCancelButton: false,
    			confirmButtonClass: "btn-danger",
    			confirmButtonText: "Yes",
    			closeOnConfirm: false

    		});

    		$('.emp_select').prop('checked',false);

    	}else{

    		if($(this).is(':checked')){
    			_listId[index] =  empid;

    		}else{
    			delete _listId[index];
    		}

    	}
    	console.log(_listId);
    });
    var number_of_leave = 0;
    var no_of_actual_work;
    $(document).on('keyup','#no_of_leave_field',function(){

    	number_of_leave = $(this).val();

    	no_of_actual_work = (number_of_leave) ? (number_of_actual_work - number_of_leave) : (number_of_actual_work + number_of_leave);

    	$('#no_of_actual_work').val(no_of_actual_work);
    })

    $(document).on('change','#leave_type',function(){
    	if($(this).val()){
    		$('.benefits-content :input').attr('disabled',false);
    	}else{
    		$('.benefits-content :input').attr('disabled',true);
    		$('.leave-field').attr('disabled',false);
    	}
    });
    var representation_amount = 0;
    var transportation_amount = 0;
    var rata_id;
    var employee_id;
    $(document).on('click','#namelist tr',function(){
    	employee_id = $(this).data('empid');

    	if(_Year && _Month){

    		$.ajax({
    			url:base_url+module_prefix+module+'/getRataInfo',
    			data:{
    				'id':employee_id,
    				'year':_Year,
    				'month':_Month,
    			},
    			type:'GET',
    			dataType:'JSON',
    			success:function(data){
    				console.log(data);
    				clear_form_elements('benefits-content');
    				$('#tbl_leave').html('');
    				rata_id = '';
    				if(data.rata !== null){

	    				rata_id = data.rata.id;
	    				employee_id = data.rata.employee_id;
	    				number_of_actual_work = (data.rata.number_of_actual_work) ? data.rata.number_of_actual_work : data.rata.number_of_work_days;
	    				$('#no_of_actual_work').val(number_of_actual_work);
	    				$('#no_of_work_days').val(data.rata.number_of_work_days);

	    				arr = [];
	    				leave_type = [];
	    				number_of_leave = [];
	    				$.each(data.leave,function(k,v){
	    					arr += '<tr>';
	    					arr += '<td>'+v.leave_date+'</td>';

	    					arr += '<td>'+v.leave_type+'</td>';
	    					arr += '</tr>';

	    				})
	    				$('#tbl_leave').html(arr);
	    				item = '';
	    				$.each(data.leave_list,function(k,v){
	    					item += v+', ('+k+')';
	    				});


	    				percentage = (data.rata.percentage_of_rata_value) ? (data.rata.percentage_of_rata_value * 100) : '100';

						// <!-- GENERATE RATA TABLE --!>

						tblRata.clear().draw();

						representation_amount = (data.rata.representation_amount) ? data.rata.representation_amount : 0;
						transportation_amount = (data.rata.transportation_amount) ? data.rata.transportation_amount : 0;
						total_rata = (parseFloat(representation_amount) + parseFloat(transportation_amount));

						tblRata.row.add( [
							representation_amount,
							transportation_amount,
							item,
							number_of_actual_work,
							percentage+'%',
							representation_amount,
							transportation_amount,
							total_rata.toFixed(2)

							]).draw( false );
    				}
				}
			});
    	}else{
    		swal({
    			title: 'Select year and month first',
    			type: "warning",
    			showCancelButton: false,
    			confirmButtonClass: "btn-warning",
    			confirmButtonText: "Yes",
    			closeOnConfirm: false

    		});
    	}

    })

$(document).on('keyup','._searchname',function(){
	clearTimeout(timer);
	timer = setTimeout(
		function(){
			$.ajax({
				type: "GET",
				url: base_url+module_prefix+module+'/show',
				data: {
					'q':$('._searchname').val(),
					'limit':$(".limit").val(),
					'check_rata':_checkrata,
					'year':_Year,
					'month':_Month,
				},
				beforeSend:function(){
				   		// $('.ajax-loader').css("visibility", "visible");

				   	},
				   	success: function(res){
				   		$(".sub-panelnamelist").html(res);

				   	},
				   	complete:function(){
				   		// $('.ajax-loader').css("visibility", "hidden");
				   	}
				 });
		},500);
});

$(document).on('change','#searchby',function(){
	var val = $(this).val();
	console.log(base_url+module_prefix+module)
	$.ajax({
		url:base_url+module_prefix+module+'/getSearchby',
		data:{'q':val},
		type:'GET',
		dataType:'JSON',
		success:function(data){

			arr = [];
			$.each(data,function(k,v){
				arr += '<option value='+v.id+'>'+v.name+'</option>';
			})

			$('#select_searchvalue').html(arr);
		}
	})

});

$(document).on('click','#btn_process_rata',function(){
	if(_listId.length == 0){
		swal("Select employee first!", "", "warning");
	}else{
		swal({
			title: "Process Rata?",
			type: "warning",
			showCancelButton: true,
			confirmButtonClass: "btn-warning",
			confirmButtonText: "Yes",
			cancelButtonText: "No",
		}).then(function(isConfirm){
			if(isConfirm.value == true){
				$.processRata();
			}else{
				return false;
			}
		});
	}

});

$.processRata = function(){
	$.ajax({
		url:base_url+module_prefix+module+'/processRata',
		data:{
			'_token':'{{ csrf_token() }}',
			'list_id':_listId,
			'year':_Year,
			'month':_Month,
			'rata_id':rata_id,
			'employee_id':employee_id
		},
		type:'POST',
		beforeSend:function(){
        	$('#btn_process_rata').html('<i class="fa fa-spinner fa-spin"></i> Processing').prop('disabled',true);
        },
		success:function(data){
			par = JSON.parse(data);
			if(par.status){
				swal({
					title: par.response,
					type: "success",
					showCancelButton: false,
					confirmButtonClass: "btn-success",
					confirmButtonText: "Yes",
					closeOnConfirm: false
				});
					$('#btn_process_rata').html('<i class="fa fa-save"></i> Process').prop('disabled',false);
					$('._searchname').trigger('keyup');
				}else{

				}
			}
	});
}

$(document).on('click','#btn_save_rata',function(){

	if(no_of_actual_work == null){
		swal("No of field is empty!", "", "warning");
	}else{
		$.saveRata();
	}

});

$.saveRata = function(){
	$.ajax({
		type:'POST',
		url:base_url+module_prefix+module+'/storeRata',
		data:{
			'_token':'{{ csrf_token() }}',
			'employee_id':employee_id,
			'rata_id':rata_id,
			'rata':{
				'no_of_actual_work':$('#no_of_actual_work').val(),
				'no_of_work_days':$('#no_of_work_days').val(),
				'no_of_used_vehicles':$('#no_of_used_vehicles').val(),
				'percentage_of_rata':$('#percentage_of_rata :selected').text(),
				'percentage_of_rata_value':$('#percentage_of_rata :selected').val(),
				'representation_amount':representation_amount,
				'transportation_amount':transportation_amount,
				'year':_Year,
				'month':_Month
			},
			'leave':{
				'inclusive_leave_date':$('#inclusive_leave_date').val(),
				'leave_type':$('#leave_type').val(),
				'no_of_leave_field':$('#no_of_leave_field').val(),
			}

		},
		success:function(data){
			par = JSON.parse(data);

			if(par.status){
				swal({
					title: par.response,
					type: "success",
					showCancelButton: false,
					confirmButtonClass: "btn-success",
					confirmButtonText: "OK",
					closeOnConfirm: false
				}).then(function(){

					clear_form_elements('benefits-content');
					$('#tbl_leave').html('');
					$('.btn_save').addClass('hidden');
					$('.btn_new').addClass('hidden');
					$('.btn_edit').removeClass('hidden');

					employee_id = ''
					rata_id = '';
					number_of_actual_work = (par.rata.rata.number_of_actual_work) ? par.rata.rata.number_of_actual_work : par.rata.number_of_work_days;
					$('#no_of_actual_work').val(number_of_actual_work);
					$('#no_of_work_days').val(par.rata.rata.number_of_work_days);

					arr = [];
					leave_type = [];
					number_of_leave = [];
					$.each(par.rata.leave,function(k,v){
						arr += '<tr>';
						arr += '<td>'+v.leave_date+'</td>';

						arr += '<td>'+v.leave_type+'</td>';
						arr += '</tr>';

					})
					$('#tbl_leave').html(arr);
					item = '';
					$.each(par.rata.leave_list,function(k,v){
						item += v+', ('+k+')';
					});
					console.log(item)
						// GENERATE TR FOR BENEFITS TAB

						percentage = (par.rata.rata.percentage_of_rata_value) ? (par.rata.rata.percentage_of_rata_value * 100) : '100';

						tblRata.clear().draw();

						representation_amount = (par.rata.rata.representation_amount) ? par.rata.rata.representation_amount : 0;
						transportation_amount = (par.rata.rata.transportation_amount) ? par.rata.rata.transportation_amount : 0;
						total_rata = (parseFloat(representation_amount) + parseFloat(transportation_amount));

						tblRata.row.add( [
							representation_amount,
							transportation_amount,
							item,
							number_of_actual_work,
							percentage+'%',
							representation_amount,
							transportation_amount,
							total_rata.toFixed(2)

							]).draw( false );
					});


			}else{
				swal({
					title: par.response,
					type: "warning",
					showCancelButton: false,
					confirmButtonClass: "btn-warning",
					confirmButtonText: "OK",
					closeOnConfirm: false
				})
			}
		}
	});
}

$(document).on('click','#delete_rata',function(){
	if(_listId.length == 0){
		swal("Select employee first!", "", "warning");
	}else{
		swal({
			title: "Delete Rata?",
			type: "warning",
			showCancelButton: true,
			confirmButtonClass: "btn-warning",
			confirmButtonText: "Yes",
			cancelButtonText: "No",
		}).then(function(isConfirm){
			if(isConfirm.value == true){
				$.deleteRata();
			}else{
				return false;
			}
		});
	}
})

$.deleteRata = function(){
	$.ajax({
		url:base_url+module_prefix+module+'/deleteRata',
		data:{'empid':_listId,'year':_Year,'month':_Month,'_token':"{{ csrf_token() }}"},
		type:'post',
		beforeSend:function(){
			$('#delete_rata').html('<i class="fa fa-spinner fa-spin"></i> Deleting').prop('disabled',true);
		},
		success:function(response){
			par = JSON.parse(response)

			if(par.status){
				swal({
					title: par.response,
					type: "success",
					showCancelButton: false,
					confirmButtonClass: "btn-success",
					confirmButtonText: "OK",
					closeOnConfirm: false
				})

				_listId = [];
				$('#delete_rata').html('<i class="fas fa-minus-circle"></i> Delete').prop('disabled',false);
				$('._searchname').trigger('keyup');
			}
		}
	});
}


});


</script>
@endsection