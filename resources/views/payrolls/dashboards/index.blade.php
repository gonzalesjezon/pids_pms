@extends('app-front')

@section('content')

<style type="text/css">
	.form-control{
		height: 27px !important;
	}
	.panel{
		border: 1px solid #dadada !important;
	}
	.panel1{
		border: 1px solid #009ae4 !important;
		background-color: #009ae4;
	}
	.panel2{
		border: 1px solid #55c747 !important;
		background-color: #55c747;
	}
	.panel3{
		border: 1px solid #fbc911 !important;
		background-color: #fbc911;
	}
	.panel4{
		border: 1px solid #ff5a5a !important;
		background-color: #ff5a5a;
	}
	.panel5{
		border: 1px solid #6ce4c3 !important;
		background-color: #6ce4c3;
	}
	.panel6{
		border: 1px solid #b66ce4 !important;
		background-color: #b66ce4;
	}
	.span-footer{
		font-size: 11px;
		font-weight: bold;
	}
	.span-float-right{
		color: #fff;
		font-size: 22px;
		position: relative;
		float: right;
	}

</style>
<div class="row" style="margin-top: 50px;margin-left: 10px;margin-right: 15px;">
	<div class="col-md-2">
		<div class="panel panel1">
			<div class="panel-body">
				<span class="span-float-right">{{ $allemployees }}</span>
			</div>
			<div class="panel-footer text-center">
				<span class="span-footer">TOTAL EMPLOYEES</span>
			</div>
		</div>
	</div>
	<div class="col-md-2">
		<div class="panel panel2">
			<div class="panel-body">
				<span class="span-float-right">{{ $permanent }}</span>
			</div>
			<div class="panel-footer text-center">
				<span class="span-footer">PERMANENT</span>
			</div>
		</div>
	</div>
	<div class="col-md-2">
		<div class="panel panel3">
			<div class="panel-body">
				<span class="span-float-right">0</span>
			</div>
			<div class="panel-footer text-center">
				<span class="span-footer">CO TERMINUS</span>
			</div>
		</div>
	</div>
	<div class="col-md-2">
		<div class="panel panel5">
			<div class="panel-body">
				<span class="span-float-right">0</span>
			</div>
			<div class="panel-footer text-center">
				<span class="span-footer">TEMPORARY</span>
			</div>
		</div>
	</div>
	<div class="col-md-2">
		<div class="panel panel6">
			<div class="panel-body">
				<span class="span-float-right">0</span>
			</div>
			<div class="panel-footer text-center">
				<span class="span-footer">CONTRACTUAL</span>
			</div>
		</div>
	</div>
		<div class="col-md-2">
		<div class="panel panel4">
			<div class="panel-body">
				<span class="span-float-right">0</span>
			</div>
			<div class="panel-footer text-center">
				<span class="span-footer">CONTRACT OF SERVICE</span>
			</div>
		</div>
	</div>
</div>
<div class="row" style="margin-left: 10px;margin-right: 15px;">
	<div class="col-md-9">
		<div class="panel">
			<div class="panel-header" style="padding-top: 20px;">
				<div class="col-md-3">
					<div class="form-group" style="margin-left: 10px;padding-top: 15px;">
						<label>Search Name:</label>
						<input type="text" name="search_name" id="search_name" class="form-control _searchname">
					</div>
				</div>
				<div class="col-md-5">
					<label>Status</label>
					<div class="form-group">
						<div class="col-md-5">
							<div class="input-group">
							<label>
								<input type="checkbox" name="permanent" id="permanent" >
								<span>Permanent</span>
							</label>
								<label><input type="checkbox" name="coterminus" id="coterminus" >
									<span>Co Terminus</span>
								</label>
							</div>
						</div>
						<div class="col-md-6">
							<div class="input-group">
								<label><input type="checkbox" name="cos" id="cos" >
									<span>Contract of Service</span>
								</label>
								<label><input type="checkbox" name="jo" id="jo" >
									<span>Job Order</span>
								</label>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="panel-body">
				<div class="row">
					{!! $controller->show() !!}
				</div>
			</div>
		</div>
		<div style="height: 30px;"></div>
	</div>
	<div class="col-md-3">
		<div class="panel" >
			<div class="panel-body">
				<h4 style="font-weight: bold;">Quick Reports</h4>
			</div>
		</div>
		<div class="panel">
			<div class="panel-body">
				<div class="row">
					<div class="col-md-6 text-center">
						<img src="{{ asset('icons/payroll_transfer_icon.png') }}" height="80px;">
					</div>
					<div class="col-md-6 text-center">
						<img src="{{ asset('icons/payroll_register_icon.png') }}" height="80px;">
					</div>
				</div>
				<div class="row">
					<div class="col-md-6 text-center">
						<span style="font-weight: bold;font-size: 12px;">Itemize</span>
					</div>
					<div class="col-md-6 text-center">
						<span style="font-weight: bold;font-size: 12px;">Contractual</span>
					</div>
				</div>
				<div class="row" style="margin-top: 5px;">
					<div class="col-md-6 text-center">
						<img src="{{ asset('icons/payslip_icon.png') }}" height="80px;" >
					</div>
					<div class="col-md-6 text-center">
						<img src="{{ asset('icons/government_icon.png') }}" height="80px;" >
					</div>
				</div>
				<div class="row" style="margin-top: 5px;">
					<div class="col-md-6 text-center">
						<span style="font-weight: bold;font-size: 12px;">Casual</span>
					</div>
					<div class="col-md-6 text-center">
						<span style="font-weight: bold;font-size: 12px;">Contract of Service</span>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection
@section('js-logic1')
<script>
$(document).ready(function () {
	$(document).on('keyup','._searchname',function(){
	clearTimeout(timer);
	timer = setTimeout(
		function(){
			$.ajax({
			   type: "GET",
			   url: base_url+module_prefix+module+'/show',
			   data: {
			   	"q":$('._searchname').val(),
			},
			   success: function(res){
			      $(".tableFixHead").html(res);
			   }
			});
		},500);
});

});
</script>
@endsection