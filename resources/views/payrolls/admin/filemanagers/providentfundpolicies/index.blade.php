@extends('app-filemanager')

@section('filemanager-content')
<div id="provident_fund" class="benefits-wrapper">
	<div class="col-md-12" style="padding-bottom: 10px;">
		<div class="col-md-6">
			<label>{{ $title }}</label>
		</div>

	</div>

	<div class="sub-panel">
		{!! $controller->show() !!}
	</div>

	<div class="formcontent">
		<form method="POST" action="{{ url($module_prefix.'/'.$module)}}" onsubmit="return false" id="form" class="myform">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<input type="hidden" name="for_update" id="for_update">
			<div class="col-md-12">
				<div class="box-1 button-style-wrapper">
					<a class="btn btn-xs btn-info btn-savebg btn_new" id="btn_saveprovidentfund"><i class="fa fa-save"></i> New</a>
					<a class="btn btn-xs btn-info btn-savebg btn_save submit hidden" id="btn_savebenefits"><i class="fa fa-save"></i> Save</a>
					<a class="btn btn-xs btn-success btn-editbg btn_edit"><i class="fa fa-edit"></i> Edit</a>
					<a class="btn btn-xs btn-danger btn_cancel"> Cancel</a>
				</div>
			</div>
			<div class="benefits-content style-box2">
				<div class="col-md-12">
					<div class="col-md-6">
						<table class="table borderless" style="border: none;">
							<tr>
								<td><label>Policy Name</label></td>
								<td>
									@if ($errors->has('policy_name'))
									    <span class="text-danger">{{ $errors->first('policy_name') }}</span>
									@endif
									<input name="policy_name" id="policy_name" class="form-control font-style2"/>
								</td>
							</tr>
							<tr>
								<td><label>Pay Period</label></td>
								<td>
									@if ($errors->has('pay_period'))
									    <span class="text-danger">{{ $errors->first('pay_period') }}</span>
									@endif
									<select name="pay_period" id="pay_period" class="form-control font-style2">
										<option value=""></option>
										<option value="Semi-Monthly">Semi-Monthly</option>
										<option value="Monthly">Monthly</option>
									</select>
								</td>
							</tr>
							<tr>
								<td><label>Deduction Period</label></td>
								<td>
									@if ($errors->has('deduction_period'))
									    <span class="text-danger">{{ $errors->first('deduction_period') }}</span>
									@endif
									<select name="deduction_period" id="deduction_period" class="form-control font-style2">
										<option value=""></option>
										<option value="First Half">First Half</option>
										<option value="Second Half">Second Half</option>
									</select>
								</td>
							</tr>
							<tr>
								<td><label>Policy Type</label></td>
								<td>
									@if ($errors->has('policy_type'))
									    <span class="text-danger">{{ $errors->first('policy_type') }}</span>
									@endif
									<select name="policy_type" id="policy_type" class="form-control font-style2">
										<option value=""></option>
										<option value="System Genearated">System Genearated</option>
										<option value="Inputted">Inputted</option>
									</select>
								</td>
							</tr>
						</table>
					</div>
					<div class="col-md-6">
						<table class="table borderless" style="border: none;">
							<tr>
								<td><label>Base On</label></td>
								<td>
									@if ($errors->has('based_on'))
									    <span class="text-danger">{{ $errors->first('based_on') }}</span>
									@endif
									<select name="based_on" id="based_on" class="form-control font-style2">
										<option value=""></option>
										<option value="Fix Monthly Rate">Fix Monthly Rate</option>
									</select>
								</td>
							</tr>
							<tr>
								<td><label>For EE Share</label></td>
								<td>
									@if ($errors->has('ee_share'))
									    <span class="text-danger">{{ $errors->first('ee_share') }}</span>
									@endif
									<select name="ee_share" id="ee_share" class="form-control font-style2">
										<option value=""></option>
										@foreach($policies as $key => $value)
										<option value="{{ $value->rate }}">{{ $value->rate*100 }} %</option>
										@endforeach
									</select>
								</td>
							</tr>
							<tr>
								<td><label>For ER Share</label></td>
								<td>
									@if ($errors->has('er_share'))
									    <span class="text-danger">{{ $errors->first('er_share') }}</span>
									@endif
									<input type="text" name="er_share" id="er_share" class="form-control font-style2"/>

								</td>
							</tr>
						</table>
					</div>
				</div>
			</div>

		</form>

	</div>
</div>
@endsection

@section('js-logic2')
<script type="text/javascript">
	$(document).ready(function(){

		$('.benefits-content :input').attr("disabled",true);
		$('.btn_new').on('click',function(){
			$('.benefits-content :input').attr("disabled",false);
			$('.btn_new').addClass('hidden');
			$('.btn_edit').addClass('hidden');
			$('.btn_save').removeClass('hidden');
		});
		$('.btn_edit').on('click',function(){
			$('.benefits-content :input').attr("disabled",false);
			$('.btn_edit').addClass('hidden');
			$('.btn_new').addClass('hidden');
			$('.btn_save').removeClass('hidden');
		});
		$('.btn_cancel').on('click',function(){
			$('.benefits-content :input').attr("disabled",true);
			$('.btn_new').removeClass('hidden');
			$('.btn_save').addClass('hidden');
			$('.btn_edit').removeClass('hidden');
		});

		$(".btn_cancel").click(function() {
			myform = "myform";
			clear_form_elements(myform);
			$('.error-msg').remove();
			$('#for_update').val('');
		});
	})
</script>
@endsection