@extends('app-filemanager')

@section('filemanager-content')
<div id="salary" class="benefits-wrapper">
	<div class="col-md-12" style="padding-bottom: 10px;">
		<div class="col-md-6">
			<label>{{ $title }}</label>
		</div>
		<div class="col-md-6">
			
		</div>
	</div>
		
	<div class="sub-panel" >
				
		{!! $controller->show() !!}

	</div>

	<div class="formcontent">
		<form method="POST" action="{{ url($module_prefix.'/'.$module)}}" onsubmit="return false" id="form" class="myform">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<div class="col-md-12">
				<div class="box-1 button-style-wrapper">
					<a class="btn btn-xs btn-info btn-savebg btn_new" ><i class="fa fa-save"></i> New</a>
					<a class="btn btn-xs btn-info btn-savebg btn_save savejobgrade hidden" id="btn_savebenefits"><i class="fa fa-save"></i> Save</a>
					<!-- <a class="btn btn-xs btn-success btn-editbg btn_edit"><i class="fa fa-edit"></i> Edit</a> -->
					<a class="btn btn-xs btn-danger btn_cancel"> Cancel</a>
				</div>
			</div>
			<div class="benefits-content style-box2">
				<div class="col-md-12">
					<div class="col-md-6">
						
						<table class="table borderless" style="border: none;">
							<tr>
								<td><label>Job Grade</label></td>
								<td>
									<input type="text" name="job_grade" id="job_grade" class="form-control font-style2">
								</td>
							</tr>
							<tr>
								<td><label>Step Increment</label></td>
								<td>
									<select name="step_inc" id="step_inc" class="form-control font-style2">
										<option value=""></option>
										<option value="step1">Step 1</option>
										<option value="step2">Step 2</option>
										<option value="step3">Step 3</option>
										<option value="step4">Step 4</option>
										<option value="step5">Step 5</option>
										<option value="step6">Step 6</option>
										<option value="step7">Step 7</option>
										<option value="step8">Step 8</option>
									</select>
								</td>
							</tr>
							<tr>
								<td><label>Amount</label></td>
								<td>
									<input type="text" name="amount" id="amount" class="form-control font-style2 onlyNumber"/>
										
								</td>
						</table>
					</div>
					<div class="col-md-6">
						<table class="table borderless" style="border: none;">
							<tr>
								<td><label>Effectivity Date</label></td>
								<td>
									<input type="text" name="effectivity_date" id="effectivity_date" class="form-control font-style2"/>
										
								</td>
							</tr>
						</table>
					</div>
				</div>
			</div>
			
		</form>
		
	</div>
</div>
@endsection

@section('js-logic2')
<script type="text/javascript">
	$(document).ready(function(){
		
		$('.benefits-content :input').attr("disabled",true);
		$('.btn_new').on('click',function(){
			$('.benefits-content :input').attr("disabled",false);
			$('.btn_new').addClass('hidden');
			$('.btn_edit').addClass('hidden');
			$('.btn_save').removeClass('hidden');
		});
		$('.btn_edit').on('click',function(){
			$('.benefits-content :input').attr("disabled",false);
			$('.btn_edit').addClass('hidden');
			$('.btn_new').addClass('hidden');
			$('.btn_save').removeClass('hidden');
		});
		$('.btn_cancel').on('click',function(){
			$('.benefits-content :input').attr("disabled",true);
			$('.btn_new').removeClass('hidden');
			$('.btn_save').addClass('hidden');
			$('.btn_edit').removeClass('hidden');
		});

		$('.onlyNumber').keypress(function (event) {
	    	return isNumber(event, this)
		});

		$(".onlyNumber").keyup(function(){
			amount  = $(this).val();
			if(amount == 0){
				$(this).val('');
			}else{
				plainAmount = amount.replace(/\,/g,'')
				$(this).val(commaSeparateNumber(plainAmount));
			}
		})


		$('#effectivity_date').datepicker({
			dateFormat:'yy-mm-dd'
		})

		$(".btn_cancel").click(function() {
			myform = "myform";
			clear_form_elements(myform);
			$('.error-msg').remove();
		});

		var btn;
		/*serialize All form ON SUBMIT*/
		$(document).off('click',".savejobgrade").on('click',".savejobgrade",function(){
				btn = $(this);

				$("#form").ajaxForm({
					beforeSend:function(){
						
					},
					success:function(data){
						par  =  JSON.parse(data);
						if(par.status){	

							swal({  title: par.response,   
									text: '',   
									type: "success",   
									icon: 'success',
									
								}).then(function(){
									$('.sub-panel').load(base_url+module_prefix+module+'/datatable.blade.php');
									// window.location.href = base_url+module_prefix+module;
									
								});

						}else{

							swal({  title: par.response,   
									text: '',   
									type: "error",   
									icon: 'error',
									
								});

						}

						btn.button('reset');
					},
					error:function(data){
						$error = data.responseJSON;
						/*reset popover*/	
						$('input[type="text"], select').popover('destroy');

						/*add popover*/
						block = 0;
						$(".error-msg").remove();
						$.each($error,function(k,v){
							var messages = v.join(', ');
							msg = '<div class="error-msg err-'+k+'" style="color:red;"><i class="fa fa-exclamation-triangle" style="color:rgb(255, 184, 0);"></i> '+messages+'</div>';
							$('input[name="'+k+'"], textarea[name="'+k+'"], select[name="'+k+'"]').after(msg).attr('data-content',messages);
							if(block == 0){
								$('html, body').animate({
							        scrollTop: $('.err-'+k).offset().top - 250
							    }, 500);
							    block++;
							}
						})
						$('.saving').replaceWith(btn);     
					},
					always:function(){
						setTimeout(function(){
								$('.saving').replaceWith(btn);     
							},300) 
					}
				}).submit();

		});


	})
</script>
@endsection