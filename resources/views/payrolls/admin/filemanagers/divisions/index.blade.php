@extends('app-filemanager')

@section('filemanager-content')
<!-- <div style="height: 50px;"></div> -->
<!-- <div class="col-md-12"> -->
	<div class="panel panel-default">
		<div class="panel-heading">{{ $title }}</div>
		<div class="panel-body">
			<div class="row">
				<div class="col-md-12">
					<div class="sub-panel">
						{!! $controller->show() !!}
					</div>
				</div>
			</div>
			<div class="row">
				<div class="button-wrapper" style="position: relative;top: -20px;margin-left: 7px;">
					<a class="btn btn-xs btn-info btn-savebg btn_new" id="newDivisions" data-btnnew="newDivisions" data-btncancel="cancelDivisions" data-btnedit="editDivisions" data-btnsave="saveDivisions"><i class="fa fa-save"></i> New</a>

					<a class="btn btn-xs btn-info btn-editbg btn_edit hidden" id="editDivisions" data-btnnew="newDivisions" data-btncancel="cancelDivisions" data-btnedit="editDivisions" data-btnsave="saveDivisions"><i class="fa fa-save"></i> Edit</a>

					<a class="btn btn-xs btn-info btn-savebg btn_save submit hidden" data-form="form" data-btnnew="newDivisions" data-btncancel="cancelDivisions" data-btnedit="editDivisions" data-btnsave="saveDivisions" id="saveDivisions"><i class="fa fa-save"></i> Save</a>

					<a class="btn btn-xs btn-danger btn_cancel hidden" data-btnnew="newDivisions" data-btncancel="cancelDivisions" data-form="myform" data-btnedit="editDivisions" data-btnsave="saveDivisions"id="cancelDivisions"> Cancel</a>
				</div>
				<div class="col-md-4">
					<form method="POST" action="{{ url($module_prefix.'/'.$module) }}" onsubmit="return false" id="form">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<input type="hidden" name="division_id" id="division_id">
						<div class="form-group newDivisions">
							<label>Code</label>
							<input type="text" name="code" id="code" class="form-control">
						</div>
						<div class="form-group newDivisions">
							<label>Name</label>
							<input type="text" name="name" id="name" class="form-control">
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<br>
<!-- </div> -->

@endsection
@section('js-logic2')
<script type="text/javascript">
$(document).ready(function(){
$('.newDivisions :input').attr('disabled',true);
$('.newDivisions').attr('disabled',true);
$('.btn_new').on('click',function(){
	btnnew = $(this).data('btnnew');
	btnsave = $(this).data('btnsave');
	btncancel = $(this).data('btncancel');
	$('.'+btnnew+' :input').attr("disabled",false);
	$('.'+btnnew).attr('disabled',false);
	$('#'+btnnew).addClass('hidden');
	$('#'+btnsave).removeClass('hidden');
	$('#'+btncancel).removeClass('hidden');
});

$('.btn_edit').on('click',function(){
	btnnew = $(this).data('btnnew');
	btnsave = $(this).data('btnsave');
	btncancel = $(this).data('btncancel');
	btnedit = $(this).data('btnedit');
	$('.'+btnnew+' :input').attr("disabled",false);
	$('.'+btnnew).attr('disabled',false);
	$('#'+btnnew).addClass('hidden');
	$('#'+btnedit).addClass('hidden');
	$('#'+btnsave).removeClass('hidden');
	$('#'+btncancel).removeClass('hidden');
});

$('.btn_cancel').on('click',function(){
	btnnew = $(this).data('btnnew');
	btnsave = $(this).data('btnsave');
	btncancel = $(this).data('btncancel');
	btnedit = $(this).data('btnedit');

	$('.'+btnnew+' :input').attr("disabled",true);
	$('.'+btnnew).attr('disabled',true);
	$('#'+btnnew).removeClass('hidden');
	$('#'+btnedit).addClass('hidden');
	$('#'+btnsave).addClass('hidden');
	$('#'+btncancel).addClass('hidden');

	form = $(this).data('form');
	clear_form_elements(form);
	$('.error-msg').remove();
});


})
</script>
@endsection