@extends('app-filemanager')

@section('filemanager-content')
<div id="benefits" class="benefits-wrapper">
	<div class="col-md-12" style="padding-bottom: 10px;">
		<div class="col-md-6">
			<label>BENEFITS</label>
		</div>

	</div>

	<div class="sub-panel" style="z-index: 1;">

		{!! $controller->show() !!}

	</div>
	<div class="formcontent">
		<form method="POST" action="{{ url($module_prefix.'/'.$module)}}" onsubmit="return false" id="form" class="myform">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<input type="hidden" name="for_update" id="for_update">
			<div class="box-1 button-style-wrapper" style="z-index: 2">
				<div class="col-md-12">
					<a class="btn btn-xs btn-info btn-savebg btn_new" id="btn_savebenefits"><i class="fa fa-save"></i> New</a>
					<a class="btn btn-xs btn-info btn-savebg btn_save hidden submit"><i class="fa fa-save"></i> Save </a>
					<a class="btn btn-xs btn-success btn-editbg btn_edit"><i class="fa fa-edit"></i> Edit</a>
					<a class="btn btn-xs btn-danger btn_cancel"> Cancel</a>
				</div>
			</div>

			<div class="benefits-content style-box2">
				<div class="col-md-12">
					<div class="col-md-7">
						<table class="table borderless" style="border: none;">
							<tr>
								<td><label>Code</label></td>
								<td>
									@if ($errors->has('code'))
									    <span class="text-danger">{{ $errors->first('code') }}</span>
									@endif
									<input type="text" name="code" id="code" class="form-control font-style2"  >

								</td>
							</tr>
							<tr>
								<td><label>Name</label></td>
								<td>
									@if ($errors->has('name'))
									    <span class="text-danger">{{ $errors->first('name') }}</span>
									@endif
									<input type="text" name="name" id="name" class="form-control font-style2">
								</td>
							</tr>
							<tr>
								<td><label>Type</label></td>
								<td>
									@if ($errors->has('tax_type'))
									    <span class="text-danger">{{ $errors->first('tax_type') }}</span>
									@endif
									<select name="tax_type" id="tax_type" class="form-control font-style2">
										<option value=""></option>
										<option value="Non Taxable">Non Taxable</option>
										<option value="Taxable">Taxable</option>
										<option value="Taxable in excess of threshold">Taxable in excess of threshold</option>
									</select>
								</td>
							</tr>
					<!-- 		<tr>
								<td><label>De Minimis Type</label></td>
								<td>
									@if ($errors->has('de_minimis_type'))
									    <span class="text-danger">{{ $errors->first('de_minimis_type') }}</span>
									@endif
									<select name="de_minimis_type" id="de_minimis_type" class="form-control font-style2">
										<option></option>
										<option value="NA">NA</option>
										<option value="Monetized unused 10 VL/SL - government">Monetized unused 10 VL/SL - government</option>
										<option value="Medical Cash Allowance">Medical Cash Allowance</option>
										<option value="Rice Subsidy">Rice Subsidy</option>
										<option value="Uniform and clothing allowance">Uniform and clothing allowance</option>
										<option value="Actual medical assistance">Actual medical assistance</option>
										<option value="Laundry Allowance">Laundry Allowance</option>
									</select>
								</td>
							</tr> -->
							<tr>
								<td><label>Computation Type</label></td>
								<td>
									@if ($errors->has('computation_type'))
									    <span class="text-danger">{{ $errors->first('computation_type') }}</span>
									@endif
									<select name="computation_type" id="computation_type" class="form-control font-style2">
										<option></option>
										<option value="Based on Attendance">Based on Attendance</option>
										<option value="Fixed Monthly">Fixed Monthly</option>
									</select>
								</td>
							</tr>
							<tr>
								<td><label>Amount</label></td>
								<td>
									<input type="text" name="amount" id="amount" class="form-control onlyNumber">
								</td>
							</tr>
							<tr>
								<td><label>Remarks</label></td>
								<td>
									<input type="text" name="remarks" id="remarks" class="form-control">
								</td>
							</tr>
							<tr>
							<!-- <tr>
								<td><label>Payroll Group</label></td>
								<td>
									@if ($errors->has('payroll_group'))
									    <span class="text-danger">{{ $errors->first('payroll_group') }}</span>
									@endif
									<select name="payroll_group" id="payroll_group" v-model="form.payroll_group" class="form-control font-style2">
										<option></option>
										<option value="Bonuses and Cash Gifts">Bonuses and Cash Gifts</option>
										<option value="General Payroll">General Payroll</option>
									</select>
								</td>
							</tr> -->


						</table>
					</div>
					<div class="col-md-5">
						<!-- <div class="box-1">
							<label>Reporting</label>
							<table class="table borderless" >
								<tr>
									<td><label>ITR Classifications</label></td>
									<td>
										@if ($errors->has('itr_classification'))
									    	<span class="text-danger">{{ $errors->first('itr_classification') }}</span>
										@endif
										<select name="itr_classification" id="itr_classification" class="form-control font-style2">
											<option></option>
											<option value="13th Month Pay and Other Benefits">13th Month Pay and Other Benefits</option>
											<option value="Basic Salary">Basic Salary</option>
											<option value="Commission">Commission</option>
											<option value="Cost of Living Allowance">Cost of Living Allowance</option>
											<option value="De Minimis">De Minimis</option>
											<option value="Fees Including Director Fee">Fees Including Director Fee</option>
											<option value="Fixed Housing Allowance">Fixed Housing Allowance</option>
											<option value="Hazard Pay">Hazard Pay</option>
											<option value="Profit Sharing">Profit Sharing</option>
											<option value="Representation">Representation</option>
											<option value="Salaries and Other Forms of Compensation">Salaries and Other Forms of Compensation</option>
										</select>
									</td>
								</tr>
								<tr>
									<td><label>Alphalist Classifications</label></td>
									<td>
										@if ($errors->has('alphalist_classification'))
									    	<span class="text-danger">{{ $errors->first('alphalist_classification') }}</span>
										@endif
										<select name="alphalist_classification" id="alphalist_classification"  class="form-control font-style2">
											<option></option>
											<option value="13th Month Pay and Other Benefits">13th Month Pay and Other Benefits</option>
											<option value="Basic Salary">Basic Salary</option>
											<option value="Contributions and Union Dues">Contributions and Union Dues</option>
											<option value="De Minimis">De Minimis</option>
											<option value="Hazard Pay">Hazard Pay</option>
											<option value="Non-Payroll">Non-Payroll</option>
											<option value="Salaries and Other Forms of Compensation">Salaries and Other Forms of Compensation</option>
										</select>
									</td>
								</tr>
							</table>
						</div> -->
						<!-- <div class="box-1">
							<label>Include in Computation</label>
							<table class="table borderless">
								<tr>
									<td>
										<input type="checkbox" name="chk_benefitssss" id="chk_benefitssss">
										<label>SSS</label>
									</td>
									<td>
										<input type="checkbox" name="chk_benefitgsis" id="chk_benefitgsis">
										<label>GSIS</label>
									</td>
									<td>
										<input type="checkbox" name="chk_benefitpagibig" id="chk_benefitpagibig">
										<label>Pagibig</label>
									</td>
									<td>
										<input type="checkbox" name="chk_benefitphilhealth" id="chk_benefitphilhealth">
										<label>Philhealth</label>
									</td>
								</tr>
								<tr>
									<td colspan="2">
										<input type="checkbox" name="chk_benefitbasicpay" id="chk_benefitbasicpay">
										<label>Basic Salary</label>
									</td>
									<td colspan="2">
										<input type="checkbox" name="chk_benefitsovertime" id="chk_benefitsovertime">
										<label>Overtime</label>
									</td>

								</tr>
								<tr>
									<td colspan="2">
										<input type="checkbox" name="chk_benefits13month" id="chk_benefits13month">
										<label>13 Month Pay</label>
									</td>
									<td colspan="2">
										<input type="checkbox" name="chk_benefitsannualized" id="chk_benefitsannualized">
										<label>Annualized(Asumed)</label>
									</td>
								</tr>
							</table>
						</div> -->
					</div>
				</div>

			</div>
		</form>

	</div>

</div>


@endsection
@section('js-logic2')
<!-- <script src="{{ asset('js/payroll-vue/benefits.js') }} "></script> -->
<script type="text/javascript">
	$(document).ready(function(){

		$('.benefits-content :input').attr("disabled",true);
		$('.btn_new').on('click',function(){
			$('.benefits-content :input').attr("disabled",false);
			$('.btn_new').addClass('hidden');
			$('.btn_edit').addClass('hidden');
			$('.btn_save').removeClass('hidden');
		});
		$('.btn_edit').on('click',function(){
			$('.benefits-content :input').attr("disabled",false);
			$('.btn_edit').addClass('hidden');
			$('.btn_new').addClass('hidden');
			$('.btn_save').removeClass('hidden');
		});
		$('.btn_cancel').on('click',function(){
			$('.benefits-content :input').attr("disabled",true);
			$('.btn_new').removeClass('hidden');
			$('.btn_save').addClass('hidden');
			$('.btn_edit').removeClass('hidden');
		});


		$(".btn_cancel").click(function() {
			myform = "myform";
			clear_form_elements(myform);
			$('#for_update').val('');
			$('.error-msg').remove();
		});

		$('#tbl_benefits tr').on('click',function(){
			var id = 0;
			id = $(this).data('id');
			$.ajax({
				url:base_url+module_prefix+module+'/getItem',
				data:{'id':id},
				type:'GET',
				dataType:'JSON',
				success:function(data){

					$('#code').val(data.code);
					$('#name').val(data.name);
					$('#tax_type').val(data.tax_type);
					$('#de_minimis_type').val(data.de_minimis_type);
					$('#computation_type').val(data.computation_type);
					$('#amount').val(data.amount);
					$('#payroll_group').val(data.payroll_group);
					$('#itr_classification').val(data.itr_classification);
					$('#alphalist_classification').val(data.alphalist_classification);
					$('#for_update').val(data.id);
				}
			})
		});

	    $('.onlyNumber').keypress(function (event) {
	    	return isNumber(event, this)
		});

		$(".onlyNumber").keyup(function(){
			amount  = $(this).val();
			if(amount == 0){
				$(this).val('');
			}else{
				plainAmount = amount.replace(/\,/g,'')
				$(this).val(commaSeparateNumber(plainAmount));
			}
		})

	});

</script>
@endsection

