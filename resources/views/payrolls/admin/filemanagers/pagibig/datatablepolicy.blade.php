<div class="col-md-12">
	<table class="table datatable" id="pagibigpolicy">
		<thead>
			<tr>
				<th>Policy Name</th>
				<th>Deduction Period</th>
				<th>Policy Type</th>
				<th>Based On</th>
			</tr>
		</thead>
		<tbody>
				@foreach($data as $value)
				<tr data-id="{{ $value->id}}">
					<td>{{ $value->policy_name }}</td>
					<td>{{ $value->deduction_period }}</td>
					<td>{{ $value->policy_type }}</td>
					<td>{{ $value->based_on}}</td>
				</tr>
				@endforeach
			
		</tbody>
	</table>
</div>
<script type="text/javascript">
$(document).ready(function(){

	var table = $('#pagibigpolicy').DataTable({
		'dom':'<lf<t>pi>',
		"paging": false,
	 	"scrollY":"250px",
        "scrollCollapse": true,
	});

	$('#pagibigpolicy tbody').on( 'click', 'tr', function () {
	    if ( $(this).hasClass('selected') ) {

	        $(this).removeClass('selected');

	        rows = table.rows(0).data();

	        _code = rows[0][0];


	    }
	    else {
	        table.$('tr.selected').removeClass('selected');
	        $(this).addClass('selected');
	    }
	});

	$('#button').click( function () {
	    table.row('.selected').remove().draw( false );
	});

	$('#pagibigpolicy tr').on('click',function(){
		var id = 0;	
		id = $(this).data('id');
		$.ajax({
			url:base_url+module_prefix+module+'/getItem',
			data:{'id':id},
			type:'GET',
			dataType:'JSON',
			success:function(data){

				$('#policy_name').val(data.policy_name);
				$('#pay_period').val(data.pay_period);
				$('#deduction_period').val(data.deduction_period);
				$('#policy_type').val(data.policy_type);
				$('#based_on').val(data.based_on);
				
				$('#for_update').val(data.id);
			}
		})
	});

})
</script>
