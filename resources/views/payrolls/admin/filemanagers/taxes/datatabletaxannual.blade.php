<div class="col-md-12">
	<table class="table datatable" id="taxannual">
		<thead>
			<tr>
				<th>Annual Tax Below</th>
				<th>Annual Tax Above</th>
				<th>Rate</th>
			</tr>
		</thead>
		<tbody>
				@foreach($data as $value)
				<tr data-id="{{ $value->id }}">
					<td>{{ $value->below }}</td>
					<td>{{ $value->above }}</td>
					<td>{{ $value->rate }}</td>
					
				</tr>
				@endforeach
			
		</tbody>
	</table>
	
</div>
<script type="text/javascript">
$(document).ready(function(){

	var table = $('#taxannual').DataTable({
		'dom':'<lf<t>pi>',
		"paging": false,
	 	// "scrollY":"250px",
   //      "scrollCollapse": true,
	});

	$('#taxannual tbody').on( 'click', 'tr', function () {
	    if ( $(this).hasClass('selected') ) {

	        $(this).removeClass('selected');

	        rows = table.rows(0).data();

	        _code = rows[0][0];


	    }
	    else {
	        table.$('tr.selected').removeClass('selected');
	        $(this).addClass('selected');
	    }
	});

	$('#button').click( function () {
	    table.row('.selected').remove().draw( false );
	});

	$('#taxannual tr').on('click',function(){
		var id = 0;	
		id = $(this).data('id');
		$.ajax({
			url:base_url+module_prefix+module+'/getTaxannual',
			data:{'id':id},
			type:'GET',
			dataType:'JSON',
			success:function(data){

				$('#below').val(data.below);
				$('#annual_effectivity_date').val(data.effectivity_date);
				$('#above').val(data.above);
				$('#rate').val(data.rate);
				
				$('#update_taxannual').val(data.id);
			}
		})
	});

})
</script>
