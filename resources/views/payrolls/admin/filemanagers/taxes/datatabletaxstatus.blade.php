<div class="col-md-12">
	<table class="table datatable" id="taxesstatus">
		<thead>
			<tr>
				<th>Tax Status</th>
				<th>Description</th>
				<th>Exemption</th>
			</tr>
		</thead>
		<tbody>
				@foreach($data as $value)
				<tr>
					<td>{{ $value->tax_status }}</td>
					<td>{{ $value->description }}</td>
					<td>{{ $value->exemption }}</td>
				</tr>
				@endforeach
			
		</tbody>
	</table>
	
</div>
<script type="text/javascript">
$(document).ready(function(){

	var table = $('#taxesstatus').DataTable({
		"paging": false,
	 	"scrollY":"250px",
        "scrollCollapse": true,
	});

	$('#taxesstatus tbody').on( 'click', 'tr', function () {
	    if ( $(this).hasClass('selected') ) {

	        $(this).removeClass('selected');

	        rows = table.rows(0).data();

	        _code = rows[0][0];


	    }
	    else {
	        table.$('tr.selected').removeClass('selected');
	        $(this).addClass('selected');
	    }
	});

	$('#button').click( function () {
	    table.row('.selected').remove().draw( false );
	});

	$('#taxesstatus tr').on('click',function(){
		var id = 0;	
		id = $(this).data('id');
		$.ajax({
			url:base_url+module_prefix+module+'/getItem',
			data:{'id':id},
			type:'GET',
			dataType:'JSON',
			success:function(data){

				$('#tax_status').val(data.tax_status);
				$('#description').val(data.description);
				$('#exemption').val(data.exemption);
				
				$('#update_taxstatus').val(data.id);
			}
		})
	});

})
</script>
