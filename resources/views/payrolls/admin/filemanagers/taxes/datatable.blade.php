<div class="col-md-12">
	<table class="table  disableinput">
		<tr>
			<td><label>DAILY</label></td>
			<td>1</td>
			<td>2</td>
			<td>3</td>
			<td>4</td>
			<td>5</td>
			<td>6</td>
		</tr>
		<tr>
			<td><label>Compensation Level (CL)</label></td>
			<td>685 and below</td>
			<td>685</td>
			<td>1,096</td>
			<td>2,192</td>
			<td>5,479</td>
			<td>21,918</td>
		</tr>
		<tr>
			<td><label>Prescribed Minimum Withholding Tax</label> </td>
			<td>
				<input type="text" name="daily[]" id="daily0"  class="form-control font-style2">

			</td>
			<td>
				<input type="text" name="daily[]" id="daily1" class="form-control font-style2">
				<br>+20% over CL
			</td>
			<td>
				<input type="text" name="daily[]" id="daily2" class="form-control font-style2 onlyNumber"><br>+25% over CL
			</td>
			<td>
				<input type="text" name="daily[]" id="daily3" class="form-control font-style2 onlyNumber"><br>+30% over CL
			</td>
			<td>
				<input type="text" name="daily[]" id="daily4" class="form-control font-style2 onlyNumber"><br>+32% over CL
			</td>
			<td>
				<input type="text" name="daily[]" id="daily5" class="form-control font-style2 onlyNumber"><br>+35% over CL
			</td>
		</tr>
		<tr>
			<td><label>WEEKLY</label></td>
			<td>1</td>
			<td>2</td>
			<td>3</td>
			<td>4</td>
			<td>5</td>
			<td>6</td>
		</tr>
		<tr>
			<td><label>Compensation Level (CL)</label></td>
			<td>4,808 and below</td>
			<td>4,808</td>
			<td>7,692</td>
			<td>15,385</td>
			<td>38,462</td>
			<td>153,846</td>
		</tr>
		<tr>
			<td><label>Prescribed Minimum Withholding Tax</label> </td>
			<td><input type="text" name="weekly[]" id="weekly0" class="form-control font-style2 "></td>
			<td>
				<input type="text" name="weekly[]" id="weekly1" class="form-control font-style2 ">
				<br>+20% over CL
			</td>
			<td>
				<input type="text" name="weekly[]" id="weekly2" class="form-control font-style2 onlyNumber">
				<br>+25% over CL
			</td>
			<td>
				<input type="text" name="weekly[]" id="weekly3" class="form-control font-style2 onlyNumber">
				<br>+30% over CL
			</td>
			<td>
				<input type="text" name="weekly[]" id="weekly4" class="form-control font-style2 onlyNumber">
				<br>+32% over CL
			</td>
			<td>
				<input type="text" name="weekly[]" id="weekly5" class="form-control font-style2 onlyNumber">
				<br>+35% over CL
			</td>
		</tr>
		<tr>
			<td><label>SEMI-MONTHLY</label></td>
			<td>1</td>
			<td>2</td>
			<td>3</td>
			<td>4</td>
			<td>5</td>
			<td>6</td>
		</tr>
		<tr>
			<td><label>Compensation Level (CL)</label></td>
			<td>10,047 and below</td>
			<td>10,047</td>
			<td>16,667</td>
			<td>33,333</td>
			<td>83,333</td>
			<td>333,333</td>
		</tr>
		<tr>
			<td><label>Prescribed Minimum Withholding Tax</label> </td>
			<td>
				<input type="text" name="semimonthly[]" id="semimonthly0" class="form-control font-style2 ">

			</td>
			<td>
				<input type="text" name="semimonthly[]" id="semimonthly1" class="form-control font-style2 ">
				<br>+20% over CL
			</td>
			<td>
				<input type="text" name="semimonthly[]" id="semimonthly2" class="form-control font-style2 onlyNumber">
				<br>+25% over CL
			</td>
			<td>
				<input type="text" name="semimonthly[]" id="semimonthly3" class="form-control font-style2 onlyNumber">
				<br>+30% over CL
			</td>
			<td>
				<input type="text" name="semimonthly[]" id="semimonthly4" class="form-control font-style2 onlyNumber">
				<br>+32% over CL
			</td>
			<td>
				<input type="text" name="semimonthly[]" id="semimonthly5" class="form-control font-style2 onlyNumber">
				<br>+35% over CL
			</td>
		</tr>
		<tr>
			<td><label>MONTHLY</label></td>
			<td>1</td>
			<td>2</td>
			<td>3</td>
			<td>4</td>
			<td>5</td>
			<td>6</td>
		</tr>
		<tr>
			<td><label>Compensation Level (CL)</label></td>
			<td>20,833 and below</td>
			<td>20,833</td>
			<td>33,333</td>
			<td>66,667</td>
			<td>166,667</td>
			<td>666,667</td>
		</tr>
		<tr>
			<td><label>Prescribed Minimum Withholding Tax</label> </td>
			<td><input type="text" name="monthly[]" id="monthly0" class="form-control font-style2 "></td>
			<td>
				<input type="text" name="monthly[]" id="monthly1" class="form-control font-style2 ">
				<br>+20% over CL
			</td>
			<td>
				<input type="text" name="monthly[]" id="monthly2" class="form-control font-style2 onlyNumber">
				<br>+25% over CL
			</td>
			<td>
				<input type="text" name="monthly[]" id="monthly3" class="form-control font-style2 onlyNumber">
				<br>+30% over CL
			</td>
			<td>
				<input type="text" name="monthly[]" id="monthly4" class="form-control font-style2 onlyNumber">
				<br>+32% over CL
			</td>
			<td>
				<input type="text" name="monthly[]" id="monthly5" class="form-control font-style2 onlyNumber">
				<br>+35% over CL
			</td>
		</tr>
	</table>

</div>
<script type="text/javascript">
$(document).ready(function(){

	data = <?php echo $taxtable ?>;

	$('#effectivity_date').val(data[0].effectivity_date);
	
	$('#daily0').val(data[0].salary_bracket_level1);
	$('#daily1').val(data[0].salary_bracket_level2);
	$('#daily2').val(data[0].salary_bracket_level3);
	$('#daily3').val(data[0].salary_bracket_level4);
	$('#daily4').val(data[0].salary_bracket_level5);
	$('#daily5').val(data[0].salary_bracket_level6);

	$('#weekly0').val(data[3].salary_bracket_level1);
	$('#weekly1').val(data[3].salary_bracket_level2);
	$('#weekly2').val(data[3].salary_bracket_level3);
	$('#weekly3').val(data[3].salary_bracket_level4);
	$('#weekly4').val(data[3].salary_bracket_level5);
	$('#weekly5').val(data[3].salary_bracket_level6);

	$('#semimonthly0').val(data[2].salary_bracket_level1);
	$('#semimonthly1').val(data[2].salary_bracket_level2);
	$('#semimonthly2').val(data[2].salary_bracket_level3);
	$('#semimonthly3').val(data[2].salary_bracket_level4);
	$('#semimonthly4').val(data[2].salary_bracket_level5);
	$('#semimonthly5').val(data[2].salary_bracket_level6);

	$('#monthly0').val(data[1].salary_bracket_level1);
	$('#monthly1').val(data[1].salary_bracket_level2);
	$('#monthly2').val(data[1].salary_bracket_level3);
	$('#monthly3').val(data[1].salary_bracket_level4);
	$('#monthly4').val(data[1].salary_bracket_level5);
	$('#monthly5').val(data[1].salary_bracket_level6);


	
})
</script>
