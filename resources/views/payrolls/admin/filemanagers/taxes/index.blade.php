@extends('app-filemanager')

@section('filemanager-content')
<div id="tax" class="benefits-wrapper">
	<div class="col-md-12" style="padding-bottom: 10px;">
		<div class="col-md-6">
			<label>{{ $title }}</label>
		</div>
		<div class="col-md-6">

		</div>
	</div>

	<div class="tab-container">
		<ul class="nav nav-tabs">
			<li class="active"><a href="#table_tax">Tax Table</a></li>
			<li><a href="#table_taxannual">Annual Tax Table</a></li>
			<li><a href="#policy_tax">Policy</a></li>
		</ul>
	</div>

	<div class="tab-content">
		<div id="table_tax" class="tab-pane fade in active">

			<div class="formcontent">
				<form method="POST" action="{{ url($module_prefix.'/'.$module)}}" onsubmit="return false" id="form" class="myform">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<input type="hidden" name="update_taxtable" id="update_taxtable">
					<div class="col-md-12">
						<div class="box-1 button-style-wrapper" style="margin-top: 15px;margin-bottom: 15px;">
							<a class="btn btn-xs btn-info btn-savebg btn_new" id="btn_savetbltax"><i class="fa fa-save"></i> New</a>
							<a class="btn btn-xs btn-info btn-savebg btn_save submit hidden" id="btn_savebenefits"><i class="fa fa-save"></i> Save</a>
							<!-- <a class="btn btn-xs btn-success btn-editbg btn_edit"><i class="fa fa-edit"></i> Edit</a> -->
							<a class="btn btn-xs btn-danger " id="btnCancelTax"> Cancel</a>
						</div>
					</div>
					<div class="sub-panel" style="margin-top: 50px;" >

						{!! $controller->show() !!}

						<div class="col-md-5">
							<table class="table borderless disableinput" style="border: none;">
								<!-- <tr>
									<td><label>Status</label></td>
									<td>
										@if ($errors->has('status'))
										    <span class="text-danger">{{ $errors->first('status') }}</span>
										@endif
										<input type="text" name="status" id="status" class="form-control font-style2">
									</td>
								</tr> -->
								<tr>
									<td><label>Effectivity Date</label></td>
									<td>
										@if ($errors->has('effectivity_date'))
										    <span class="text-danger">{{ $errors->first('effectivity_date') }}</span>
										@endif
										<input type="text" name="effectivity_date" id="effectivity_date" class="form-control font-style2">
									</td>
								</tr>
							</table>

						</div>

					</div>

				</form>

			</div>
		</div>
		<div id="table_taxannual" class="tab-pane fade in">
			<div class="sub-panel" style="margin-top: 50px;" >

				{!! $controller->showTaxstatusannual() !!}

			</div>
			<div class="formcontent">
				<form method="POST" action="{{ url($module_prefix.'/'.$module.'/storeTaxstatusannual')}}" onsubmit="return false" id="form3" class="myform">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<input type="hidden" name="update_taxannual" id="update_taxannual">
					<div class="col-md-12">
						<div class="box-1 button-style-wrapper">
							<a class="btn btn-xs btn-info btn-savebg btn_new" id="btn_savetbltaxannual"><i class="fa fa-save"></i> New</a>
							<a class="btn btn-xs btn-info btn-savebg btn_save submit3 hidden" id="btn_savebenefits"><i class="fa fa-save"></i> Save</a>
							<a class="btn btn-xs btn-success btn-editbg btn_edit"><i class="fa fa-edit"></i> Edit</a>
							<a class="btn btn-xs btn-danger btn_cancel"> Cancel</a>
						</div>
					</div>
					<div class="benefits-content style-box2">
						<div class="col-md-12">
							<div class="col-md-6">
								<table class="table borderless" style="border: none;">
									<tr>
										<td><label>Effective Date</label></td>
										<td>
											@if ($errors->has('effectivity_date'))
											    <span class="text-danger">{{ $errors->first('effectivity_date') }}</span>
											@endif
											<input type="text" name="effectivity_date" id="annual_effectivity_date" class="form-control font-style2">
										</td>
									</tr>
									<tr>
										<td><label>Below</label></td>
										<td>
											@if ($errors->has('below'))
											    <span class="text-danger">{{ $errors->first('below') }}</span>
											@endif
											<input type="text" name="below" id="below" class="form-control font-style2">
										</td>
									</tr>
									<tr>
										<td><label>Above</label></td>
										<td>
											@if ($errors->has('above'))
											    <span class="text-danger">{{ $errors->first('above') }}</span>
											@endif
											<input type="text" name="above" id="above" class="form-control font-style2">
										</td>
									</tr>
									<tr>
										<td><label>Rate</label></td>
										<td>
											@if ($errors->has('rate'))
											    <span class="text-danger">{{ $errors->first('rate') }}</span>
											@endif
											<input type="text" name="rate" id="rate" class="form-control font-style2">
										</td>
									</tr>
								</table>
							</div>
							<div class="col-md-6">
							</div>
						</div>
					</div>

				</form>

			</div>
		</div>
		<div id="policy_tax" class="tab-pane fade in">
			<div class="sub-panel" style="margin-top: 50px;" >

				{!! $controller->showTaxpolicy() !!}

			</div>

			<div class="formcontent">
				<form method="POST" action="{{ url($module_prefix.'/'.$module.'/storeTaxpolicy')}}" onsubmit="return false" id="form4" class="myform">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<input type="hidden" name="update_taxpolicy" id="update_taxpolicy">
					<div class="col-md-12">
						<div class="box-1 button-style-wrapper">
							<a class="btn btn-xs btn-info btn-savebg btn_new" id="btn_savepolicytax"><i class="fa fa-save"></i> New</a>
							<a class="btn btn-xs btn-info btn-savebg btn_save submit4 hidden" id="btn_savebenefits"><i class="fa fa-save"></i> Save</a>
							<a class="btn btn-xs btn-success btn-editbg btn_edit"><i class="fa fa-edit"></i> Edit</a>
							<a class="btn btn-xs btn-danger btn_cancel"> Cancel</a>
						</div>
					</div>
					<div class="benefits-content style-box2">
						<div class="col-md-12">
							<div class="col-md-6">
								<table class="table borderless" style="border: none;">
									<tr>
										<td><label>Policy Name</label></td>
										<td>
											@if ($errors->has('policy_name'))
											    <span class="text-danger">{{ $errors->first('policy_name') }}</span>
											@endif
											<input type="text" name="policy_name" id="policy_name" class="form-control font-style2">
										</td>
									</tr>
									<tr>
										<td><label>Pay Period</label></td>
										<td>
											@if ($errors->has('pay_period'))
											    <span class="text-danger">{{ $errors->first('pay_period') }}</span>
											@endif
											<select name="pay_period" id="pay_period" class="form-control font-style2">
												<option value=""></option>
												<option value="Semi-Monthly">Semi-Monthly</option>
												<option value="Monthly">Monthly</option>
											</select>
										</td>
									</tr>
									<tr>
										<td><label>Deduction Period</label></td>
										<td>
											@if ($errors->has('deduction_period'))
											    <span class="text-danger">{{ $errors->first('deduction_period') }}</span>
											@endif
											<select name="deduction_period" id="deduction_period" class="form-control font-style2">
												<option value=""></option>
												<option value="Both">Both</option>
												<option value="First Half">First Half</option>
												<option value="Second Half">Second Half</option>
											</select>
										</td>
									</tr>
									<tr>
										<td><label>Policy Type</label></td>
										<td>
											@if ($errors->has('policy_type'))
											    <span class="text-danger">{{ $errors->first('policy_type') }}</span>
											@endif
											<select name="policy_type" id="policy_type" class="form-control font-style2">
												<option value=""></option>
												<option value="System Generated">System Generated</option>
												<option value="Inputted">Inputted</option>

											</select>
										</td>
									</tr>
									<tr>
										<td><label>Job Grade Rate</label></td>
										<td>
											@if ($errors->has('job_grade_rate'))
											    <span class="text-danger">{{ $errors->first('job_grade_rate') }}</span>
											@endif
											<select name="job_grade_rate" id="job_grade_rate" class="form-control font-style2">
												<option value=""></option>
												<option value=".02">2 %</option>
												<option value=".03">3 %</option>
												<option value=".05">5 %</option>
												<option value=".10">10 %</option>
											</select>
										</td>
									</tr>
								</table>
							</div>
							<div class="col-md-6">
								<table class="table borderless" style="border: none;">
									<tr>
										<td><label>Base On</label></td>
										<td>
											@if ($errors->has('based_on'))
											    <span class="text-danger">{{ $errors->first('based_on') }}</span>
											@endif
											<select name="based_on" id="based_on" class="form-control font-style2">
												<option value=""></option>
												<option value="Gross Salary">Gross Salary</option>
												<option value="Gross Taxable">Gross Taxable</option>
											</select>
										</td>
									</tr>
									<tr>
										<td><label>Computation</label></td>
										<td>
											@if ($errors->has('computation'))
											    <span class="text-danger">{{ $errors->first('computation') }}</span>
											@endif
											<select name="computation" id="computation" class="form-control font-style2">
												<option value=""></option>
												<option value="Gross Taxable">Gross Taxable</option>
											</select>
										</td>
									</tr>
									<tr>
										<td>
											<input type="radio" name="is_withholding" id="chk_monthly" value="Monthly" >
											<label>Monthly</label>

										</td>
									</tr>
									<tr>
										<td>
											<input type="radio" name="is_withholding" id="chk_annual" value="Annual">
											<label>Annualized</label>

										</td>
									</tr>
								</table>
							</div>
						</div>
					</div>

				</form>

			</div>


		</div>


	</div>
</div>
@endsection

@section('js-logic2')
<script type="text/javascript">
	$(document).ready(function(){

		$('.benefits-content :input').attr("disabled",true);

		$('.disableinput :input').attr("disabled",true);
		$('.btn_new').on('click',function(){
			$('.benefits-content :input').attr("disabled",false);
			$('.disableinput :input').attr("disabled",false);
			$('.btn_new').addClass('hidden');
			$('.btn_edit').addClass('hidden');
			$('.btn_save').removeClass('hidden');
		});
		$('.btn_edit').on('click',function(){
			$('.benefits-content :input').attr("disabled",false);
			$('.disableinput :input').attr("disabled",false);
			$('.btn_edit').addClass('hidden');
			$('.btn_new').addClass('hidden');
			$('.btn_save').removeClass('hidden');
		});
		$('.btn_cancel').on('click',function(){
			$('.benefits-content :input').attr("disabled",true);
			$('.disableinput :input').attr("disabled",true);
			$('.btn_new').removeClass('hidden');
			$('.btn_save').addClass('hidden');
			$('.btn_edit').removeClass('hidden');
		});

		$('#btnCancelTax').on('click',function(){
			$('.disableinput :input').attr("disabled",true);
			$('.btn_new').removeClass('hidden');
			$('.btn_save').addClass('hidden');
		})

		$('.nav-tabs a').click(function(){
			$(this).tab('show');
		})

		$('#effectivity_date').datepicker({
			dateFormat:'yy-mm-dd'
		})

		$('#annual_effectivity_date').datepicker({
			dateFormat:'yy-mm-dd'
		})

		$(".btn_cancel").click(function() {
			myform = "myform";
			clear_form_elements(myform);
			$('.error-msg').remove();
			$('#update_taxpolicy').val('');
			$('#update_taxannual').val('');
			$('update_taxtable').val('');
		});

		$('.onlyNumber').keypress(function (event) {
	    	return isNumber(event, this)
		});

		$(".onlyNumber").keyup(function(){
			amount  = $(this).val();
			if(amount == 0){
				$(this).val('');
			}else{
				plainAmount = amount.replace(/\,/g,'')
				$(this).val(commaSeparateNumber(plainAmount));
			}
		})
	})
</script>
@endsection