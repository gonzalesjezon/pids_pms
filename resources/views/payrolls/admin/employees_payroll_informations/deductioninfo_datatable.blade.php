<div class="col-md-12">
	<table class="table table-responsive datatable" id="tbl_deductioninfo">
		<thead>
				<tr>
					<th>Deduction Name</th>
					<th>Amount</th>
					<th>Date Start</th>
					<th>Date End</th>
					<th>Pay Period</th>
				</tr>
			</thead>
			<tbody>
				
			</tbody>
	</table>
	
</div>
<script type="text/javascript">
$(document).ready(function(){

	 var table = $('#tbl_deductioninfo').DataTable({
	 	'dom':'<lf<t>pi>',
	 	"paging": false,
	 	// "scrollY":"250px",
   //      "scrollCollapse": true,
	 });

	$('#tbl_deductioninfo tbody').on( 'click', 'tr', function () {
	    if ( $(this).hasClass('selected') ) {

	        $(this).removeClass('selected');

	        clear_form_elements('myForm5');

	        deduct_info_id 			= $(this).data('id');
			deduct_employee_id		= $(this).data('employeeid');
			_deductionId 			= $(this).data('deductionid');
			_amount 				= $(this).data('amount');
			_dateStart				= $(this).data('datestart');
			_dateEnd				= $(this).data('dateend');
			_payPeriod				= $(this).data('payperiod');
			_dateTerminated			= $(this).data('dateterminated');
			name					= $(this).data('name');
			deduction_rate			= $(this).data('deduction_rate');


			$('#deduction_id').val(_deductionId);
			$('#input_deductamount').val(_amount);
			$('#select_deductperiod').val(_payPeriod);
			$('#date_deductstart').val(_dateStart);
			$('#date_deductterminated').val(_dateTerminated);
			$('#date_deductend').val(_dateEnd);
			$('#deduct_employee_id').val(deduct_employee_id);
			$('#deductinfo_id').val(deduct_info_id);

			switch(name){
				case 'PROVIDENT FUND':
				$('.providentFund').removeClass('hidden');
				$('#deduction_rate').val(deduction_rate);
				break;
				default:
				$('.providentFund').addClass('hidden');
				$('#deduction_rate').val('');
				break;
			}

			btnnew = $(this).data('btnnew');
			btnsave = $(this).data('btnsave');
			btnedit = $(this).data('btnedit');
			btncancel = $(this).data('btncancel');
			btndelete = $(this).data('btndelete');

			if(!$('#'+btnsave).is(':visible')){
				$('#'+btnedit).removeClass('hidden');
				$('#'+btncancel).removeClass('hidden');
				$('#'+btndelete).removeClass('hidden');
				$('#'+btnnew).addClass('hidden');
			}

	    }
	    else {
	        table.$('tr.selected').removeClass('selected');
	        $(this).addClass('selected');
	    }
	} );

	$('#button').click( function () {
	    table.row('.selected').remove().draw( false );
	} );

	
})
</script>
