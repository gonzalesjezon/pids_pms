<table class="table scroll datatable" id="namelist">
	<thead>
		<tr>
			<th>ID</th>
			<th>Name</th>
		</tr>
	</thead>
	<tbody >

		@foreach($data as $key => $value)
<!--
			@if($value->with_setup === 1) -->
				<tr data-empid="{{ $value->id }}" data-btnnew="newSalary" data-btnedit="editSalary" data-btnsave="saveSalary" data-btncancel="cancelSalary">
					<td>{{ ucwords(strtolower($value->lastname)) }} {{ ucwords(strtolower($value->firstname)) }} {{ ucwords(strtolower($value->middlename)) }}</td>
				</tr>
<!-- 			@else
				<tr data-empid="{{ $value->id }}" data-btnnew="newSalary" data-btnedit="editSalary" data-btnsave="saveSalary" data-btncancel="cancelSalary">
					<td>{{ ucwords(strtolower($value->lastname)) }} {{ ucwords(strtolower($value->firstname)) }} {{ ucwords(strtolower($value->middlename)) }}</td>
				</tr>
		    @endif -->

		@endforeach

	</tbody>
</table>
<script type="text/javascript">
	$(document).ready(function(){
});
</script>