<div class="col-md-12">
	<table class="table table-responsive datatable" id="tbl_loaninfo">
		<thead>
			<tr>
				<th>Loan Name</th>
				<th>Total Loan</th>
				<th>Loan Balance</th>
				<th>Amortization</th>
				<th>Date Start</th>
				<th>Date End</th>
			</tr>
		</thead>
		<tbody>
			
		</tbody>
	</table>
	
</div>
<script type="text/javascript">
$(document).ready(function(){

	 var table = $('#tbl_loaninfo').DataTable({
	 	'dom':'<lf<t>pi>',
	 	"paging": false,
	 	// "scrollY":"250px",
   //      "scrollCollapse": true,
	 });

	$('#tbl_loaninfo tbody').on( 'click', 'tr', function () {
	    if ( $(this).hasClass('selected') ) {

	        $(this).removeClass('selected');

	        clear_form_elements('myForm4');

	        loaninfo_id 	 = $(this).data('id');
			loan_employee_id = $(this).data('employeeid');
			_loanId 		 = $(this).data('loanid');
			_payPeriod		 = $(this).data('payperiod');
			_loanAmount		 = $(this).data('loanamount');
			_loanBal		 = $(this).data('loanbalance');
			_amortization 	 = $(this).data('amortization');
			_dateStart		 = $(this).data('datestart');
			_dateEnd		 = $(this).data('dateend');
			_dateGranted	 = $(this).data('dategranted');
			_dateTerminated	 = $(this).data('dateterminated');


			$('#loan_id').val(_loanId);
			$('#loan_payperiod').val(_payPeriod);
			$('#loan_dategranted').val(_dateGranted);
			$('#loan_datestart').val(_dateStart);
			$('#total_loanamount').val(_loanAmount);
			$('#loan_dateend').val(_dateEnd);
			$('#loan_totalbalance').val(_loanBal);
			$('#loan_amortization').val(_amortization);
			$('#date_dateterminated').val(_dateTerminated);
			$('#loan_employee_id').val(loan_employee_id);
			$('#loaninfo_id').val(loaninfo_id);

			btnnew = $(this).data('btnnew');
			btnsave = $(this).data('btnsave');
			btnedit = $(this).data('btnedit');
			btncancel = $(this).data('btncancel');
			btndelete = $(this).data('btndelete');

			if(!$('#'+btnsave).is(':visible')){
				$('#'+btnedit).removeClass('hidden');
				$('#'+btndelete).removeClass('hidden');
				$('#'+btncancel).removeClass('hidden');
				$('#'+btnnew).addClass('hidden');
			}


	    }
	    else {
	        table.$('tr.selected').removeClass('selected');
	        $(this).addClass('selected');
	    }
	} );

	$('#button').click( function () {
	    table.row('.selected').remove().draw( false );
	} );

})
</script>
