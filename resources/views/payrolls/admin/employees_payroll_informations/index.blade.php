@extends('app-front')

@section('content')
<div class="row">
	<div class="col-md-12">
		<hr>
		<div class="col-md-3">
			<h5 >Filter Employee By</h5>
			<table class="table borderless" style="border:none;margin-bottom: 0px;">
				<tr>
					<td colspan="3">
						<div class="col-md-6">
							<span>Search by:</span>
						</div>
						<div class="col-md-6">
							<select class="search-by form-control font-style2 select2 " id="searchby" name="searchby">
								<option value=""></option>
								<option value="company">Company</option>
								<option value="department">Department</option>
								<option value="office">Offices</option>
								<option value="division">Divisions</option>
								<option value="position">Positions</option>
							</select>
						</div>
					</td>
				</tr>
				<tr>
					<td colspan="3">
						<div class="col-md-6">
							<span>Search value:</span>
						</div>
						<div class="col-md-6">
							<select class="search-value form-control font-style2 select2 " name="select_searchvalue" id="select_searchvalue">
								<option value=""></option>
							</select>
						</div>
					</td>
				</tr>
				<tr>
					<td colspan="3">
						<div class="col-md-6">
							<span>Employment Status:</span>
						</div>
						<div class="col-md-6">
							<select class="form-control font-style2 select2" name="emp_status" id="emp_status">
								<option value=""></option>
								<option value="plantilla">Plantilla</option>
								<option value="nonplantilla">Non Plantilla</option>
							</select>
						</div>
					</td>
				</tr>
				<tr>
					<td colspan="3">
						<div class="col-md-6">
							<span>Employee type:</span>
						</div>
						<div class="col-md-6">
							<select class="employee-type form-control font-style2 select2" name="emp_type" id="emp_type">
								<option value=""></option>
								<option value="active">Active</option>
								<option value="inactive">InActive</option>
							</select>
						</div>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<span>Search</span>
					</td>
					<td>
						<a class="btn btn-xs btn-danger btnfilter" style="float: right;line-height: 16px;margin-bottom: 3px;" ><i class="fa fa-filter"></i>Filter</a>
					</td>
				</tr>
				<tr>
					<td colspan="3">
						<input type="text" name="filter_search" class="form-control search1">
					</td>
				</tr>
			</table>
			<div class="sub-panelnamelist ">
				{!! $controller->show() !!}
			</div>
		</div>
		<div class="col-md-9">
			<div class="newSummary-name">
				<label id="lbl_empname" style="text-transform: uppercase;"></label>
			</div>
			<br>
			<div class="tab-container" id="plantilla">
				<ul class="nav nav-tabs">
					<li class="active"><a href="#empsummary">Summary</a></li>
					<li><a href="#salaryinfo" >Salary Info</a></li>
					<li><a href="#benefitsinfo" class="checkEmployeeinfo">Benefits Info</a></li>
					<li><a href="#loansinfo" class="checkEmployeeinfo">Loans Info</a></li>
					<li><a href="#deducinfo" class="checkEmployeeinfo">Deduction Info</a></li>
				</ul>

				<div class="tab-content">
					<!-- EMPLOYEE SUMMARY -->

					<div id="empsummary" class="tab-pane fade in active">
						<div class="col-md-12">
							<div class="button-wrapper" style="position: relative;top: 10px;left: 5px;" >
								<a class="btn btn-xs btn-info btn-savebg btn_new" id="newSummary" data-btnnew="newSummary" data-btncancel="cancelSummary" data-btnedit="editSummary" data-btnsave="saveSummary"><i class="fa fa-save"></i> New</a>

								<a class="btn btn-xs btn-info btn-editbg btn_edit hidden" id="editSummary" data-btnnew="newSummary" data-btncancel="cancelSummary" data-btnedit="editSummary" data-btnsave="saveSummary"><i class="fa fa-save"></i> Edit</a>

								<a class="btn btn-xs btn-info btn-savebg btn_save submitme hidden" data-form="form" data-btnnew="newSummary" data-btncancel="cancelSummary" data-btnedit="editSummary" data-btnsave="saveSummary" id="saveSummary"><i class="fa fa-save"></i> Save</a>
								<a class="btn btn-xs btn-danger btn_cancel hidden" data-btnnew="newSummary" data-btncancel="cancelSummary" data-form="myform" data-btnedit="editSummary" data-btnsave="saveSummary"id="cancelSummary"> Cancel</a>
							</div>
							<form method="POST" action="{{ url($module_prefix.'/'.$module)}}" onsubmit="return false" id="form" class="myform">
								<input type="hidden" name="_token" value="{{ csrf_token() }}">
								<input type="hidden" name="employeeinfo_id" id="employeeinfo_id" class="employeeinfo_id">
								<input type="hidden" name="with_setup" id="with_setup">
								<div class="col-md-4">
									<div class="border-style02">
										<table class="table borderless">
											<tr>
												<td>
													<span>Employee Status</span>
												</td>
												<td>
													<input type="text" name="empstatus" id="empstatus" class="form-control font-style2 "  readOnly>
												</td>
											</tr>
											<tr>
												<td>
													<span>Employee No.</span>
												</td>
												<td>
													<input type="text" name="emp_no" id="emp_no" class="form-control font-style2" readOnly>
												</td>
											</tr>
											<tr>
												<td>
													<span>BP No.</span>
												</td>
												<td>
													<input type="text" name="bp_no" id="bp_no" class="form-control font-style2 newSummary">
												</td>
											</tr>
										</table>
									</div>
									<div class="border-style02 margintop-25">
										<table class="table borderless">
											<tr>
												<td>
													<span>Position Item No.</span>
												</td>
												<td>
													<input type="text" name="position_itemno" id="position_itemno" class="form-control font-style2" readOnly>
												</td>
											</tr>
											<tr>
												<td>
													<span>Position</span>
												</td>
												<td>
													<input type="text" name="position" id="position" class="form-control font-style2" readOnly>
												</td>
											</tr>
											<tr>
												<td>
													<span>Salary Grade</span>
												</td>
												<td>
													<input type="text" name="salary_grade" id="salary_grade" class="form-control font-style2 _input" readOnly>
												</td>
											</tr>
											<tr>
												<td>
													<span>Step Increment</span>
												</td>
												<td>
													<input type="text" name="input_stepinc" id="input_stepinc" class="form-control font-style2 _input" readOnly>
												</td>
											</tr>
											<tr>
												<td>
													<span>Office</span>
												</td>
												<td>
													<input type="text" name="office" id="office" class="form-control font-style2 _input" readOnly>
												</td>
											</tr>
											<tr>
												<td>
													<span>Division</span>
												</td>
												<td>
													<input type="text" name="division" id="division" class="form-control font-style2 _input" readOnly>
												</td>
											</tr>

											<tr>
												<td>
													<span>Resp Center</span>
												</td>
												<td>
													<select class="form-control font-style2 newSummary customselect" id="select_respcenter _input">
														<option></option>
													</select>
												</td>
											</tr>
										</table>
									</div>

									<div class="col-md-12" style="clear: both;width: 206%;position: relative;top: -32px;left: -4px;">
										<div class="border-style2">
											<table class="table borderless">
												<tr class="text-center">
													<td>Government Policy / Contribution</td>
													<td>Policy</td>
													<td>Contribution</td>
												</tr>
												<tr>
													<td><span>GSIS Policy</span></td>
													<td>
														<select class="font-style2 form-control newSummary customselect" name="gsispolicy_id" id="select_gsispolicy">
															<option value=""></option>
															@foreach($gsis as $value)
															<option data-policytype="{{ $value->policy_type }}" data-percent="{{ $value->ee_percentage }}" data-erpercent="{{ $value->er_percentage }}" value="{{ $value->id }}" >{{ $value->policy_name }}</option>
															@endforeach
														</select>
													</td>
													<td>
														<input type="text" name="gsis_contribution" id="gsis_contribution" class="form-control font-style2 onlyNumber newSummary ">
														<input type="hidden" name="er_gsis_share" id="er_gsis_share">
													</td>
												</tr>
												<tr>
													<td><span>Philhealth Policy</span></td>
													<td>
														<select class="font-style2 form-control newSummary customselect" id="select_philhealthpolicy" name="philhealthpolicy_id">
															<option value=""></option>
															@foreach($philhealth as $value)
															<option value="{{ $value->id }}" data-policytype="{{ $value->policy_type }}" data-above="{{ $value->above }}" data-below="{{ $value->below }}">{{ $value->policy_name }}</option>
															@endforeach
														</select>
													</td>
													<td>
														<input type="text" name="philhealth_contribution" id="philhealth_contribution" class="form-control font-style2 onlyNumber newSummary ">
														<input type="hidden" name="er_philhealth_share" id="er_philhealth_share">
													</td>
												</tr>
												<tr>
													<td><span>Pagibig Policy</span></td>
													<td>
														<select class="font-style2 form-control newSummary customselect" id="select_pagibigbpolicy" name="pagibigpolicy_id">
															<option value=""></option>
															@foreach($pagibig as $value)
															<option value="{{ $value->id }}" data-policytype="{{ $value->policy_type }}">{{ $value->policy_name }}</option>
															@endforeach
														</select>
													</td>
													<td>
														<input type="text" name="pagibig_contribution" id="pagibig_contribution" class="form-control font-style2 onlyNumber newSummary">
														<input type="hidden" name="er_pagibig_share" id="er_pagibig_share">
													</td>
												</tr>
												<tr>
													<td  colspan="2"></td>
													<td>
														<input type="text" name="pagibig_personal" id="personal_share" class="form-control font-style2 onlyNumber newSummary" placeholder="Personal Share">
													</td>
												</tr>
													<td><span>Pagibig II</span></td>
													<td></td>
													<td >
														<input type="text" name="pagibig2" id="pagibig2" class="form-control font-style2 onlyNumber newSummary">
													</td>
												<tr>
													<td><span>Tax Policy</span></td>
													<td>
														<select class="font-style2 form-control newSummary customselect" id="select_taxspolicy" name="taxpolicy_id">
															<option value=""></option>
															@foreach($tax_policy as $value)
															<option data-policytype="{{ $value->policy_type }}" value="{{ $value->id }}" >{{ $value->policy_name }}</option>
															@endforeach
														</select>
													</td>
													<td>
														<input type="text" name="tax_contribution" id="tax_contribution" class="form-control font-style2 onlyNumber" readOnly>
													</td>
												</tr>

											</table>
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="border-style2">
										<table class="table borderless">
											<tr>
												<td>
													<span>Monthly Rate</span>
												</td>
												<td>
													<input type="text" name="monthly_rate_amount" class="form-control font-style2 onlyNumber _input" id="monthly_rate" readOnly />
												</td>
											</tr>
											<tr>
												<td>
													<span>Daily Rate</span>
												</td>
												<td>
													<input type="text" name="daily_rate_amount" class="form-control font-style2 onlyNumber _input" id="daily_rate" readOnly />
												</td>
											</tr>
											<tr>
												<td>
													<span>Weekly Rate</span>
												</td>
												<td>
													<input type="text" name="weekly_rate" class="form-control font-style2 onlyNumber _input" id="weekly_rate" readOnly />
												</td>
											</tr>
											<tr>
												<td>
													<span>Annual Rate</span>
												</td>
												<td>
													<input type="text" name="annual_rate_amount" class="form-control font-style2 onlyNumber _input" id="annual_rate" readOnly />
												</td>
											</tr>

											<tr>
												<td>
													<span>Wages Status</span>
												</td>
												<td>
													<select class="form-control font-style2 newSummary customselect" name="wagestatus_id" id="wagestatus_id">
														<option value=""></option>
														@foreach($wagerate as $value)
														<option value="{{ $value->id }}">{{ $value->wage_region }}</option>
														@endforeach
													</select>
												</td>
											</tr>
											<tr>
												<td>
													<span>Union Dues</span>
												</td>
												<td>
													<select class="form-control font-style2 newSummary customselect" id="select_uniondues">
														<option></option>
													</select>

												</td>
											</tr>

										</table>

									</div>
									<div class="border-style2 margintop-25">
										<table class="table borderless">
											<tr>
												<td class="text-center" colspan="2"><span>For Job Order/Contractual</span></td>
												<td></td>
											</tr>
											<tr>
												<td>
													<span>Assumption to Duty</span>
												</td>
												<td>
													<input type="text" class="form-control font-style2 _input" id="assumption_to_duty" name="assumption_to_duty" readOnly />
												</td>
												<td></td>
											</tr>
											<tr>
												<td>
													<span>End of Contract</span>
												</td>
												<td>
													<input type="text" class="form-control font-style2  _input" id="end_of_contract" name="end_of_contract" readOnly />
												</td>
												<td></td>
											</tr>
										</table>
									</div>
								</div>
								<div class="col-md-4">
									<div class="border-style2 ">
										<table class="table borderless newSummary">
											<tr>
												<td><span>TIN</span></td>
												<td>
													<input type="text" name="tax_id_number" id="tax_id_number" class="form-control font-style2">
												</td>
											</tr>
											<tr>
												<td><span>ATM No</span></td>
												<td>
													<input type="text" name="atm_no" id="atm_no" class="form-control font-style2">
												</td>
											</tr>
											<tr>
												<td>
													<span>Bank</span>
												</td>
												<td>
													<select class="form-control font-style2 customselect"  id="select_bank" name="bank_id">
														<option value=""></option>
														@foreach($bank as $value)
															<option data-bankbranch="{{ $value->branch_name }}" value="{{ $value->id }}"  >{{ $value->name }}</option>
														@endforeach
													</select>
												</td>
											</tr>
											<tr>
												<td>
													<span>Bank Branch</span>
												</td>
												<td>
													<input type="text" class="form-control font-style2 customselect" id="bank_branch" name="bank_branch" readonly />
												</td>
											</tr>
										</table>
									</div>
									<div class="border-style2 margintop-25 newSummary">
										<table class="table borderless">
											<tr>
												<td>
													<span>No. of Days in a Year</span>
												</td>
												<td>
													<input type="text" class="form-control font-style2" id="input_daysinayear" name="no_ofdays_inayear" />
												</td>
											</tr>
											<tr>
												<td>
													<span>No. of Working Days</span>
												</td>
												<td>
													<input type="text" class="form-control font-style2" id="input_daysinamonth" name="no_ofdays_inamonth" />
												</td>
											</tr>
											<tr>
												<td>
													<span>Total hours in a day</span>
												</td>
												<td>
													<input type="text" class="form-control font-style2" id="input_hoursinaday" name="total_hours_inaday"  />

												</td>
											</tr>
											<tr>
												<td>
													<span>Overtime Balance</span>
												</td>
												<td>
													<input type="text" class="form-control font-style2 onlyNumber" id="overtime_balance_amount" name="overtime_balance_amount" />

												</td>
											</tr>
										</table>
									</div>

								<input type="hidden" name="tax_bracket" id="tax_bracket">
								<input type="hidden" name="tax_bracket_amount" id="tax_bracket_amount">
								<input type="hidden" name="tax_inexcess" id="tax_inexcess">
								<input type="hidden" name="employee_id" id="employee_id">
								<input type="hidden" name="c_range" id="c_range">
							</form>
							</div>
						</div>
					</div>

					<!-- EMPLOYEE SUMMARY -->

					<!-- SALARY INFO -->
					<div id="salaryinfo" class="tab-pane fade in ">
							<div class="sub-panel" style="margin-top: 50px;z-index: 1;">
								{!! $controller->showSalaryinfo() !!}
							</div>

							<div class="col-md-12">
								<div class="box-1 button-style-wrapper" >
									<a class="btn btn-xs btn-info btn-savebg btn_new" id="newSalary" data-btnnew="newSalary" data-btnedit="editSalary" data-btnsave="saveSalary" data-btncancel="cancelSalary"><i class="fa fa-save"></i> New</a>
									<a class="btn btn-xs btn-success btn-editbg btn_edit hidden" data-btnnew="newSalary" data-btnedit="editSalary" data-btnsave="saveSalary" data-btncancel="cancelSalary" id="editSalary"><i class="fa fa-edit"></i> Edit</a>
									<a class="btn btn-xs btn-info btn-savebg btn_save submitme hidden" data-btnnew="newSalary" data-btnedit="editSalary" data-btnsave="saveSalary" data-btncancel="cancelSalary" data-form="form2" id="saveSalary"><i class="fa fa-save"></i> Save</a>
									<a class="btn btn-xs btn-danger btn_cancel hidden" data-btnnew="newSalary" data-btnedit="editSalary" data-btnsave="saveSalary" data-btncancel="cancelSalary" data-form="myform2" id="cancelSalary"> Cancel</a>
								</div>
								<form method="POST" action="{{ url($module_prefix.'/'.$module.'/storeSalaryinfo')}}" onsubmit="return false" id="form2" class="myform2">
									<input type="hidden" name="_token" value="{{ csrf_token() }}">
									<div class="col-md-7">
										<div class="formcontent" id="_salaryForm">
											<table class="table borderless">
												<tr>
													<td><span>Effectivity Date</span></td>
													<td>
														<input type="text" name="salary_effectivity_date" id="effective_salarydate" class="form-control font-style2 datepicker newSalary">
													</td>

												</tr>
												<tr>
													<td>Description</td>
													<td >
														<select name="salary_description" id="salary_description" class="form-control  font-style2 newSalary">
															<option value=""></option>
															<option value="Initial Salary">Initial Salary</option>
															<option value="Salary Adjustment">Salary Adjustment</option>
															<option value="Salary Increase">Salary Increase</option>
														</select>
													</td>

												</tr>
												<tr>
													<td>Position Item</td>
													<td>
														@if ($errors->has('positionitem_id'))
														    <span class="text-danger">{{ $errors->first('positionitem_id') }}</span>
														@endif
														<select name="positionitem_id" id="positionitem_id" class="form-control font-style2 newSalary">
															<option value=""></option>
															@foreach($positionitem as $items)
																<option value="{{ $items->RefId }}">{{ $items->name }}</option>
															@endforeach
														</select>
													</td>
												</tr>
												<tr>
													<td>Position</td>
													<td>
														@if ($errors->has('position_id'))
														    <span class="text-danger">{{ $errors->first('position_id') }}</span>
														@endif
														<select name="position_id" id="position_id" class="form-control font-style2 newSalary">
															<option value=""></option>
															@foreach($position as $items)
																<option value="{{ $items->RefId }}">{{ $items->name }}</option>
															@endforeach
														</select>
													</td>
												</tr>
												<tr>
										<!-- 			<td></td> -->
											<!-- 		<td >
														<label class="radio-inline"><input type="radio" name="optSalary" id="optSG" value="optSG">Salary Grade</label>
														<label class="radio-inline"><input type="radio" name="optSalary" id="optJG" value="optJG">Job Grade</label>

													</td> -->
												</tr>
												<tr class="divsg">
													<td><span>Salary Grade</span></td>
													<td>
														@if ($errors->has('salary_grade'))
														    <span class="text-danger">{{ $errors->first('salary_grade') }}</span>
														@endif
														<select name="salarygrade_id" id="salarygrade_id" class="form-control font-style2 newSalary">
															<option value=""></option>
															@foreach($sg_data as $items)
																<option value="{{ $items->id }}">{{ $items->salary_grade }}</option>
															@endforeach
														</select>
													</td>
												</tr>
												<tr class="divjg hidden">
													<td><span>Job Grade</span></td>
													<td>
														@if ($errors->has('jobgrade_id'))
														    <span class="text-danger">{{ $errors->first('jobgrade_id') }}</span>
														@endif
														<select name="jobgrade_id" id="jobgrade_id" class="form-control font-style2 newSalary">
															<option value=""></option>
															@foreach($jg_data as $items)
																<option value="{{ $items->id }}">{{ $items->job_grade }}</option>
															@endforeach
														</select>
													</td>
												</tr>
												<tr class="divsg">
													<td><span>Step Increment</span></td>
													<td>
														<select name="step_inc" id="step_inc" class="form-control font-style2 newSalary">
															<option value=""></option>
															<option value="step1">Step 1</option>
															<option value="step2">Step 2</option>
															<option value="step3">Step 3</option>
															<option value="step4">Step 4</option>
															<option value="step5">Step 5</option>
															<option value="step6">Step 6</option>
															<option value="step7">Step 7</option>
															<option value="step8">Step 8</option>
														</select>
													</td>
												</tr>
												<tr class="divsg hidden">
													<td><span>Amount</span></td>
													<td>
														<input type="text" name="amount" id="amount" class="form-control font-style2"/>

													</td>
												</tr>
												<tr class="divjg hidden">
													<td><span>Step Increment</span></td>
													<td>
														<select name="jgstep_inc" id="jgstep_inc" class="form-control font-style2 newSalary">
															<option value=""></option>
															<option value="step1">Step 1</option>
															<option value="step2">Step 2</option>
															<option value="step3">Step 3</option>
															<option value="step4">Step 4</option>
															<option value="step5">Step 5</option>
															<option value="step6">Step 6</option>
															<option value="step7">Step 7</option>
															<option value="step8">Step 8</option>
														</select>
													</td>
												</tr>
												<tr class="divjg hidden">
													<td><span>Amount</span></td>
													<td>
														<input type="text" name="jgamount" id="jgamount" class="form-control font-style2" readonly />

													</td>
												</tr>
												<tr>
													<td>Old Rate</td>
													<td>
														<input type="text" name="salary_old_rate" id="old_rate" class="form-control font-style2 onlyNumber newSalary">
													</td>


												</tr>
												<tr>
													<td>Adjustment</td>
													<td>
														<input type="text" name="salary_adjustment" id="salary_adjustment" class="form-control font-style2 onlyNumber newSalary">
													</td>
												</tr>
												<tr>
													<td>New Rate</td>
													<td>
														<input type="text" name="salary_new_rate" id="new_rate" class="form-control font-style2 onlyNumber" readonly>
													</td>
												</tr>
											</table>
										</div>

									</div>
									<!-- <input type="hidden" name="salarygrade_id" id="salarygrade_id"> -->
									<!-- <input type="hidden" name="jobgrade_id" id="jobgrade_id"> -->
									<input type="hidden" name="employee_id" id="salary_employee_id">
									<input type="hidden" name="sgjginfo_id" id="sgjginfo_id">

								</form>

							</div>
					</div>
					<!-- SALARY INFO -->

					<!-- BENEFITS INFO -->
					<div id="benefitsinfo" class="tab-pane fade in ">

						<div class="sub-panel" style="margin-top: 50px;z-index: 1;">
							{!! $controller->showBenefitinfo() !!}
						</div>

						<div class="col-md-12">
							<div class="box-1 button-style-wrapper" >
								<a class="btn btn-xs btn-info btn-savebg btn_new" id="newBenefit" data-btnnew="newBenefit" data-btnedit="editBenefit" data-btnsave="saveBenefit" data-btncancel="cancelBenefit"><i class="fa fa-save"></i> New</a>

									<a class="btn btn-xs btn-info btn-savebg btn_save submitme hidden" data-btnnew="newBenefit" data-btnedit="editBenefit" data-btnsave="saveBenefit" data-btncancel="cancelBenefit" data-btndelete="deleteBenefit" data-form="form3" id="saveBenefit"><i class="fa fa-save"></i> Save</a>

									<a class="btn btn-xs btn-danger btn_delete hidden" data-btnnew="newBenefit" data-btnedit="editBenefit" data-btnsave="saveBenefit" data-btncancel="cancelBenefit" data-btndelete="deleteBenefit" data-form="form3" id="deleteBenefit"><i class="fas fa-trash"></i> Delete</a>

									<a class="btn btn-xs btn-danger btn_cancel hidden" data-btnnew="newBenefit" data-btnedit="editBenefit" data-btnsave="saveBenefit" data-btncancel="cancelBenefit" data-btndelete="deleteBenefit" data-form="myform3" id="cancelBenefit"> Cancel</a>
							</div>
							<form method="POST" action="{{ url($module_prefix.'/'.$module.'/storeBenefitinfo')}}" onsubmit="return false" id="form3" class="myform3" >
								<input type="hidden" name="_token" value="{{ csrf_token() }}">
								<div class="formcontent">
									<table class="table borderless">
										<tr>
											<td>
												<span>Effective Date</span>
											</td>
											<td>
												<input type="text" name="benefit_effectivity_date" id="benefit_effectivedate" class="form-control font-style2 datepicker newBenefit">
											</td>
											<td>
												<span>Pay Period</span>
											</td>
											<td>
												<select class="form-control font-style2 newBenefit" name="benefit_pay_period" id="benefits_deduction_rate">
													<option value=""></option>
													<option value="Weekly">Weekly</option>
													<option value="Semi Monthly">Semi Monthly</option>
													<option value="Monthly">Monthly</option>
												</select>
											</td>

										</tr>
										<tr>
											<td>
												<span>Benefits</span>
											</td>
											<td>
												<select class="form-control font-style2 newBenefit" name="benefit_id" id="benefit_id">
													<option value=""></option>
													@foreach($benefit as $value)
													<option data-amount="{{ $value->amount }}"  value="{{ $value->id }}">{{ $value->name }}</option>
													@endforeach
												</select>
											</td>
											<td>


											</td>
											<td>
												<div class="weekly hidden">
													<div class="col-md-5">
														<span>
															<input type="radio" name="weekly" id="week1" value="Week 1" >Week 1
														</span>
													</div>
													<div class="col-md-5">
														<span>
															<input type="radio" name="weekly" id="week2" value="Week 2" >Week 2
														</span>
													</div>
												</div>
												<div class="semi-monthly hidden">
													<div class="col-md-5">
														<span>
															<input type="radio" name="semi_monthly" id="firsthalf" value="First Half" >First Half
														</span>
													</div>
													<div class="col-md-5">

														<span>
															<input type="radio" name="semi_monthly" id="secondhalf" value="Second Half" >Second Half
														</span>
													</div>
												</div>

											</td>
										</tr>
										<tr>
											<td>
												<span>Description</span>
											</td>
											<td>
												<input type="text" name="benefit_description" class="form-control font-style2 newBenefit" id="benefits_description">
											</td>
											<td></td>
											<td>
												<div class="weekly hidden">
													<div class="col-md-5">
														<span>
															<input type="radio" name="weekly" id="week3" value="Week 3" >Week 3
														</span>
													</div>
													<div class="col-md-5">
														<div>
														<span>
															<input type="radio" name="weekly" id="week4" value="Week 4" >Week 4
														</span>
													</div>

												</div>
											</td>
										</tr>
										<tr>
											<td>
												<span>Amount</span>
											</td>
											<td>
												<input type="text" name="benefit_amount" id="benefits_amount" class="form-control font-style2 onlyNumber newBenefit">
											</td>
											<td></td>
											<td></td>
										</tr>
										<tr>
											<td>Covered Period</td>
											<td>
												<div class="col-md-6">
													<input type="text" name="date_from" id="date_from" class="form-control newBenefit datepicker" placeholder="Date From">

												</div>
												<div class="col-md-6">

													<input type="text" name="date_to" id="date_to" class="form-control newBenefit datepicker" placeholder="Date To">
												</div>
											</td>
											<td colspan="2"></td>
										</tr>
									</table>
								</div>
								<input type="hidden" name="benefitinfo_id" id="benefitinfo_id">
								<input type="hidden" name="employeeinfo_id" id="employeeinfo_id" class="employeeinfo_id">
								<input type="hidden" name="employee_id" id="benefit_employee_id" class="benefit_employee_id">
							</form>

						</div>

					</div>
					<!-- BENEFITS INFO -->

					<!-- LOANS INFO -->
					<div id="loansinfo" class="tab-pane fade in">

						<div class="sub-panel" style="margin-top: 50px;z-index: 1;">

								{!! $controller->showLoaninfo() !!}

						</div>

						<div class="col-md-12">
							<div class="box-1 button-style-wrapper" >
								<a class="btn btn-xs btn-info btn-savebg btn_new" id="newLoan" data-btnnew="newLoan" data-btnedit="editLoan" data-btnsave="saveLoan" data-btncancel="cancelLoan" data-btndelete="deleteLoan"><i class="fa fa-save"></i> New</a>

									<a class="btn btn-xs btn-success btn-editbg btn_edit hidden" data-btnnew="newLoan" data-btnedit="editLoan" data-btnsave="saveLoan" data-btncancel="cancelLoan" data-btndelete="deleteLoan" id="editLoan"><i class="fa fa-edit"></i> Edit</a>

									<a class="btn btn-xs btn-info btn-savebg btn_save submitme hidden" data-btnnew="newLoan" data-btnedit="editLoan" data-btnsave="saveLoan" data-btncancel="cancelLoan" data-btndelete="deleteLoan" data-form="form4" id="saveLoan"><i class="fa fa-save"></i> Save</a>

									<a class="btn btn-xs btn-danger btn_delete hidden" data-btnnew="newLoan" data-btnedit="editLoan" data-btnsave="saveLoan" data-btncancel="cancelLoan" data-btndelete="deleteLoan" data-form="form3" id="deleteLoan"><i class="fas fa-trash"></i> Delete</a>

									<a class="btn btn-xs btn-danger btn_cancel hidden" data-btnnew="newLoan" data-btnedit="editLoan" data-btnsave="saveLoan" data-btncancel="cancelLoan" data-btndelete="deleteLoan"   id="cancelLoan" data-form="myform4"> Cancel</a>
							</div>
							<form method="POST" action="{{ url($module_prefix.'/'.$module.'/storeLoaninfo')}}" onsubmit="return false" id="form4" class="myform4">
								<input type="hidden" name="_token" value="{{ csrf_token() }}">
								<div class="formcontent">
									<table class="table borderless">
										<tr>
											<td>Loan Name</td>
											<td>
												<select class="form-control font-style2  newLoan " name="loan_id" id="loan_id">
													<option value=""></option>
													@foreach($loans as $value)
													<option value="{{ $value->id }}">{{ $value->name }}</option>
													@endforeach
												</select>
											</td>
											<td class="text-right">Pay Period</td>
											<td>
												<select class="form-control font-style2  newLoan " name="loan_pay_period" id="loan_payperiod">
													<option value=""></option>
													<option value="semimonthly">Semi Monthly</option>
													<option value="monthly">Monthly</option>
												</select>
											</td>
										</tr>
										<tr>
											<td>Date Granted</td>
											<td>
												<input type="text" name="loan_date_granted" class="form-control font-style2 datepicker newLoan" id="loan_dategranted">
											</td>
											<td class="text-right">Date Start</td>
											<td>
												<input type="text" name="loan_date_started" class="form-control font-style2 datepicker newLoan" id="loan_datestart">
											</td>

										</tr>
										<tr>
											<td>Total Loan Amount</td>
											<td>
												<input type="text" name="loan_totalamount" class="form-control font-style2 onlyNumber newLoan" id="total_loanamount">
											</td>
											<td class="text-right">Date End</td>
											<td>
												<input type="text" name="loan_date_end" id="loan_dateend" class="form-control font-style2 datepicker newLoan">
											</td>
										</tr>
										<tr>
											<td>Total Loan Balance</td>
											<td>
												<input type="text" name="loan_totalbalance" id="loan_totalbalance" class="form-control font-style2 onlyNumber newLoan">
											</td>
											<td></td>
											<td class="text-right" >
												<label class="font-style2" style="color:#000;font-weight: normal;    position: relative;top: 20px;">
														<input type="checkbox" name="chk_loanterminated" id="chk_laonterminated" >
													Terminated
													</label>
											</td>

										</tr>
										<tr>
											<td>Amortization</td>
											<td>
												<input type="text" name="loan_amortization" id="loan_amortization" class="form-control font-style2 onlyNumber newLoan">
											</td>
											<td class="text-right">Date Terminated</td>
											<td>
												<input type="text" name="loan_date_terminated" id="date_dateterminated" class="form-control font-style2 datepicker" disabled>
											</td>

										</tr>
									</table>

								</div>
								<input type="hidden" name="employee_id" id="loan_employee_id">
								<input type="hidden" name="loaninfo_id" id="loaninfo_id">
								<input type="hidden" name="employeeinfo_id" id="employeeinfo_id" class="employeeinfo_id">
							</form>
						</div>
					</div>
					<!-- LOANS INFO -->

					<!-- DEDUCTION INFO -->
					<div id="deducinfo" class="tab-pane fade in">
						<div class="sub-panel" style="margin-top: 50px;z-index: 1;">
							{!! $controller->showDeductioninfo() !!}
						</div>

						<div class="col-md-12">
							<div class="box-1 button-style-wrapper" >
									<a class="btn btn-xs btn-info btn-savebg btn_new" id="newDeduct" data-btnnew="newDeduct" data-btnedit="editDeduct" data-btnsave="saveDeduct" data-btncancel="cancelDeduct" data-btndelete="deleteDeduct"><i class="fa fa-save"></i> New</a>

									<a class="btn btn-xs btn-info btn-savebg btn_save submitme hidden" data-btnnew="newDeduct" data-btnedit="editDeduct" data-btnsave="saveDeduct" data-btncancel="cancelDeduct" data-btndelete="deleteDeduct" data-form="form5" id="saveDeduct"><i class="fa fa-save"></i> Save</a>

									<a class="btn btn-xs btn-danger btn_delete hidden" data-btnnew="newDeduct" data-btnedit="editDeduct" data-btnsave="saveDeduct" data-btncancel="cancelDeduct" data-btndelete="deleteDeduct" data-form="form3" id="deleteDeduct"><i class="fas fa-trash"></i> Delete</a>

									<a class="btn btn-xs btn-danger btn_cancel hidden" data-btnnew="newDeduct" data-btnedit="editDeduct" data-btnsave="saveDeduct" data-btncancel="cancelDeduct" data-btndelete="deleteDeduct" id="cancelDeduct" data-form="myform5"> Cancel</a>
							</div>
							<form method="POST" action="{{ url($module_prefix.'/'.$module.'/storeDeductioninfo')}}" onsubmit="return false" id="form5" class="myform5">
								<input type="hidden" name="_token" value="{{ csrf_token() }}">
								<div class="formcontent">
									<table class="table borderless">
										<tr>
											<td>Deduction Name</td>
											<td>
												<select class="form-control font-style2  newDeduct " name="deduction_id" id="deduction_id">
													<option value=""></option>
													@foreach($deductions as $value)
													<option value="{{ $value->id }}" data-name="{{ $value->name }}">{{ $value->name }}</option>
													@endforeach
												</select>
											</td>
											<td>Amount</td>
											<td>
												<input type="text" name="deduct_amount" class="form-control font-style2 onlyNumber newDeduct" id="input_deductamount">
											</td>
										</tr>
										<tr class="providentFund hidden">
											<td>Rate</td>
											<td>
												<select id="deduction_rate" name="deduction_rate" class="form-control newDeduct">
													<option value=""></option>
													@foreach($rate as $key => $value)
													<option value="{{ $value->rate }}">{{ $value->rate*100 }} %</option>
													@endforeach
												</select>
											</td>
											<td colspan="2"></td>
										</tr>
										<tr>
											<td>Pay Period</td>
											<td>
												<select class="form-control font-style2  newDeduct " name="deduct_pay_period" id="select_deductperiod">
													<option value=""></option>
													<option value="semimonthly">Semi Monthly</option>
													<option value="monthly">Monthly</option>
												</select>
											</td>
											<td>

											</td>
											<td class="text-right"><label class="font-style2" style="color:#000;font-weight: normal;    position: relative;top: 20px;">
														<input type="checkbox" name="chk_deduct" id="chk_deduct" >
													Terminated
													</label></td>
										</tr>
										<tr>
											<td>Date Start</td>
											<td>
												<input type="text" name="deduct_date_start" id="date_deductstart" class="form-control font-style2 datepicker newDeduct">
											</td>
											<td >Date Terminated</td>
											<td>
												<input type="text" name="deduct_date_terminated" id="date_deductterminated" class="form-control font-style2 datepicker" disabled>
											</td>

										</tr>
										<tr>
											<td>Date End</td>
											<td>
												<input type="text" name="deduct_date_end" id="date_deductend" class="form-control font-style2 datepicker newDeduct">
											</td>
											<td></td>
											<td></td>
										</tr>
									</table>
								</div>
								<input type="hidden" name="deductinfo_id" id="deductinfo_id">
								<input type="hidden" name="employee_id" id="deduct_employee_id">
								<input type="hidden" name="employeeinfo_id" id="employeeinfo_id" class="employeeinfo_id">
							</form>

						</div>


					</div>
					<!-- DEDUCTION INFO -->

				</div>
			</div>
			<!-- NON PLANTILLA -->
			<div class="tab-container hidden" id="nonplantilla">
				<ul class="nav nav-tabs">
					<li class="active"><a href="#">Summary</a></li>
				</ul>

				<div class="tab-content">
					<div class="nonplantilla">
						<div class="col-md-12">
							<div class="button-wrapper" style="position: relative;top: 10px;left: 5px;" >
								<a class="btn btn-xs btn-info btn-savebg btn_new" id="newNonPlantilla" data-btnnew="newNonPlantilla" data-btncancel="cancelNonPlantilla" data-btnedit="editNonPlantilla" data-btnsave="saveNonPlantilla"><i class="fa fa-save"></i> New</a>

								<a class="btn btn-xs btn-info btn-editbg btn_edit hidden" id="editNonPlantilla" data-btnnew="newNonPlantilla" data-btncancel="cancelNonPlantilla" data-btnedit="editNonPlantilla" data-btnsave="saveNonPlantilla"><i class="fa fa-save"></i> Edit</a>

								<a class="btn btn-xs btn-info btn_save_nonplantilla hidden" data-form="form" data-btnnew="newNonPlantilla" data-btncancel="cancelNonPlantilla" data-btnedit="editNonPlantilla" data-btnsave="saveNonPlantilla" id="saveNonPlantilla"><i class="fa fa-save"></i> Save</a>

								<a class="btn btn-xs btn-danger btn_cancel hidden" data-btnnew="newNonPlantilla" data-btncancel="cancelNonPlantilla" data-form="myform" data-btnedit="editNonPlantilla" data-btnsave="saveNonPlantilla"id="cancelNonPlantilla"> Cancel</a>
							</div>


							<input type="hidden" name="jo_employeeinfo_id" id="jo_employeeinfo_id">
							<div class="col-md-4">
								<div class="border-style02">
									<table class="table borderless">
										<tr>
											<td>
												<span>Employee Status</span>
											</td>
											<td>
												<input type="text" name="jo_employee_status" id="jo_employee_status" class="form-control font-style2 "  readOnly>
											</td>
										</tr>
										<tr>
											<td>
												<span>Employee No.</span>
											</td>
											<td>
												<input type="text" name="jo_employee_no" id="jo_employee_no" class="form-control font-style2" readOnly>
											</td>
										</tr>
										<tr>
											<td>
												<span>BP No.</span>
											</td>
											<td>
												<input type="text" name="jo_bp_no" id="jo_bp_no" class="form-control font-style2 newNonPlantilla">
											</td>
										</tr>
									</table>
								</div>
								<div class="border-style02 margintop-25">
									<table class="table borderless">
										<tr>
											<td>
												<span>Position Item No.</span>
											</td>
											<td>
												<input type="text" name="jo_position_itemno" id="jo_position_itemno" class="form-control font-style2" readOnly>
											</td>
										</tr>
										<tr>
											<td>
												<span>Position</span>
											</td>
											<td>
												<input type="text" name="jo_position" id="jo_position" class="form-control font-style2" readOnly>
											</td>
										</tr>
										<tr>
											<td>
												<span>Office</span>
											</td>
											<td>
												<input type="text" name="jo_office" id="jo_office" class="form-control font-style2 _input" readOnly>
											</td>
										</tr>
										<tr>
											<td>
												<span>Division</span>
											</td>
											<td>
												<input type="text" name="jo_division" id="jo_division" class="form-control font-style2 _input" readOnly>
											</td>
										</tr>
									</table>
								</div>
							</div>
							<div class="col-md-4">
								<div class="border-style2">
									<table class="table borderless">
										<tr class="text-center">
											<td>
												<span>Daily Rate</span>
											</td>
											<td>
												<input type="text" name="job_order_daily_rate" class="form-control font-style2 onlyNumber newNonPlantilla" id="job_order_daily_rate" placeholder="0.00" />
											</td>
										</tr>
										<tr class="hidden">
											<td>
												<span>Monthly Rate</span>
											</td>
											<td>
												<input type="text" name="jo_monthly_rate_amount" class="form-control font-style2 onlyNumber" id="jo_monthly_rate_amount" placeholder="0.00" readonly />
											</td>
										</tr >
										<tr class="hidden">
											<td>
												<span>Annual Rate</span>
											</td>
											<td>
												<input type="text" name="jo_annual_rate_amount" class="form-control font-style2 onlyNumber" id="jo_annual_rate_amount" placeholder="0.00" readonly />
											</td>

										</tr>
									</table>
								</div>
								<div class="border-style2" style="margin-top: -30px;">
									<table class="table borderless">
										<tr class="text-center">
											<td colspan="2"><span>Tax Policy</span></td>
											<!-- <td><span>Contributions</span></td> -->
										</tr>
										<tr class="text-center">
											<td colspan="2">
												<select class="font-style2 form-control newNonPlantilla customselect" id="jo_taxpolicy_id" name="jo_taxpolicy_id">
													<option value=""></option>
													@foreach($jo_tax_policy as $value)
													<option data-taxrate="{{ $value->job_grade_rate }}" value="{{ $value->id }}">{{ $value->policy_name }}</option>
													@endforeach
												</select>
											</td>
										<!-- 	<td class="hidden">
												<input type="text" name="jo_tax_amount" id="jo_tax_amount" class="form-control font-style2 onlyNumber" readOnly>
											</td> -->
										</tr>
										<tr class="text-center">
											<td colspan="2">
												<select class="font-style2 form-control newNonPlantilla customselect" id="jo_taxpolicy_two_id" name="jo_taxpolicy_two_id">
													<option value=""></option>
													@foreach($jo_tax_policy_two as $value)
													<option data-taxrate="{{ $value->job_grade_rate }}" value="{{ $value->id }}">{{ $value->policy_name }}</option>
													@endforeach
												</select>
											</td>
										<!-- 	<td class="hidden">
												<input type="text" name="jo_tax_amount_two" id="jo_tax_amount_two" class="form-control font-style2 onlyNumber" readOnly>
											</td> -->
										</tr>
									</table>
								</div>
								<div class="border-style2 margintop-25">
									<table class="table borderless">
										<tr>
											<td class="text-center" colspan="2"><span>For Job Order/Contractual</span></td>
											<td></td>
										</tr>
										<tr>
											<td>
												<span>Assumption to Duty</span>
											</td>
											<td>
												<input type="text" class="form-control font-style2 newNonPlantilla " id="jo_assumption_to_duty" name="jo_assumption_to_duty" readOnly />
											</td>
											<td></td>
										</tr>
										<tr>
											<td>
												<span>End of Contract</span>
											</td>
											<td>
												<input type="text" class="form-control font-style2 newNonPlantilla " id="jo_end_of_contract" name="jo_end_of_contract" readOnly />
											</td>
											<td></td>
										</tr>
									</table>
								</div>
							</div>
							<div class="col-md-4">
								<div class="border-style2 ">
									<table class="table borderless newNonPlantilla">
										<tr>
											<td><span>TIN</span></td>
											<td>
												<input type="text" name="jo_tax_id_number" id="jo_tax_id_number" class="form-control font-style2 newNonPlantilla">
											</td>
										</tr>
										<tr>
											<td><span>ATM No</span></td>
											<td>
												<input type="text" name="jo_atm_no" id="jo_atm_no" class="form-control font-style2 newNonPlantilla">
											</td>
										</tr>
										<tr>
											<td>
												<span>Bank</span>
											</td>
											<td>
												<select class="form-control font-style2 customselect newNonPlantilla"  id="jo_bank_id" name="jo_bank_id">
													<option value=""></option>
													@foreach($bank as $value)
														<option data-jobankbranch="{{ $value->branch_name }}" value="{{ $value->id }}"  >{{ $value->name }}</option>
													@endforeach
												</select>
											</td>
										</tr>
										<tr>
											<td>
												<span>Bank Branch</span>
											</td>
											<td>
												<input type="text" class="form-control font-style2 customselect" id="jo_bank_branch" name="jo_bank_branch" readonly />
											</td>
										</tr>
									</table>
								</div>
								<div class="border-style2 margintop-25 newNonPlantilla">
									<table class="table borderless">
										<tr class="hidden">
											<td>
												<span>Total hours in a day</span>
											</td>
											<td>
												<input type="text" class="form-control font-style2 newNonPlantilla" id="jo_total_hours_inaday" name="jo_total_hours_inaday" />
											</td>
										</tr>
										<tr class="hidden">
											<td>
												<span>No. of Working Days</span>
											</td>
											<td>
												<input type="text" class="form-control font-style2 newNonPlantilla" id="jo_no_ofdays_inamonth" name="jo_no_ofdays_inamonth" />
											</td>
										</tr>
										<tr class="hidden">
											<td>
												<span>No. of Months in a Year</span>
											</td>
											<td>
												<input type="text" class="form-control font-style2 newNonPlantilla" id="jo_no_ofdays_inayear" name="jo_no_ofdays_inayear" />
											</td>
										</tr>
										<tr>
											<td>
												<span>Overtime Balance</span>
											</td>
											<td>
												<input type="text" class="form-control font-style2 onlyNumber newNonPlantilla" id="jo_overtime_balance_amount" name="jo_overtime_balance_amount" />
											</td>
										</tr>
									</table>
								</div>

							</div>
						</div>
					</div>
				</div>
			</div>

		</div>

	</div>


</div>
<!-- <div class="ajax-loader">
  <img src="{{ asset('images/ajax-loader1.gif') }}" class="img-responsive" />
</div> -->
<br>
@endsection

@section('js-logic1')
<script type="text/javascript">
$(document).ready(function(){
	var _monthlyRate;
	var taxAmountBR;
	var _taxDue;
	$('.newNonPlantilla :input').attr('disabled',true);
	$('.newNonPlantilla').attr('disabled',true);
	$('.newSummary :input').attr('disabled',true);
	$('.newSummary').attr('disabled',true);
	$('.newSalary :input').attr('disabled',true);
	$('.newSalary').attr('disabled',true);
	$('.newBenefit :input').attr('disabled',true);
	$('.newBenefit').attr('disabled',true);
	$('.newLoan :input').attr('disabled',true);
	$('.newLoan').attr('disabled',true);
	$('.newDeduct :input').attr('disabled',true);
	$('.newDeduct').attr('disabled',true);

	$('.btn_new').on('click',function(){
		$('#benefitinfo_id').val('');
		$('#deductinfo_id').val('');
		$('#loaninfo_id').val('');

		btnnew = $(this).data('btnnew');
		btnsave = $(this).data('btnsave');
		btncancel = $(this).data('btncancel');
		$('.'+btnnew+' :input').attr("disabled",false);
		$('.'+btnnew).attr('disabled',false);
		$('#'+btnnew).addClass('hidden');
		$('#'+btnsave).removeClass('hidden');
		$('#'+btncancel).removeClass('hidden');
		_taxDue = 0;
	});

	$('.btn_edit').on('click',function(){
		btnnew = $(this).data('btnnew');
		btnsave = $(this).data('btnsave');
		btncancel = $(this).data('btncancel');
		btnedit = $(this).data('btnedit');
		btndelete = $(this).data('btndelete');
		$('.'+btnnew+' :input').attr("disabled",false);
		$('.'+btnnew).attr('disabled',false);
		$('#'+btnnew).addClass('hidden');
		$('#'+btnedit).addClass('hidden');
		$('#'+btnsave).removeClass('hidden');
		$('#'+btndelete).removeClass('hidden');
		$('#'+btncancel).removeClass('hidden');
	});

	$('.btn_cancel').on('click',function(){
		$('#benefitinfo_id').val('');
		$('#deductinfo_id').val('');
		$('#loaninfo_id').val('');

		btnnew = $(this).data('btnnew');
		btnsave = $(this).data('btnsave');
		btncancel = $(this).data('btncancel');
		btnedit = $(this).data('btnedit');
		btndelete = $(this).data('btndelete');

		$('.'+btnnew+' :input').attr("disabled",true);
		$('.'+btnnew).attr('disabled',true);
		$('#'+btnnew).removeClass('hidden');
		$('#'+btnedit).addClass('hidden');
		$('#'+btnsave).addClass('hidden');
		$('#'+btncancel).addClass('hidden');
		$('#'+btndelete).addClass('hidden');
		$('.weekly').addClass('hidden');
		$('.semi-monthly').addClass('hidden');
		$('.providentFund').addClass('hidden');
		form = $(this).data('form');
		clear_form_elements(form);
		clear_form_elements('nonplantilla');
		$('.error-msg').remove();

	});

	$('.select2').select2();

	$('.onlyNumber').keypress(function (event) {
		return isNumber(event, this)
	});

	$(".onlyNumber").keyup(function(){
		amount  = $(this).val();
		if(amount == 0){
			$(this).val('');
		}else{
			plainAmount = amount.replace(/\,/g,'')
			$(this).val(commaSeparateNumber(plainAmount));
		}
	});


	// DATE PICKER
	$('.datepicker').datepicker({
		dateFormat:'yy-mm-dd'
	});


	$('.nav-tabs a').click(function(){
		tab = $(this).text();
		switch(tab){
			case 'Benefits Info':
				if($('#employeeinfo_id').val()){
					$(this).tab('show');
				}else{
					swal({  title: 'Please setup first the summary',
							text: '',
							type: "warning",
							icon: 'warning',

						});
				}
			break;
			case 'Summary':
				$(this).tab('show');
			break;
			case 'Salary Info':
				$(this).tab('show');
			break;
			case 'Loans Info':
				if($('#employeeinfo_id').val()){
					$(this).tab('show');
				}else{
					swal({  title: 'Please setup first the summary',
							text: '',
							type: "warning",
							icon: 'warning',

						});
				}
			break;
			case 'Deduction Info':
				if($('#employeeinfo_id').val()){
					$(this).tab('show');
				}else{
					swal({  title: 'Please setup first the summary',
							text: '',
							type: "warning",
							icon: 'warning',
						});
				}
			break;
		}

	})


	$(document).on('change','#searchby',function(){
		var val = $(this).val();

		$.ajax({
			url:base_url+module_prefix+module+'/getSearchby',
			data:{'q':val},
			type:'GET',
			dataType:'JSON',
			success:function(data){

				arr = [];
				$.each(data,function(k,v){
					arr += '<option value='+v.RefId+'>'+v.Name+'</option>';
				})

				$('#select_searchvalue').html(arr);
			}
		})

	})

	$('#chk_deduct').on('click',function(){

		check = $(this).prop('checked');
		if(check){
			$('#date_deductterminated').removeAttr('disabled');
		}else{
			$('#date_deductterminated').attr('disabled',true);
		}

	});

	$('#deduction_id').on('change',function(){
		name = $(this).find(':selected').data('name');

		switch(name){
			case 'PROVIDENT FUND':
				$('.providentFund').removeClass('hidden');
			break;
			default:
			$('.providentFund').addClass('hidden');
			break;
		}
	});

	$('#deduction_rate').on('change',function(){
		rate = $(this).find(':selected').val();
		deduct_amount = (parseFloat(_monthlyRate)*rate);

		deduct_amount = (deduct_amount) ? commaSeparateNumber(parseFloat(deduct_amount).toFixed(2)) : '';
		$('#input_deductamount').val(deduct_amount);
	});


	$('#chk_laonterminated').on('click',function(){

		check = $(this).prop('checked');
		if(check){
			$('#date_dateterminated').removeAttr('disabled');
		}else{
			$('#date_dateterminated').attr('disabled',true);
		}

	});
	var _newRate = 0;
	$('#salary_adjustment').on('keyup',function(){
		_adjustment = $(this).val().replace(/\,/g,'');

		_newRate =  ((_adjustment) ? parseFloat(_monthlyRate) + parseFloat(_adjustment) : _monthlyRate);

		$('#new_rate').val(commaSeparateNumber(_newRate));
	})

	$(document).on('change','#benefits_payperiod',function(){
		val = $(this).val();

		$('#week1').prop('checked', false);
		$('#week2').prop('checked', false);
		$('#week3').prop('checked', false);
		$('#week4').prop('checked', false);
		$('#semi_monthly').prop('checked', false);
		$('#semi_monthly').prop('checked', false);

		switch(val){
			case 'Weekly':
				$('.weekly').addClass('show').removeClass('hidden');
				$('.semi-monthly').removeClass('show').addClass('hidden');
			break;

			case 'Semi Monthly':
				$('.weekly').removeClass('show').addClass('hidden');
				$('.semi-monthly').addClass('show').removeClass('hidden');
			break;

			default:
				$('.weekly').removeClass('show').addClass('hidden');
				$('.semi-monthly').removeClass('show').addClass('hidden');
			break;
		}
	});

	var _pagibigcontribution = 100;
	var _employer_pagibig_share
	$(document).on('change','#select_pagibigbpolicy',function(){
		policytype = $(this).find(':selected').data('policytype');

		switch(policytype){
			case 'System Generated':
				$('#pagibig_contribution').attr('readOnly',true);
				$('#pagibig_contribution').val(_pagibigcontribution);
				$('#er_pagibig_share').val(_pagibigcontribution);
				_employer_pagibig_share = _pagibigcontribution;
			break;

			case 'Inputted':
				$('#pagibig_contribution').val('');
				$('#pagibig_contribution').attr('readOnly',false);
			break;

			default:

			$('#select_pagibigbpolicy').val('');
			$('#pagibig_contribution').val('');
			$('#pagibig_contribution').attr('readOnly',false);
		}

	});


	var _clRange;
	var _prescribeTax;
	var _prescribedPercentage
	var _totalContribution;
	var _gsiscontribution;
	var _employer_gsis_share;
	var _philhealthcontribution;
	var _employer_philhealth_share;

	var _first;
	var _second;
	var _newdata;
	var _arrItem;
	$(document).on('change','#select_taxspolicy',function(){
		policytype = $(this).find(':selected').data('policytype');

		switch(policytype){
			case 'System Generated':
				$('#tax_contribution').attr('readOnly',true);
				if(!_taxDue){
					_gross_salary = 0;
					_totalContribution = (parseFloat(_gsiscontribution) + parseFloat(_philhealthcontribution) + parseFloat(_pagibigcontribution))
					_gross_salary = (_monthlyRate) - Math.abs(_totalContribution);
					// _gross_salary = _monthlyRate;

					$.each(_arrItem,function(k,v){
						if(_gross_salary > parseFloat(_newdata.first[k]) && _gross_salary < parseFloat(_newdata.second[k])){

							_prescribeTax 		  = _arrItem[k][0];
							_prescribedPercentage = _arrItem[k][1];
							_clRange			  = _newdata.first[k];
						}

					});

					if(_gross_salary <= 20833){
						_clRange 			  = 0;
						_prescribedPercentage = 0;
					}

					$('#tax_bracket').val(parseFloat(_clRange));
					$('#tax_bracket_amount').val(parseFloat(_prescribeTax));

					_grossTaxable = 0;
					_grossTaxable = (Math.abs(parseFloat(_gross_salary)) - Math.abs(parseFloat(_clRange)));

					$('#tax_inexcess').val(Math.abs(_grossTaxable));
					_taxDue = 0;
					_taxPercentage = (Math.abs(_grossTaxable)*_prescribedPercentage);
					_taxDue = (Math.abs(parseFloat(_taxPercentage)) + Math.abs(parseFloat(_prescribeTax)));
				}
				$('#tax_contribution').val(commaSeparateNumber(Math.abs(parseFloat(_taxDue).toFixed(2))));

				// console.log('_arrItem '+_arrItem);
				// console.log('_clRange '+_clRange);
				// console.log('_gsiscontribution '+_gsiscontribution);
				// console.log('_philhealthcontribution '+_philhealthcontribution);
				// console.log('_pagibigcontribution '+_pagibigcontribution);
				// console.log('_prescribeTax '+_prescribeTax);
				// console.log('_totalContribution '+_totalContribution);
				// console.log('_gross_salary '+_gross_salary);
				// console.log('_grossTaxable '+_grossTaxable);
				// console.log('_taxPercentage '+_taxPercentage);
				// console.log('_monthlyRate '+_monthlyRate);

			break;

			case 'Inputted':
				$('#tax_contribution').val('');
				$('#tax_contribution').attr('readOnly',false);
			break;

			default:

			$('#select_taxspolicy').val('');
			$('#tax_contribution').val('');
			$('#tax_contribution').attr('readOnly',false);
			break;
		}

	});


	$(document).on('change','#select_gsispolicy',function(){
		ee_percent = $(this).find(':selected').data('percent');
		er_percent = $(this).find(':selected').data('erpercent');
		policytype = $(this).find(':selected').data('policytype');
		_gsiscontribution = 0;
		switch(policytype){
			case 'System Generated':
				$('#gsis_contribution').attr('readOnly',true);
				_gsiscontribution = (parseFloat(_monthlyRate)*ee_percent);
				_employer_gsis_share = (parseFloat(_monthlyRate)*er_percent)

				if(_gsiscontribution){
					$('#gsis_contribution').val(commaSeparateNumber(parseFloat(_gsiscontribution).toFixed(2)));
					$('#er_gsis_share').val(_employer_gsis_share);
				}
			break;

			case 'Inputted':
				$('#gsis_contribution').val('');
				$('#er_gsis_share').val('');
				$('#gsis_contribution').attr('readOnly',false);
			break;

			default:

			$('#select_gsispolicy').val('');
			$('#gsis_contribution').val('');
			$('#er_gsis_share').val('');
			$('#gsis_contribution').attr('readOnly',false);
		}

	});

	$(document).on('change','#select_philhealthpolicy',function(){
		below = 0;
		above = 0;
		_philhealthcontribution = 0;
		policytype = $(this).find(':selected').data('policytype');
		below = $(this).find(':selected').data('below');
		above = $(this).find(':selected').data('above');

		switch(policytype){
			case 'System Generated':
				$('#philhealth_contribution').attr('readOnly',true);

				if(_monthlyRate >= 41999){
					_philhealthcontribution = (above/2);
					_employer_philhealth_share = _philhealthcontribution;
				}else{
					_philhealthcontribution = ((parseFloat(_monthlyRate)*below)/2);
					_employer_philhealth_share = _philhealthcontribution;
				}

				if(_philhealthcontribution){
					$('#philhealth_contribution').val(commaSeparateNumber(parseFloat(_philhealthcontribution).toFixed(2)) );
					$('#er_philhealth_share').val(_employer_philhealth_share);
				}

			break;

			case 'Inputted':
				$('#philhealth_contribution').val('');
				$('#philhealth_contribution').attr('readOnly',false);
			break;

			default:

			$('#select_pagibigbpolicy').val('');
			$('#philhealth_contribution').val('');
			$('#philhealth_contribution').attr('readOnly',false);
		}

	});

	$(document).on('change','#select_bank',function(){
		_branchName = $(this).find(':selected').data('bankbranch');
		$('#bank_branch').val(_branchName);
	});


	$(document).on('click','#namelist tr',function(){
		id = $(this).data('empid');
		$('#employee_id').val(id);
		$('#salary_employee_id').val(id);
		$('#benefit_employee_id').val(id);
		$('#loan_employee_id').val(id);
		$('#deduct_employee_id').val(id);

		$.ajax({
			url:base_url+module_prefix+module+'/getEmployeesinfo',
			data:{'id':id,'employeeinfo_id':$('#employeeinfo_id').val(),},
			type:'GET',
			dataType:'JSON',
			success:function(data){
				_newdata = data;
				$('._input').val('');
				clear_form_elements('myform');
				clear_form_elements('myform2');
				clear_form_elements('myform3');
				clear_form_elements('myform4');
				clear_form_elements('myform5');
				$('.providentFund').addClass('hidden');

				if(data.pmsemployeeinfo !== undefined && data.pmsemployeeinfo !== null){
					$('#editSummary').removeClass('hidden');
					$('#newSummary').addClass('hidden');
					$('#saveSummary').addClass('hidden');
					$('.employeeinfo_id').val(data.pmsemployeeinfo.id);
					$('#bp_no').val(data.pmsemployeeinfo.bp_no);
					$('._taxperiod').addClass('hidden');
					if(data.pmsemployeeinfo.wagestatus_id != null){
						$('#wagestatus_id').val(data.pmsemployeeinfo.wagestatus_id);
					}
					if(data.salaryinfo.salarygrade_id !== null){
						$('#salary_grade').val(data.salaryinfo.salarygrade.Code);

					}
					$('#atm_no').val(data.pmsemployeeinfo.atm_no);
					$('#tax_id_number').val(data.pmsemployeeinfo.tax_id_number);

					if(data.pmsemployeeinfo.bank_id != null){
						$('#select_bank').val(data.pmsemployeeinfo.bank_id);
						$("#bank_branch").val(data.pmsemployeeinfo.banks.branch_name);
					}

					$('#input_daysinayear').val(data.pmsemployeeinfo.no_ofdays_inayear);
					$('#input_daysinamonth').val(data.pmsemployeeinfo.no_ofdays_inamonth);
					$('#input_hoursinaday').val(data.pmsemployeeinfo.total_hours_inaday);
					if(data.pmsemployeeinfo.gsispolicy_id != null){
						$('#select_gsispolicy').val(data.pmsemployeeinfo.gsispolicy_id);
					}
					_gsiscontribution = 0;
					if(data.pmsemployeeinfo.gsispolicy_id !== null && data.pmsemployeeinfo.gsispolicy_id !== 'undefined'){
						if(data.pmsemployeeinfo.gsispolicy.policy_type == "System Generated"){
							_gsiscontribution = data.pmsemployeeinfo.gsis_contribution;
							$('#gsis_contribution').val(commaSeparateNumber(data.pmsemployeeinfo.gsis_contribution)).prop('readOnly',true);
							$('#er_gsis_share').val(data.pmsemployeeinfo.er_gsis_share)
						}else{
							$('#gsis_contribution').val(commaSeparateNumber(data.pmsemployeeinfo.gsis_contribution));
							$('#er_gsis_share').val(data.pmsemployeeinfo.er_gsis_share)
						}
					}
					_taxDue = 0;
					if(data.pmsemployeeinfo.taxpolicy_id !== null && data.pmsemployeeinfo.taxpolicy_id !== 'undefined'){
						if(data.pmsemployeeinfo.taxpolicy.policy_type == "System Generated"){
							_taxDue = (data.pmsemployeeinfo.tax_contribution) ? data.pmsemployeeinfo.tax_contribution : '';

							$('#tax_contribution').val(commaSeparateNumber(_taxDue)).prop('readOnly',true);
						}else{

							$('#tax_contribution').val(commaSeparateNumber(_taxDue));
						}
					}

					_philhealthcontribution = 0;
					if(data.pmsemployeeinfo.philhealthpolicy_id !== null){
						$('#select_philhealthpolicy').val(data.pmsemployeeinfo.philhealthpolicy_id);
						if(data.pmsemployeeinfo.philhealthpolicy.policy_type == "System Generated"){
							_philhealthcontribution = data.pmsemployeeinfo.philhealth_contribution;
							$('#philhealth_contribution').val(commaSeparateNumber(data.pmsemployeeinfo.philhealth_contribution)).prop('readOnly',true);
							$('#er_philhealth_share').val(data.pmsemployeeinfo.er_philhealth_share)
						}else{
							$('#philhealth_contribution').val(commaSeparateNumber(data.pmsemployeeinfo.philhealth_contribution));
							$('#er_philhealth_share').val(data.pmsemployeeinfo.er_philhealth_share)
						}
					}

					if(data.pmsemployeeinfo.pagibigpolicy_id !== null){
						$('#select_pagibigbpolicy').val(data.pmsemployeeinfo.pagibigpolicy_id);
						if(data.pmsemployeeinfo.pagibigpolicy_id !==  null  && data.pmsemployeeinfo.pagibigpolicy_id !== 'undefined'){

							if(data.pmsemployeeinfo.pagibigpolicy.policy_type == "System Generated"){
								$('#pagibig_contribution').val(commaSeparateNumber(data.pmsemployeeinfo.pagibig_contribution)).prop('readOnly',true);
								$('#er_pagibig_share').val(data.pmsemployeeinfo.er_pagibig_share)
							}else{
								$('#pagibig_contribution').val(commaSeparateNumber(data.pmsemployeeinfo.pagibig_contribution));
								$('#er_pagibig_share').val(data.pmsemployeeinfo.er_pagibig_share)
							}
						}
					}

					$('#pagibig2').val(data.pmsemployeeinfo.pagibig2);
					$('#personal_share').val(data.pmsemployeeinfo.pagibig_personal);


					if(data.pmsemployeeinfo.taxpolicy_id !== null){
						$('#select_taxspolicy').val(data.pmsemployeeinfo.taxpolicy_id);
					}

				}else{
					$('.newSummary :input').attr('disabled',true);
					$('.newSummary').attr('disabled',true);
					$('._taxperiod').removeClass('hidden');
					$('#editSummary').addClass('hidden');
					$('#saveSummary').addClass('hidden');
					$('#cancelSummary').addClass('hidden');
					$('#newSummary').removeClass('hidden');
					assumption_date = (data.employeeinfo !== null) ? data.employeeinfo.assumption_date : '';
					$('#effective_salarydate').val();
					if(data.salaryinfo.salarygrade_id !== null){
						$('#salary_grade').val(data.salaryinfo.salarygrade.Code);
					}
					$('.employeeinfo_id').val('');


				}

				employee_no = (data.employeeinfo !== null) ? data.employeeinfo.employees.employee_number : '';
				$('#emp_no').val(employee_no);
				if(data.employeeinfo.employeestatus !== null){
					$('#empstatus').val(data.employeeinfo.employeestatus.Name);
				}
				if(data.employeeinfo.positionitems !== null){
					$('#position_itemno').val(data.employeeinfo.positionitems.Name);
				}
				if(data.employeeinfo.positions !== null){
					$('#position').val(data.employeeinfo.positions.Name);
				}
				if(data.employeeinfo.offices !== null){
					$('#office').val(data.employeeinfo.offices.Name);
				}
				$('#lbl_empname').text(data.employeeinfo.employees.lastname+' '+data.employeeinfo.employees.firstname+' '+data.employeeinfo.employees.middlename+'.')
				$('#assumption_to_duty').val(data.employeeinfo.assumption_date);

				if(data.employeeinfo.divisions !== null){
					$('#division').val(data.employeeinfo.divisions.Name);
				}

				with_setup = (data.employeeinfo.employees.with_setup !== null) ? data.employeeinfo.employees.with_setup : '';
				$('#with_setup').val(with_setup);
				$('#positionitem_id').val(data.employeeinfo.position_item_id);
				$('#position_id').val(data.employeeinfo.position_id);

				if(data.salaryinfo !== null){

					_monthlyRate = data.salaryinfo.salary_new_rate;
					overtime_balance_amount = (_monthlyRate/2);
					$('#monthly_rate').val(commaSeparateNumber(parseFloat(_monthlyRate).toFixed(2)));
					$('#overtime_balance_amount').val(commaSeparateNumber(parseFloat(overtime_balance_amount).toFixed(2)));

					$('#new_rate').val(commaSeparateNumber(_monthlyRate));
				}

				item = "";
				_arrItem = [];
				$.each(data.cl.monthlyCL,function(k,v){
					item  = v.split('-');
					_arrItem.push(item);

				});

				_dailyRate = 0;
				if(data.salaryinfo !== null){
					_dailyRate = parseFloat(data.salaryinfo.salary_new_rate)/22;
					$('#daily_rate').val(commaSeparateNumber(_dailyRate.toFixed(2)));

				}
				_weeklyRate = 0;
				if(data.salaryinfo !== null){
					_weeklyRate = parseFloat(data.salaryinfo.salary_new_rate)/4;
					$('#weekly_rate').val(commaSeparateNumber(_weeklyRate.toFixed(2)));
				}

				_annualRate = 0;
				if(data.salaryinfo !== null){
					_annualRate = parseFloat(data.salaryinfo.salary_new_rate)*12;
					$('#annual_rate').val(commaSeparateNumber(_annualRate.toFixed(2)));
					$('#input_stepinc').val(data.salaryinfo.step_inc);
				}

				overtime_balance_amount = (_annualRate) ? _annualRate/2  : '';
				$('#overtime_balance_amount').val(commaSeparateNumber(parseFloat(overtime_balance_amount).toFixed(2)) );


				// GENERATE TR FOR BENEFITS TAB

				generateBenefitInfoTable(data.benefitinfo);

				// GENERATE TR FOR LOAN INFO TAB
				generateLoanInfoTable(data.loaninfo);


				// GENERATE TR FOR DEDUCTION INFO TAB
				generateDeductionInfoTable(data.deductioninfo);


				// GENERATE TR FOR SALARY INFO TAB
				var tSalary = $('#tbl_salaryinfo').DataTable();
				var sgjg = "";
				tSalary.clear().draw();

				$.each(data.salarylist,function(k,v){

					if(v.salarygrade_id){
						sgjg = 'SG '+v.salarygrade.Code;
					}

					if(v.jobgrade_id){
						sgjg = 'JG '+v.jobgrade.job_grade;
					}

					tSalary.row.add( [
			        	data.salarylist[k].salary_effectivity_date,
			        	data.salarylist[k].salary_description,
						sgjg,
						data.salarylist[k].salary_old_rate,
						data.salarylist[k].salary_adjustment,
						data.salarylist[k].salary_new_rate,
			        ]).draw( false );

			        tSalary.rows(k).nodes().to$().attr("data-id", v.id);
			        tSalary.rows(k).nodes().to$().attr("data-employeeid", v.employee_id);
			        tSalary.rows(k).nodes().to$().attr("data-salarygradeid", v.salarygrade_id);
			        // tSalary.rows(k).nodes().to$().attr("data-jobgradeid", v.jobgrade_id);
			        tSalary.rows(k).nodes().to$().attr("data-description", v.salary_description);
			        tSalary.rows(k).nodes().to$().attr("data-oldrate", v.salary_old_rate);
			        tSalary.rows(k).nodes().to$().attr("data-adjustment", v.salary_adjustment);
			        tSalary.rows(k).nodes().to$().attr("data-newrate", v.salary_new_rate);
			        tSalary.rows(k).nodes().to$().attr("data-effectivitydate", v.salary_effectivity_date);
			        tSalary.rows(k).nodes().to$().attr("data-positionitem_id", v.positionitem_id);
			        tSalary.rows(k).nodes().to$().attr("data-position_id", v.position_id);
			        tSalary.rows(k).nodes().to$().attr("data-step_inc", v.step_inc);
			        tSalary.rows(k).nodes().to$().attr("data-btnnew", "newSalary");
			        tSalary.rows(k).nodes().to$().attr("data-btnsave", "saveSalary");
			        tSalary.rows(k).nodes().to$().attr("data-btnedit", "editSalary");
			        tSalary.rows(k).nodes().to$().attr("data-btncancel", "cancelSalary");
				});

				//  NON PLANTILLA

				clear_form_elements('nonplantilla');
				$('.newNonPlantilla').attr('disabled',true);

				$('#jo_employee_no').val(data.employeeinfo.employee_id);
				if(data.employeeinfo.employeestatus !== null){
					$('#jo_employee_status').val(data.employeeinfo.employeestatus.Name);
				}
				if(data.employeeinfo.positionitems !== null){
					$('#jo_position_itemno').val(data.employeeinfo.positionitems.Name);
				}
				if(data.employeeinfo.positions !== null){
					$('#jo_position').val(data.employeeinfo.positions.Name);
				}
				if(data.employeeinfo.offices !== null){
					$('#jo_office').val(data.employeeinfo.offices.Name);
				}
				if(data.employeeinfo.divisions !== null){
					$('#jo_division').val(data.employeeinfo.divisions.Name);
				}
				$('#jo_assumption_to_duty').val(data.employeeinfo.assumption_date);

				if(data.nonplantilla !== null){
					$('#newNonPlantilla').addClass('hidden');
					$('#editNonPlantilla').removeClass('hidden');
					$('#cancelNonPlantilla').removeClass('hidden');
					$('#jo_employeeinfo_id').val(data.nonplantilla.id);
					daily_rate 	 = data.nonplantilla.daily_rate_amount;
					$('#job_order_daily_rate').val(commaSeparateNumber(data.nonplantilla.daily_rate_amount));
					$('#jo_taxpolicy_id').val(data.nonplantilla.taxpolicy_id);
					$('#jo_taxpolicy_two_id').val(data.nonplantilla.taxpolicy_two_id);
					$('#jo_atm_no').val(data.nonplantilla.atm_no);
					$('#jo_bank_id').val(data.nonplantilla.bank_id);
					jo_overtime_balance_amount = (data.nonplantilla.overtime_balance_amount) ? commaSeparateNumber(parseFloat(data.nonplantilla.overtime_balance_amount).toFixed(2)) : '';
					$('#jo_overtime_balance_amount').val(jo_overtime_balance_amount)
					$('#jo_tax_id_number').val(data.nonplantilla.tax_id_number);
				}else{
					$('#jo_employeeinfo_id').val('');
					$('#newNonPlantilla').removeClass('hidden');
					$('#editNonPlantilla').addClass('hidden');
					$('#saveNonPlantilla').addClass('hidden');
					$('#cancelNonPlantilla').addClass('hidden');
				}
			}
		});
	});

//SUBMIT FORM
$(document).off('click',".submitme").on('click',".submitme",function(){
	btn = $(this);
	form = $(this).data('form');

		$('#'+form).ajaxForm({
			beforeSend:function(){

			},
			success:function(data){
				par  =  JSON.parse(data);

				if(par.status){

					swal({  title: par.response,
							text: '',
							type: "success",
							icon: 'success',

						}).then(function(){

							if(par.employeeload){
								$('.btn_cancel').trigger('click');
								$('.btnfilter').trigger('click');
							}

							if(par.benefitinfo){

								// GENERATE TR FOR BENEFITS TAB

								generateBenefitInfoTable(par.benefitinfo);

							}// end of BenefitInfo

							if(par.loaninfo){
								// GENERATE TR FOR LOAN INFO TAB
								generateLoanInfoTable(par.loaninfo)

							}// end of Loaninfo

							if(par.deductioninfo){
								// GENERATE TR FOR DEDUCTION INFO TAB
								generateDeductionInfoTable(par.deductioninfo);

							}// end of Deductionifno

							if(par.salaryinfo){
								// GENERATE TR FOR SALARY INFO TAB
								var tSalary = $('#tbl_salaryinfo').DataTable();
								var sgjg = "";
								tSalary.clear().draw();

								$.each(par.salaryinfo,function(k,v){

									if(v.salarygrade_id){
										sgjg = 'SG '+v.salarygrade.salary_grade;
									}else{
										sgjg = 'JG '+v.jobgrade.job_grade;
									}

									tSalary.row.add( [
							        	par.salaryinfo[k].salary_effectivity_date,
							        	par.salaryinfo[k].salary_description,
										sgjg,
										par.salaryinfo[k].salary_old_rate,
										par.salaryinfo[k].salary_adjustment,
										par.salaryinfo[k].salary_new_rate,
							        ]).draw( false );

							        tSalary.rows(k).nodes().to$().attr("data-id", v.id);
							        tSalary.rows(k).nodes().to$().attr("data-employeeid", v.employee_id);
							        tSalary.rows(k).nodes().to$().attr("data-salarygradeid", v.salarygrade_id);
							        // tSalary.rows(k).nodes().to$().attr("data-jobgradeid", v.jobgrade_id);
							        tSalary.rows(k).nodes().to$().attr("data-description", v.salary_description);
							        tSalary.rows(k).nodes().to$().attr("data-oldrate", v.salary_old_rate);
							        tSalary.rows(k).nodes().to$().attr("data-adjustment", v.salary_adjustment);
							        tSalary.rows(k).nodes().to$().attr("data-newrate", v.salary_new_rate);
							        tSalary.rows(k).nodes().to$().attr("data-effectivitydate", v.salary_effectivity_date);
							        tSalary.rows(k).nodes().to$().attr("data-btnnew", "newSalary");
							        tSalary.rows(k).nodes().to$().attr("data-btnsave", "saveSalary");
							        tSalary.rows(k).nodes().to$().attr("data-btnedit", "editSalary");
							        tSalary.rows(k).nodes().to$().attr("data-btncancel", "cancelSalary");
								});

								$('.newSalary').attr('disabled',true);
								$('#newSalary').removeClass('hidden');
								$('#saveSalary').addClass('hidden');
								clear_form_elements("myform2");

							}// end of Salaryinfo

						});// end swal

				}else{

					swal({  title: par.response,
							text: '',
							type: "error",
							icon: 'error',

						});

				}// end of main IF STATUS

				btn.button('reset');
			},
			error:function(data){
				$error = data.responseJSON;
				/*reset popover*/
				$('input[type="text"], select').popover('destroy');

				/*add popover*/
				block = 0;
				$(".error-msg").remove();
				$.each($error,function(k,v){
					var messages = v.join(', ');
					msg = '<div class="error-msg err-'+k+'" style="color:red;"><i class="fa fa-exclamation-triangle" style="color:rgb(255, 184, 0);"></i> '+messages+'</div>';
					$('input[name="'+k+'"], textarea[name="'+k+'"], select[name="'+k+'"]').after(msg).attr('data-content',messages);
					if(block == 0){
						$('html, body').animate({
					        scrollTop: $('.err-'+k).offset().top - 250
					    }, 500);
					    block++;
					}
				})
				$('.saving').replaceWith(btn);
			},
			always:function(){
				setTimeout(function(){
						$('.saving').replaceWith(btn);
					},300)
			}
		}).submit();

});

$(document).on('change', '#emp_status',function(){
	status = $(this).find(':selected').val();

	clear_form_elements('nonplantilla');
	clear_form_elements('myform');
	clear_form_elements('myform2');
	clear_form_elements('myform3');
	clear_form_elements('myform4');
	clear_form_elements('myform5');
	$('.newNonPlantilla').attr('disabled',true);
	$('#newNonPlantilla').removeClass('hidden');
	$('#saveNonPlantilla').addClass('hidden');
	$('#editNonPlantilla').addClass('hidden');
	$('#cancelNonPlantilla').addClass('hidden');
	$('.newSummary').attr('disabled',true);
	$('#newSummary').removeClass('hidden');
	$('#saveSummary').addClass('hidden');
	$('#editSummary').addClass('hidden');
	$('#cancelSummary').addClass('hidden');

	switch(status){
		case 'plantilla':
			$('#plantilla').removeClass('hidden');
			$('#nonplantilla').addClass('hidden');
		break;
		case 'nonplantilla':
			$('#nonplantilla').removeClass('hidden');
			$('#plantilla').addClass('hidden');
		break;
	}
	$('.btnfilter').trigger('click');
});

var timer;
var empstatus;
$(document).on('click','.btnfilter',function(){
	// $('input.search').addClass('searchSpinner');
	tools  = $('#tools-form').serialize()

	category  	= $('#select_searchvalue :selected').val();
	empstatus   = $('#emp_status :selected').val();
	emp_type    = $('#emp_type :selected').val();
	searchby    = $('#searchby :selected').val();

	clearTimeout(timer);
	timer = setTimeout(
				function(){
					$.ajax({
					   type: "GET",
					   url: base_url+module_prefix+module+'/show',
					   data: {'category':category,'empstatus':empstatus,'emp_type':emp_type,'searchby':searchby },
					   beforeSend:function(){
					   		// $('#loading').removeClass('hidden');
					   },
					   complete:function(){
					   		// $('#loading').addClass('hidden');
					   },
					   success: function(res){
					      $(".sub-panelnamelist").html(res);
					      // $('input.search').removeClass('searchSpinner');
					   }
					});
				},500);
})


$(document).on('keyup','.search1',function(){
	clearTimeout(timer);
	timer = setTimeout(
		function(){
			$.ajax({
			   type: "GET",
			   url: base_url+module_prefix+module+'/show',
			   data: {"q":$('.search1').val(),
			   'emp_status':empstatus,
			   'limit':$(".limit").val()},
			   beforeSend:function(){

			   },
			   success: function(res){
			      $(".sub-panelnamelist").html(res);

			   },
			   complete:function(){

			   }
			});
		},500);
});
$(document).on('change','#benefit_id',function(){
	benefit_amount = $(this).find(':selected').data('amount');
	benefit_amount = (benefit_amount) ? commaSeparateNumber(benefit_amount) : '';
	$('#benefits_amount').val(benefit_amount);
});

// # DELETE BENEFIT INFO
$(document).on('click','#deleteBenefit',function(){
	if(!benefitinfo_id){
		swal("Select employee first!", "", "warning");
	}else{
		swal({
			title: "Delete Benefit?",
			type: "warning",
			showCancelButton: true,
			confirmButtonClass: "btn-warning",
			confirmButtonText: "Yes",
			cancelButtonText: "No",
		}).then(function(isConfirm){
			if(isConfirm.value == true){
				$.deleteBenefit();
			}else{
				return false;
			}
		});
	}
});

$.deleteBenefit = function(){
	$.ajax({
		url:base_url+module_prefix+module+'/deleteBenefitinfo',
		data:{
			'_token':'{{ csrf_token() }}',
			'benefit_info_id':benefitinfo_id,
			'employee_id':benefit_employee_id
		},
		type:'POST',
		dataType:'JSON',
		success:function(res){

			swal({
				title: 'Benefit Deleted',
				text: '',
				type: "success",
				icon: 'success',
			});

			generateBenefitInfoTable(res.data);
		}
	});
}
// # DELETE LOAN INFO
$(document).on('click','#deleteLoan',function(){
	if(!loaninfo_id){
		swal("Select employee first!", "", "warning");
	}else{
		swal({
			title: "Delete Loan?",
			type: "warning",
			showCancelButton: true,
			confirmButtonClass: "btn-warning",
			confirmButtonText: "Yes",
			cancelButtonText: "No",
		}).then(function(isConfirm){
			if(isConfirm.value == true){
				$.deleteLoanInfo();
			}else{
				return false;
			}
		});
	}
});

$.deleteLoanInfo = function(){
	$.ajax({
		url:base_url+module_prefix+module+'/deleteLoanInfo',
		data:{
			'_token':'{{ csrf_token() }}',
			'loan_info_id':loaninfo_id,
			'employee_id':loan_employee_id
		},
		type:'POST',
		dataType:'JSON',
		success:function(res){
			console.log(res);
			swal({
				title: 'Loan Deleted',
				text: '',
				type: "success",
				icon: 'success',
			});

			generateLoanInfoTable(res.data);
		}
	});
}

// # DELETE LOAN INFO
$(document).on('click','#deleteDeduct',function(){
	if(!loaninfo_id){
		swal("Select employee first!", "", "warning");
	}else{
		swal({
			title: "Delete Deduction?",
			type: "warning",
			showCancelButton: true,
			confirmButtonClass: "btn-warning",
			confirmButtonText: "Yes",
			cancelButtonText: "No",
		}).then(function(isConfirm){
			if(isConfirm.value == true){
				$.deleteLoanInfo();
			}else{
				return false;
			}
		});
	}
});

$.deleteLoanInfo = function(){
	$.ajax({
		url:base_url+module_prefix+module+'/deleteDeductInfo',
		data:{
			'_token':'{{ csrf_token() }}',
			'deduct_info_id':deduct_info_id,
			'employee_id':deduct_employee_id
		},
		type:'POST',
		dataType:'JSON',
		success:function(res){
			console.log(res);
			swal({
				title: 'Deduction Deleted',
				text: '',
				type: "success",
				icon: 'success',
			});

			generateDeductionInfoTable(res.data);
		}
	});
}

function generateBenefitInfoTable(benefitinfo){
	var tBenefit = $('#tbl_benefitinfo').DataTable();

	tBenefit.clear().draw();

	$.each(benefitinfo,function(k,v){

		tBenefit.row.add( [
        	benefitinfo[k].benefit_effectivity_date,
        	benefitinfo[k].benefits.name,
			benefitinfo[k].benefit_description,
			benefitinfo[k].benefit_amount,
			benefitinfo[k].benefit_pay_period,
        ]).draw( false );

        tBenefit.rows(k).nodes().to$().attr("data-id", v.id);
        tBenefit.rows(k).nodes().to$().attr("data-employeeid", v.employee_id);
        tBenefit.rows(k).nodes().to$().attr("data-date", v.benefit_effectivity_date);
        tBenefit.rows(k).nodes().to$().attr("data-name", v.benefits.name);
        tBenefit.rows(k).nodes().to$().attr("data-description", v.benefit_description);
        tBenefit.rows(k).nodes().to$().attr("data-payperiod", v.benefit_pay_period);
        tBenefit.rows(k).nodes().to$().attr("data-paysub", v.benefit_pay_sub);
        tBenefit.rows(k).nodes().to$().attr("data-amount", v.benefit_amount);
        tBenefit.rows(k).nodes().to$().attr("data-benefitid", v.benefit_id);
        tBenefit.rows(k).nodes().to$().attr("data-datefrom", v.date_from);
        tBenefit.rows(k).nodes().to$().attr("data-dateto", v.date_to);
        tBenefit.rows(k).nodes().to$().attr("data-btnnew", "newBenefit");
        tBenefit.rows(k).nodes().to$().attr("data-btnsave", "saveBenefit");
        tBenefit.rows(k).nodes().to$().attr("data-btnedit", "editBenefit");
        tBenefit.rows(k).nodes().to$().attr("data-btndelete", "deleteBenefit");
        tBenefit.rows(k).nodes().to$().attr("data-btncancel", "cancelBenefit");


	});

	$('.newBenefit').attr('disabled',true);
	$('#newBenefit').removeClass('hidden');
	$('#saveBenefit').addClass('hidden');
	$('#editBenefit').addClass('hidden');
	$('#deleteBenefit').addClass('hidden');
	$('#cancelBenefit').addClass('hidden');
	$('.weekly').removeClass('show');
	$('.weekly').addClass('hidden');
	$('.semi-monthly').removeClass('show');
	$('.semi-monthly').addClass('hidden');
	clear_form_elements("myform3");
}

// # loan Info Table
function generateLoanInfoTable(loaninfo){
	var tLoan = $('#tbl_loaninfo').DataTable();

	tLoan.clear().draw();

	$.each(loaninfo,function(k,v){

		tLoan.row.add( [
        	loaninfo[k].loans.name,
        	loaninfo[k].loan_totalamount,
			loaninfo[k].loan_totalbalance,
			loaninfo[k].loan_amortization,
			loaninfo[k].loan_date_started,
			loaninfo[k].loan_date_end,
        ]).draw( false );

        tLoan.rows(k).nodes().to$().attr("data-id", v.id);
        tLoan.rows(k).nodes().to$().attr("data-employeeid", v.employee_id);
        tLoan.rows(k).nodes().to$().attr("data-loanamount", v.loan_totalamount);
        tLoan.rows(k).nodes().to$().attr("data-loanbalance", v.loan_totalbalance);
        tLoan.rows(k).nodes().to$().attr("data-amortization", v.loan_amortization);
        tLoan.rows(k).nodes().to$().attr("data-datestart", v.loan_date_started);
        tLoan.rows(k).nodes().to$().attr("data-dateend", v.loan_date_end);
        tLoan.rows(k).nodes().to$().attr("data-loanid", v.loan_id);
        tLoan.rows(k).nodes().to$().attr("data-payperiod", v.loan_pay_period);
        tLoan.rows(k).nodes().to$().attr("data-dategranted", v.loan_date_granted);
        tLoan.rows(k).nodes().to$().attr("data-dateterminated", v.loan_date_terminated);
        tLoan.rows(k).nodes().to$().attr("data-btnnew", "newLoan");
        tLoan.rows(k).nodes().to$().attr("data-btnsave", "saveLoan");
        tLoan.rows(k).nodes().to$().attr("data-btnedit", "editLoan");
        tLoan.rows(k).nodes().to$().attr("data-btncancel", "cancelLoan");
        tLoan.rows(k).nodes().to$().attr("data-btndelete", "deleteLoan");
	});

	$('.newLoan').attr('disabled',true);
	$('#newLoan').removeClass('hidden');
	$('#editLoan').addClass('hidden');
	$('#deleteLoan').addClass('hidden');
	$('#saveLoan').addClass('hidden');
	$('#cancelLoan').addClass('hidden');
	clear_form_elements("myform4");
}

function generateDeductionInfoTable(deductioninfo){
	var tDeduct = $('#tbl_deductioninfo').DataTable();

	tDeduct.clear().draw();

	$.each(deductioninfo,function(k,v){

		name = deductioninfo[k].deductions.name;
		tDeduct.row.add( [
        	deductioninfo[k].deductions.name,
        	deductioninfo[k].deduct_amount,
			deductioninfo[k].deduct_date_start,
			deductioninfo[k].deduct_date_end,
			deductioninfo[k].deduct_pay_period,
        ]).draw( false );

		tDeduct.rows(k).nodes().to$().attr("data-name", name);
        tDeduct.rows(k).nodes().to$().attr("data-id", v.id);
        tDeduct.rows(k).nodes().to$().attr("data-employeeid", v.employee_id);
        tDeduct.rows(k).nodes().to$().attr("data-deductionid", v.deduction_id);
        tDeduct.rows(k).nodes().to$().attr("data-amount", v.deduct_amount);
        tDeduct.rows(k).nodes().to$().attr("data-datestart", v.deduct_date_start);
        tDeduct.rows(k).nodes().to$().attr("data-dateend", v.deduct_date_end);
        tDeduct.rows(k).nodes().to$().attr("data-payperiod", v.deduct_pay_period);
        tDeduct.rows(k).nodes().to$().attr("data-deduction_rate", v.deduction_rate);
        tDeduct.rows(k).nodes().to$().attr("data-dateterminated", v.deduct_date_terminated);
        tDeduct.rows(k).nodes().to$().attr("data-btnnew", "newDeduct");
        tDeduct.rows(k).nodes().to$().attr("data-btnsave", "saveDeduct");
        tDeduct.rows(k).nodes().to$().attr("data-btnedit", "editDeduct");
        tDeduct.rows(k).nodes().to$().attr("data-btncancel", "cancelDeduct");
        tDeduct.rows(k).nodes().to$().attr("data-btndelete", "deleteDeduct");
	});

	$('.newDeduct').attr('disabled',true);
	$('#newDeduct').removeClass('hidden');
	$('#saveDeduct').addClass('hidden');
	$('#editDeduct').addClass('hidden');
	$('#deleteDeduct').addClass('hidden');
	$('#cancelDeduct').addClass('hidden');
	clear_form_elements("myform5");
}

// NON PLANTILLA
var daily_rate = 0;
var monthly_rate = 0;
var annual_rate = 0;
$(document).on('keyup','#job_order_daily_rate',function(){
	daily_rate = $(this).val().replace(',','');
	jo_overtime_balance_amount = ((daily_rate*22)*12)/2;
	jo_overtime_balance_amount = (jo_overtime_balance_amount) ? commaSeparateNumber(parseFloat(jo_overtime_balance_amount).toFixed(2)) : '';
	$('#jo_overtime_balance_amount').val(jo_overtime_balance_amount)
	// $('#jo_no_ofdays_inamonth').trigger('keyup');

});

$(document).on('keyup','#jo_no_ofdays_inamonth',function(){
	monthly_rate = (parseFloat(daily_rate) * this.value) ;
	monthly_rate = (monthly_rate) ? commaSeparateNumber(parseFloat(monthly_rate).toFixed(2)) : 0;
	$('#jo_monthly_rate_amount').val(monthly_rate);

	$('#job_order_daily_rate').trigger('keyup');

});

$(document).on('keyup','#jo_no_ofdays_inayear',function(){
	annual_rate = (parseFloat(monthly_rate.replace(',','')) * this.value) ;
	annual_rate = (annual_rate) ? (parseFloat(annual_rate)) : 0;
	annual_rate = (annual_rate) ? commaSeparateNumber(parseFloat(annual_rate).toFixed(2)) : '';
	$('#jo_annual_rate_amount').val(annual_rate);
});

// $(document).on('change','#jo_taxpolicy_id',function(){
// 	tax_rate = $(this).find(':selected').data('taxrate');
// 	if(monthly_rate){
// 		monthly_rate = monthly_rate.replace(',','');
// 		job_order_tax_rate = (parseFloat(monthly_rate)*tax_rate);
// 		job_order_tax_rate = (job_order_tax_rate) ? commaSeparateNumber(parseFloat(job_order_tax_rate).toFixed(2)) : '';
// 		$('#jo_tax_amount').val(job_order_tax_rate);

// 	}else{
// 		swal({
// 			  title: "Monthly Rate is Empty",
// 			  type: "warning",
// 			  showCancelButton: false,
// 			  confirmButtonClass: "btn-danger",
// 			  confirmButtonText: "Yes",
// 			  closeOnConfirm: false
// 		});
// 		$(this).val('');
// 	}
// });
// $(document).on('change','#jo_taxpolicy_two_id',function(){
// 	tax_rate = $(this).find(':selected').data('taxrate');
// 	if(monthly_rate){
// 		job_order_tax_rate = (parseFloat(monthly_rate)*tax_rate);
// 		job_order_tax_rate = (job_order_tax_rate) ? commaSeparateNumber(parseFloat(job_order_tax_rate).toFixed(2)) : '';
// 		$('#jo_tax_amount_two').val(job_order_tax_rate);

// 	}else{
// 		swal({
// 			  title: "Monthly Rate is Empty",
// 			  type: "warning",
// 			  showCancelButton: false,
// 			  confirmButtonClass: "btn-danger",
// 			  confirmButtonText: "Yes",
// 			  closeOnConfirm: false
// 		});
// 		$(this).val('');
// 	}
// });

$(document).on('change','#jo_bank_id',function(){
	branch_name = $(this).find(':selected').data('jobankbranch');
	$('#jo_bank_branch').val(branch_name);
});

$(document).on('click','.btn_save_nonplantilla',function(){
	$.ajax({
		url:base_url+module_prefix+module+'/storeNonPlantilla',
		type:'POST',
		data:{
			'_token':"{{ csrf_token() }}",
			'employeeinfo_id':$('#jo_employeeinfo_id').val(),
			'employee_no':$('#jo_employee_no').val(),
			'job_order_daily_rate':$('#job_order_daily_rate').val(),
			'job_order_tax_rate':$('#jo_tax_amount').val(),
			'jo_taxpolicy_id':$('#jo_taxpolicy_id').val(),
			'jo_atm_no':$('#jo_atm_no').val(),
			'jo_bank_id':$('#jo_bank_id').val(),
			'jo_no_ofdays_inamonth':$('#jo_no_ofdays_inamonth').val(),
			'jo_total_hours_inaday':$('#jo_total_hours_inaday').val(),
			'jo_no_ofdays_inayear':$('#jo_no_ofdays_inayear').val(),
			'jo_taxpolicy_two_id':$('#jo_taxpolicy_two_id').val(),
			'jo_tax_rate_amount_two':$('#jo_tax_amount_two').val(),
			'jo_tax_id_number':$('#jo_tax_id_number').val(),
			'jo_monthly_rate_amount':$('#jo_monthly_rate_amount').val(),
			'jo_annual_rate_amount':$('#jo_annual_rate_amount').val(),
			'jo_overtime_balance_amount':$('#jo_overtime_balance_amount').val(),
		},
		success:function(data){
			par = JSON.parse(data)
			swal({
			  title: par.response,
			  type: "success",
			  showCancelButton: false,
			  confirmButtonClass: "btn-info",
			  confirmButtonText: "Yes",
			  closeOnConfirm: false
			});
			$('.newNonPlantilla').attr('disabled',true);
			$('#newNonPlantilla').removeClass('hidden');
			$('#saveNonPlantilla').addClass('hidden');
			$('#cancelNonPlantilla').addClass('hidden');
			clear_form_elements("nonplantilla");
		}
	})
});




})
</script>
@endsection
