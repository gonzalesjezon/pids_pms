@extends('app-reports')

@section('reports-content')
<link rel="stylesheet" type="text/css" href="{{ asset('css/printpayslip.css')}}">
<div class="loan-reports">
	<label>{{ $title }}</label>
	<table class="table borderless" id="loan-reports">
		<tr>
			<td ><span><b>Employee Name</b></span>
				<select class="form-control select2" name="employee_id" id="employee_id" style="margin:auto;width: 100%;">
					<option value=""></option>
					@foreach($employeeinfo as $value)
					<option value="{{ $value->id }}">{{ $value->lastname }} {{ $value->firstname }} {{ $value->middlename }}</option>
					@endforeach
				</select>
			</td>
		</tr>
		<tr>
			<td>
				<div class="row">
					<div class="col-md-12">
						<span class="lineheight" style="margin-left: 10px;"><b>Covered Date</b></span>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="col-md-6">
							<select class="form-control select2" name="month" id="select_month">
								<option value=""></option>
							</select>
						</div>
						<div class="col-md-6">
							<select class="form-control select2" name="year" id="select_year">
								<option value=""></option>
							</select>
						</div>

					</div>
				</div>
			</td>
		</tr>

	</table>
	<div class="reports-bot">
		<div class="col-md-6">
			<a class="btn btn-success btn-xs btn-editbg">
				Post
			</a>
		</div>
		<div class="col-md-6 text-right">
			<button type="button" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#prnModal" id="preview">Preview</button>
		</div>

	</div>
</div>

<div class="modal fade border0 in" id="prnModal" role="dialog">
	 <div class="modal-dialog border0 model-size">
	    <div class="mypanel border0">
	       <div class="panel-top bgSilver">
	          <a href="#" data-toggle="tooltip" data-placement="top" title="" data-reports="reports" id="print">
	             <i class="fa fa-print" aria-hidden="true"></i>Print
	          </a>
	          <button type="button" class="close" data-dismiss="modal">×</button>
	       </div>
	       <div style="height: 30px;"></div>
	       <div class="panel-body" >
		       <div class="row">
		       		<div class="col-md-12">
		       			<div class="reports" id="reports" style="width: 760px;margin: auto;">
		       				<div class="report-header">
		       					<div class="col-md-12 text-center" style="margin: auto;">
		       						<b>PHILIPPINE INSTITUTE FOR DEVELOPMENT STUDIES</b> <br>
									<b>PAYSLIP FOR THE MONTH OF <span id="pay_period"></span></b>
		       					</div>
		       				</div>
		       				<div style="border-bottom: 1px solid #e3e3e3; clear: both;"></div>
		       				<div class="reports-content">
		       				<div class="reports-body">
		       					<table class="table borderless" style="border: none;width: 98%; margin: auto;;" id="reports-table">
		       						<tr >
	       								<td  style="font-weight: bold;padding: 20px 0px 20px 6px;">
	       									EMPLOYEE NAME :<span id="fullname" style="margin-left: 15px;"></span>
	       								</td>
		       						</tr>
		       						<tr>
		       							<td style="font-weight: bold;">EARNINGS</td>
		       						</tr>
		       						<tr>
		       							<td>
		       								<span style="margin-left: 25px;">Basic Salary</span>
		       							</td>
		       							<td  class="text-right" ><span id="basic_salary" style="margin-right: 55px;"></span></td>
		       						</tr>
		       						<tr>
		       							<td>
		       								<span style="margin-left: 25px;">PERA</span>
		       							</td>
		       							<td class="text-right" ><span id="pera_amount" style="margin-right: 55px;"></span></td>
		       						</tr>
		       						<tr>
		       							<td>
		       								<span style="margin-left: 25px;">RATA</span>
		       							</td>
		       							<td class="text-right" >
		       								<span id="rata_amount" style="margin-right: 55px;"></span>
		       							</td>
		       						</tr>
		       						<tr style="font-weight: bold;">
		       							<td>GROSS SALARY</td>
		       							<td class="text-right"><span id="gross_amount"></span></td>
		       						</tr>
		       						<tr>
		       							<td style="font-weight: bold;padding: 20px 0px 10px 6px;">
		       								<span>DEDUCTIONS:</span>
		       							</td>
		       						</tr>
		       						<tr>
		       							<td>
		       								<span style="margin-left: 25px;">GSIS  Contribution</span>
		       							</td>
		       							<td class="text-right"><span id="gsis_contribution_amount" style="margin-right: 55px;"></span></td>
		       						</tr>
		       						<tr>
		       							<td>
		       								<span style="margin-left: 25px;">Pag-ibig  Contribution</span>
		       							</td>
		       							<td class="text-right"><span id="pagibig_contribution_amount" style="margin-right: 55px;"></span></td>
		       						</tr>
		       						<tr>
		       							<td>
		       								<span style="margin-left: 25px;">PhilHealth  Contribution</span>
		       							</td>
		       							<td class="text-right"><span id="philhealth_contribution_amount" style="margin-right: 55px;"></span></td>
		       						</tr>
		       						<tr>
		       							<td>
		       								<span style="margin-left: 25px;">PIDS Provident Fund Contribution</span>
		       							</td>
		       							<td class="text-right"><span id="provident_fund_contribution_amount" style="margin-right: 55px;"></span></td>
		       						</tr>
		       						<tr>
		       							<td>
		       								<span style="margin-left: 25px;">GSIS Conso Loan</span>
		       							</td>
		       							<td class="text-right"><span id="gsis_conso_loan_amount" style="margin-right: 55px;"></span></td>
		       						</tr>
		       						<tr>
		       							<td>
		       								<span style="margin-left: 25px;">GSIS Policy Loan</span>
		       							</td>
		       							<td class="text-right"><span id="gsis_policy_loan_amount" style="margin-right: 55px;"></span></td>
		       						</tr>
		       						<tr>
		       							<td>
		       								<span style="margin-left: 25px;">GSIS Housing Loan</span>
		       							</td>
		       							<td class="text-right"><span id="gsis_policy_housing_loan_amount" style="margin-right: 55px;"></span></td>
		       						</tr>
		       						<tr>
		       							<td>
		       								<span style="margin-left: 25px;">HDMF Multi Purpose Loan</span>
		       							</td>
		       							<td class="text-right"><span id="hdmf_loan_amount" style="margin-right: 55px;"></span></td>
		       						</tr>
		       						<tr>
		       							<td>
		       								<span style="margin-left: 25px;">Withholding Tax</span>
		       							</td>
		       							<td class="text-right" >
		       								<span id="withholding_tax_amount" style="margin-right: 55px;"></span>
		       							</td>
		       						</tr>
		       						<tr>
		       							<td style="font-weight: bold;padding: 20px 0px 10px 6px;">
		       								TOTAL DEDUCTIONS
		       							</td>
		       							<td class="text-right" style="padding-top: 20px;">
		       								<span id="tota_deduction_amount"></span>
		       							</td>
		       						</tr>
		       						<tr>
		       							<td style="font-weight: bold;padding: 20px 0px 10px 6px;">
		       								NET PAY
		       							</td>
		       							<td class="text-right" style="padding-top: 20px;">
		       								<span id="net_pay_amount" style="font-weight: bold;"></span>
		       							</td>
		       						</tr>
		       						<tr style="border-bottom:2px solid #333">
		       							<td style="padding: 20px 0px 10px 6px;" colspan="2">
		       							</td>
		       						</tr>
		       					</table>
		       				</div>
		       				<div class="row" style="margin-left: 10px;margin-right: 15px;" >
		       					<table class="table borderless" style="border:none;">
		       						<tr>
		       							<td class="text-left">NET PAY FOR THE WEEK</td>
		       							<td></td>
		       							<td class="text-left" style="padding-left: 3em;padding-right: 3em;">
		       								ADJUSTMENT
		       							</td>
		       							<td class="text-right" style="padding-left: 3em;">NET CASH</td>
		       						</tr>
		       					</table>
		       				</div>
		       				<div class="row" style="margin-left: 10px;margin-right: 15px;">
		       					<table class="table borderless" style="border: none;">
		       						<tr style="border: none;">
		       							<td style="width: 90px;">1st Week</td>
		       							<td class="text-right" style="width: 10em;">
		       								<span class="first_period_amount"></span>
		       							</td>
		       							<td class="text-right" style="width: 15em;"></td>
		       							<td class="text-right" style="width: 15em;">
		       								<span class="first_period_amount"></span>
		       							</td>
		       						</tr>
		       						<tr>
		       							<td style="width: 90px;">2nd Week</td>
		       							<td class="text-right" style="width: 10em;">
		       								<span class="second_period_amount"></span>
		       							</td>
		       							<td class="text-right" style="width: 15em;"></td>
		       							<td class="text-right" style="width: 15em;">
		       								<span class="second_period_amount"></span>
		       							</td>
		       						</tr>
		       						<tr>
		       							<td style="width: 90px;">3rd Week</td>
		       							<td class="text-right" style="width: 10em;">
		       								<span class="third_period_amount"></span>
		       							</td>
		       							<td class="text-right" style="width: 15em;"></td>
		       							<td class="text-right" style="width: 15em;">
		       								<span class="third_period_amount"></span>
		       							</td>
		       						</tr>
		       						<tr>
		       							<td style="width: 90px;">4th Week</td>
		       							<td class="text-right" style="width: 10em;">
		       								<span class="fourth_period_amount"></span>
		       							</td>
		       							<td class="text-right" style="width: 15em;"></td>
		       							<td class="text-right" style="width: 15em;">
		       								<span class="fourth_period_amount"></span>
		       							</td>
		       						</tr>
		       					</table>
		       				</div>
		       				<div class="row" style="padding-left: 15px;margin-top: 10px;">
		       					<div class="col-md-12">
		       						<i style="font-size:12px;">Reflections: All our dreams can come true, if we have the courage to pursue them. – Walt Disney</i>
		       					</div>
		       				</div>
		       				<br><br>
		       				<div class="reports-footer">
		       					<div class="col-md-6">
		       						<div class="footer-left-message">
			       						<i>
			       							This is a computer generated document and does
											not require any signature if without alterations
			       						</i>

		       						</div>
		       					</div>
		       				</div>
		       			</div>
		       		</div>
		       </div>
	       </div>

	    </div>
	 </div>
</div>

@endsection

@section('js-logic2')
<script type="text/javascript">
$(document).ready(function(){

	// GENERATE YEAR
	var year = [];
	year += '<option ></option>';
	for(y = 2018; y <= 2100; y++) {
        year += '<option value='+y+'>'+y+'</option>';
	}
    $('#select_year').html(year);

    // GENERATE MONTH
    month = ["January","February","March","April","May","June","July","August","September","October", "November","December"];
    mArr = [];

    mArr += '<option ></option>';
    for ( m =  0; m <= month.length - 1; m++) {
    	mArr += '<option '+month[m]+'>'+month[m]+'</option>';
    }
    $('#select_month').html(mArr);


// ************************************************
	var _Year;
	var _Month;
	var _empid;
	var _searchvalue;
	var _emp_status;
	var _emp_type;
	var _searchby;
	$('.select2').select2();

	$('#select_year').select2({
		allowClear:true,
	    placeholder: "Year",
	});

	$('#select_month').select2({
		allowClear:true,
	    placeholder: "Month",
	});

	$(document).on('change','#select_year',function(){
		_Year = "";
		_Year = $(this).find(':selected').val();

	})
	$(document).on('change','#select_month',function(){
		_Month = "";
		_Month = $(this).find(':selected').val();
	})
	$(document).on('change','#employee_id',function(){
		_empid = "";
		_empid = $(this).find(':selected').val();

	})

	$(document).on('change','#select_searchvalue',function(){
		_searchvalue = "";
		_searchvalue = $(this).find(':selected').val();

	})

	$(document).on('change','#emp_status',function(){
		_emp_status = "";
		_emp_status = $(this).find(':selected').val();

	})
	$(document).on('change','#emp_type',function(){
		_emp_type = "";
		_emp_type = $(this).find(':selected').val();

	})
	$(document).on('change','#searchby',function(){
		_searchby = "";
		_searchby = $(this).find(':selected').val();

	})
	$(document).on('click','#print',function(){
		print = $(this).data('reports');
		PrintElem(print);
	});


	$(document).on('change','#searchby',function(){
		var val = $(this).val();

		$.ajax({
			url:base_url+module_prefix+module+'/getSearchby',
			data:{'q':val},
			type:'GET',
			dataType:'JSON',
			success:function(data){

				arr = [];
				$.each(data,function(k,v){
					arr += '<option value='+v.RefId+'>'+v.Name+'</option>';
				})

				$('#select_searchvalue').html(arr);
			}
		})

	});


$(document).on('click','#preview',function(){

	year = (_Year) ? _Year : '';
	month = (_Month) ? _Month : '';
	emp_type = (_emp_type) ? _emp_type : '';
	emp_status = (_emp_status) ? _emp_status : '';
	month = (_Month) ? _Month : '';
	category = (_searchvalue) ? _searchvalue : '';
	searchby = (_searchby) ? _searchby : '';

	$.ajax({
		url:base_url+module_prefix+module+'/show',
		data:{
			'id':_empid,
			'year':year,
			'month':month,
			'emp_type':emp_type,
			'emp_status':emp_status,
			'category':category,
			'searchby':searchby,
		},
		type:'GET',
		dataType:'JSON',
		success:function(data){
			console.log(data)
			basic_salary = (data.transaction.basic_net_pay) ? data.transaction.basic_net_pay : 0.00;
			$('#basic_salary').text(commaSeparateNumber(parseFloat(basic_salary).toFixed(2)));

			month = data.transaction.month;
			year = data.transaction.year;

			$('#pay_period').text(month+' '+year);

			fullname = data.transaction.employees.lastname+' '+data.transaction.employees.firstname+' '+data.transaction.employees.middlename;
			$('#fullname').text(fullname);

			var transportation_amount = 0;
			var representation_amount = 0;
			if(data.rata !== null){
				transportation_amount = (data.rata.transportation_amount !== null) ? data.rata.transportation_amount : 0;
				representation_amount = (data.rata.representation_amount !== null) ? data.rata.representation_amount : 0;
			}

			rata_amount = (parseFloat(transportation_amount).toFixed(4) + parseFloat(representation_amount).toFixed(4));

			pera_amount = (data.transaction.benefit_transaction !== null) ? data.transaction.benefit_transaction.amount : 0;

			gross_amount = (parseFloat(basic_salary) + parseFloat(pera_amount));


			taxamount = (data.employeeinfo.tax_contribution) ? data.employeeinfo.tax_contribution : 0.00;
			gsiscontribution = (data.employeeinfo.gsis_contribution) ? data.employeeinfo.gsis_contribution : 0.00;
			pagibigcontribution = (data.employeeinfo.pagibig_contribution) ? data.employeeinfo.pagibig_contribution : 0.00;
			philhealtcontribution = (data.employeeinfo.philhealth_contribution) ? data.employeeinfo.philhealth_contribution : 0.00;

			totalcontribution = (parseFloat(philhealtcontribution) + parseFloat(pagibigcontribution) + parseFloat(gsiscontribution));

			var consoloan;
			var policyloan;
			var calamityloan;
			var optionallife;
			var housingloan;
			var mpl;
			var educloan;
			var mp2;
			$.each(data.loaninfo,function(k,v){
				if(data.loaninfo[k].loans.name == 'POLICY LOAN'){
					policyloan = (data.loaninfo[k].loan_amortization) ? data.loaninfo[k].loan_amortization : 0;
				}

				if(data.loaninfo[k].loans.name == 'CONSOLOAN'){
					consoloan = (data.loaninfo[k].loan_amortization) ? data.loaninfo[k].loan_amortization : 0;
				}

				if(data.loaninfo[k].loans.name == 'EMERGENCY/CALAMITY LOAN'){
					calamityloan = (data.loaninfo[k].loan_amortization) ? data.loaninfo[k].loan_amortization : 0;

				}

				if(data.loaninfo[k].loans.name == 'OPTIONAL LIFE'){
					optionallife = (data.loaninfo[k].loan_amortization) ? data.loaninfo[k].loan_amortization : 0;
				}

				if(data.loaninfo[k].loans.name == 'MULTI PURPOSE LOAN'){
					mpl = (data.loaninfo[k].loan_amortization) ? data.loaninfo[k].loan_amortization : 0;
				}

				if(data.loaninfo[k].loans.name == 'EDUC LOAN'){
					educloan = (data.loaninfo[k].loan_amortization) ? data.loaninfo[k].loan_amortization : 0;
				}
				if(data.loaninfo[k].loans.name == 'HOUSING LOAN'){
					housingloan = (data.loaninfo[k].loan_amortization) ? data.loaninfo[k].loan_amortization : 0;
				}
			});

			policyloan = (policyloan) ? policyloan : 0;
			calamityloan = (calamityloan) ? calamityloan : 0;
			optionallife = (optionallife) ? optionallife : 0;
			consoloan = (consoloan) ? consoloan : 0;
			mpl = (mpl) ? mpl : 0;
			mp2 = (mp2) ? mp2 : 0;
			educloan = (educloan) ? educloan : 0;
			housingloan = (housingloan) ? housingloan : 0;

			total_loan_amount = (parseFloat(policyloan) + parseFloat(consoloan) + parseFloat(housingloan) + parseFloat(mpl) + parseFloat(taxamount));

			tota_deduction_amount = (parseFloat(totalcontribution) + parseFloat(total_loan_amount));

			net_pay_amount = (parseFloat(gross_amount) - parseFloat(tota_deduction_amount));

			// ===== COMPUTE WEEKLY NET PAY =====

			compute_net_amount = ((parseFloat(net_pay_amount) - parseFloat(pera_amount) - parseFloat(rata_amount))/4);

			first_period_amount = (parseFloat(compute_net_amount) + parseFloat(rata_amount));
			second_period_amount = parseFloat(compute_net_amount) + parseFloat(pera_amount);
			third_period_amount = compute_net_amount;
			fourth_period_amount = parseFloat(net_pay_amount) - parseFloat(first_period_amount) - parseFloat(second_period_amount) - parseFloat(third_period_amount);

			// ===== COMPUTE WEEKLY NET PAY =====


			taxamount = (taxamount !== 0) ?commaSeparateNumber(parseFloat(taxamount).toFixed(2)) : '';
			gsiscontribution = (gsiscontribution !== 0) ?commaSeparateNumber(parseFloat(gsiscontribution).toFixed(2)) : '';
			pagibigcontribution = (pagibigcontribution !== 0) ?commaSeparateNumber(parseFloat(pagibigcontribution).toFixed(2)) : '';
			philhealtcontribution = (philhealtcontribution !== 0) ?commaSeparateNumber(parseFloat(philhealtcontribution).toFixed(2)) : '';
			rata_amount = (rata_amount !== 0) ?commaSeparateNumber(parseFloat(rata_amount).toFixed(2)) : '';
			pera_amount = (pera_amount !== 0) ?commaSeparateNumber(parseFloat(pera_amount).toFixed(2)) : '';
			gross_amount = (gross_amount !== 0) ?commaSeparateNumber(parseFloat(gross_amount).toFixed(2)) : '';
			tota_deduction_amount = (tota_deduction_amount !== 0) ?commaSeparateNumber(parseFloat(tota_deduction_amount).toFixed(2)) : '';
			mpl = (mpl !== 0) ?commaSeparateNumber(parseFloat(mpl).toFixed(2)) : '';
			optionallife = (optionallife !== 0) ?commaSeparateNumber(parseFloat(optionallife).toFixed(2)) : '';
			calamityloan = (calamityloan !== 0) ?commaSeparateNumber(parseFloat(calamityloan).toFixed(2)) : '';
			policyloan = (policyloan !== 0) ?commaSeparateNumber(parseFloat(policyloan).toFixed(2)) : '';
			consoloan = (consoloan !== 0) ?commaSeparateNumber(parseFloat(consoloan).toFixed(2)) : '';
			housingloan = (housingloan !== 0) ?commaSeparateNumber(parseFloat(housingloan).toFixed(2)) : '';
			mp2 = (mp2 !== 0) ?commaSeparateNumber(parseFloat(mp2).toFixed(2)) : '';
			net_pay_amount = (net_pay_amount !== 0) ?commaSeparateNumber(parseFloat(net_pay_amount).toFixed(2)) : '';
			first_period_amount = (first_period_amount !== 0) ?commaSeparateNumber(parseFloat(first_period_amount).toFixed(2)) : '';
			second_period_amount = (second_period_amount !== 0) ?commaSeparateNumber(parseFloat(second_period_amount).toFixed(2)) : '';
			third_period_amount = (third_period_amount !== 0) ?commaSeparateNumber(parseFloat(third_period_amount).toFixed(2)) : '';
			fourth_period_amount = (fourth_period_amount !== 0) ?commaSeparateNumber(parseFloat(fourth_period_amount).toFixed(2)) : '';
			$('#pera_amount').text(pera_amount);
			$('#rata_amount').text(rata_amount);
			$('#gross_amount').text(gross_amount);
			$('#withholding_tax_amount').text(taxamount);
			$('#gsis_contribution_amount').text(gsiscontribution);
			$('#pagibig_contribution_amount').text(pagibigcontribution);
			$('#philhealth_contribution_amount').text(philhealtcontribution);
			$('#tota_deduction_amount').text(tota_deduction_amount);
			$('#net_pay_amount').text(net_pay_amount);
			$('#mpl').text(mpl);
			$('#optionallife').text(optionallife);
			$('#calamityloan').text(calamityloan);
			$('#gsis_policy_loan_amount').text(policyloan);
			$('#gsis_conso_loan_amount').text(consoloan);
			$('#housingloan').text(housingloan);
			$('#mp2').text(mp2);
			$('.first_period_amount').text(first_period_amount);
			$('.second_period_amount').text(second_period_amount);
			$('.third_period_amount').text(third_period_amount);
			$('.fourth_period_amount').text(fourth_period_amount);


		}
	})


});

function PrintElem(elem)
{
    var mywindow = window.open('', 'PRINT', 'height=700px,width=1260px');

    mywindow.document.write('<html><head><title>' + document.title  + '</title>');
    mywindow.document.write('</head><body >');
    mywindow.document.write('<h1>' + document.title  + '</h1>');
    mywindow.document.write($('#'+elem).html());
    mywindow.document.write('</body></html>');

    mywindow.document.close(); // necessary for IE >= 10
    mywindow.focus(); // necessary for IE >= 10*/

    mywindow.print();
    mywindow.close();

    return true;
}

})
</script>
@endsection