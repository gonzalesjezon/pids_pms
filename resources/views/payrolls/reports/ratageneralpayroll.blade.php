@extends('app-reports')

@section('reports-content')
<link rel="stylesheet" type="text/css" href="{{ asset('css/printlandscapetwo.css')}}">
<div class="loan-reports">
	<label>{{ $title }}</label>
	<table class="table borderless" id="loan-reports">
		<tr>
			<td>
				<div class="row">
					<div class="col-md-12">
						<span class="lineheight" style="margin-left: 10px;"><b>Covered Date</b></span>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="col-md-6">
							<select class="form-control select2" name="month" id="select_month">
								<option value=""></option>
							</select>
						</div>
						<div class="col-md-6">
							<select class="form-control select2" name="year" id="select_year">
								<option value=""></option>
							</select>
						</div>

					</div>
				</div>
			</td>
		</tr>

	</table>
	<div class="reports-bot">
		<div class="col-md-6">
			<a class="btn btn-success btn-xs btn-editbg">
				Post
			</a>
		</div>
		<div class="col-md-6 text-right">
			<button type="button" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#prnModal" id="preview">Preview</button>
		</div>

	</div>
</div>

<div class="modal fade border0 in" id="prnModal" role="dialog">
	 <div class="modal-dialog border0 model-size">
	    <div class="mypanel border0" style="height:100%;">
	       <div class="panel-top bgSilver">
	          <a href="#" data-toggle="tooltip" data-placement="top" title="" id="print">
	             <i class="fa fa-print" aria-hidden="true"></i>Print
	          </a>
	          <button type="button" class="close" data-dismiss="modal">×</button>
	       </div>
	       <div style="height: 30px;"></div>
	       <div class="panel-body" id="reports" style="margin-left: 15px; margin-right: 15px;">
	       		<div class="row">
	       			<div class="col-xs-12">
	       				PHILIPPINE INSTITUTE FOR DEVELOPMENT STUDIES <br>
						Computation of Representation and Transportation Allowance for Salary Deduction <br>
						For the month of <span id="covered_period"></span>
	       			</div>
	       		</div>
	       		<div class="row" style="font-weight: bold;margin-top: 20px;">
	       			<div class="col-xs-6 text-center">
	       				Per General Payroll
	       			</div>
	       			<div class="col-xs-6 text-center">
	       				---- Deductions On ----
	       			</div>
	       		</div>
	       		<div class="row">
	       			<div class="col-xs-12">
	       				<table class="table">
	       					<thead>
		       					<tr style="font-weight: bold;" class="text-center">
		       						<td>Name</td>
		       						<td>Representation <br> Allowance</td>
		       						<td>Transportation <br> Allowance</td>
		       						<td>Actual Work <br> Performance in mo.</td>
		       						<td>Adjustment of RATA<br> for the month</td>
		       						<td>Representation <br> Allowance</td>
		       						<td>Transportation <br> Allowance</td>
		       						<td>Total</td>
		       					</tr>
	       					</thead>
	       					<tbody id="tbl_body"></tbody>
	       				</table>
	       			</div>
	       		</div>
	       		<div class="row" style="margin-top: 30px;">
	       			<div class="col-xs-6">
	       				<span style="margin-left: 25px;">Certified Correct:</span>
	       			</div>
	       		</div>
	       		<div class="row" style="margin-top: 30px;">
	       			<div class="col-xs-6">
	       				<span style="margin-left: 25px;">
	       					MARISA S. ABOGADO
	       				</span> <br>
	       				<span style="margin-left: 25px;">
	       					DC II -  Acctg. & Control
	       				</span>
	       			</div>
	       			<div class="col-xs-6">
	       				<span>
	       					MA. DANA E. PATUAR	<br>
							DC III - Administrative Division
	       				</span>
	       			</div>
	       		</div>
	       </div>
	    </div>
	 </div>
</div>

@endsection

@section('js-logic2')
<script type="text/javascript">
$(document).ready(function(){
	// GENERATE YEAR
	var year = [];
	year += '<option ></option>';
	for(y = 2018; y <= 2100; y++) {
        year += '<option value='+y+'>'+y+'</option>';
	}
    $('#select_year').html(year);

    // GENERATE MONTH
    month = ["January","February","March","April","May","June","July","August","September","October", "November","December"];
    mArr = [];

    mArr += '<option ></option>';
    for ( m =  0; m <= month.length - 1; m++) {
    	mArr += '<option '+month[m]+'>'+month[m]+'</option>';
    }
    $('#select_month').html(mArr);


// ************************************************
	var _Year;
	var _Month;
	var _empid;
	var _searchvalue;
	var _emp_status;
	var _emp_type;
	var _searchby;
	$('.select2').select2();

	$('#select_year').select2({
		allowClear:true,
	    placeholder: "Year",
	});

	$('#select_month').select2({
		allowClear:true,
	    placeholder: "Month",
	});

	$(document).on('change','#select_year',function(){
		_Year = "";
		_Year = $(this).find(':selected').val();

	})
	$(document).on('change','#select_month',function(){
		_Month = "";
		_Month = $(this).find(':selected').val();
	})
	$(document).on('change','#employee_id',function(){
		_empid = "";
		_empid = $(this).find(':selected').val();

	})

	$(document).on('change','#select_searchvalue',function(){
		_searchvalue = "";
		_searchvalue = $(this).find(':selected').val();

	})

	$(document).on('change','#emp_status',function(){
		_emp_status = "";
		_emp_status = $(this).find(':selected').val();

	})
	$(document).on('change','#emp_type',function(){
		_emp_type = "";
		_emp_type = $(this).find(':selected').val();

	})
	$(document).on('change','#searchby',function(){
		_searchby = "";
		_searchby = $(this).find(':selected').val();

	})


	$(document).on('change','#searchby',function(){
		var val = $(this).val();

		$.ajax({
			url:base_url+module_prefix+module+'/getSearchby',
			data:{'q':val},
			type:'GET',
			dataType:'JSON',
			success:function(data){

				arr = [];
				$.each(data,function(k,v){
					arr += '<option value='+v.RefId+'>'+v.Name+'</option>';
				})

				$('#select_searchvalue').html(arr);
			}
		})

	});

$(document).on('click','#preview',function(){

	year = (_Year) ? _Year : '';
	month = (_Month) ? _Month : '';

	$.ajax({
		url:base_url+module_prefix+module+'/show',
		data:{
			'year':year,
			'month':month,
		},
		type:'GET',
		dataType:'JSON',
		success:function(data){
			console.log(data)
			if(data !== null){

				arr = [];
				net_rep_amount			= 0;
				net_transpo_amount 		= 0;
				net_amount 				= 0;

				$.each(data,function(k,v){
					firstname = (v.employees) ? v.employees.firstname : '';
					lastname = (v.employees) ? v.employees.lastname : '';
					middlename = (v.employees) ? v.employees.middlename : '';

					fullanme = lastname+' '+firstname+' '+middlename;

					rep_amount = (v.representation_amount) ? v.representation_amount : 0;
					transpo_amount = (v.transportation_amount) ? v.transportation_amount : 0;
					adjust_percentage = (v.adjustment_percentage) ? v.adjustment_percentage : 0;
					adjust_rep_amount = (v.adjustment_rep_amount) ? v.adjustment_rep_amount : 0;
					adjust_transpo_amount = (v.adjustment_transpo_amount) ? v.adjustment_transpo_amount : 0;

					total_amount = parseFloat(adjust_rep_amount) + parseFloat(adjust_transpo_amount);

					net_rep_amount += parseFloat(adjust_rep_amount);
					net_transpo_amount += parseFloat(adjust_transpo_amount);
					net_amount += parseFloat(total_amount);

					rep_amount = (rep_amount !== 0) ? commaSeparateNumber(parseFloat(rep_amount).toFixed(2)) : '';
					transpo_amount = (transpo_amount !== 0) ? commaSeparateNumber(parseFloat(transpo_amount).toFixed(2)) : '';
					adjust_rep_amount = (adjust_rep_amount !== 0) ? commaSeparateNumber(parseFloat(adjust_rep_amount).toFixed(2)) : '';
					adjust_transpo_amount = (adjust_transpo_amount !== 0) ? commaSeparateNumber(parseFloat(adjust_transpo_amount).toFixed(2)) : '';
					adjust_percentage = (adjust_percentage !== 0) ? parseFloat(adjust_percentage)*100 +'%' : '';
					total_amount = (total_amount !== 0) ? commaSeparateNumber(parseFloat(total_amount).toFixed(2)) : '';

					arr += '<tr>';
					arr += '<td>'+fullanme+'</td>';
					arr += '<td class="text-right">'+rep_amount+'</td>';
					arr += '<td class="text-right">'+transpo_amount+'</td>';
					arr += '<td></td>';
					arr += '<td class="text-center">'+adjust_percentage+'</td>';
					arr += '<td class="text-right">'+adjust_rep_amount+'</td>';
					arr += '<td class="text-right">'+adjust_transpo_amount+'</td>';
					arr += '<td class="text-right">'+total_amount+'</td>';
					arr += '</tr>';
				});

				net_rep_amount = (net_rep_amount !== 0) ? commaSeparateNumber(parseFloat(net_rep_amount).toFixed(2)) : '';
				net_transpo_amount = (net_transpo_amount !== 0) ? commaSeparateNumber(parseFloat(net_transpo_amount).toFixed(2)) : '';
				net_amount = (net_amount !== 0) ? commaSeparateNumber(parseFloat(net_amount).toFixed(2)) : '';

				arr += '<tr style="font-weight:bold;">';
				arr += '<td>TOTAL</td>';
				arr += '<td class="text-right"></td>';
				arr += '<td class="text-right"></td>';
				arr += '<td></td>';
				arr += '<td class="text-center"></td>';
				arr += '<td class="text-right">'+adjust_rep_amount+'</td>';
				arr += '<td class="text-right">'+adjust_transpo_amount+'</td>';
				arr += '<td class="text-right">'+total_amount+'</td>';
				arr += '</tr>';

				$('#tbl_body').html(arr);

				$('#covered_period').text(_Month+' '+_Year);
			}
		}
	})


});
$('#print').on('click',function(){
	$('#reports').printThis();
})
})
</script>
@endsection