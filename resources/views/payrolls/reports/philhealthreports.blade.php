@extends('app-reports')

@section('reports-content')

<link rel="stylesheet" type="text/css" href="{{ asset('css/printlandscapetwo.css')}}">
<div class="loan-reports">
	<label>{{ $title }}</label>
	<table class="table borderless" id="loan-reports">
		<tr>
			<td>
				<div class="row">
					<div class="col-md-12">
						<span class="lineheight" style="margin-left: 10px;"><b>Covered Date</b></span>
					</div>

				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="col-md-6">
							<select class="form-control select2" name="month" id="select_month">
								<option value=""></option>
							</select>
						</div>
						<div class="col-md-6">
							<select class="form-control select2" name="year" id="select_year">
								<option value=""></option>
							</select>
						</div>

					</div>
				</div>

			</td>

		</tr>

	</table>
	<div class="reports-bot">
		<div class="col-md-6">
			<a class="btn btn-success btn-xs btn-editbg">
				Post
			</a>
		</div>
		<div class="col-md-6 text-right">
			<button type="button" class="btn btn-danger btn-xs hidden" data-toggle="modal" data-target="#prnModal" id="btnModal">Preview</button>
			<a class="btn btn-danger btn-xs" id="preview">Preview</a>
		</div>

	</div>
</div>

<div class="modal fade border0 in" id="prnModal" role="dialog">
	 <div class="modal-dialog border0 model-size">
	    <div class="mypanel border0" style="height:550px;width:100%;overflow-y:auto;overflow-x:auto;overflow: scroll;" >
	       <div class="panel-top bgSilver">
	          <a href="#" data-toggle="tooltip" data-placement="top" title="" id="print">
	             <i class="fa fa-print" aria-hidden="true"></i>Print
	          </a>
	          <button type="button" class="close" data-dismiss="modal">×</button>
	       </div>
	       <div style="height: 30px;"></div>
	       <div class="container-fluid" id="reports">
	       		<div class="row">
	       			<div class="col-md-12 text-left" style="margin: auto;">
	       				PHILIPPINE INSTITUTE FOR DEVELOPMENT STUDIES <br>
						Phil-Health  Contribution <br>
						For the month of <span id="month_year"></span>
	       			</div>
	       		</div>
	       		<div class="row">
	       			<div class="col-md-12">
						<table class="table" id="payroll_transfer" style="font-size:12px;">
							 <thead>
							 	<tr class="text-center">
							 		<td>#</td>
							 		<td>Name of Employee</td>
							 		<td>Basic Salary</td>
							 		<td>EE Share</td>
							 		<td>ER Share</td>
							 		<td>Total</td>
							 	</tr>
							 </thead>
							 <tbody id="tbl_body">
							 </tbody>
						</table>
	       			</div>
	       		</div>
	       </div>
	 	</div>
	</div>
</div>
@endsection

@section('js-logic2')
<script type="text/javascript">
$(document).ready(function(){
	// GENERATE YEAR
	var year = [];
	year += '<option ></option>';
	for(y = 2018; y <= 2100; y++) {
        year += '<option value='+y+'>'+y+'</option>';
	}
    $('#select_year').html(year);

    // GENERATE MONTH
    month = ["January","February","March","April","May","June","July","August","September","October", "November","December"];
    mArr = [];

    mArr += '<option ></option>';
    for ( m =  0; m <= month.length - 1; m++) {
    	mArr += '<option '+month[m]+'>'+month[m]+'</option>';
    }
    $('#select_month').html(mArr);


// ************************************************
	var _Year;
	var _Month;
	var _empid;
	var _searchvalue;
	var _emp_status;
	var _emp_type;
	var _searchby;
	$('.select2').select2();

	$('#select_year').select2({
		allowClear:true,
	    placeholder: "Year",
	});

	$('#select_month').select2({
		allowClear:true,
	    placeholder: "Month",
	});

	$(document).on('change','#select_year',function(){
		_Year = "";
		_Year = $(this).find(':selected').val();

	})
	$(document).on('change','#select_month',function(){
		_Month = "";
		_Month = $(this).find(':selected').val();
	})
	$(document).on('change','#employee_id',function(){
		_empid = "";
		_empid = $(this).find(':selected').val();

	})

	$(document).on('change','#select_searchvalue',function(){
		_searchvalue = "";
		_searchvalue = $(this).find(':selected').val();

	})

	$(document).on('change','#emp_status',function(){
		_emp_status = "";
		_emp_status = $(this).find(':selected').val();

	})
	$(document).on('change','#emp_type',function(){
		_emp_type = "";
		_emp_type = $(this).find(':selected').val();

	})
	$(document).on('change','#searchby',function(){
		_searchby = "";
		_searchby = $(this).find(':selected').val();

	})


	$(document).on('change','#searchby',function(){
		var val = $(this).val();

		$.ajax({
			url:base_url+module_prefix+module+'/getSearchby',
			data:{'q':val},
			type:'GET',
			dataType:'JSON',
			success:function(data){

				arr = [];
				$.each(data,function(k,v){
					arr += '<option value='+v.RefId+'>'+v.Name+'</option>';
				})

				$('#select_searchvalue').html(arr);
			}
		})

	});

	$(document).on('click','#preview',function(){

		if(!_Year && !_Month){
			swal({
				  title: "Select year and month first",
				  type: "warning",
				  showCancelButton: false,
				  confirmButtonClass: "btn-danger",
				  confirmButtonText: "Yes",
				  closeOnConfirm: false

			});
		}else{
			$.ajax({
				url:base_url+module_prefix+module+'/show',
				data:{'month':_Month,'year':_Year},
				type:'GET',
				dataType:'JSON',
				success:function(data){
				console.log(data);
					if(data.length !== 0){
						arr = [];
						ctr = 0;

						net_monthly_amount = 0;
						net_ee_share_amount = 0;
						net_er_share_amount = 0;
						net_total_share_amount = 0;

						$.each(data,function(key,val){
							sub_monthly_amount = 0;
							sub_ee_share_amount = 0;
							sub_er_share_amount = 0;
							sub_total_share_amount = 0;

							arr += '<tr style="border:1px solid #c0c0c0;">';
							arr += '<td></td>';
							arr += '<td colspan="5"><b>'+key+'<b></td>';
							arr += '</tr>';

							$.each(val,function(k,v){

								monthly_amount = (v.employeeinfo) ? v.employeeinfo.monthly_rate_amount : 0;
								ee_share_amount = (v.employeeinfo) ? v.employeeinfo.philhealth_contribution : 0;
								er_share_amount = (v.employeeinfo) ? v.employeeinfo.er_philhealth_share : 0;

								total_share_amount = parseFloat(ee_share_amount) + parseFloat(er_share_amount);

								sub_monthly_amount += parseFloat(monthly_amount);
								sub_ee_share_amount += parseFloat(ee_share_amount);
								sub_er_share_amount += parseFloat(er_share_amount);
								sub_total_share_amount += parseFloat(total_share_amount);


								monthly_amount = (monthly_amount !== 0) ? commaSeparateNumber(parseFloat(monthly_amount).toFixed(2)) : '';
								ee_share_amount = (ee_share_amount !== 0) ? commaSeparateNumber(parseFloat(ee_share_amount).toFixed(2)) : '';
								er_share_amount = (er_share_amount !== 0) ? commaSeparateNumber(parseFloat(er_share_amount).toFixed(2)) : '';
								total_share_amount = (total_share_amount !== 0) ? commaSeparateNumber(parseFloat(total_share_amount).toFixed(2)) : '';

								arr += '<tr>';
								arr += '<td  lass=" text-center">'+(ctr+1)+'</td>';
								arr += '<td class="border-black">'+v.employees.lastname+' '+v.employees.firstname+' '+v.employees.middlename+'</td>';
								arr += '<td class="text-right">'+monthly_amount+'</td>';
								arr += '<td class="text-right">'+ee_share_amount+'</td>';
								arr += '<td class="text-right">'+er_share_amount+'</td>';
								arr += '<td class="text-right">'+total_share_amount+'</td>';
								arr += '</tr>';
								ctr++;

							});
							ctr = 0;
							net_monthly_amount += parseFloat(sub_monthly_amount);
							net_ee_share_amount += parseFloat(sub_ee_share_amount);
							net_er_share_amount += parseFloat(sub_er_share_amount);
							net_total_share_amount += parseFloat(sub_total_share_amount);

							sub_monthly_amount = (sub_monthly_amount !== 0) ? commaSeparateNumber(parseFloat(sub_monthly_amount).toFixed(2)) : '';
							sub_ee_share_amount = (sub_ee_share_amount !== 0) ? commaSeparateNumber(parseFloat(sub_ee_share_amount).toFixed(2)) : '';
							sub_er_share_amount = (sub_er_share_amount !== 0) ? commaSeparateNumber(parseFloat(sub_er_share_amount).toFixed(2)) : '';
							sub_total_share_amount = (sub_total_share_amount !== 0) ? commaSeparateNumber(parseFloat(sub_total_share_amount).toFixed(2)) : '';

							arr += '<tr style="font-weight:bold;">';
							arr += '<td ></td>';
							arr += '<td  lass=" text-center">SUB TOTAL</td>';
							arr += '<td class="text-right">'+sub_monthly_amount+'</td>';
							arr += '<td class="text-right">'+sub_ee_share_amount+'</td>';
							arr += '<td class="text-right">'+sub_er_share_amount+'</td>';
							arr += '<td class="text-right">'+sub_total_share_amount+'</td>';
							arr += '</tr>';

						});

						net_monthly_amount = (net_monthly_amount !== 0) ? commaSeparateNumber(parseFloat(net_monthly_amount).toFixed(2)) : '';
						net_ee_share_amount = (net_ee_share_amount !== 0) ? commaSeparateNumber(parseFloat(net_ee_share_amount).toFixed(2)) : '';
						net_er_share_amount = (net_er_share_amount !== 0) ? commaSeparateNumber(parseFloat(net_er_share_amount).toFixed(2)) : '';
						net_total_share_amount = (net_total_share_amount !== 0) ? commaSeparateNumber(parseFloat(net_total_share_amount).toFixed(2)) : '';

						arr += '<tr style="font-weight:bold;">';
						arr += '<td ></td>';
						arr += '<td  lass=" text-center">GRAND TOTAL</td>';
						arr += '<td class="text-right">'+net_monthly_amount+'</td>';
						arr += '<td class="text-right">'+net_ee_share_amount+'</td>';
						arr += '<td class="text-right">'+net_er_share_amount+'</td>';
						arr += '<td class="text-right">'+net_total_share_amount+'</td>';
						arr += '</tr>';

						$('#tbl_body').html(arr);

						$('#month_year').text(_Month+' '+_Year);

						$('#btnModal').trigger('click');


					}else{
						swal({
							title: "No Records Found",
							type: "warning",
							showCancelButton: false,
							confirmButtonClass: "btn-danger",
							confirmButtonText: "Yes",
							closeOnConfirm: false
						});
					}
				}
			})
		}
	});

$('#print').on('click',function(){
	$('#reports').printThis();
})

})
</script>
@endsection