@extends('app-reports')

@section('reports-content')
<link rel="stylesheet" type="text/css" href="{{ asset('css/printportrait.css')}}">
<div class="loan-reports">
	<label>{{ $title }}</label>
	<table class="table borderless" id="loan-reports">
		<tr>
			<td>
				<div class="row">
					<div class="col-md-12">
						<span class="lineheight" style="margin-left: 10px;"><b>Covered Date</b></span>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="col-md-6">
							<select class="form-control select2" name="month" id="select_month">
								<option value=""></option>
							</select>
						</div>
						<div class="col-md-6">
							<select class="form-control select2" name="year" id="select_year">
								<option value=""></option>
							</select>
						</div>

					</div>
				</div>
			</td>
		</tr>

	</table>
	<div class="reports-bot">
		<div class="col-md-6">
			<a class="btn btn-success btn-xs btn-editbg">
				Post
			</a>
		</div>
		<div class="col-md-6 text-right">
			<button type="button" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#prnModal" id="preview">Preview</button>
		</div>

	</div>
</div>

<div class="modal fade border0 in" id="prnModal" role="dialog">
	 <div class="modal-dialog border0 model-size">
	    <div class="mypanel border0">
	       <div class="panel-top bgSilver">
	          <a href="#" data-toggle="tooltip" data-placement="top" title="" id="print">
	             <i class="fa fa-print" aria-hidden="true"></i>Print
	          </a>
	          <button type="button" class="close" data-dismiss="modal">×</button>
	       </div>
	       <div style="height: 30px;"></div>
	       <div class="panel-body" id="reports" style="margin-left: 15px; margin-right: 15px;width: 760px;margin: auto;">
	       		<div class="row">
	       			<div class="col-xs-12">
						Philippine Institute for Development Studies	<br>
						Schedule of  Salary Adjustment -  <span id="employee_name"></span> 	   (Promotion) <br>
						For the period <span id="covered_period"></span>
	       			</div>
	       		</div>
	       		<div class="row" style="margin-top: 50px;">
	       			<div class="col-xs-12">Basic Pay</div>
	       		</div>
	       		<div class="row">
	       			<div class="col-xs-6">
	       				<span style="margin-left: 25px;">As Senior Research Specialist</span>
	       			</div>
	       			<div class="col-xs-6 text-right"></div>
	       		</div>
	       		<div class="row">
	       			<div class="col-xs-6">
	       				<span style="margin-left: 25px;">  As Research Specialist</span>
	       			</div>
	       			<div class="col-xs-6 text-right">
	       				<span style="border-bottom: 1px solid #333;">
	       					000000
	       				</span>
	       			</div>
	       		</div>
	       		<div class="row">
	       			<div class="col-xs-6">
	       				<span >Salary Adjustment</span>
	       			</div>
	       			<div class="col-xs-6 text-right">
	       				<span style="border-bottom: 2px solid #333;">
	       					000000
	       				</span>
	       			</div>
	       		</div>
	       		<div class="row" style="margin-top: 30px;">
	       			<div class="col-xs-6">
	       				<span >Salary Adjustment</span>
	       			</div>
	       			<div class="col-xs-6 text-right">
	       				<span>
	       					000000
	       				</span>
	       			</div>
	       		</div>
	       		<div class="row">
	       			<div class="col-xs-6">
	       				<span >Less:</span>
	       			</div>
	       		</div>
	       		<div class="row">
	       			<div class="col-xs-6">
	       				<span style="margin-left: 25px;">GSIS Contribution</span>
	       			</div>
	       			<div class="col-xs-6 text-right">
	       				<span >
	       					000000
	       				</span>
	       			</div>
	       		</div>
	       		<div class="row">
	       			<div class="col-xs-6">
	       				<span style="margin-left: 25px;">PhilHealth Contribution </span>
	       			</div>
	       			<div class="col-xs-6 text-right">
	       				<span >
	       					000000
	       				</span>
	       			</div>
	       		</div>
	       		<div class="row">
	       			<div class="col-xs-6">
	       				<span style="margin-left: 25px;">Provident Fund Contribution</span>
	       			</div>
	       			<div class="col-xs-6 text-right">
	       				<span >
	       					000000
	       				</span>
	       			</div>
	       		</div>
	       		<div class="row">
	       			<div class="col-xs-6">
	       				<span style="margin-left: 25px;">Withholding Tax</span>
	       			</div>
	       			<div class="col-xs-6 text-right">
	       				<span style="border-bottom: 1px solid #333;">
	       					000000
	       				</span>
	       			</div>
	       		</div>
	       		<div class="row">
	       			<div class="col-xs-6">
	       				<span >Total Deduction</span>
	       			</div>
	       			<div class="col-xs-6 text-right">
	       				<span style="border-bottom: 1px solid #333;">
	       					000000
	       				</span>
	       			</div>
	       		</div>
	       		<div class="row">
	       			<div class="col-xs-6">
	       				<span >Net Amount Due</span>
	       			</div>
	       			<div class="col-xs-6 text-right">
	       				<span style="border-bottom: 2px solid #333;">
	       					000000
	       				</span>
	       			</div>
	       		</div>
	       		<div class="row" style="margin-top: 30px;">
	       			<div class="col-xs-6">
	       				<span style="margin-left: 25px;">Certified Correct:</span>
	       			</div>
	       		</div>
	       		<div class="row" style="margin-top: 30px;">
	       			<div class="col-xs-6">
	       				<span style="margin-left: 25px;">
	       					MARISA S. ABOGADO
	       				</span> <br>
	       				<span style="margin-left: 25px;">
	       					DC II -  Acctg. & Control
	       				</span>
	       			</div>
	       			<div class="col-xs-6">
	       				<span>
	       					MA. DANA E. PATUAR	<br>
							DC III - Administrative Division
	       				</span>
	       			</div>
	       		</div>
	       </div>
	    </div>
	 </div>
</div>

@endsection

@section('js-logic2')
<script type="text/javascript">
$(document).ready(function(){
	// GENERATE YEAR
	var year = [];
	year += '<option ></option>';
	for(y = 2018; y <= 2100; y++) {
        year += '<option value='+y+'>'+y+'</option>';
	}
    $('#select_year').html(year);

    // GENERATE MONTH
    month = ["January","February","March","April","May","June","July","August","September","October", "November","December"];
    mArr = [];

    mArr += '<option ></option>';
    for ( m =  0; m <= month.length - 1; m++) {
    	mArr += '<option '+month[m]+'>'+month[m]+'</option>';
    }
    $('#select_month').html(mArr);


// ************************************************
	var _Year;
	var _Month;
	var _empid;
	var _searchvalue;
	var _emp_status;
	var _emp_type;
	var _searchby;
	$('.select2').select2();

	$('#select_year').select2({
		allowClear:true,
	    placeholder: "Year",
	});

	$('#select_month').select2({
		allowClear:true,
	    placeholder: "Month",
	});

	$(document).on('change','#select_year',function(){
		_Year = "";
		_Year = $(this).find(':selected').val();

	})
	$(document).on('change','#select_month',function(){
		_Month = "";
		_Month = $(this).find(':selected').val();
	})
	$(document).on('change','#employee_id',function(){
		_empid = "";
		_empid = $(this).find(':selected').val();

	})

	$(document).on('change','#select_searchvalue',function(){
		_searchvalue = "";
		_searchvalue = $(this).find(':selected').val();

	})

	$(document).on('change','#emp_status',function(){
		_emp_status = "";
		_emp_status = $(this).find(':selected').val();

	})
	$(document).on('change','#emp_type',function(){
		_emp_type = "";
		_emp_type = $(this).find(':selected').val();

	})
	$(document).on('change','#searchby',function(){
		_searchby = "";
		_searchby = $(this).find(':selected').val();

	})


	$(document).on('change','#searchby',function(){
		var val = $(this).val();

		$.ajax({
			url:base_url+module_prefix+module+'/getSearchby',
			data:{'q':val},
			type:'GET',
			dataType:'JSON',
			success:function(data){

				arr = [];
				$.each(data,function(k,v){
					arr += '<option value='+v.RefId+'>'+v.Name+'</option>';
				})

				$('#select_searchvalue').html(arr);
			}
		})

	});

$(document).on('click','#preview',function(){

	// year = (_Year) ? _Year : '';
	// month = (_Month) ? _Month : '';
	// emp_type = (_emp_type) ? _emp_type : '';
	// emp_status = (_emp_status) ? _emp_status : '';
	// month = (_Month) ? _Month : '';
	// category = (_searchvalue) ? _searchvalue : '';
	// searchby = (_searchby) ? _searchby : '';

	// $.ajax({
	// 	url:base_url+module_prefix+module+'/show',
	// 	data:{
	// 		'id':_empid,
	// 		'year':year,
	// 		'month':month,
	// 		'emp_type':emp_type,
	// 		'emp_status':emp_status,
	// 		'category':category,
	// 		'searchby':searchby,
	// 	},
	// 	type:'GET',
	// 	dataType:'JSON',
	// 	success:function(data){

	// 	}
	// })


});
$('#print').on('click',function(){
	$('#reports').printThis();
})

})
</script>
@endsection