@extends('app-reports')

@section('reports-content')
<link rel="stylesheet" type="text/css" href="{{ asset('css/printportrait.css')}}">
<style type="text/css">
	.add-margin{
		margin-left: 15px;
		margin-right: 15px;
	}
</style>

<div class="loan-reports">
	<label>{{ $title }}</label>
	<table class="table borderless" id="loan-reports">
		<tr>
			<td>
				<div class="row" >
					<div class="col-md-12">
						<span class="lineheight" style="margin-left: 10px;"><b>Select Employee</b></span>
					</div>
				</div>
				<div class="row" style="margin-left: 0px;margin-right: 0px;">
					<div class="col-md-12">
						<select class="form-control select2" id="employee_id" name="employee_id">
							<option></option>
							@foreach($employee as $key => $value)
							<option value="{{ $value->id }}">{{ ucwords(strtolower($value->lastname)) }} {{ ucwords(strtolower($value->firstname)) }} {{ ucwords(strtolower($value->middlename)) }}</option>
							@endforeach
						</select>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<span class="lineheight" style="margin-left: 10px;"><b>Covered Date</b></span>
					</div>
				</div>
				<div class="row" style="margin-left: 0px;margin-right: 0px;">
					<div class="col-md-6">
						<select class="form-control select2" name="month" id="select_month">
							<option value=""></option>
						</select>
					</div>
					<div class="col-md-6">
						<select class="form-control select2" name="year" id="select_year">
							<option value=""></option>
						</select>
					</div>
				</div>
			</td>
		</tr>

	</table>
	<div class="reports-bot">
		<div class="col-md-6">
			<a class="btn btn-success btn-xs btn-editbg">
				Post
			</a>
		</div>
		<div class="col-md-6 text-right">
			<button type="button" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#prnModal" id="preview">Preview</button>
		</div>

	</div>
</div>

<div class="modal fade border0 in" id="prnModal" role="dialog">
	 <div class="modal-dialog border0 model-size">
	    <div class="mypanel border0">
	       <div class="panel-top bgSilver">
	          <a href="#" data-toggle="tooltip" data-placement="top" title="" id="print">
	             <i class="fa fa-print" aria-hidden="true"></i>Print
	          </a>
	          <button type="button" class="close" data-dismiss="modal">×</button>
	       </div>
	       <div style="height: 30px;"></div>
	       <br>
	       <div class="panel style1" id="reports" style="width: 1080px;margin: auto;padding: 20px;">
		       <div class="row add-margin">
		       		<div class="col-md-12 text-center" style="font-weight: bold;margin: auto;">
		       			AUTHORITY TO DEBIT / CREDIT ACCOUNT
		       		</div>
		       </div>
		       <div class="row">
		       		<div class="col-md-2">Employer Code</div>
		       		<div class="col-md-10">07594</div>
		       </div>
		       <div class="row">
		       		<div class="col-md-2">Employer's Name:</div>
		       		<div class="col-md-10">PHILIPPINE INSTITUTE FOR DEVELOPMENT STUDIES</div>
		       </div>
		       <div class="row">
		       		<div class="col-md-2">Address:</div>
		       		<div class="col-md-10">18F Three Cyberpod Centris, North Tower EDSA cor. Quezon Ave., Quezon City</div>
		       </div>
		       <div class="row">
		       		<div class="col-md-2">Tel. No.:</div>
		       		<div class="col-md-10">877-4000; 877-4008</div>
		       </div>
		       <div class="row text-center" style="margin-top: 20px;">
		       		<div class="col-md-4">NAME OF EMPLOYEE</div>
		       		<div class="col-md-4">ACCOUNT NO.</div>
		       		<div class="col-md-4">AMOUNT</div>
		       </div>
		       <div class="row text-left" style="margin-top: 5px;">
		       		<div class="col-md-4"><span id="employee_name"></span></div>
		       		<div class="col-md-4 text-center"><span id="account_no"></span></div>
		       		<div class="col-md-4 text-center"><span id="amount"></span></div>
		       </div>
		       <div class="row text-left" style="margin-top: 5px;">
		       		<div class="col-md-8"><span>Total</span></div>
		       		<div class="col-md-4"><span id="total_amount"></span></div>
		       </div>
		       <div class="row text-left" style="margin-top: 5px;">
		       		<div class="col-md-8"><span>Less:  1% Service Fee</span></div>
		       		<div class="col-md-4"><span id="less_percentage_amount"></span></div>
		       </div>
		       <div class="row text-left" style="margin-top: 5px;">
		       		<div class="col-md-8"><span>Net Collection</span></div>
		       		<div class="col-md-4"><span id="net_collection_amount"></span></div>
		       </div>
		       <div class="row text-left" style="margin-top: 5px;">
		       		<div class="col-md-8"><span>Add:  Penalty for late remittance (for the month ______)</span></div>
		       		<div class="col-md-4"><span id="penalty_amount"></span></div>
		       </div>
		       <div class="row text-left" style="margin-top: 5px;font-weight: bold;">
		       		<div class="col-md-8"><span>TOTAL REMITTANCE</span></div>
		       		<div class="col-md-4"><span id="remittance_amount"></span></div>
		       </div>
		       <div class="row text-center" style="margin-top: 20px;">
		       		<div class="col-md-6">Mode of  Payments:  [  ]  Thru Bank</div>
		       		<div class="col-md-6">[  ]  Direct Payment to NHMFC</div>
		       </div>
		       <div class="row" style="margin-top: 20px;">
		       		<div class="col-md-12">
		       			<table class="table" style="width: 350px;margin: auto;">
		       				<tr>
		       					<td style="padding: 5px;padding-bottom: 20px;" colspan="4" class="text-left" >To be  filled by NHMFC or Bank:</td>
		       				</tr>
		       				<tr>
		       					<td style="padding: 5px;padding-bottom: 20px;" class="text-left">Payment received by:</td>
		       				</tr>
		       				<tr>
		       					<td style="padding: 5px;" class="text-left">MBR No./Date</td>
		       				</tr>
		       			</table>
		       		</div>
		       </div>
		       <div class="row text-left" style="margin-top: 20px;">
		       		<div class="col-md-6">Certified Correct:</div>
		       		<div class="col-md-6">Noted by:</div>
		       </div>
		       <div class="row text-left" style="margin-top: 50px;">
		       		<div class="col-md-6">
		       			MARISA S. ABOGADO <br>
		       			DC II  -  Acctg. & Control Div.
		       		</div>
		       		<div class="col-md-6">
		       			ANDREA S. AGCAOILI	<br>
						Department Manager III	<br>
						Administrative and Finance Department
		       		</div>
		       </div>
		       <div class="row text-left" style="margin-top: 20px;">
		       		<div class="col-md-12">Reminders:</div>
		       </div>
		       <div class="row text-left" style="margin-top: 20px;">
		       		<div class="col-md-12 text-justify">
		       			<span style="padding-left: 10px;">
		       				1.  If paid thru bank please prepare SPD in two copies; 1 for NHMFC and 1 for Employer's <br>
						    file, but if paid directly at NHMFC, please prepare SPD in 4 copies; 3 for NHMFC and 1 	<br>
						    for employer's file.
		       			</span>
		       		</div>
		       </div>
		       <div class="row text-left" >
		       		<div class="col-md-12 text-justify">
		       			<span style="padding-left: 10px;">
		       				2.  Deductions for the month must be remitted on or before the 10th of the following month;
		       			</span>
		       		</div>
		       </div>
		       <div class="row text-left" >
		       		<div class="col-md-12 text-justify">
		       			<span style="padding-left: 10px;">
		       				3.  Penalty for late remittance is equivalent to 1/10 of 1% of Net Collections per day of delay
		       			</span>
		       		</div>
		       </div>
		       <div class="row text-left" >
		       		<div class="col-md-12 text-justify">
		       			<span style="padding-left: 10px;">
		       				4.  Penalty for late submission of report is P200.00 per day of delay.
		       			</span>
		       		</div>
		       </div>
	       </div>
	       <br>
	    </div>
	 </div>
</div>

@endsection

@section('js-logic2')
<script type="text/javascript">
$(document).ready(function(){
	// GENERATE YEAR
	var year = [];
	year += '<option ></option>';
	for(y = 2018; y <= 2100; y++) {
        year += '<option value='+y+'>'+y+'</option>';
	}
    $('#select_year').html(year);

    // GENERATE MONTH
    month = ["January","February","March","April","May","June","July","August","September","October", "November","December"];
    mArr = [];

    mArr += '<option ></option>';
    for ( m =  0; m <= month.length - 1; m++) {
    	mArr += '<option '+month[m]+'>'+month[m]+'</option>';
    }
    $('#select_month').html(mArr);


// ************************************************
	var _Year;
	var _Month;
	var _empid;
	var _searchvalue;
	var _emp_status;
	var _emp_type;
	var _searchby;
	$('.select2').select2();

	$('#select_year').select2({
		allowClear:true,
	    placeholder: "Year",
	});

	$('#select_month').select2({
		allowClear:true,
	    placeholder: "Month",
	});

	$(document).on('change','#select_year',function(){
		_Year = "";
		_Year = $(this).find(':selected').val();

	})
	$(document).on('change','#select_month',function(){
		_Month = "";
		_Month = $(this).find(':selected').val();
	})
	$(document).on('change','#employee_id',function(){
		_empid = "";
		_empid = $(this).find(':selected').val();

	})

	$(document).on('change','#select_searchvalue',function(){
		_searchvalue = "";
		_searchvalue = $(this).find(':selected').val();

	})

	$(document).on('change','#emp_status',function(){
		_emp_status = "";
		_emp_status = $(this).find(':selected').val();

	})
	$(document).on('change','#emp_type',function(){
		_emp_type = "";
		_emp_type = $(this).find(':selected').val();

	})
	$(document).on('change','#searchby',function(){
		_searchby = "";
		_searchby = $(this).find(':selected').val();

	})


	$(document).on('change','#searchby',function(){
		var val = $(this).val();

		$.ajax({
			url:base_url+module_prefix+module+'/getSearchby',
			data:{'q':val},
			type:'GET',
			dataType:'JSON',
			success:function(data){

				arr = [];
				$.each(data,function(k,v){
					arr += '<option value='+v.RefId+'>'+v.Name+'</option>';
				})

				$('#select_searchvalue').html(arr);
			}
		})

	});

$(document).on('click','#preview',function(){

	year = (_Year) ? _Year : '';
	month = (_Month) ? _Month : '';
	emp_type = (_emp_type) ? _emp_type : '';
	emp_status = (_emp_status) ? _emp_status : '';
	month = (_Month) ? _Month : '';
	category = (_searchvalue) ? _searchvalue : '';
	searchby = (_searchby) ? _searchby : '';

	$.ajax({
		url:base_url+module_prefix+module+'/show',
		data:{
			'id':_empid,
			'year':year,
			'month':month,
		},
		type:'GET',
		dataType:'JSON',
		success:function(data){
			console.log(data);
		}
	})


});

$('#print').on('click',function(){
	$('#reports').printThis();
})

})
</script>
@endsection