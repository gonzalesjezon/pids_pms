@extends('app-reports')

@section('reports-content')

<link rel="stylesheet" type="text/css" href="{{ asset('css/printlandscapetwo.css')}}">
<div class="loan-reports">
	<label>{{ $title }}</label>
	<table class="table borderless" id="loan-reports">
		<tr>
			<td>
				<div class="row">
					<div class="col-md-12">
						<span class="lineheight" style="margin-left: 10px;"><b>Covered Date</b></span>
					</div>

				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="col-md-6">
							<select class="form-control select2" name="month" id="select_month">
								<option value=""></option>
							</select>
						</div>
						<div class="col-md-6">
							<select class="form-control select2" name="year" id="select_year">
								<option value=""></option>
							</select>
						</div>

					</div>
				</div>

			</td>

		</tr>

	</table>
	<div class="reports-bot">
		<div class="col-md-6">
			<a class="btn btn-success btn-xs btn-editbg">
				Post
			</a>
		</div>
		<div class="col-md-6 text-right">
			<button type="button" class="btn btn-danger btn-xs hidden" data-toggle="modal" data-target="#prnModal" id="btnModal">Preview</button>
			<a class="btn btn-danger btn-xs" id="preview">Preview</a>
		</div>

	</div>
</div>

<div class="modal fade border0 in" id="prnModal" role="dialog">
	 <div class="modal-dialog border0 model-size">
	    <div class="mypanel border0" style="height:550px;width:100%;overflow-y:auto;overflow-x:auto;overflow: scroll;" >
	       <div class="panel-top bgSilver">
	          <a href="#" data-toggle="tooltip" data-placement="top" title="" id="print">
	             <i class="fa fa-print" aria-hidden="true"></i>Print
	          </a>
	          <button type="button" class="close" data-dismiss="modal">×</button>
	       </div>
	       <div style="height: 30px;"></div>
	       <div class="container-fluid" id="reports">
	       		<div class="row">
	       			<div class="col-md-12 text-center" style="margin: auto;">
	       				<b>PHILIPPINE INSTITUTE FOR DEVELOPMENT STUDIES</b> <br>
						18F Three Cyberpod Centris, North Tower EDSA cor. Quezon Ave., Quezon City <br>
						<b>GENERAL PAYROLL FOR THE MONTH OF <span id="month_year"></span></b>
	       			</div>
	       		</div>
	       		<div class="row">
	       			<div class="col-md-12">
						<table class="table" id="payroll_transfer" style="font-size:12px;width: 1600px">
							 <thead>
							 	<tr class="text-center">
							 		<td rowspan="4" >#</td>
							 		<td rowspan="4" style="line-height: 80px;">
							 			<span >Name of Employee</span>
							 		</td>
							 		<td colspan="4">Earnings</td>
							 		<td colspan="3">Witholding</td>
							 		<td colspan="5">Loans</td>
							 		<td rowspan="5">TOTAL</td>
							 		<td rowspan="3">Due to Officer & Employees</td>
							 		<td rowspan="5">Net Pay</td>
							 		<td colspan="2">Net Pay</td>
							 	</tr>
							 	<tr class="text-center">
							 		<td rowspan="3" style="width: 95px; line-height: 75px;">Basic Salary</td>
							 		<td rowspan="3" style="line-height: 75px">PERA</td>

							 	</tr>
							 	<tr class="text-center">
							 		<td>Representation</td>
							 		<td rowspan="2" style="line-height: 45px;line-height: 75px">Total</td>
							 		<td>Tax Withheld</td>
							 		<td>GSIS</td>
							 		<td>Philhealth</td>
							 		<td>NHMFC/HDMF HOUSING</td>
							 		<td >MP2</td>
							 		<td>GSIS Salary</td>
							 		<td>Educ. Assist</td>
							 		<td>Optional Loan</td>
							 		<td style="width: 70px;">01-07</td>
							 		<td style="width: 70px;">16-22</td>
							 	</tr>
							 	<tr class="text-center">
							 		<td>Transportation</td>
							 		<td>Provident Fund</td>
							 		<td>Pagibig</td>
							 		<td>Total</td>
							 		<td>GSIS HOUSING</td>
							 		<td>M-Purpose</td>
							 		<td>GSIS Emergency</td>
							 		<td>Policy</td>
							 		<td>Optional Cont</td>
							 		<td>Coop Ded'n</td>
							 		<td style="width: 70px;">8-15</td>
							 		<td style="width: 70px;">23-30</td>
							 	</tr>
							 </thead>
							 <tbody id="tbl_body">
							 </tbody>
						</table>
	       			</div>
	       		</div>
	       </div>
	 	</div>
	</div>
</div>
@endsection

@section('js-logic2')
<script type="text/javascript">
$(document).ready(function(){
	// GENERATE YEAR
	var year = [];
	year += '<option ></option>';
	for(y = 2018; y <= 2100; y++) {
        year += '<option value='+y+'>'+y+'</option>';
	}
    $('#select_year').html(year);

    // GENERATE MONTH
    month = ["January","February","March","April","May","June","July","August","September","October", "November","December"];
    mArr = [];

    mArr += '<option ></option>';
    for ( m =  0; m <= month.length - 1; m++) {
    	mArr += '<option '+month[m]+'>'+month[m]+'</option>';
    }
    $('#select_month').html(mArr);


// ************************************************
	var _Year;
	var _Month;
	var _empid;
	var _searchvalue;
	var _emp_status;
	var _emp_type;
	var _searchby;
	$('.select2').select2();

	$('#select_year').select2({
		allowClear:true,
	    placeholder: "Year",
	});

	$('#select_month').select2({
		allowClear:true,
	    placeholder: "Month",
	});

	$(document).on('change','#select_year',function(){
		_Year = "";
		_Year = $(this).find(':selected').val();

	})
	$(document).on('change','#select_month',function(){
		_Month = "";
		_Month = $(this).find(':selected').val();
	})
	$(document).on('change','#employee_id',function(){
		_empid = "";
		_empid = $(this).find(':selected').val();

	})

	$(document).on('change','#select_searchvalue',function(){
		_searchvalue = "";
		_searchvalue = $(this).find(':selected').val();

	})

	$(document).on('change','#emp_status',function(){
		_emp_status = "";
		_emp_status = $(this).find(':selected').val();

	})
	$(document).on('change','#emp_type',function(){
		_emp_type = "";
		_emp_type = $(this).find(':selected').val();

	})
	$(document).on('change','#searchby',function(){
		_searchby = "";
		_searchby = $(this).find(':selected').val();

	})


	$(document).on('change','#searchby',function(){
		var val = $(this).val();

		$.ajax({
			url:base_url+module_prefix+module+'/getSearchby',
			data:{'q':val},
			type:'GET',
			dataType:'JSON',
			success:function(data){

				arr = [];
				$.each(data,function(k,v){
					arr += '<option value='+v.RefId+'>'+v.Name+'</option>';
				})

				$('#select_searchvalue').html(arr);
			}
		})

	});

	$(document).on('click','#preview',function(){

		if(!_Year && !_Month){
			swal({
				  title: "Select year and month first",
				  type: "warning",
				  showCancelButton: false,
				  confirmButtonClass: "btn-danger",
				  confirmButtonText: "Yes",
				  closeOnConfirm: false

			});
		}else{
			$.ajax({
				url:base_url+module_prefix+module+'/show',
				data:{'month':_Month,'year':_Year},
				type:'GET',
				dataType:'JSON',
				success:function(data){
				console.log(data);
					if(data.length !== 0){
						arr = [];
						ctr = 0;

							net_basic_amount 			= 0;
							net_pera_amount  			= 0;
							net_rep_amount	 			= 0;
							net_transpo_amount 			= 0;
							net_totalEarnings  			= 0;
							net_taxAmount 				= 0;
							net_gsisContAmount 			= 0;
							net_philhealthContAmount 	= 0;
							net_pagibigContAmount 		= 0;
							net_totalContribution 		= 0;
							net_hdmf_housing_loan 		= 0;
							net_gsis_housing_loan 		= 0;
							net_mpl_amount 				= 0;
							net_salary_loan_amount 		= 0;
							net_emergency_loan_amount 	= 0;
							net_educ_loan_amount 		= 0;
							net_policy_loan_amount 		= 0;
							net_optional_loan_amount 	= 0;
							net_optional_cont_amount 	= 0;
							net_totalLoans 				= 0;
							net_coopAmount				= 0;
							net_providentAmount 		= 0;
							net_pay 					= 0;
							net_first_amount 			= 0;
							net_second_amount 			= 0;
							net_third_amount 			= 0;
							net_fourth_amount 			= 0;

							$.each(data,function(key,val){
								sub_basic_amount 			= 0;
								sub_pera_amount  			= 0;
								sub_rep_amount	 			= 0;
								sub_transpo_amount 			= 0;
								sub_totalEarnings  			= 0;
								sub_taxAmount 				= 0;
								sub_gsisContAmount 			= 0;
								sub_philhealthContAmount 	= 0;
								sub_pagibigContAmount 		= 0;
								sub_totalContribution 		= 0;
								sub_hdmf_housing_loan 		= 0;
								sub_gsis_housing_loan 		= 0;
								sub_mpl_amount 				= 0;
								sub_salary_loan_amount 		= 0;
								sub_emergency_loan_amount 	= 0;
								sub_educ_loan_amount 		= 0;
								sub_policy_loan_amount 		= 0;
								sub_optional_loan_amount 	= 0;
								sub_optional_cont_amount 	= 0;
								sub_totalLoans 				= 0;
								sub_coopAmount				= 0;
								sub_providentAmount 		= 0;
								sub_pay 					= 0;
								sub_first_amount 			= 0;
								sub_second_amount 			= 0;
								sub_third_amount 			= 0;
								sub_fourth_amount 			= 0;

								arr += '<tr style="border:1px solid #c0c0c0;">';
								arr += '<td style="border:none;"></td>';
								arr += '<td colspan="16" style="border:none;"><b>'+key+'<b></td>';
								arr += '</tr>';

								$.each(val,function(k,v){
									rep_amount 				= 0;
									transpo_amount 			= 0;
									basic_amount 			= 0;
									pera_amount 			= 0;
									hdmf_housing_loan 		= 0;
									gsis_housing_loan 		= 0;
									mpl_amount 				= 0;
									salary_loan_amount 		= 0;
									emergency_loan_amount 	= 0;
									educ_loan_amount 		= 0;
									policy_loan_amount 		= 0;
									optional_loan_amount 	= 0;
									optional_cont_amount 	= 0;
									coop_amount				= 0;
									provident_amount 		= 0;
									first_total_amount 		= 0;
									second_total_amount 	= 0;
									third_total_amount 		= 0;
									fourth_total_amount 	= 0;

									arr += '<tr class="border-top-black">';
										arr += '<td rowspan="2" class="border-black text-center">'+(ctr+1)+'</td>';
										// EMPLOYEE
										arr += '<td rowspan="2" class="border-black">&nbsp;'+v.employees.lastname+' '+v.employees.firstname+' '+v.employees.middlename+'</td>';

										if(v.loaninfo !== null){
											$.each(v.loaninfo,function(k1,v1){
												switch(v1.loans.name){
													case 'HDMF HOUSING LOAN':
														hdmf_housing_loan = (v1.loan_amortization) ? v1.loan_amortization : 0;
													break;
													case 'GSIS HOUSING LOAN':
														gsis_housing_loan = (v1.loan_amortization) ? v1.loan_amortization : 0;
													break;
													case 'MULTI PURPOSE LOAN':
														mpl_amount = (v1.loan_amortization) ? v1.loan_amortization : 0;
													break;
													case 'GSIS SALARY LOAN':
														salary_loan_amount = (v1.loan_amortization) ? v1.loan_amortization : 0;
													break;
													case 'GSIS EMERGENCY LOAN':
														emergency_loan_amount = (v1.loan_amortization) ? v1.loan_amortization : 0;
													break;
													case 'EDUCATION ASST LOAN':
														educ_loan_amount = (v1.loan_amortization) ? v1.loan_amortization : 0;
													break;
													case 'POLICY LOAN':
														policy_loan_amount = (v1.loan_amortization) ? v1.loan_amortization : 0;
													break;
													case 'OPTIONAL LOAN':
														optional_loan_amount = (v1.loan_amortization) ? v1.loan_amortization : 0;
													break;
													case 'OPTIONAL CONTRI':
														optional_cont_amount = (v1.loan_amortization) ? v1.loan_amortization : 0;
													break;
												}
											});
											$.each(v.deduction_transaction,function(k2,v2){
												switch(v2.deductions.name){
													case 'COOP':
														coop_amount = (v2.amount) ? v2.amount : 0;
													break;
													case 'PROVIDENT FUND':
														provident_amount = (v2.amount) ? v2.amount : 0;
													break;
												}
											});

										}
								if(v.rata !== null){
									rep_amount = (v.rata.representation_amount !== null) ? v.rata.representation_amount : 0;
									transpo_amount = (v.rata.transportation_amount !== null) ? v.rata.transportation_amount : 0;
								}

								// ======== EARNINGS ==========

								basic_amount = (v.salaryinfo) ? v.salaryinfo.salary_new_rate : 0;
								pera_amount = (v.benefit_transaction !== null) ? v.benefit_transaction.amount : 0;

								totalEarnings = (parseFloat(basic_amount) + parseFloat(pera_amount) + parseFloat(rep_amount) + parseFloat(transpo_amount));

								sub_basic_amount += parseFloat(basic_amount);
								sub_pera_amount += parseFloat(pera_amount);
								sub_rep_amount += parseFloat(rep_amount);
								sub_transpo_amount += parseFloat(transpo_amount);
								sub_totalEarnings += parseFloat(totalEarnings);

								// ======== EARNINGS ==========

								// ======== WITHOLDING ==========
								tax_amount = (v.employeeinfo.tax_contribution !== null) ? v.employeeinfo.tax_contribution : 0;
								gsis_cont_amount = (v.employeeinfo.gsis_contribution !== null) ? v.employeeinfo.gsis_contribution : 0;
								philhealth_cont_amount = (v.employeeinfo.philhealth_contribution) ? v.employeeinfo.philhealth_contribution : 0;
								pagibig_cont_amount = (v.employeeinfo.pagibig_contribution !== null) ? v.employeeinfo.pagibig_contribution : 0;

								totalContribution = (parseFloat(gsis_cont_amount) + parseFloat(philhealth_cont_amount) + parseFloat(pagibig_cont_amount));

								sub_taxAmount += parseFloat(tax_amount);
								sub_providentAmount += parseFloat(provident_amount);
								sub_gsisContAmount += parseFloat(gsis_cont_amount);
								sub_philhealthContAmount += parseFloat(philhealth_cont_amount);
								sub_pagibigContAmount += parseFloat(pagibig_cont_amount);
								sub_totalContribution += parseFloat(totalContribution);
								// ======== WITHOLDING ==========

								// ======== LOANS ==========
								totalLoans = (parseFloat(hdmf_housing_loan) + parseFloat(gsis_housing_loan) + parseFloat(mpl_amount) + parseFloat(salary_loan_amount) + parseFloat(emergency_loan_amount) + parseFloat(educ_loan_amount) + parseFloat(policy_loan_amount) + parseFloat(optional_loan_amount) + parseFloat(optional_cont_amount))

								sub_hdmf_housing_loan 		+= parseFloat(hdmf_housing_loan);
								sub_gsis_housing_loan 		+= parseFloat(gsis_housing_loan);
								sub_mpl_amount 				+= parseFloat(mpl_amount);
								sub_salary_loan_amount 		+= parseFloat(salary_loan_amount);
								sub_emergency_loan_amount 	+= parseFloat(emergency_loan_amount);
								sub_educ_loan_amount 		+= parseFloat(educ_loan_amount);
								sub_policy_loan_amount 		+= parseFloat(policy_loan_amount);
								sub_optional_loan_amount 	+= parseFloat(optional_loan_amount);
								sub_optional_cont_amount 	+= parseFloat(optional_cont_amount);
								sub_totalLoans 				+= parseFloat(totalLoans);
								// ======== LOANS ==========

								// ======== COOP DEDUCTION ==========
								total_pay = (parseFloat(totalEarnings) - parseFloat(totalContribution) - parseFloat(totalLoans) - parseFloat(provident_amount) - parseFloat(tax_amount) - parseFloat(coop_amount));

								sub_coopAmount 				+= parseFloat(coop_amount);
								sub_pay += parseFloat(total_pay);

								// ======== COOP DEDUCTION ==========

								// ======= DIVIDE NET PAY =========
								rata_amount = parseFloat(rep_amount) + parseFloat(transpo_amount);
								first_total_amount = (parseFloat(total_pay)	- parseFloat(pera_amount) - parseFloat(rata_amount))/4 + parseFloat(rata_amount);
								third_total_amount = (parseFloat(total_pay) - parseFloat(pera_amount) - parseFloat(rep_amount) - parseFloat(transpo_amount))/4;
								second_total_amount = (parseFloat(third_total_amount) + parseFloat(pera_amount));
								fourth_total_amount = (parseFloat(total_pay) - parseFloat(first_total_amount) - parseFloat(second_total_amount) - parseFloat(third_total_amount));

								sub_first_amount += parseFloat(first_total_amount);
								sub_second_amount += parseFloat(second_total_amount);
								sub_third_amount += parseFloat(third_total_amount);
								sub_fourth_amount += parseFloat(first_total_amount);
								// ======= DIVIDE NET PAY =========

basic_amount = (basic_amount !== 0) ? commaSeparateNumber(parseFloat(basic_amount).toFixed(2)) : '';
pera_amount = (pera_amount !== 0) ? commaSeparateNumber(parseFloat(pera_amount).toFixed(2)) : '';
rep_amount = (rep_amount !== 0) ? commaSeparateNumber(parseFloat(rep_amount).toFixed(2)) : '';
transpo_amount = (transpo_amount !== 0) ? commaSeparateNumber(parseFloat(transpo_amount).toFixed(2)) : '';
totalEarnings = (totalEarnings !== 0) ? commaSeparateNumber(parseFloat(totalEarnings).toFixed(2)) : '';

tax_amount = (tax_amount !== 0) ? commaSeparateNumber(parseFloat(tax_amount).toFixed(2)) : '';
gsis_cont_amount = (gsis_cont_amount !== 0) ? commaSeparateNumber(parseFloat(gsis_cont_amount).toFixed(2)) : '';
provident_amount = (provident_amount !== 0) ? commaSeparateNumber(parseFloat(provident_amount).toFixed(2)) : '';
philhealth_cont_amount = (philhealth_cont_amount !== 0) ? commaSeparateNumber(parseFloat(philhealth_cont_amount).toFixed(2)) : '';
pagibig_cont_amount = (pagibig_cont_amount !== 0) ? commaSeparateNumber(parseFloat(pagibig_cont_amount).toFixed(2)) : '';
totalContribution = (totalContribution !== 0) ? commaSeparateNumber(parseFloat(totalContribution).toFixed(2)) : '';

hdmf_housing_loan 	 = (hdmf_housing_loan !== 0) ? commaSeparateNumber(parseFloat(hdmf_housing_loan).toFixed(2)) : '';
gsis_housing_loan 	 = (gsis_housing_loan !== 0) ? commaSeparateNumber(parseFloat(gsis_housing_loan).toFixed(2)) : '';
mpl_amount 			 = (mpl_amount !== 0) ? commaSeparateNumber(parseFloat(mpl_amount ).toFixed(2)) : '';
salary_loan_amount 	 = (salary_loan_amount 	!== 0) ? commaSeparateNumber(parseFloat(salary_loan_amount 	).toFixed(2)) : '';
emergency_loan_amount  = (emergency_loan_amount !== 0) ? commaSeparateNumber(parseFloat(emergency_loan_amount ).toFixed(2)) : '';
educ_loan_amount 	 = (educ_loan_amount !== 0) ? commaSeparateNumber(parseFloat(educ_loan_amount).toFixed(2)) : '';
policy_loan_amount 	 = (policy_loan_amount !== 0) ? commaSeparateNumber(parseFloat(policy_loan_amount).toFixed(2)) : '';
optional_loan_amount  = (optional_loan_amount !== 0) ? commaSeparateNumber(parseFloat(optional_loan_amount ).toFixed(2)) : '';
optional_cont_amount  = (optional_cont_amount !== 0) ? commaSeparateNumber(parseFloat(optional_cont_amount ).toFixed(2)) : '';
totalLoans  = (totalLoans !== 0) ? commaSeparateNumber(parseFloat(totalLoans ).toFixed(2)) : '';

coop_amount  = (coop_amount !== 0) ? commaSeparateNumber(parseFloat(coop_amount ).toFixed(2)) : '';
total_pay  = (total_pay !== 0) ? commaSeparateNumber(parseFloat(total_pay ).toFixed(2)) : '';

first_total_amount  = (first_total_amount !== 0) ? commaSeparateNumber(parseFloat(first_total_amount ).toFixed(2)) : '';
second_total_amount  = (second_total_amount !== 0) ? commaSeparateNumber(parseFloat(second_total_amount ).toFixed(2)) : '';
third_total_amount  = (third_total_amount !== 0) ? commaSeparateNumber(parseFloat(third_total_amount ).toFixed(2)) : '';
fourth_total_amount  = (fourth_total_amount !== 0) ? commaSeparateNumber(parseFloat(fourth_total_amount ).toFixed(2)) : '';



								arr += '<td rowspan="2" class="text-right">'+basic_amount+'</td>'
								arr += '<td rowspan="2" class="text-right">'+pera_amount+'</td>';
								arr += '<td class="text-right">'+rep_amount+'</td>';
								arr += '<td rowspan="2" class="text-right">'+totalEarnings+'</td>';
								arr += '<td class="text-right">'+tax_amount+'</td>';
								arr += '<td class="text-right">'+gsis_cont_amount+'</td>';
								arr += '<td class="text-right">'+philhealth_cont_amount+'</td>';
								arr += '<td class="text-right">'+hdmf_housing_loan+'</td>';
								arr += '<td class="text-right"></td>'; // pagibig 2
								arr += '<td class="text-right">'+salary_loan_amount+'</td>';
								arr += '<td class="text-right">'+educ_loan_amount+'</td>';
								arr += '<td class="text-right">'+optional_loan_amount+'</td>';
								arr += '<td class="text-right" rowspan="2">'+totalLoans+'</td>';
								arr += '<td class="text-right"></td>';
								arr += '<td class="text-right" style="border-bottom:none;"></td>';
								arr += '<td class="text-right">'+first_total_amount+'</td>';
								arr += '<td class="text-right">'+third_total_amount+'</td>';
								arr += '</tr>';

								arr += '<tr>';
								arr += '<td class="text-right">'+transpo_amount+'</td>';
								arr += '<td class="text-right">'+provident_amount+'</td>';
								arr += '<td class="text-right">'+pagibig_cont_amount+'</td>';
								arr += '<td class="text-right">'+totalContribution+'</td>';
								arr += '<td class="text-right">'+gsis_housing_loan+'</td>';
								arr += '<td class="text-right">'+mpl_amount+'</td>';
								arr += '<td class="text-right">'+emergency_loan_amount+'</td>';
								arr += '<td class="text-right">'+policy_loan_amount+'</td>';
								arr += '<td class="text-right">'+optional_cont_amount+'</td>';
								arr += '<td class="text-right">'+coop_amount+'</td>';
								arr += '<td class="text-right" style="border-top:none;">'+total_pay+'</td>';
								arr += '<td class="text-right">'+second_total_amount+'</td>';
								arr += '<td class="text-right">'+fourth_total_amount+'</td>';
								arr += '</tr>';
								ctr++;

								});
								ctr = 0;

net_basic_amount 		+= parseFloat(sub_basic_amount)
net_pera_amount  		+= parseFloat(sub_pera_amount)
net_rep_amount	 		+= parseFloat(sub_rep_amount)
net_transpo_amount 		+= parseFloat(sub_transpo_amount)
net_totalEarnings  		+= parseFloat(sub_totalEarnings)
net_taxAmount 			+= parseFloat(sub_taxAmount)
net_gsisContAmount 		+= parseFloat(sub_gsisContAmount)
net_philhealthContAmount += parseFloat(sub_philhealthContAmount)
net_pagibigContAmount 	+= parseFloat(sub_pagibigContAmount)
net_totalContribution 	+= parseFloat(sub_totalContribution)
net_hdmf_housing_loan 	+= parseFloat(sub_hdmf_housing_loan)
net_gsis_housing_loan 	+= parseFloat(sub_gsis_housing_loan)
net_mpl_amount 			+= parseFloat(sub_mpl_amount)
net_salary_loan_amount 	+= parseFloat(sub_salary_loan_amount)
net_emergency_loan_amount += parseFloat(sub_emergency_loan_amount)
net_educ_loan_amount 	+= parseFloat(sub_educ_loan_amount )
net_policy_loan_amount 	+= parseFloat(sub_policy_loan_amount )
net_optional_loan_amount += parseFloat(sub_optional_loan_amount)
net_optional_cont_amount += parseFloat(sub_optional_cont_amount)
net_totalLoans 			+= parseFloat(sub_totalLoans )
net_coopAmount			+= parseFloat(sub_coopAmount)
net_providentAmount 	+= parseFloat(sub_providentAmount)
net_pay 				+= parseFloat(sub_pay)
net_first_amount 		+= parseFloat(sub_first_amount)
net_second_amount 		+= parseFloat(sub_second_amount)
net_third_amount 		+= parseFloat(sub_third_amount)
net_fourth_amount 		+= parseFloat(sub_fourth_amount)

sub_basic_amount = (sub_basic_amount !== 0) ? commaSeparateNumber(parseFloat(sub_basic_amount).toFixed(2)) : '';
sub_pera_amount = (sub_pera_amount !== 0) ? commaSeparateNumber(parseFloat(sub_pera_amount).toFixed(2)) : '';
sub_rep_amount = (sub_rep_amount !== 0) ? commaSeparateNumber(parseFloat(sub_rep_amount).toFixed(2)) : '';
sub_transpo_amount = (sub_transpo_amount !== 0) ? commaSeparateNumber(parseFloat(sub_transpo_amount).toFixed(2)) : '';
sub_totalEarnings = (sub_totalEarnings !== 0) ? commaSeparateNumber(parseFloat(sub_totalEarnings).toFixed(2)) : '';

sub_taxAmount = (sub_taxAmount !== 0) ? commaSeparateNumber(parseFloat(sub_taxAmount).toFixed(2)) : '';
sub_gsisContAmount = (sub_gsisContAmount !== 0) ? commaSeparateNumber(parseFloat(sub_gsisContAmount).toFixed(2)) : '';
sub_providentAmount = (sub_providentAmount !== 0) ? commaSeparateNumber(parseFloat(sub_providentAmount).toFixed(2)) : '';
sub_philhealthContAmount = (sub_philhealthContAmount !== 0) ? commaSeparateNumber(parseFloat(sub_philhealthContAmount).toFixed(2)) : '';
sub_pagibigContAmount = (sub_pagibigContAmount !== 0) ? commaSeparateNumber(parseFloat(sub_pagibigContAmount).toFixed(2)) : '';
sub_totalContribution = (sub_totalContribution !== 0) ? commaSeparateNumber(parseFloat(sub_totalContribution).toFixed(2)) : '';

sub_hdmf_housing_loan 	 = (sub_hdmf_housing_loan !== 0) ? commaSeparateNumber(parseFloat(sub_hdmf_housing_loan).toFixed(2)) : '';
sub_gsis_housing_loan 	 = (sub_gsis_housing_loan !== 0) ? commaSeparateNumber(parseFloat(sub_gsis_housing_loan).toFixed(2)) : '';
sub_mpl_amount 			 = (sub_mpl_amount !== 0) ? commaSeparateNumber(parseFloat(sub_mpl_amount).toFixed(2)) : '';
sub_salary_loan_amount 	 = (sub_salary_loan_amount !== 0) ? commaSeparateNumber(parseFloat(sub_salary_loan_amount).toFixed(2)) : '';
sub_emergency_loan_amount  = (sub_emergency_loan_amount !== 0) ? commaSeparateNumber(parseFloat(sub_emergency_loan_amount ).toFixed(2)) : '';
sub_educ_loan_amount 	 = (sub_educ_loan_amount !== 0) ? commaSeparateNumber(parseFloat(sub_educ_loan_amount).toFixed(2)) : '';
sub_policy_loan_amount 	 = (sub_policy_loan_amount !== 0) ? commaSeparateNumber(parseFloat(sub_policy_loan_amount).toFixed(2)) : '';
sub_optional_loan_amount  = (sub_optional_loan_amount !== 0) ? commaSeparateNumber(parseFloat(sub_optional_loan_amount ).toFixed(2)) : '';
sub_optional_cont_amount  = (sub_optional_cont_amount !== 0) ? commaSeparateNumber(parseFloat(sub_optional_cont_amount ).toFixed(2)) : '';
sub_totalLoans 			 = (sub_totalLoans !== 0) ? commaSeparateNumber(parseFloat(sub_totalLoans).toFixed(2)) : '';

sub_coopAmount 			 = (sub_coopAmount !== 0) ? commaSeparateNumber(parseFloat(sub_coopAmount).toFixed(2)) : '';
sub_pay 			 = (sub_pay !== 0) ? commaSeparateNumber(parseFloat(sub_pay).toFixed(2)) : '';
sub_first_amount 			 = (sub_first_amount !== 0) ? commaSeparateNumber(parseFloat(sub_first_amount).toFixed(2)) : '';
sub_second_amount 			 = (sub_second_amount !== 0) ? commaSeparateNumber(parseFloat(sub_second_amount).toFixed(2)) : '';
sub_third_amount 			 = (sub_third_amount !== 0) ? commaSeparateNumber(parseFloat(sub_third_amount).toFixed(2)) : '';
sub_fourth_amount 			 = (sub_fourth_amount !== 0) ? commaSeparateNumber(parseFloat(sub_fourth_amount).toFixed(2)) : '';

								arr += '<tr style="font-weight:bold;">';
								arr += '<td rowspan="2"></td>'
								arr += '<td rowspan="2" class="text-center">SUB TOTAL</td>'
								arr += '<td rowspan="2" class="text-right">'+sub_basic_amount+'</td>'
								arr += '<td rowspan="2" class="text-right">'+sub_pera_amount+'</td>';
								arr += '<td class="text-right">'+sub_rep_amount+'</td>';
								arr += '<td rowspan="2" class="text-right">'+sub_totalEarnings+'</td>';
								arr += '<td class="text-right">'+sub_taxAmount+'</td>';
								arr += '<td class="text-right">'+sub_gsisContAmount+'</td>';
								arr += '<td class="text-right">'+sub_philhealthContAmount+'</td>';
								arr += '<td class="text-right">'+sub_hdmf_housing_loan+'</td>';
								arr += '<td class="text-right"></td>'; //pagibig 2
								arr += '<td class="text-right">'+sub_salary_loan_amount+'</td>';
								arr += '<td class="text-right">'+sub_educ_loan_amount+'</td>';
								arr += '<td class="text-right">'+sub_optional_loan_amount+'</td>';
								arr += '<td rowspan="2" class="text-right">'+sub_totalLoans+'</td>';
								arr += '<td class="text-right"></td>';
								arr += '<td class="text-right"></td>';
								arr += '<td class="text-right">'+sub_first_amount+'</td>';
								arr += '<td class="text-right">'+sub_third_amount+'</td>';
								arr += '</tr>';

								arr += '<tr style="font-weight:bold;">';
								arr += '<td class="text-right">'+sub_transpo_amount+'</td>';
								arr += '<td class="text-right">'+sub_providentAmount+'</td>';
								arr += '<td class="text-right">'+sub_pagibigContAmount+'</td>';
								arr += '<td class="text-right">'+sub_totalContribution+'</td>';
								arr += '<td class="text-right">'+sub_gsis_housing_loan+'</td>';
								arr += '<td class="text-right">'+sub_mpl_amount+'</td>';
								arr += '<td class="text-right">'+sub_emergency_loan_amount+'</td>';
								arr += '<td class="text-right">'+sub_policy_loan_amount+'</td>';
								arr += '<td class="text-right">'+sub_optional_cont_amount+'</td>';
								arr += '<td class="text-right">'+sub_coopAmount+'</td>';
								arr += '<td class="text-right">'+sub_pay+'</td>';
								arr += '<td class="text-right">'+sub_second_amount+'</td>';
								arr += '<td class="text-right">'+sub_fourth_amount+'</td>';
								arr += '</tr>';

							});

net_basic_amount = (net_basic_amount !== 0) ? commaSeparateNumber(parseFloat(net_basic_amount ).toFixed(2)) : '';
net_pera_amount = (net_pera_amount !== 0) ? commaSeparateNumber(parseFloat(net_pera_amount).toFixed(2)) : '';
net_rep_amount	= (net_rep_amount !== 0) ? commaSeparateNumber(parseFloat(net_rep_amount).toFixed(2)) : '';
net_transpo_amount 	= (net_transpo_amount !== 0) ? commaSeparateNumber(parseFloat(net_transpo_amount).toFixed(2)) : '';
net_totalEarnings = (net_totalEarnings !== 0) ? commaSeparateNumber(parseFloat(net_totalEarnings).toFixed(2)) : '';
net_taxAmount = (net_taxAmount !== 0) ? commaSeparateNumber(parseFloat(net_taxAmount ).toFixed(2)) : '';
net_gsisContAmount 	= (net_gsisContAmount !== 0) ? commaSeparateNumber(parseFloat(net_gsisContAmount ).toFixed(2)) : '';
net_philhealthContAmount = (net_philhealthContAmount !== 0) ? commaSeparateNumber(parseFloat(net_philhealthContAmount ).toFixed(2)) : '';
net_pagibigContAmount 	 = (net_pagibigContAmount !== 0) ? commaSeparateNumber(parseFloat(net_pagibigContAmount).toFixed(2)) : '';
net_totalContribution 	= (net_totalContribution !== 0) ? commaSeparateNumber(parseFloat(net_totalContribution).toFixed(2)) : '';
net_hdmf_housing_loan= (net_hdmf_housing_loan !== 0) ? commaSeparateNumber(parseFloat(net_hdmf_housing_loan).toFixed(2)) : '';
net_gsis_housing_loan= (net_gsis_housing_loan !== 0) ? commaSeparateNumber(parseFloat(net_gsis_housing_loan).toFixed(2)) : '';
net_mpl_amount	= (net_mpl_amount !== 0) ? commaSeparateNumber(parseFloat(net_mpl_amount).toFixed(2)) : '';
net_salary_loan_amount 	= (net_salary_loan_amount !== 0) ? commaSeparateNumber(parseFloat(net_salary_loan_amount).toFixed(2)) : '';
net_emergency_loan_amount = (net_emergency_loan_amount !== 0) ? commaSeparateNumber(parseFloat(net_emergency_loan_amount ).toFixed(2)) : '';
net_educ_loan_amount 	= (net_educ_loan_amount !== 0) ? commaSeparateNumber(parseFloat(net_educ_loan_amount 	).toFixed(2)) : '';
net_policy_loan_amount 	= (net_policy_loan_amount !== 0) ? commaSeparateNumber(parseFloat(net_policy_loan_amount 	).toFixed(2)) : '';
net_optional_loan_amount = (net_optional_loan_amount !== 0) ? commaSeparateNumber(parseFloat(net_optional_loan_amount ).toFixed(2)) : '';
net_optional_cont_amount = (net_optional_cont_amount !== 0) ? commaSeparateNumber(parseFloat(net_optional_cont_amount ).toFixed(2)) : '';
net_totalLoans 	= (net_totalLoans !== 0) ? commaSeparateNumber(parseFloat(net_totalLoans).toFixed(2)) : '';
net_coopAmount	= (net_coopAmount !== 0) ? commaSeparateNumber(parseFloat(net_coopAmount).toFixed(2)) : '';
net_providentAmount = (net_providentAmount 	!== 0) ? commaSeparateNumber(parseFloat(net_providentAmount).toFixed(2)) : '';
net_pay = (net_pay !== 0) ? commaSeparateNumber(parseFloat(net_pay).toFixed(2)) : '';
net_first_amount = (net_first_amount !== 0) ? commaSeparateNumber(parseFloat(net_first_amount).toFixed(2)) : '';
net_second_amount = (net_second_amount !== 0) ? commaSeparateNumber(parseFloat(net_second_amount).toFixed(2)) : '';
net_third_amount = (net_third_amount !== 0) ? commaSeparateNumber(parseFloat(net_third_amount).toFixed(2)) : '';
net_fourth_amount = (net_fourth_amount !== 0) ? commaSeparateNumber(parseFloat(net_fourth_amount).toFixed(2)) : '';

						arr += '<tr style="font-weight:bold;">';
						arr += '<td rowspan="2"></td>'
						arr += '<td rowspan="2" class="text-center">GRAND TOTAL</td>'
						arr += '<td rowspan="2" class="text-right">'+net_basic_amount+'</td>'
						arr += '<td rowspan="2" class="text-right">'+net_pera_amount+'</td>';
						arr += '<td class="text-right">'+net_rep_amount+'</td>';
						arr += '<td rowspan="2" class="text-right">'+net_totalEarnings+'</td>';
						arr += '<td class="text-right">'+net_taxAmount+'</td>';
						arr += '<td class="text-right">'+net_gsisContAmount+'</td>';
						arr += '<td class="text-right">'+net_philhealthContAmount+'</td>';
						arr += '<td class="text-right">'+net_hdmf_housing_loan+'</td>';
						arr += '<td class="text-right"></td>'; //pagibig 2
						arr += '<td class="text-right">'+net_salary_loan_amount+'</td>';
						arr += '<td class="text-right">'+net_educ_loan_amount+'</td>';
						arr += '<td class="text-right">'+net_optional_loan_amount+'</td>';
						arr += '<td rowspan="2" class="text-right">'+net_totalLoans+'</td>';
						arr += '<td class="text-right"></td>';
						arr += '<td class="text-right"></td>';
						arr += '<td class="text-right">'+net_first_amount+'</td>';
						arr += '<td class="text-right">'+net_third_amount+'</td>';
						arr += '</tr>';

						arr += '<tr style="font-weight:bold;">';
						arr += '<td class="text-right">'+net_transpo_amount+'</td>';
						arr += '<td class="text-right">'+net_providentAmount+'</td>';
						arr += '<td class="text-right">'+net_pagibigContAmount+'</td>';
						arr += '<td class="text-right">'+net_totalContribution+'</td>';
						arr += '<td class="text-right">'+net_gsis_housing_loan+'</td>';
						arr += '<td class="text-right">'+net_mpl_amount+'</td>';
						arr += '<td class="text-right">'+net_emergency_loan_amount+'</td>';
						arr += '<td class="text-right">'+net_policy_loan_amount+'</td>';
						arr += '<td class="text-right">'+net_optional_cont_amount+'</td>';
						arr += '<td class="text-right">'+net_coopAmount+'</td>';
						arr += '<td class="text-right">'+net_pay+'</td>';
						arr += '<td class="text-right">'+net_second_amount+'</td>';
						arr += '<td class="text-right">'+net_fourth_amount+'</td>';
						arr += '</tr>';

						arr += '<tr>';
						arr += '<td colspan="10" style="border:none">(1) I CERTIFY ON MY OFFICIAL OATH THAT THE ABOVE PAYROLL IS CORRECT <br> AND THAT THE SERVICES HAVE BEEN DULY RENDERED AS STATED</td>';
						arr += '<td colspan="10" style="border:none">(2) APPROVED, PAYABLE FROM APPROPRIATION FOR * P ________________________</td>';
						arr += '</tr>';
						arr += '<tr class="text-center">';
						arr += '<td colspan="10" style="border:none">_____________________________________ <br> Director for Operations and Finance</td>';
						arr += '<td colspan="10" style="border:none">_________________________ <br> Vice President</td>';
						arr += '</tr>';

						$('#tbl_body').html(arr);

						$('#month_year').text(_Month+' '+_Year);

						$('#btnModal').trigger('click');


					}else{
						swal({
							title: "No Records Found",
							type: "warning",
							showCancelButton: false,
							confirmButtonClass: "btn-danger",
							confirmButtonText: "Yes",
							closeOnConfirm: false
						});
					}
				}
			})
		}
	});

$('#print').on('click',function(){
	$('#reports').printThis();
})

})
</script>
@endsection