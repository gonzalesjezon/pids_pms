@extends('app-reports')

@section('reports-content')
<link rel="stylesheet" type="text/css" href="{{ asset('css/printportrait.css')}}">
<style type="text/css">
	.add-margin{
		margin-left: 15px;
		margin-right: 15px;
	}
</style>

<div class="loan-reports">
	<label>{{ $title }}</label>
	<table class="table borderless" id="loan-reports">
		<tr>
			<td>
				<div class="row">
					<div class="col-md-12">
						<span class="lineheight" style="margin-left: 10px;"><b>Covered Date</b></span>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="col-md-6">
							<select class="form-control select2" name="month" id="select_month">
								<option value=""></option>
							</select>
						</div>
						<div class="col-md-6">
							<select class="form-control select2" name="year" id="select_year">
								<option value=""></option>
							</select>
						</div>

					</div>
				</div>
			</td>
		</tr>

	</table>
	<div class="reports-bot">
		<div class="col-md-6">
			<a class="btn btn-success btn-xs btn-editbg">
				Post
			</a>
		</div>
		<div class="col-md-6 text-right">
			<button type="button" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#prnModal" id="preview">Preview</button>
		</div>

	</div>
</div>

<div class="modal fade border0 in" id="prnModal" role="dialog">
	 <div class="modal-dialog border0 model-size">
	    <div class="mypanel border0">
	       <div class="panel-top bgSilver">
	          <a href="#" data-toggle="tooltip" data-placement="top" title="" id="print">
	             <i class="fa fa-print" aria-hidden="true"></i>Print
	          </a>
	          <button type="button" class="close" data-dismiss="modal">×</button>
	       </div>
	       <div style="height: 30px;"></div>
	       <br>
	       <div class="panel style1" id="reports" style="width: 1080px;margin: auto;">
		       <div class="row add-margin">
		       		<div class="col-md-12 text-center" style="font-weight: bold;margin: auto;">
		       			AUTHORITY TO DEBIT / CREDIT ACCOUNT
		       		</div>
		       </div>
		       <br>
		       <div class="row add-margin">
		       		<div class="col-md-12">
				       <div class="row add-margin" style="font-weight: bold;">
				       		<div class="col-md-2">
				       			DATE :
				       		</div>
				       		<div class="col-md-10">
				       			<span id="covered_date">2018-9-23</span>
				       		</div>
				       </div>
				       <div class="row add-margin" style="font-weight: bold;">
				       		<div class="col-md-2">
				       			TO :
				       		</div>
				       		<div class="col-md-10">
				       		</div>
				       </div>
				       <div class="row add-margin" style="margin-top: 60px;">
				       		<div class="col-md-2" style="font-weight: bold;">
				       			Subject :
				       		</div>
				       		<div class="col-md-10">
				       			Authority to Debit / Credit Account
				       		</div>
				       </div>
				       <div class="row add-margin" style="margin-top: 15px;">
				       		<div class="col-md-2">
				       			Please Debit
				       		</div>
				       		<div class="col-md-8 text-center" style="border-bottom: 1px solid #333;">
				       			Current Account No. 1872-1037-39 / Philippine Institute for Development Studies
				       		</div>
				       </div>
				       <div class="row add-margin" style="margin-top: 15px;">
				       		<div class="col-md-2">
				       			the amount of PESOS:
				       		</div>
				       		<div class="col-md-8 text-center" style="border-bottom: 1px solid #333;">
				       			&nbsp;
				       		</div>
				       </div>
				       <div class="row add-margin" style="margin-top: 15px;">
				       		<div class="col-md-8" style="border-bottom: 1px solid #333;">
				       			&nbsp;
				       		</div>
				       		<div class="col-md-2 text-center" style="border-bottom: 1px solid #333;">
				       			(P)
				       		</div>
				       </div>
				       <div class="row add-margin" style="margin-top: 15px;">
				       		<div class="col-md-2">
				       			and credit
				       		</div>
				       		<div class="col-md-8 text-center" style="border-bottom: 1px solid #333;">
				       			to the account of the attached list of employees of the Institute the amount indicated
				       		</div>
				       </div>
				       <div class="row add-margin" style="margin-top: 15px;">
				       		<div class="col-md-2" style="border-bottom: 1px solid #333;">
				       			opposite their names.
				       		</div>
				       		<div class="col-md-8 text-center" style="border-bottom: 1px solid #333;">
				       			&nbsp;
				       		</div>
				       </div>
				       <div class="row add-margin" style="margin-top: 15px;">
				       		<div class="col-md-2">
				       			Representing  :
				       		</div>
				       		<div class="col-md-8 text-center" style="border-bottom: 1px solid #333;">
				       			payment of their salaries and allowances for the period .
				       		</div>
				       </div>
				       <div class="row add-margin" style="margin-top: 15px;">
				       		<div class="col-md-10" style="border-bottom: 1px solid #333;">
				       			&nbsp;
				       		</div>
				       </div>
				       <div class="row add-margin" style="margin-top: 15px;">
				       		<div class="col-md-10" style="border-bottom: 1px solid #333;">
				       			This also authorizes you to debit my / our account for the corresponding service charge.
				       		</div>
				       </div>
				       <div class="row add-margin" style="margin-top: 15px;">
				       		<div class="col-md-10" >
				       			<span style="border-bottom: 1px solid #333;">For Joint Account :</span> I/We declare under penalty of perjury that my/our co-depositor/s is/are still living.
				       		</div>
				       </div>
				       <div class="row add-margin" style="margin-top: 50px;">
				       		<div class="col-md-6">
				       			ANDREA S. AGCAOILI <br>
								Department Manager III <br>
								Administrative and Finance Department
				       		</div>
				       		<div class="col-md-6">
				       			MARIFE M. BALLESTEROS <br>
								Vice President
				       		</div>
				       </div>
				       <div class="row add-margin" style="margin-top: 15px;">
				       		<div class="col-md-10" style="border-top: 1px solid #333;">
				       			FOR BANK’S USE ONLY
				       		</div>
				       </div>
				       <div class="row add-margin" style="margin-top: 15px;">
				       		<div class="col-md-3" style="border: 1px solid #333;padding-bottom: 10px;">
				       			Signature Verified by: <br>
				       			_____________________
				       		</div>
				       		<div class="col-md-2" style="border: 1px solid #333;padding-bottom: 10px;margin-left: 5px;">
				       			Checked by: <br>
				       			_____________________
				       		</div>
				       		<div class="col-md-2" style="border: 1px solid #333;padding-bottom: 10px;margin-left: 5px;">
				       			Approved by: <br>
				       			_____________________
				       		</div>
				       		<div class="col-md-3" style="border: 1px solid #333;padding-bottom: 10px;margin-left: 5px;">
				       			Sight Verified by: <br>
				       			_____________________
				       		</div>
				       </div>
				       <div class="row add-margin" style="margin-top: 15px;">
				       		<div class="col-md-10" style="border-top: 1px solid #333;">
				       			Validation Process:
				       		</div>
				       </div>
		       		</div>
		       </div>
	       </div>
	       <br>
	    </div>
	 </div>
</div>

@endsection

@section('js-logic2')
<script type="text/javascript">
$(document).ready(function(){
	// GENERATE YEAR
	var year = [];
	year += '<option ></option>';
	for(y = 2018; y <= 2100; y++) {
        year += '<option value='+y+'>'+y+'</option>';
	}
    $('#select_year').html(year);

    // GENERATE MONTH
    month = ["January","February","March","April","May","June","July","August","September","October", "November","December"];
    mArr = [];

    mArr += '<option ></option>';
    for ( m =  0; m <= month.length - 1; m++) {
    	mArr += '<option '+month[m]+'>'+month[m]+'</option>';
    }
    $('#select_month').html(mArr);


// ************************************************
	var _Year;
	var _Month;
	var _empid;
	var _searchvalue;
	var _emp_status;
	var _emp_type;
	var _searchby;
	$('.select2').select2();

	$('#select_year').select2({
		allowClear:true,
	    placeholder: "Year",
	});

	$('#select_month').select2({
		allowClear:true,
	    placeholder: "Month",
	});

	$(document).on('change','#select_year',function(){
		_Year = "";
		_Year = $(this).find(':selected').val();

	})
	$(document).on('change','#select_month',function(){
		_Month = "";
		_Month = $(this).find(':selected').val();
	})
	$(document).on('change','#employee_id',function(){
		_empid = "";
		_empid = $(this).find(':selected').val();

	})

	$(document).on('change','#select_searchvalue',function(){
		_searchvalue = "";
		_searchvalue = $(this).find(':selected').val();

	})

	$(document).on('change','#emp_status',function(){
		_emp_status = "";
		_emp_status = $(this).find(':selected').val();

	})
	$(document).on('change','#emp_type',function(){
		_emp_type = "";
		_emp_type = $(this).find(':selected').val();

	})
	$(document).on('change','#searchby',function(){
		_searchby = "";
		_searchby = $(this).find(':selected').val();

	})


	$(document).on('change','#searchby',function(){
		var val = $(this).val();

		$.ajax({
			url:base_url+module_prefix+module+'/getSearchby',
			data:{'q':val},
			type:'GET',
			dataType:'JSON',
			success:function(data){

				arr = [];
				$.each(data,function(k,v){
					arr += '<option value='+v.RefId+'>'+v.Name+'</option>';
				})

				$('#select_searchvalue').html(arr);
			}
		})

	});

$(document).on('click','#preview',function(){

	// year = (_Year) ? _Year : '';
	// month = (_Month) ? _Month : '';
	// emp_type = (_emp_type) ? _emp_type : '';
	// emp_status = (_emp_status) ? _emp_status : '';
	// month = (_Month) ? _Month : '';
	// category = (_searchvalue) ? _searchvalue : '';
	// searchby = (_searchby) ? _searchby : '';

	// $.ajax({
	// 	url:base_url+module_prefix+module+'/show',
	// 	data:{
	// 		'id':_empid,
	// 		'year':year,
	// 		'month':month,
	// 		'emp_type':emp_type,
	// 		'emp_status':emp_status,
	// 		'category':category,
	// 		'searchby':searchby,
	// 	},
	// 	type:'GET',
	// 	dataType:'JSON',
	// 	success:function(data){

	// 	}
	// })


});

$('#print').on('click',function(){
	$('#reports').printThis();
})

})
</script>
@endsection