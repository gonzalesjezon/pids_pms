@extends('app-front')

@section('content')
<div style="height:5px;"></div>
<div class="row web-content">
		<div class="col-md-6">
			<div class="panel-group">
				<div class="panel panel-default">
					<div class="panel-heading"><b>TOTAL NUMBER OF EMPLOYEES</b></div>
					<div class="panel-body">
						<ul class="list-group">
							<li class="list-group-item">TOTAL EMPLOYEES <span class="badge">0</span></li>
							<li class="list-group-item">NEWLY REGULAR<span class="badge">0</span></li>
							<li class="list-group-item">END OF CONTRACT <span class="badge">0</span></li>
						</ul>
					</div>

				</div>

			</div>

			<div class="panel-group">
				<div class="panel panel-default">
					<div class="panel-heading"><b>NOTIFICATION FOR</b></div>
					<div class="panel-body">
						<ul class="list-group">
							<li class="list-group-item">BIRTHDAY CELEBRANTS FOR THE MONTH <span class="badge">0</span></li>
							<li class="list-group-item">ANNIVERSARY<span class="badge">0</span></li>
							<li class="list-group-item">PROMOTED <span class="badge">0</span></li>
							<li class="list-group-item">RETIREES <span class="badge">0</span></li>
						</ul>
					</div>

				</div>

			</div>

			<div class="panel-group">
				<div class="panel panel-default">
					<div class="panel-heading"><b>STEP INCREMENT AND LOYALTY</b></div>
					<div class="panel-body">
						<ul class="list-group">
							<li class="list-group-item">3 YEARS <span class="badge">0</span></li>
							<li class="list-group-item">4 - 10 YEARS<span class="badge">0</span></li>
							<li class="list-group-item">11 - 15 YEARS <span class="badge">0</span></li>
							<li class="list-group-item">16 UP <span class="badge">0</span></li>
						</ul>
					</div>

				</div>

			</div>

			<div class="panel-group">
				<div class="panel panel-default">
					<div class="panel-heading"><b>EMPLOYEES DOCUMENT / ID(#) / etc</b></div>
					<div class="panel-body">
						<ul class="list-group">
							<li class="list-group-item">NO TRANSCRIPT OF RECORD <span class="badge">0</span></li>
							<li class="list-group-item">NO BIRTH CERTIFICATE<span class="badge">0</span></li>
						</ul>
					</div>

				</div>

			</div>
		</div>
		<div class="col-md-6">
			<div class="panel-group">
				<div class="panel panel-default">
					<div class="panel-heading"><b>REMINDER</b></div>
					<div class="panel-body">
						<ul class="list-group">
							<li class="list-group-item">Employees with No Picture <span class="badge">0</span></li>
							<li class="list-group-item">Employees with No Lastname <span class="badge">0</span></li>
							<li class="list-group-item">Employees with No Firstname <span class="badge">0</span></li>
							<li class="list-group-item">Employees with No Philhealth <span class="badge">0</span></li>
							<li class="list-group-item">Employees with No GSIS <span class="badge">0</span></li>
							<li class="list-group-item">Employees with No SSS <span class="badge">0</span></li>
							<li class="list-group-item">Employees with No TIN <span class="badge">0</span></li>
							<li class="list-group-item">Employees with No PAGIBIG <span class="badge">0</span></li>
							<li class="list-group-item">Employees with No Agency ID <span class="badge">0</span></li>
						</ul>
					</div>

				</div>

			</div>
		</div>
</div>
<br>

@endsection
@section('js-logic1')
<script>
   $(document).ready(function () {

   });
</script>
@endsection