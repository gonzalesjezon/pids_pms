@extends('app-front')

@section('content')
<div class="row">
	<div class="col-md-12">
		<hr>
		<div class="col-md-3" style="padding: 0px 15px 0px 15px;">
			<h5 ><b>Filter Employee By</b></h5>
			<label>Transaction Period</label>
			<div class="row">
				<div class="col-md-6">
					<select class="employee-type form-control font-style2 select2" id="select_month" name="select_month" placeholder="Month">
						<option value=""></option>
					</select>
				</div>
				<div class="col-md-6">
					<select class="employee-type form-control font-style2 select2" id="select_year" name="select_year" placeholder="Year" >
						<option value=""></option>
					</select>
				</div>
			</div>
			<br>
			<div class="row">
				<div class="col-md-4">
					<a class="btn btn-xs btn-info btnfilter hidden"style="background-color: #164c8a;" style="float: left;line-height: 16px;" ><i class="fa fa-filter"></i>Filter</a>
				</div>
				<div class="col-md-8 text-right">
					<button class="btn btn-xs btn-info" id="process_payroll" style="background-color: #164c8a;"><i class="far fa-save"></i>&nbsp;Process</button>
					<a class="btn btn-xs btn-danger" id="delete_payroll" ><i class="fas fa-minus-circle"></i>&nbsp;Delete</a>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 ">
					<div class="col-md-2">
						<span>Search</span>
					</div>
					<div class="col-md-10 text-right">
						<a class="radiobut-style radio-inline">
							<input type="radio" name="chk_wpayroll" id="wpayroll" value="wpayroll">
							W/Payroll
						</a>
						<a class="radiobut-style radio-inline ">
							<input type="radio" name="chk_wpayroll" id="woutpayroll" value="wopayroll">
							W/ Out Payroll
						</a>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<input type="text" name="filter_search" class="form-control _searchname">
				</div>
			</div>
			<div class="row" style="padding-top: 30px;">
				<div class="col-md-12">
					<div class="namelist" style="position: relative;top: -25px;">
						{!! $controller->show() !!}
					</div>
				</div>
			</div>
		</div>

		<div class="col-md-9">
			<div class="col-md-12">
				<div class="col-md-6">
					<div class="progress hidden">
						  <div class="progress-bar" role="progressbar" id="progressBar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
						  <span class="sr-only">0% Complete</span>
					</div>
					<div class="newSummary-name">
						<label id="lbl_empname" style="text-transform: uppercase;"></label>
					</div>
				</div>
				<div class="col-md-6">

				</div>
			</div>
			<div class="button-wrapper" style="position: relative;top: 10px;left: 5px;" >
<!-- 				<a class="btn btn-xs btn-info btn-savebg btn_new" id="newAttendance" data-btnnew="newAttendance" data-btncancel="cancelAttendance" data-btnedit="editAttendance" data-btnsave="saveAttendance"><i class="fa fa-save"></i> New</a> -->

				<a class="btn btn-xs btn-info btn-editbg btn_edit" id="editAttendance" data-btnnew="newAttendance" data-btncancel="cancelAttendance" data-btnedit="editAttendance" data-btnsave="saveAttendance"><i class="fa fa-save"></i> Edit</a>

				<a class="btn btn-xs btn-info btn-savebg btn_save update_payroll hidden" data-form="form" data-btnnew="newAttendance" data-btncancel="cancelAttendance" data-btnedit="editAttendance" data-btnsave="saveAttendance" id="saveAttendance"><i class="fa fa-save"></i> Save</a>
				<a class="btn btn-xs btn-danger btn_cancel hidden" data-btnnew="newAttendance" data-btncancel="cancelAttendance" data-form="myform" data-btnedit="editAttendance" data-btnsave="saveAttendance"id="cancelAttendance"> Cancel</a>
			</div>
			<br>
			<div class="tab-container">
				<ul class="nav nav-tabs">
					<li class="active"><a href="#payrollsummary">Payroll Summary</a></li>
					<li><a href="#attendance">Attendance</a></li>
					<li><a href="#benefitsinfo">Benefits Info</a></li>
					<li><a href="#contribution">Contributions</a></li>
					<li><a href="#loansinfo">Loans Info</a></li>
					<li><a href="#deducinfo">Deduction Info</a></li>
				</ul>
			</div>
			<div class="tab-content myForm">
				<!-- PAYROLL SUMMARY -->
				<div id="payrollsummary" class="tab-pane fade in active">
					<input type="hidden" name="transaction_id" id="transaction_id">
					<input type="hidden" name="salaryinfo_id" id="salaryinfo_id">
					<div class="col-md-12">
						<div class="col-md-9">
							<div class="border-style2">
								<table class="table borderless">
									<tr class="text-center">
										<td></td>
										<td>Actual</td>
										<td>Adjustment</td>
										<td>Total</td>
									</tr>
									<tr>
										<td>Basic Pay</td>
										<td>
											<input type="text" name="actual_basicpay" id="input_basicpayactual" class="form-control font-style2 onlyNumber actual_workdays_amount" readonly>
										</td>
										<td>
											<input type="text" name="adjust_basicpay" id="input_basicpayadjust" class="form-control font-style2 onlyNumber adjust_workdays_amount" readonly>
										</td>
										<td>
											<input type="text" name="total_basicpay" id="input_basicpayatotal" class="form-control font-style2 total_workdays_amount total_basicpay" readonly>
										</td>
									</tr>
									<tr>
										<td>LWOP</td>
										<td>
											<input type="text" name="actual_lwop_amount" id="actual_lwop_amount" class="form-control font-style2 onlyNumber computeabsences actual_lwop_amount" readonly>
										</td>
										<td>
											<input type="text" name="adjust_lwop_amount" id="adjust_lwop_amount" class="form-control font-style2 onlyNumber computeabsences adjust_lwop_amount" readonly>
										</td>
										<td>
											<input type="text" name="total_lwop_amount" id="total_lwop_amount" class="form-control font-style2 onlyNumber total_lwop_amount" readonly>
										</td>
									</tr>
									<tr>
										<td>Tardines</td>
										<td>
											<input type="text" name="actual_tardiness_amount" id="actual_tardiness_amount" class="form-control font-style2 onlyNumber actual_tardiness_amount" readonly>
										</td>
										<td>
											<input type="text" name="adjust_tardiness_amount" id="adjust_tardiness_amount" class="form-control font-style2 onlyNumber adjust_tardiness_amount" readonly>
										</td>
										<td>
											<input type="text" name="total_tardiness_amount" id="total_tardiness_amount" class="form-control font-style2 onlyNumber" readonly>
										</td>
									</tr>
									<tr>
										<td>Undertime</td>
										<td>
											<input type="text" name="actual_undertime_amount" id="actual_undertime_amount" class="form-control font-style2 onlyNumber actual_undertime_amount" readonly>
										</td>
										<td>
											<input type="text" name="adjust_undertime_amount" id="adjust_undertime_amount" class="form-control font-style2 onlyNumber adjust_undertime_amount" readonly>
										</td>
										<td>
											<input type="text" name="total_undertime_amount" id="total_undertime_amount" class="form-control font-style2 onlyNumber total_undertime_amount" readonly>
										</td>
									</tr>
									<tr>
										<td></td>
										<td></td>
										<td class="text-right">Basic Net Pay</td>
										<td><input type="text" name="basic_net_pay" id="basic_net_pay" class="form-control font-style2 onlyNumber basic_net_pay" readonly></td>
									</tr>

								</table>
							</div>

							<div class="border-style2" style="margin-top: -25px;">
								<table class="table borderless">
									<tr class="text-center">
										<td></td>
										<td>Actual</td>
										<td>Adjustment</td>
										<td>Total</td>
									</tr>
									<tr>
										<td>Total Contributions</td>
										<td>
											<input type="text" name="actual_contribution" id="input_actualcontribution" class="form-control font-style2 onlyNumber computecontribution" readonly>
										</td>
										<td>
											<input type="text" name="adjust_contribution" id="input_adjustcontribution" class="form-control font-style2 onlyNumber computecontribution" readonly>
										</td>
										<td>
											<input type="text" name="total_contribution" id="input_totalcontribution" class="form-control font-style2 onlyNumber" readonly>
										</td>
									</tr>
									<tr>
										<td>Total Loans</td>
										<td>
											<input type="text" name="actual_loan" id="input_actualloan" class="form-control font-style2 onlyNumber computeloan" readonly>
										</td>
										<td>
											<input type="text" name="adjust_loan" id="input_adjustloan" class="form-control font-style2 onlyNumber computeloan" readonly>
										</td>
										<td>
											<input type="text" name="total_loan" id="input_totalloans" class="form-control font-style2 onlyNumber" readonly>
										</td>
									</tr>
									<tr>
										<td>Total Other Deductions</td>
										<td>
											<input type="text" name="actual_otherdeduct" id="input_actualotherdeduct" class="form-control font-style2 onlyNumber computeotherdeduction" readonly>
										</td>
										<td>
											<input type="text" name="adjust_otherdeduct" id="input_adjustotherdeduct" class="form-control font-style2 onlyNumber computeotherdeduction" readonly>
										</td>
										<td>
											<input type="text" name="total_otherdeduct" id="input_totalotherdeduct" class="form-control font-style2 onlyNumber" readonly>
										</td>
									</tr>
									<tr>
										<td></td>
										<td></td>
										<td class="text-right">Total Deductions</td>
										<td><input type="text" name="net_deduction" id="net_deduction" class="form-control font-style2 onlyNumber" readonly></td>
									</tr>
								</table>
							</div>
							<br>
						</div>

						<div class="col-md-3">
							<div class="box-grosspay" style="padding-top: 22px;">
								<label style="display: flex;">Gross Pay</label>
								<span id="grosspay">0.00</span>
							</div>

							<div class="box-grosspay" style="padding-top: 22px;">
								<label style="display: flex;">Gross Taxable Pay</label>
								<span id="grosstaxablepay">0.00</span>
							</div>

							<div class="box-grosspay" style="padding-top: 22px;">
								<label style="display: flex;">Net Pay</label>
								<span id="netpay">0.00</span>
							</div>

							<div class="box-grosspay" style="padding-top: 22px;">
								<label>Hold</label><br>
								<input type="checkbox" name="chk_holdpay" id="chk_holdpay" >

							</div>
						</div>

					</div>
				</div>
				<!-- PAYROLL SUMMARY -->
				<!-- ATTENDANCE -->
				<div id="attendance" class="tab-pane fade in">
					<div class="col-md-12">
						<div class="border-style2">
							<input type="hidden" name="attendance_id" id="attendance_id">
							<table class="table borderless">
								<tr>
									<td></td>
									<td class="text-center"><span>Actual</span></td>
									<td class="text-center"><span>Adjustment</span></td>
									<td class="text-center"><span>Total</span></td>
								</tr>
								<tr>
									<td>Work Days</td>
									<td>
										<input type="text" name="actual_workdays" id="actual_workdays" class="form-control font-style2 input attendance workdays" maxlength="2" readonly>
									</td>
									<td class="newAttendance">
										<input type="text" name="adjust_workdays" id="adjust_workdays" class="form-control font-style2 input attendance  workdays isNumber" maxlength="2">
									</td>
									<td>
										<input type="text" name="total_workdays_amount" id="total_workdays_amount" class="form-control font-style2 input total_basicpay" placeholder="0.00" readonly>
									</td>
								</tr>
								<tr>
									<td>LWOP</td>
									<td class="newAttendance">
										<input type="text" name="actual_lwop" id="actual_lwop" class="form-control font-style2 input attendance absences isNumber" maxlength="2">
									</td>
									<td class="newAttendance">
										<input type="text" name="adjust_lwop" id="adjust_lwop" class="form-control font-style2 input attendance absences isNumber" maxlength="2">
									</td>
									<td>
										<input type="text" name="total_lwop" id="total_lwop" class="form-control font-style2 input total_lwop_amount" placeholder="0.00" readonly>
									</td>
								</tr>
								<tr>
									<td>Tardiness</td>
									<td class="newAttendance">
										<input type="text" name="actual_tardiness" id="actual_tardiness" class="form-control font-style2 attendance input computetardines isNumber">
									</td>
									<td class="newAttendance">
										<input type="text" name="adjust_tardiness" id="adjust_tardiness" class="form-control font-style2 attendance input computetardines isNumber">
									</td>
									<td>
										<input type="text" name="total_tardiness_amount" id="total_tardiness_amount" class="form-control font-style2 input total_tardiness_amount " readonly placeholder="0.00">
									</td>
								</tr>
								<tr>
									<td>Undertime</td>
									<td class="newAttendance">
										<input type="text" name="actual_undertime" id="actual_undertime" class="form-control attendance font-style2 input  computeundertime isNumber">
									</td>
									<td class="newAttendance">
										<input type="text" name="adjust_undertime" id="adjust_undertime" class="form-control attendance font-style2 input computeundertime isNumber">
									</td>
									<td>
										<input type="text" name="total_undertime_amount" id="total_undertime_amount" class="form-control font-style2 input total_undertime_amount " readonly placeholder="0.00">
									</td>
								</tr>
							</table>
						</div>
						<br>
					</div>
				</div>
				<!-- ATTENDANCE -->
				<!-- BENF /  ALLOW INFO -->
				<div id="benefitsinfo" class="tab-pane fade in">
					<div class="sub-panel" style="margin-top: 45px;">
						{!! $controller->showBenefitinfo() !!}
					</div>
					<div class="button-wrapper" style="margin-left: 10px;">
						<a class="btn btn-xs btn-info btn-savebg btn_new" id="newBenefitInfo" data-btnnew="newBenefitInfo" data-btncancel="cancelBenefitInfo" data-btnedit="editBenefitInfo" data-btnsave="saveBenefitInfo"><i class="fa fa-save"></i> New</a>

						<a class="btn btn-xs btn-info btn-editbg btn_edit hidden" id="editBenefitInfo" data-btnnew="newBenefitInfo" data-btncancel="cancelBenefitInfo" data-btnedit="editBenefitInfo" data-btnsave="saveBenefitInfo"><i class="fa fa-save"></i> Edit</a>

						<a class="btn btn-xs btn-info btn-savebg btn_save submitme hidden" data-form="form" data-btnnew="newBenefitInfo" data-btncancel="cancelBenefitInfo" data-btnedit="editBenefitInfo" data-btnsave="saveBenefitInfo" id="saveBenefitInfo"><i class="fa fa-save"></i> Save</a>
						<a class="btn btn-xs btn-danger btn_cancel hidden" data-btnnew="newBenefitInfo" data-btncancel="cancelBenefitInfo" data-form="myform" data-btnedit="editBenefitInfo" data-btnsave="saveBenefitInfo"id="cancelBenefitInfo"> Cancel</a>
					</div>
					<div class="border-style2">
						<form method="post" action="{{ url($module_prefix.'/'.$module.'/storeBenefitInfoTransaction')}}" id="form" onsubmit="return false" class="myform">
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
							<input type="hidden" name="employee_id" id="benefit_employee_id">
							<input type="hidden" name="transaction_id" id="benefit_transaction_id">
							<input type="hidden" name="benefit_year" id="benefit_year">
							<input type="hidden" name="benefit_month" id="benefit_month">
							<div class="col-md-3">
								<div class="form-group newBenefitInfo">
									<label>Allowance</label>
									<select class="form-control" id="benefit_id" name="benefit_id">
										<option value=""></option>
										@foreach($benefit as $key => $value)
										<option value="{{ $value->id }}">{{ $value->name }}</option>
										@endforeach
									</select>
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group newBenefitInfo">
									<label>Amount</label>
									<input type="text" name="bt_amount" id="bt_amount" class="form-control onlyNumber">
								</div>
							</div>
						</form>
					</div>
				</div>
				<!-- BENF /  ALLOW INFO -->
				<!-- CONTRIBUTIONS -->
				<div id="contribution" class="tab-pane fade in">
					<div class="col-md-12">
						<div class="border-style2">
							<table class="table borderless">
								<tr class="text-center">
									<td colspan="2"><label style="margin-left: 80px;">Employee Share</label></td>
									<td colspan="1"><label style="margin-right: 32px;">Employer Share</label></td>
									<td colspan="3"><label style="margin-right: 24px;">ECC</label></td>
								</tr>
								<tr>
									<td>GSIS</td>
									<td>
										<input type="text" name="gsis_ee_share" id="gsis_ee_share" class="form-control font-style2 onlyNumber" readonly>
									</td>
									<td>
										<input type="text" name="gsis_er_share" id="gsis_er_share" class="form-control font-style2 onlyNumber" readonly>
									</td>
									<td>
										<input type="text" name="ecc_amount" id="ecc_amount" class="form-control font-style2 onlyNumber" readonly>
									</td>
									<td colspan="2"></td>
								</tr>
								<tr>
									<td>Philhealth</td>
									<td>
										<input type="text" name="philhealth_ee_share" id="philhealth_ee_share" class="form-control font-style2 onlyNumber" readonly>
									</td>
									<td>
										<input type="text" name="philhealth_er_share" id="philhealth_er_share" class="form-control font-style2 onlyNumber" readonly>
									</td>
									<td colspan="3"></td>
								</tr>
								<tr>
									<tr>
									<td>Pag-ibig</td>
									<td>
										<input type="text" name="pagibig_ee_share" id="pagibig_ee_share" class="form-control font-style2 onlyNumber" readonly>
									</td>
									<td>
										<input type="text" name="pagibig_er_share" id="pagibig_er_share" class="form-control font-style2 onlyNumber" readonly>
									</td>
									<td colspan="3"></td>
								</tr>
								<tr>
									<td>Withholding Tax</td>
									<td>
										<input type="text" name="witholding_tax" id="witholding_tax" class="form-control font-style2 onlyNumber" readonly>
									</td>
									<td colspan="4"></td>
								</tr>
								<tr>
									<td>Provident Fund</td>
									<td>
										<input type="text" name="input_contribEmpSharePfund" id="input_contribEmpSharePfund" class="form-control font-style2 onlyNumber" readonly>
									</td>
									<td>
										<input type="text" name="input_contribEmprSharePfund" id="input_contribEmprSharePfund" class="form-control font-style2 onlyNumber" readonly>
									</td>
									<td colspan="3"></td>
								</tr>
								<tr>
									<td>Association Dues</td>
									<td>
										<input type="text" name="input_contribEmpShareAdues" id="input_contribEmpShareAdues" class="form-control font-style2 onlyNumber" readonly>
									</td>
									<td colspan="4"></td>
								</tr>
								<tr>
									<td>Pag-ibig Fund II</td>
									<td>
										<input type="text" name="pagibig2" id="pagibig2" class="form-control font-style2 onlyNumber" readonly>
									</td>
									<td colspan="4"></td>
								</tr>
								<tr>
									<td>GSIS UOLI Premium I</td>
									<td>
										<input type="text" name="input_contribEmpShareGsisUoliPremI" id="input_contribEmpShareGsisUoliPremI" class="form-control font-style2 onlyNumber" readonly>
									</td>
									<td colspan="4"></td>
								</tr>
								<tr>
									<td>GSIS UOLI Premium II</td>
									<td>
										<input type="text" name="input_contribEmpShareGsisUoliPremII" id="input_contribEmpShareGsisUoliPremII" class="form-control font-style2 onlyNumber" readonly>
									</td>
									<td colspan="4"></td>
								</tr>
								<tr>
									<td>GSIS UOLI Premium II</td>
									<td>
										<input type="text" name="input_contribEmpShareGsisUoliPremIII" id="input_contribEmpShareGsisUoliPremIII" class="form-control font-style2 onlyNumber" readonly>
									</td>
									<td colspan="4"></td>
								</tr>
								<tr>
									<td>GSIS UOLI Premium IV</td>
									<td>
										<input type="text" name="input_contribEmpShareGsisUoliPremIV" id="input_contribEmpShareGsisUoliPremII" class="form-control font-style2 onlyNumber" readonly>
									</td>
									<td colspan="4"></td>
								</tr>
								<tr>
									<td>GSIS UOLI Premium V</td>
									<td>
										<input type="text" name="input_contribEmpShareGsisUoliPremV" id="input_contribEmpShareGsisUoliPremV" class="form-control font-style2 onlyNumber" readonly>
									</td>
									<td colspan="4"></td>
								</tr>
							</table>
						</div>
						<br>
					</div>
				</div>
				<!-- CONTRIBUTIONS -->
				<!-- LOANS INFO -->
				<div id="loansinfo" class="tab-pane fade in">
					<div class="sub-panel" style="margin-top: 50px;z-index: 1;">

						{!! $controller->showLoaninfo() !!}

					</div>
				</div>
				<!-- LOANS INFO  -->
				<!-- DEDUCT INFO -->
				<div id="deducinfo" class="tab-pane fade in">
					<div class="sub-panel" style="margin-top: 50px;z-index: 1;">
					{!! $controller->showDeductioninfo() !!}
					</div>

					<div class="button-wrapper" style="margin-left: 10px;">
						<a class="btn btn-xs btn-info btn-savebg btn_new" id="newDeductionInfo" data-btnnew="newDeductionInfo" data-btncancel="cancelDeductionInfo" data-btnedit="editDeductionInfo" data-btnsave="saveDeductionInfo"><i class="fa fa-save"></i> New</a>

						<a class="btn btn-xs btn-info btn-editbg btn_edit hidden" id="editDeductionInfo" data-btnnew="newDeductionInfo" data-btncancel="cancelDeductionInfo" data-btnedit="editDeductionInfo" data-btnsave="saveDeductionInfo"><i class="fa fa-save"></i> Edit</a>

						<a class="btn btn-xs btn-info btn-savebg btn_save submitme hidden" data-form="formDeduction" data-btnnew="newDeductionInfo" data-btncancel="cancelDeductionInfo" data-btnedit="editDeductionInfo" data-btnsave="saveDeductionInfo" id="saveDeductionInfo"><i class="fa fa-save"></i> Save</a>
						<a class="btn btn-xs btn-danger btn_cancel hidden" data-btnnew="newDeductionInfo" data-btncancel="cancelDeductionInfo" data-form="myform" data-btnedit="editDeductionInfo" data-btnsave="saveDeductionInfo"id="cancelDeductionInfo"> Cancel</a>
					</div>
					<div class="border-style2">
						<form method="post" action="{{ url($module_prefix.'/'.$module.'/storeDeductionInfoTransaction')}}" id="formDeduction" onsubmit="return false" class="myformDeduction">
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
							<input type="hidden" name="employee_id" id="deduction_employee_id">
							<input type="hidden" name="transaction_id" id="deduction_transaction_id">
							<input type="hidden" name="deduction_year" id="deduction_year">
							<input type="hidden" name="deduction_month" id="deduction_month">
							<div class="col-md-3">
								<div class="form-group newDeductionInfo">
									<label>Deduction</label>
									<select class="form-control" id="deduction_id" name="deduction_id">
										<option value=""></option>
										@foreach($deductions as $key => $value)
										<option value="{{ $value->id }}">{{ $value->name }}</option>
										@endforeach
									</select>
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group newDeductionInfo">
									<label>Amount</label>
									<input type="text" name="deduction_amount" id="deduction_amount" class="form-control onlyNumber">
								</div>
							</div>
						</form>
					</div>

				</div>
				<!-- DEDUCT INFO -->
			</div>
		</div>
	</div>
</div>
@endsection

@section('js-logic1')
<script type="text/javascript">
$(document).ready(function(){

var regularDayRate = 1.25;
var specialHolidayRate = 1.50;
var regularHolidayRate = 1.50;

var tDeduct;
var tLoan;
var tBenefit;
var bt_amount;
var deduction_amount;
var editClicked = false;

var monthlyRate;
var totalWorkDays;
var payRatePerDay;
var payPerHour;
var taxContribution;
var hoursPerDay = 8;
var pagibigAmount;
var allowance;

var totalLWOP;
var totalTardiness;
var totalUndertime;
var totalBasicPay;

// GENERATE YEAR
var year = [];
year += '<option ></option>';
for(y = 2018; y <= 2100; y++) {
    year += '<option value='+y+'>'+y+'</option>';
}
$('#select_year').html(year);

// GENERATE MONTH
month = ["January","February","March","April","May","June","July","August","September","October", "November","December"];
mArr = [];

mArr += '<option ></option>';
for ( m =  0; m <= month.length - 1; m++) {
	mArr += '<option '+month[m]+'>'+month[m]+'</option>';
}
$('#select_month').html(mArr);


// ************************************************
var _bool = false;
var _Year;
var _Month;
$(document).on('change','#select_year',function(){
	_Year = "";
	_Year = $(this).find(':selected').val();
	$('#benefit_year').val(_Year);
	$('#deduction_year').val(_Year);
	if(_bool == true){
		$('.btnfilter').trigger('click');
	}

})
$(document).on('change','#select_month',function(){
	_Month = "";
	_Month = $(this).find(':selected').val();
	$('#benefit_month').val(_Month);
	$('#deduction_month').val(_Month);
	if(_bool == true){
		$('.btnfilter').trigger('click');
	}
})

$('.select2').select2();

$('#select_month').select2({
    allowClear:true,
    placeholder: "Month",
});

$('#select_year').select2({
    allowClear:true,
    placeholder: "Year"
});
$('.newAttendance :input').attr('disabled',true);
$('.newAttendance').attr('disabled',true);

$('.newBenefitInfo :input').attr('disabled',true);
$('.newDeductionInfo :input').attr('disabled',true);

$('#woutpayroll').prop('checked','checked');

$(document).on('change','#benefit_id',function(){
	benefit_id = $(this).find(':selected').val();
})

$(document).on('change','#select_period',function(){
	period = $(this).val();
	arr = [];
	switch(period){
		case 'semimonthly':
			arr += '<option value="First Half">First Half</option>';
			arr += '<option value="Second Half">Second Half</option>';
		break;
		case 'monthly':
		break;
		default:
		$('#select_subperiod').html();
	}
	$('#select_subperiod').html(arr);
});


$(document).on('change','#searchby',function(){
	var val = $(this).val();
	console.log(base_url+module_prefix+module)
	$.ajax({
		url:base_url+module_prefix+module+'/getSearchby',
		data:{'q':val},
		type:'GET',
		dataType:'JSON',
		success:function(data){
			arr = [];
			$.each(data,function(k,v){
				arr += '<option value='+v.id+'>'+v.name+'</option>';
			})
			$('#select_searchvalue').html(arr);
		}
	})
});



$('.nav-tabs a').click(function(){
	$('.nav-tabs a').click(function(){
	tab = $(this).text();

	switch(tab){
		case 'Attendance':
				if(editClicked == false){
					swal({
						title: "Click Edit",
						type: "warning",
						showCancelButton: false,
						confirmButtonClass: "btn-warning",
						confirmButtonText: "Yes",
						closeOnConfirm: false
					});

				}else{
					$(this).tab('show');
				}
		break;
		case 'Benefits Info':
			if(editClicked == false){
				swal({
					title: "Click Edit",
					type: "warning",
					showCancelButton: false,
					confirmButtonClass: "btn-warning",
					confirmButtonText: "Yes",
					closeOnConfirm: false
					});
			}else{
				$(this).tab('show');
			}
		break;
		case 'Deduction Info':
			if(editClicked == false){
				swal({
					title: "Click Edit",
					type: "warning",
					showCancelButton: false,
					confirmButtonClass: "btn-warning",
					confirmButtonText: "Yes",
					closeOnConfirm: false
					});
			}else{
				$(this).tab('show');
			}
		break;
		default:
			$(this).tab('show');
		break;
	}
});

});

$('.isNumber').keypress(function (event) {
    return isNumber(event, this)
});

$('.onlyNumber').keypress(function (event) {
    return isNumber(event, this)
});

$('.onlyNumber').prop('placeholder','0.00');

$(".onlyNumber").keyup(function(){
	amount  = $(this).val();
	if(amount == 0){
		$(this).val('');
	}else{
		plainAmount = amount.replace(/\,/g,'')
		$(this).val(commaSeparateNumber(plainAmount));
	}
});

$('.attendance').keypress(function (event) {
	return isNumber(event, this)
});


var firstWorkDaysAmount;
var adjustWorkDaysAmount;
$(document).on('keyup','#actual_workdays',function(){
	actual_workdays = $(this).val();
	actualWorkDaysAmount = compute_workingdays(payRatePerDay,actual_workdays);

	totalBasicPay = compute_basicpay(actualWorkDaysAmount,adjustWorkDaysAmount);

	grossPay 	= compute_grosspay(totalBasicPay,allowance);
	basicNet = compute_basic_net(totalBasicPay,totalLWOP,totalUndertime,totalTardiness);
	grossTaxable = compute_gross_taxable(basicNet,pagibigAmount);
	netPay = compute_cos_net_pay(grossTaxable,taxContribution);

	actual_amount = (actualWorkDaysAmount) ? commaSeparateNumber(parseFloat(actualWorkDaysAmount).toFixed(2)) : '';
	basic_pay = (totalBasicPay) ? commaSeparateNumber(parseFloat(totalBasicPay).toFixed(2)) : '';
	basic_net = (basicNet) ? commaSeparateNumber(parseFloat(basicNet).toFixed(2)) : '';
	gross_pay = (grossPay) ? commaSeparateNumber(parseFloat(grossPay).toFixed(2)) : '';
	gross_taxable_amount = (grossTaxable) ? commaSeparateNumber(parseFloat(grossTaxable).toFixed(2)) : '';
	net_pay = (netPay) ? commaSeparateNumber(parseFloat(netPay).toFixed(2)) : '';

	$('.actual_workdays_amount').val(actual_amount);
	$('.total_basicpay').val(basic_pay);
	$('.basic_net').val(basic_net);
	$('#grosspay').text(basic_net);
	$('#grosstaxablepay').text(gross_taxable_amount);
	$('#netpay').text(net_pay);
	$('.gross_pay').val(gross_pay);
	$('.net_pay').val(netPay);
	$('.gross_taxable_pay').val(grossTaxable);
	$('.basic_net_pay').val(basic_net);
});

$(document).on('keyup','#adjust_workdays',function(){
	adjust_workdays = $(this).val();
	adjustWorkDaysAmount = compute_workingdays(payRatePerDay,adjust_workdays);

	totalBasicPay = compute_basicpay(actualWorkDaysAmount,adjustWorkDaysAmount);
	grossPay = compute_grosspay(totalBasicPay,allowance);
	basicNet = compute_basic_net(totalBasicPay,totalLWOP,totalUndertime,totalTardiness);
	grossTaxable = compute_gross_taxable(basicNet,pagibigAmount);
	netPay = compute_cos_net_pay(grossTaxable,taxContribution);

	adjust_amount = (adjustWorkDaysAmount) ? commaSeparateNumber(parseFloat(adjustWorkDaysAmount).toFixed(2)) : '';
	basic_pay = (totalBasicPay) ? commaSeparateNumber(parseFloat(totalBasicPay).toFixed(2)) : '';
	basic_net = (basicNet) ? commaSeparateNumber(parseFloat(basicNet).toFixed(2)) : '';
	gross_pay = (grossPay) ? commaSeparateNumber(parseFloat(grossPay).toFixed(2)) : '';
	gross_taxable_amount = (grossTaxable) ? commaSeparateNumber(parseFloat(grossTaxable).toFixed(2)) : '';
	net_pay = (netPay) ? commaSeparateNumber(parseFloat(netPay).toFixed(2)) : '';

	$('.adjust_workdays_amount').val(adjust_amount);
	$('.total_basicpay').val(basic_pay);
	$('.basic_net').val(basic_net);
	$('#grosspay').text(basic_net);
	$('#grosstaxablepay').text(gross_taxable_amount);
	$('#netpay').text(net_pay);
	$('.gross_pay').val(gross_pay);
	$('.net_pay').val(netPay);
	$('.gross_taxable_pay').val(grossTaxable);
	$('.basic_net_pay').val(basic_net);
});

var actualLWOPAmount;
var adjustLWOPAmount;
$(document).on('keyup','#actual_lwop',function(){
	actual_lwop = $(this).val();
	actualLWOPAmount = compute_workingdays(payRatePerDay,actual_lwop);

	totalLWOP = compute_lwop(actualLWOPAmount,adjustLWOPAmount);
	basicNet = compute_basic_net(totalBasicPay,totalLWOP,totalUndertime,totalTardiness);

	grossTaxable = compute_gross_taxable(basicNet,pagibigAmount);
	netPay = compute_cos_net_pay(grossTaxable,taxContribution);

	actual_lwop_amount = (actualLWOPAmount) ? commaSeparateNumber(parseFloat(actualLWOPAmount).toFixed(2)) : '';
	total_lwop_amount = (totalLWOP) ? commaSeparateNumber(parseFloat(totalLWOP).toFixed(2)) : '';
	basic_net = (basicNet) ? commaSeparateNumber(parseFloat(basicNet).toFixed(2)) : '';
	gross_taxable_amount = (grossTaxable) ? commaSeparateNumber(parseFloat(grossTaxable).toFixed(2)) : '';
	net_pay = (netPay) ? commaSeparateNumber(parseFloat(netPay).toFixed(2)) : '';

	$('.actual_lwop_amount').val(actual_lwop_amount);
	$('.total_lwop_amount').val(total_lwop_amount);
	$('.basic_net').val(basic_net);
	$('#grosspay').text(basic_net);
	$('#grosstaxablepay').text(gross_taxable_amount);
	$('#netpay').text(net_pay);
	$('.gross_pay').val(basicNet);
	$('.net_pay').val(netPay);
	$('.gross_taxable_pay').val(grossTaxable);
	$('.basic_net_pay').val(basic_net);
});

$(document).on('keyup','#adjust_lwop',function(){
	adjust_lwop = $(this).val();
	adjustLWOPAmount = compute_workingdays(payRatePerDay,adjust_lwop);

	totalLWOP = compute_lwop(adjustLWOPAmount,adjustLWOPAmount);
	basicNet = compute_basic_net(totalBasicPay,totalLWOP,totalUndertime,totalTardiness);
	grossTaxable = compute_gross_taxable(basicNet,pagibigAmount);
	netPay = compute_cos_net_pay(grossTaxable,taxContribution);

	adjust_lwop_amount = (adjustLWOPAmount) ? commaSeparateNumber(parseFloat(adjustLWOPAmount).toFixed(2)) : '';
	total_lwop_amount = (totalLWOP) ? commaSeparateNumber(parseFloat(totalLWOP).toFixed(2)) : '';
	basic_net = (basicNet) ? commaSeparateNumber(parseFloat(basicNet).toFixed(2)) : '';
	gross_taxable_amount = (grossTaxable) ? commaSeparateNumber(parseFloat(grossTaxable).toFixed(2)) : '';
	net_pay = (netPay) ? commaSeparateNumber(parseFloat(netPay).toFixed(2)) : '';

	$('.adjust_lwop_amount').val(adjust_lwop_amount);
	$('.total_lwop_amount').val(total_lwop_amount);
	$('.basic_net').val(basic_net);
	$('#grosspay').text(basic_net);
	$('#grosstaxablepay').text(gross_taxable_amount);
	$('#netpay').text(net_pay);
	$('.gross_pay').val(basicNet);
	$('.net_pay').val(netPay);
	$('.gross_taxable_pay').val(grossTaxable);
	$('.basic_net_pay').val(basic_net);

});

var actualTardinessAmount;
var adjustTardinessAmount;
$(document).on('keyup','#actual_tardiness',function(){
	actual_tardiness = $(this).val();

	actualTardinessAmount = compute_rate_per_minute(payPerHour,actual_tardiness);

	totalTardiness = compute_totaltardines(actualTardinessAmount,adjustTardinessAmount)
	basicNet = compute_basic_net(totalBasicPay,totalLWOP,totalUndertime,totalTardiness);

	netPay = compute_cos_net_pay(grossTaxable,taxContribution);
	grossTaxable = compute_gross_taxable(basicNet,pagibigAmount);
	actual_tardiness_amount = (actualTardinessAmount) ? commaSeparateNumber(parseFloat(actualTardinessAmount).toFixed(2)) : '';
	total_tardiness_amount = (totalTardiness) ? commaSeparateNumber(parseFloat(totalTardiness).toFixed(2)) : '';
	basic_net = (basicNet) ? commaSeparateNumber(parseFloat(basicNet).toFixed(2)) : '';
	gross_taxable_amount = (grossTaxable) ? commaSeparateNumber(parseFloat(grossTaxable).toFixed(2)) : '';
	net_pay = (netPay) ? commaSeparateNumber(parseFloat(netPay).toFixed(2)) : '';

	$('.actual_tardiness_amount').val(actual_tardiness_amount);
	$('.total_tardiness_amount').val(total_tardiness_amount);
	$('.basic_net').val(basic_net);
	$('#grosspay').text(basic_net);
	$('#grosstaxablepay').text(gross_taxable_amount);
	$('#netpay').text(net_pay);
	$('.gross_pay').val(basicNet);
	$('.net_pay').val(netPay);
	$('.gross_taxable_pay').val(grossTaxable);
	$('.basic_net_pay').val(basic_net);

});

$(document).on('keyup','#adjust_tardiness',function(){
	adjust_tardiness = $(this).val();

	adjustTardinessAmount = compute_rate_per_minute(payPerHour,adjust_tardiness);

	totalTardiness = compute_totaltardines(actualTardinessAmount,adjustTardinessAmount)
	basicNet = compute_basic_net(totalBasicPay,totalLWOP,totalUndertime,totalTardiness);
	grossTaxable = compute_gross_taxable(basicNet,pagibigAmount);

	adjust_tardiness_amount = (adjustTardinessAmount) ? commaSeparateNumber(parseFloat(adjustTardinessAmount).toFixed(2)) : '';
	total_tardiness_amount = (totalTardiness) ? commaSeparateNumber(parseFloat(totalTardiness).toFixed(2)) : '';
	basic_net = (basicNet) ? commaSeparateNumber(parseFloat(basicNet).toFixed(2)) : '';
	gross_taxable_amount = (grossTaxable) ? commaSeparateNumber(parseFloat(grossTaxable).toFixed(2)) : '';
	netPay = compute_cos_net_pay(grossTaxable,taxContribution);
	net_pay = (netPay) ? commaSeparateNumber(parseFloat(netPay).toFixed(2)) : '';

	$('.adjust_tardiness_amount').val(adjust_tardiness_amount);
	$('.total_tardiness_amount').val(total_tardiness_amount);
	$('.basic_net').val(basic_net);
	$('#grosspay').text(basic_net);
	$('#grosstaxablepay').text(gross_taxable_amount);
	$('#netpay').text(net_pay);
	$('.gross_pay').val(basicNet);
	$('.net_pay').val(netPay);
	$('.gross_taxable_pay').val(grossTaxable);
	$('.basic_net_pay').val(basic_net);

});

var actualUndertimeAmount;
var adjustUndertimeAmount;
$(document).on('keyup','#actual_undertime',function(){
	actual_undertime = $(this).val();

	actualUndertimeAmount = compute_rate_per_minute(payPerHour,actual_undertime);

	totalUndertime = compute_totaltardines(actualUndertimeAmount,adjustUndertimeAmount)
	basicNet = compute_basic_net(totalBasicPay,totalLWOP,totalUndertime,totalTardiness);
	grossTaxable = compute_gross_taxable(basicNet,pagibigAmount);

	actual_undertime_amount = (actualUndertimeAmount) ? commaSeparateNumber(parseFloat(actualUndertimeAmount).toFixed(2)) : '';
	basic_net = (basicNet) ? commaSeparateNumber(parseFloat(basicNet).toFixed(2)) : '';
	total_undertime_amount = (totalUndertime) ? commaSeparateNumber(parseFloat(totalUndertime).toFixed(2)) : '';
	gross_taxable_amount = (grossTaxable) ? commaSeparateNumber(parseFloat(grossTaxable).toFixed(2)) : '';
	netPay = compute_cos_net_pay(grossTaxable,taxContribution);
	net_pay = (netPay) ? commaSeparateNumber(parseFloat(netPay).toFixed(2)) : '';

	$('.actual_undertime_amount').val(actual_undertime_amount);
	$('.total_undertime_amount').val(total_undertime_amount);
	$('.basic_net').val(basic_net);
	$('#grosspay').text(basic_net);
	$('#grosstaxablepay').text(gross_taxable_amount);
	$('#netpay').text(net_pay);
	$('.gross_pay').val(basicNet);
	$('.net_pay').val(netPay);
	$('.gross_taxable_pay').val(grossTaxable);
	$('.basic_net_pay').val(basic_net);
});

$(document).on('keyup','#adjust_undertime',function(){
	adjust_undertime = $(this).val();

	adjustUndertimeAmount = compute_rate_per_minute(payPerHour,adjust_undertime);

	totalUndertime = compute_totaltardines(actualUndertimeAmount,adjustUndertimeAmount)
	basicNet = compute_basic_net(totalBasicPay,totalLWOP,totalUndertime,totalTardiness);
	grossTaxable = compute_gross_taxable(basicNet,pagibigAmount);
	netPay = compute_cos_net_pay(grossTaxable,taxContribution);

	adjust_undertime_amount = (adjustUndertimeAmount) ? commaSeparateNumber(parseFloat(adjustUndertimeAmount).toFixed(2)) : '';
	basic_net = (basicNet) ? commaSeparateNumber(parseFloat(basicNet).toFixed(2)) : '';
	total_undertime_amount = (totalUndertime) ? commaSeparateNumber(parseFloat(totalUndertime).toFixed(2)) : '';
	gross_taxable_amount = (grossTaxable) ? commaSeparateNumber(parseFloat(grossTaxable).toFixed(2)) : '';
	net_pay = (netPay) ? commaSeparateNumber(parseFloat(netPay).toFixed(2)) : '';

	$('.adjust_undertime_amount').val(adjust_undertime_amount);
	$('.total_undertime_amount').val(total_undertime_amount);
	$('.basic_net').val(basic_net);
	$('#grosspay').text(basic_net);
	$('#grosstaxablepay').text(gross_taxable_amount);
	$('#netpay').text(net_pay);
	$('.gross_pay').val(basicNet);
	$('.net_pay').val(netPay);
	$('.gross_taxable_pay').val(grossTaxable);
	$('.basic_net_pay').val(basic_net);
});



var _listId = [];
$(document).on('click','#check_all',function(){
	if(!_Year && !_Month){
		swal({
			title: "Select year and month first",
			type: "warning",
			showCancelButton: false,
			confirmButtonClass: "btn-danger",
			confirmButtonText: "Yes",
			closeOnConfirm: false
		});
		$('#check_all').prop('checked',false);
	}else{
		if ($(this).is(':checked')) {
	        $('.emp_select').prop('checked', 'checked');
	        $('.emp_select:checked').each(function(){
	        	_listId.push($(this).val())
	        });
	    } else {
	        $('.emp_select').prop('checked', false)
	        _listId = [];
	    }
	}
});

$(document).on('click','.emp_select',function(){
	empid = $(this).val();
	index = $(this).data('key');
	if(!_Year && !_Month){
		swal({
			title: "Select year and month first",
			type: "warning",
			showCancelButton: false,
			confirmButtonClass: "btn-danger",
			confirmButtonText: "Yes",
			closeOnConfirm: false
		});
		$('.emp_select').prop('checked',false);
	}else{
		if($(this).is(':checked')){
			_listId[index] =  empid;

		}else{
			delete _listId[index];
		}
	}
	console.log(_listId);
});


$('#process_payroll').on('click',function(){
	if(_listId.length == 0){
		swal("Select employee first!", "", "warning");
	}else{
		swal({
			title: "Process Payroll?",
			type: "warning",
			showCancelButton: true,
			confirmButtonClass: "btn-warning",
			confirmButtonText: "Yes",
			cancelButtonText: "No",
		}).then(function(isConfirm){
			if(isConfirm.value == true){
				$.processPayroll();
			}else{
				return false;
			}
		});
	}
});

$.processPayroll = function(){
	$.ajax({
        type:'POST',
        data:{
        	'empid':_listId,
        	'_token':"{{ csrf_token() }}"
        	,'year':_Year,
        	'month':_Month,
        	'transaction_id':$('#transaction_id').val(),
        	'attendance_id':$('#attendance_id').val(),
        	'salaryinfo_id':$('#salaryinfo_id').val()
        },
        url: base_url+module_prefix+module+'/processPayroll',
        beforeSend:function(){
        	// $('#process_payroll').html('<i class="fa fa-spinner fa-spin"></i> Processing').prop('disabled',true);
        },
        success:function(data) {
        	par = JSON.parse(data);
        	if(par.status){
           		swal({
					  title: par.response,
					  type: "success",
					  showCancelButton: false,
					  confirmButtonClass: "btn-success",
					  confirmButtonText: "OK",
					  closeOnConfirm: false
				}).then(function(){
					$('#process_payroll').html('<i class="fa fa-save"></i> Process').prop('disabled',false);
					$('.btn_save').addClass('hidden');
					$('.btn_cancel').addClass('hidden');
					$('.btn_new').removeClass('hidden');
					$('#transaction_id').val('');
					$('.newAttendance :input').attr('disabled',true);
					$('.newAttendance').attr('disabled',true);
					clear_form_elements('attendance');
					$('.btnfilter').trigger('click');
					_listId = [];
				});
        	}else{
        		swal({
            	   title: par.response,
				   type: "warning",
				   showCancelButton: false,
				   confirmButtonClass: "btn-warning",
				   confirmButtonText: "OK",
				   closeOnConfirm: false
        		});
        	}
        },
        complete:function(){
        }
	})
}

$(document).on('click','#namelist tr',function(){
	_id = $(this).data('empid');

	$.ajax({
		url:base_url+module_prefix+module+'/getEmployeesinfo',
		data:{'id':_id,'year':_Year,'month':_Month},
		type:'get',
		dataType:'JSON',
		success:function(data){
			console.log(data)
			clear_form_elements('myForm');
			$('#lbl_empname').text('');
			$('#grosspay').text(0.00);
			$('#netpay').text(0.00);
			$('#grosstaxablepay').text(0.00);
			$('#transaction_id').val('');
			$('#attendance_id').val('');
    		$('#employee_id').val('');
			if(data.transaction !== null){
				fullname = data.transaction.employees.lastname+' '+data.transaction.employees.firstname+' '+ data.transaction.employees.middlename;

				totalWorkDays = data.transaction.actual_workdays;
				taxContribution  = (data.employeeinfo.tax_contribution) ? data.employeeinfo.tax_contribution : 0;
				gsis_ee_share = (data.employeeinfo.gsis_contribution) ? data.employeeinfo.gsis_contribution : 0;
				er_gsis_share = (data.employeeinfo.er_gsis_share) ? data.employeeinfo.er_gsis_share : 0;
				philhealth_ee_share = (data.employeeinfo.philhealth_contribution) ? data.employeeinfo.philhealth_contribution : 0;
				philhealth_er_share = (data.employeeinfo.er_philhealth_share) ? data.employeeinfo.er_philhealth_share : 0;
				er_pagibig_share = (data.employeeinfo.er_pagibig_share) ? data.employeeinfo.er_pagibig_share : 0;
				pagibig2 = (data.employeeinfo.pagibig2) ? data.employeeinfo.pagibig2 : 0;

				allowance 			  = data.benefitinfo.amount;
				monthlyRate 		  = data.transaction.basic_net_pay;
    			payRatePerDay 		  = (parseFloat(monthlyRate))/totalWorkDays;
    			payPerHour 		  	  = (parseFloat(payRatePerDay)) / hoursPerDay;
				totalContribution 	  = (data.transaction.total_contribution) ? data.transaction.total_contribution : 0;
				totalLoans 		  	  = (data.transaction.total_loan) ? data.transaction.total_loan : 0;
				totalOtherDeductions  = (data.transaction.total_otherdeduct !== null) ? data.transaction.total_otherdeduct : 0.00;
				eccAmount 			  = (data.transaction.ecc_amount !== null) ? data.transaction.ecc_amount : 0.00;

				if(data.employeeinfo.pagibig_personal !== null){
					er_pagibig_share = data.employeeinfo.pagibig_contribution;
					er_pagibig_personal = data.employeeinfo.pagibig_personal;
					er_pagibig = (parseFloat(er_pagibig_share) + parseFloat(er_pagibig_personal));
				}else{
					er_pagibig = data.employeeinfo.pagibig_contribution;
				}

				$('#lbl_empname').text(fullname);
				$('#transaction_id').val(data.transaction.id);
				$('#benefit_transaction_id').val(data.transaction.id);
				$('#benefit_employee_id').val(data.transaction.employee_id);
				$('#deduction_transaction_id').val(data.transaction.id);
				$('#deduction_employee_id').val(data.transaction.employee_id);

				$('#salaryinfo_id').val(data.salaryinfo.id);
				$('#ecc_amount').val();
				$('#gsis_ee_share').val(commaSeparateNumber(parseFloat(gsis_ee_share).toFixed(2)));
				$('#gsis_er_share').val(commaSeparateNumber(parseFloat(er_gsis_share).toFixed(2)));
				$('#philhealth_ee_share').val(commaSeparateNumber(parseFloat(philhealth_ee_share).toFixed(2)));
				$('#philhealth_er_share').val(commaSeparateNumber(parseFloat(philhealth_er_share).toFixed(2)));
				$('#pagibig_ee_share').val(commaSeparateNumber(parseFloat(er_pagibig).toFixed(2)));
				$('#pagibig_er_share').val(commaSeparateNumber(parseFloat(er_pagibig_share).toFixed(2)));
				$('#pagibig2').val(commaSeparateNumber(parseFloat(pagibig2).toFixed(2)));
				$('#witholding_tax').val(commaSeparateNumber(parseFloat(taxContribution).toFixed(2)));
				$('#rate_regularday').val('125%');
				$('#rate_restday').val('150%');
				$('#rate_regularholiday').val('150%');
				$('#actual_workdays').val(totalWorkDays).trigger('keyup');
			}


			// GENERATE TR FOR DEDUCTION INFO TAB
			DeductionInfo(data);

			// GENERATE TR FOR LOAN INFO TAB
			tLoan = $('#tbl_loanTransact').DataTable();
			tLoan.clear().draw();
			$.each(data.loaninfo,function(k,v){
				tLoan.row.add( [
		        	data.loaninfo[k].loans.name,
		        	data.loaninfo[k].loan_totalamount,
					data.loaninfo[k].loan_totalbalance,
					data.loaninfo[k].loan_amortization,
					data.loaninfo[k].loan_date_started,
					data.loaninfo[k].loan_date_end,
		        ]).draw( false );
		        tLoan.rows(k).nodes().to$().attr("data-id", v.id);
		        tLoan.rows(k).nodes().to$().attr("data-employeeid", v.employee_id);
		        tLoan.rows(k).nodes().to$().attr("data-loanamount", v.loan_totalamount);
		        tLoan.rows(k).nodes().to$().attr("data-loanbalance", v.loan_totalbalance);
		        tLoan.rows(k).nodes().to$().attr("data-amortization", v.loan_amortization);
		        tLoan.rows(k).nodes().to$().attr("data-datestart", v.loan_date_started);
		        tLoan.rows(k).nodes().to$().attr("data-dateend", v.loan_date_end);
		        tLoan.rows(k).nodes().to$().attr("data-loanid", v.loan_id);
		        tLoan.rows(k).nodes().to$().attr("data-payperiod", v.loan_pay_period);
		        tLoan.rows(k).nodes().to$().attr("data-dategranted", v.loan_date_granted);
		        tLoan.rows(k).nodes().to$().attr("data-dateterminated", v.loan_date_terminated);
		        tLoan.rows(k).nodes().to$().attr("data-btnnew", "newLoan");
		        tLoan.rows(k).nodes().to$().attr("data-btnsave", "saveLoan");
		        tLoan.rows(k).nodes().to$().attr("data-btnedit", "editLoan");
		        tLoan.rows(k).nodes().to$().attr("data-btncancel", "cancelLoan");
			});

			// GENERATE TR FOR BENEFITS TAB
			BenefitInfo(data);

		}
	})
});

function DeductionInfo(data){
	tDeduct = $('#tbl_deductTransact').DataTable();
	tDeduct.clear().draw();
	$.each(data.deductioninfo,function(k,v){
		deduction_name 	 = (data.deductioninfo[k].deductions) ? data.deductioninfo[k].deductions.name : '';
		amount 			 = (v.amount) ? v.amount : 0;
		date_start 		 = (data.deductioninfo[k].deduct_date_start) ? data.deductioninfo[k].deduct_date_start : '';
		date_end 	 	 = (data.deductioninfo[k].deduct_date_end) ? data.deductioninfo[k].deduct_date_end : '';
		pay_period 		 = (data.deductioninfo[k].deduct_pay_period) ? data.deductioninfo[k].deduct_pay_period : '';
		tDeduct.row.add( [
        	deduction_name,
        	amount,
			date_start,
			date_end,
			pay_period,
        ]).draw( false );
        tDeduct.rows(k).nodes().to$().attr("data-id", v.id);
        tDeduct.rows(k).nodes().to$().attr("data-employeeid", v.employee_id);
        tDeduct.rows(k).nodes().to$().attr("data-deductionid", v.deduction_id);
        tDeduct.rows(k).nodes().to$().attr("data-amount", v.amount);
        tDeduct.rows(k).nodes().to$().attr("data-datestart", date_start);
        tDeduct.rows(k).nodes().to$().attr("data-dateend", date_end);
        tDeduct.rows(k).nodes().to$().attr("data-payperiod", pay_period);
        tDeduct.rows(k).nodes().to$().attr("data-btnnew", "newDeduct");
        tDeduct.rows(k).nodes().to$().attr("data-btnsave", "saveDeduct");
        tDeduct.rows(k).nodes().to$().attr("data-btnedit", "editDeduct");
        tDeduct.rows(k).nodes().to$().attr("data-btncancel", "cancelDeduct");
	});
}

function BenefitInfo(data){
	tBenefit = $('#tbl_benefitTransact').DataTable();
	tBenefit.clear().draw();

	benefit_name 	 = (data.benefitinfo.benefits) ? data.benefitinfo.benefits.name : '';
	amount 			 = (data.benefitinfo.amount) ? data.benefitinfo.amount : 0;
	effectivity_date = (data.benefitinfo.benefit_effectivity_date) ? data.benefitinfo.benefit_effectivity_date : '';
	description 	 = (data.benefitinfo.benefit_description) ? data.benefitinfo.benefit_description : '';
	pay_period 		 = (data.benefitinfo.benefit_pay_period) ? data.benefitinfo.benefit_pay_period : '';

	amount = (amount !== 0) ? commaSeparateNumber(parseFloat(amount).toFixed(2)) : 0;

	tBenefit.row.add( [
    	effectivity_date,
    	benefit_name,
		description,
		amount,
		pay_period,
    ]).draw( false );
    tBenefit.rows(0).nodes().to$().attr("data-id", data.benefitinfo.id);
    tBenefit.rows(0).nodes().to$().attr("data-employeeid", data.benefitinfo.employee_id);
    tBenefit.rows(0).nodes().to$().attr("data-benefit_id", data.benefitinfo.benefit_id);
    tBenefit.rows(0).nodes().to$().attr("data-benefit_info_id", data.benefitinfo.benefit_info_id);
    tBenefit.rows(0).nodes().to$().attr("data-description", data.benefitinfo.benefit_description);
    tBenefit.rows(0).nodes().to$().attr("data-btnnew", "newBenefit");
    tBenefit.rows(0).nodes().to$().attr("data-btnsave", "saveBenefit");
    tBenefit.rows(0).nodes().to$().attr("data-btnedit", "editBenefit");
    tBenefit.rows(0).nodes().to$().attr("data-btncancel", "cancelBenefit");
}


_checkpayroll = ""
$('input[type=radio][name=chk_wpayroll]').change(function() {
 	if(!_Year){
 		swal({
			  title: 'Select year and month first',
			  type: "warning",
			  showCancelButton: false,
			  confirmButtonClass: "btn-warning",
			  confirmButtonText: "OK",
			  closeOnConfirm: false
		})
		$(this).prop('checked',false);
 	}else{
		if (this.value == 'wpayroll') {
			$('#process_payroll').prop('disabled',true);
			_bool = true;
        	_checkpayroll = 'wpayroll';
        	_listId = [];
        }
        else if (this.value == 'wopayroll') {
        	$('#process_payroll').prop('disabled',false);
            _checkpayroll = 'wopayroll';
            _listId = [];
        }
        $('.btnfilter').trigger('click');
 	}
});

$(document).on('click','#delete_payroll',function(){
	if(_listId.length == 0){
		swal("Select employee first!", "", "warning");
	}else{
		swal({
			title: "Delete Payroll?",
			type: "warning",
			showCancelButton: true,
			confirmButtonClass: "btn-warning",
			confirmButtonText: "Yes",
			cancelButtonText: "No",
		}).then(function(isConfirm){
			if(isConfirm.value == true){
				$.deletePayroll();
			}else{
				return false;
			}
		});
	}
});

$.deletePayroll = function(){
	$.ajax({
		url:base_url+module_prefix+module+'/deletePayroll',
		data:{'empid':_listId,'year':_Year,'month':_Month,'_token':"{{ csrf_token() }}"},
		type:'post',
		beforeSend:function(){
			$('#delete_payroll').html('<i class="fa fa-spinner fa-spin"></i> Deleting').prop('disabled',true);
		},
		success:function(response){
			par = JSON.parse(response);
			if(par.status){
				swal({
				  title: par.response,
				  type: "success",
				  showCancelButton: false,
				  confirmButtonClass: "btn-success",
				  confirmButtonText: "OK",
				  closeOnConfirm: false
			})
			_listId = [];
			$('.btnfilter').trigger('click');
			$('#delete_payroll').html('<i class="fas fa-minus-circle"></i> Delete').prop('disabled',false);
			clear_form_elements('myForm');
			tDeduct.clear().draw();
			tLoan.clear().draw();
			tBenefit.clear().draw();
			$("#_salaryrate").text(0.00)
			$("#totalamount").text(0.00)
			$("#totalOT").text(0.00)
			$("#totalGrossPay").text(0.00)
			$("#grosspay").text(0.00)
			$("#grosstaxablepay").text(0.00)
			$("#netpay").text(0.00)
			$('#transaction_id').val('');
			$('attendance_id').val('');
			}
		}
	});
}

var timer;
$(document).on('click','.btnfilter',function(){
	// $('input.search').addClass('searchSpinner');
	tools  	  = $('#tools-form').serialize()
	year 	  = $('#select_year :selected').val();
	month 	  = $('#select_month :selected').val();
	category  = $('#select_searchvalue :selected').val();
	empstatus = $('#emp_status :selected').val();
	searchby  = $('#searchby :selected').val();
	subperiod = $('#select_subperiod :selected').val();
	period	  = $('#select_period :selected').val();

	clearTimeout(timer);
	timer = setTimeout(
		function(){
			$.ajax({
			   type: "GET",
			   url: base_url+module_prefix+module+'/show',
			   data: {'category':category,'empstatus':empstatus,'searchby':searchby,'year':year,'month':month,'subperiod':subperiod,'period':period,'checkpayroll':_checkpayroll },
			   beforeSend:function(){
			   		$('#loading').removeClass('hidden');
			   },
			   complete:function(){
			   		$('#loading').addClass('hidden');
			   },
			   success: function(res){
			   	// console.log(res);
			      $(".namelist").html(res);
			   }
			});
		},500);
});


$(document).on('keyup','._searchname',function(){

	clearTimeout(timer);
	timer = setTimeout(
		function(){
			$.ajax({
			   type: "GET",
			   url: base_url+module_prefix+module+'/show',
			   data: {
			   	"q":$('._searchname').val(),
			   	'check_payroll':_checkpayroll
			},
			   success: function(res){
			      $(".namelist").html(res);
			   }
			});
		},500);
});




$('.btn_new').on('click',function(){
	// $('#transaction_id').val('');
	btnnew = $(this).data('btnnew');
	btnsave = $(this).data('btnsave');
	btncancel = $(this).data('btncancel');
	$('.'+btnnew+' :input').attr("disabled",false);
	$('.'+btnnew).attr('disabled',false);
	$('#'+btnnew).addClass('hidden');
	$('#'+btnsave).removeClass('hidden');
	$('#'+btncancel).removeClass('hidden');
});

$('.btn_edit').on('click',function(){
	btnnew = $(this).data('btnnew');
	btnsave = $(this).data('btnsave');
	btncancel = $(this).data('btncancel');
	btnedit = $(this).data('btnedit');
	$('.'+btnnew+' :input').attr("disabled",false);
	$('.'+btnnew).attr('disabled',false);
	$('#'+btnnew).addClass('hidden');
	$('#'+btnedit).addClass('hidden');
	$('#'+btnsave).removeClass('hidden');
	$('#'+btncancel).removeClass('hidden');

	editClicked = true;
});

$('.btn_cancel').on('click',function(){
	$('#transaction_id').val('');
	btnnew = $(this).data('btnnew');
	btnsave = $(this).data('btnsave');
	btncancel = $(this).data('btncancel');
	btnedit = $(this).data('btnedit');

	$('.'+btnnew+' :input').attr("disabled",true);
	$('.'+btnnew).attr('disabled',true);
	$('#'+btnedit).removeClass('hidden');
	// $('#'+btnedit).addClass('hidden');
	$('#'+btnsave).addClass('hidden');
	$('#'+btncancel).addClass('hidden');
	clear_form_elements('nonplantilla');
	$('.error-msg').remove();

	$('#newBenefitInfo').removeClass('hidden')
	$('#editBenefitInfo').addClass('hidden')
	$('#newDeductionInfo').removeClass('hidden')
	$('#editDeductionInfo').addClass('hidden')

	editClicked = false;

});

$('.update_payroll').on('click',function(){
	if(_listId.length == 0){
		swal("Select employee first!", "", "warning");
	}else{
		swal({
			title: "Update Payroll?",
			type: "warning",
			showCancelButton: true,
			confirmButtonClass: "btn-warning",
			confirmButtonText: "Yes",
			cancelButtonText: "No",
		}).then(function(isConfirm){
			if(isConfirm.value == true){
				$.processPayroll();
			}else{
				return false;
			}
		});
	}
});

/*serialize All form ON SUBMIT*/
$(document).off('click',".submitme").on('click',".submitme",function(){
		btn = $(this);
		form = $(this).data('form');
console.log(form);
		$("#"+form).ajaxForm({
			beforeSend:function(){

			},
			success:function(data){
				par  =  JSON.parse(data);

				if(par.status){

					swal({  title: par.response,
							text: '',
							type: "success",
							icon: 'success',

						}).then(function(){

							clear_form_elements('myform')
							clear_form_elements('myformDeduction')
							$('.newBenefitInfo :input').prop('disabled',true);
							$('#cancelBenefitInfo').addClass('hidden');
							$('#saveBenefitInfo').addClass('hidden');
							$('#editBenefitInfo').addClass('hidden');
							$('#newBenefitInfo').removeClass('hidden');
							$('.newDeductionInfo :input').prop('disabled',true);
							$('#cancelDeductionInfo').addClass('hidden');
							$('#saveDeductionInfo').addClass('hidden');
							$('#editDeductionInfo').addClass('hidden');
							$('#newDeductionInfo').removeClass('hidden');
							console.log(par)
							BenefitInfo(par.transaction)
							DeductionInfo(par.transaction)
						});
				}else{

					swal({  title: par.response,
							text: '',
							type: "error",
							icon: 'error',

						});

				}

				btn.button('reset');
			},
			error:function(data){
				$error = data.responseJSON;
				/*reset popover*/
				$('input[type="text"], select').popover('destroy');

				/*add popover*/
				block = 0;
				$(".error-msg").remove();
				$.each($error,function(k,v){
					var messages = v.join(', ');
					msg = '<div class="error-msg err-'+k+'" style="color:red;"><i class="fa fa-exclamation-triangle" style="color:rgb(255, 184, 0);"></i> '+messages+'</div>';
					$('input[name="'+k+'"], textarea[name="'+k+'"], select[name="'+k+'"]').after(msg).attr('data-content',messages);
					if(block == 0){
						$('html, body').animate({
					        scrollTop: $('.err-'+k).offset().top - 250
					    }, 500);
					    block++;
					}
				})
				$('.saving').replaceWith(btn);
			},
			always:function(){
				setTimeout(function(){
						$('.saving').replaceWith(btn);
					},300)
			}
		}).submit();

});


});




</script>
@endsection