@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-7 text-center">
            <img src="{{ url('images/pidsLogo.png') }}" style="position: relative;left: -70px; margin-top: 30px;width: 530px;">
            <h4 style="font-weight: bold;position: relative; right: 75px;color: #ad5826;">PHILIPPINE INSTITUTE FOR DEVELOPMENT STUDIES</h4>
        </div>
        <div class="col-md-5" style="margin-top: 150px;">
            <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                {{ csrf_field() }}

                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                    <div class="col-md-6">
                        <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                    <label for="password" class="col-md-4 control-label">Password</label>

                    <div class="col-md-6">
                        <input id="password" type="password" class="form-control" name="password" required>

                        @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-8">
                        <div class="checkbox text-right">
                        </div>
                    </div>
                    <div class="col-md-4 text-left" style="padding-left: 0px;">
                        <button type="submit" class="btn btn-primary">
                            Login
                        </button>
                    </div>
                </div>
<!--
                <div class="form-group">
                    <div class="col-md-8 col-md-offset-8">
                        <button type="submit" class="btn btn-primary">
                            Login
                        </button>

                        <a class="btn btn-link" href="{{ route('password.request') }}">
                            Forgot Your Password?
                        </a>
                    </div>
                </div> -->
            </form>
        </div>
    </div>
</div>
@endsection
