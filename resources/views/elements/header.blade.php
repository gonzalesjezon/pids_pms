<?php
(strtolower($module) == 'dashboards') ? $dashboards = 'elActive' : $dashboards = '';
?>
<nav class="navbar navbar-fixed-top one-edge-shadow" style="border-bottom:5px solid var(--theme-bg);">
	<div class="container-fluid" style="background:#fdfdfd;">
		<div class="row">
			<div class="col-sm-2" style="height:60px;">
				<img src="{{url('/images/pidsLogoSmall.png')}}" style="height:60px;">
			</div>
			<div class="col-sm-6 text-center CompanyName" style="vertical-align:middle;line-height:60px;font-size:16pt;">
				<span style="color:var(--MainColor1);">Payroll Management System</span>
			</div>
			<div class="col-sm-4" style="height:60px;">
				<div class="col-sm-2 text-right">
					<img src="{{url('/images/nopic.png')}}" class="img-rounded" style="height:50px;margin-top:5px;">
				</div>
				<div class="col-sm-10">
					<div>&nbsp;</div>
					<div>{{ Auth::User()->UserName }}</div>
					<div class="dropdown">
						<a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" >
					    System Config
					  </a>
					  <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
					    	<div class="dropdown-item">
								<a  href="{{ route('logout') }}"
								onclick="event.preventDefault();
								document.getElementById('logout-form').submit();" style="padding-left: 10px;">
									Logout
								</a>
								<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
									{{ csrf_field() }}
								</form>
							</div>
					  </div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="header-nav">
		<div class="col-md-12">

			<div class="col-md-6">
				<div class="col-xs-1 topmenu-style2 txt-center {{ $dashboards }}">
					<a href="{{ url('dashboards')}}" class="  topmenu-style " >DASHBOARD</a>
				</div>
			</div>

			<div class="col-md-6">
				<div class="toprightmenu">
					<ul class="list-inline list-item1">
						<li><a href="#">About</a></li>
						<li><a href="#">Contact</a></li>
						<li><a href="#">Help</a></li>
				       </ul>
				  </div>
			</div>
		</div>
	</div>
</nav>
<div style="height: 18px;"></div>

