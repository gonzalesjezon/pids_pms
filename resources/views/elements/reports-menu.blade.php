<?php
(strtolower($module) == 'generalpayroll') ? $generalpayroll = 'active' : $generalpayroll = '';
(strtolower($module) == 'payslips') ? $payslips = 'active' : $payslips = '';
(strtolower($module) == 'summaryadjustments') ? $summaryadjustments = 'active' : $summaryadjustments = '';
(strtolower($module) == 'weeklypayroll') ? $weeklypayroll = 'active' : $weeklypayroll = '';
(strtolower($module) == 'authoritydebit') ? $authoritydebit = 'active' : $authoritydebit = '';
(strtolower($module) == 'lwopadjustments') ? $lwopadjustments = 'active' : $lwopadjustments = '';
(strtolower($module) == 'stepincrementreports') ? $stepincrementreports = 'active' : $stepincrementreports = '';
(strtolower($module) == 'ratageneralpayroll') ? $ratageneralpayroll = 'active' : $ratageneralpayroll = '';
(strtolower($module) == 'hdmfreports') ? $hdmfreports = 'active' : $hdmfreports = '';
(strtolower($module) == 'hdmftworeports') ? $hdmftworeports = 'active' : $hdmftworeports = '';
(strtolower($module) == 'hdmfmplreports') ? $hdmfmplreports = 'active' : $hdmfmplreports = '';
(strtolower($module) == 'philhealthreports') ? $philhealthreports = 'active' : $philhealthreports = '';
(strtolower($module) == 'providentfundreports') ? $providentfundreports = 'active' : $providentfundreports = '';
(strtolower($module) == 'nhfmcreports') ? $nhfmcreports = 'active' : $nhfmcreports = '';
?>
<style type="text/css">
    nav>ul>li>a{
        font-size: 12px;
    }
</style>
<nav class="nav-sidebar">
    <ul class="nav">
        <li  class="{{ $generalpayroll }}">
        	<a href="{{ url('payrolls/reports/generalpayroll')  }}"  >
				<span>GENERAL PAYROLL</span>
			</a>
        </li>
        <li class="{{ $payslips }}" >
            <a href="{{ url('payrolls/reports/payslips')  }}"  >
                <span>PAYSLIP</span>
            </a>
        </li>
        <li class="{{ $summaryadjustments }}" >
            <a href="{{ url('payrolls/reports/summaryadjustments')  }}"  >
                <span>SUMMARY ADJUSTMENTS</span>
            </a>
        </li>
        <li class="{{ $weeklypayroll }}" >
            <a href="{{ url('payrolls/reports/weeklypayroll')  }}"  >
                <span>WEEKLY PAYROLL</span>
            </a>
        </li>
        <li class="{{ $authoritydebit }}" >
            <a href="{{ url('payrolls/reports/authoritydebit')  }}"  >
                <span>AUTHORITY DEBIT</span>
            </a>
        </li>
        <li class="{{ $lwopadjustments }}" >
            <a href="{{ url('payrolls/reports/lwopadjustments')  }}"  >
                <span>LWOP SALARY ADJUSTMENTS</span>
            </a>
        </li>
        <li class="{{ $stepincrementreports }}" >
            <a href="{{ url('payrolls/reports/stepincrementreports')  }}"  >
                <span>STEP INCREMENTS</span>
            </a>
        </li>
        <li class="{{ $ratageneralpayroll }}" >
            <a href="{{ url('payrolls/reports/ratageneralpayroll')  }}"  >
                <span>RATA GENERAL PAYROLL</span>
            </a>
        </li>
        <li class="{{ $hdmfreports }}" >
            <a href="{{ url('payrolls/reports/hdmfreports')  }}"  >
                <span>HDMF REPORTS</span>
            </a>
        </li>
        <li class="{{ $hdmftworeports }}" >
            <a href="{{ url('payrolls/reports/hdmftworeports')  }}"  >
                <span>HDMF II REPORTS</span>
            </a>
        </li>
        <li class="{{ $hdmfmplreports }}" >
            <a href="{{ url('payrolls/reports/hdmfmplreports')  }}"  >
                <span>HDMF MPL REPORTS</span>
            </a>
        </li>
        <li class="{{ $philhealthreports }}" >
            <a href="{{ url('payrolls/reports/philhealthreports')  }}"  >
                <span>PHILHEALTH REPORTS</span>
            </a>
        </li>
        <li class="{{ $providentfundreports }}" >
            <a href="{{ url('payrolls/reports/providentfundreports')  }}"  >
                <span>PROVIDENT FUND REPORTS</span>
            </a>
        </li>
         <li class="{{ $nhfmcreports }}" >
            <a href="{{ url('payrolls/reports/nhfmcreports')  }}"  >
                <span>NHFMC REPORTS</span>
            </a>
        </li>
    </ul>
</nav>
<div style="height: 30px;"></div>



