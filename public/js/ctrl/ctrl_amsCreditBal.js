$(document).ready(function() {
   var credit = $("[name='char_NameCredits']").val();
   if (credit != "") {
      $("#LeaveScreen, #OTScreen").hide();
      if (drpVal == "OT") {
         $("#OTScreen").show(150);
      }
      if (
            drpVal == "SL" ||
            drpVal == "VL"
         ) {
         $("#LeaveScreen").show(150);
         if (drpVal == "SL") {
            $("#ForceLeave").prop("disabled",true);
         } else {
            $("#ForceLeave").prop("disabled",false);
         }
      }
   }
   $("[name='char_NameCredits']").change(function (){
      var drpVal = $(this).val();
      $("#LeaveScreen, #OTScreen").hide();
      if (drpVal == "OT") {
         $("#OTScreen").show(150);
      }
      if (
            drpVal == "SL" ||
            drpVal == "VL"
         ) {
         $("#LeaveScreen").show(150);
         if (drpVal == "SL") {
            $("#ForceLeave").prop("disabled",true);
         } else {
            $("#ForceLeave").prop("disabled",false);
         }
      }
   });
   $("#LocSAVE").click(function () {
      $("[name='char_NameCredits']").prop("disabled",false);
      //if (saveProceed() == 0) {
      //alert($("[name='hRefId']").length);
         $.ajax({
            url: "postData.e2e.php",
            type: "POST",
            data: new FormData($("[name='xForm']")[0]),
            success : function(responseTxt){
               if (responseTxt.trim() != "") {
                  alert(responseTxt);
               } else {
                  var param = setAddURL();
                  if ($("[name='txtRefId']").length) {
                     param += "&txtLName=" + $("[name='txtLName']").val();
                     param += "&txtFName=" + $("[name='txtFName']").val();
                     param += "&txtMidName=" + $("[name='txtMidName']").val();
                     param += "&txtRefId=" + $("[name='txtRefId']").val();
                  }
                  gotoscrn ($("#hProg").val(), param);
               }
            },
            enctype: 'multipart/form-data',
            processData: false,
            contentType: false,
            cache: false
         });
      //}
   });
   $(function() {
      $("#LeaveScreen, #OTScreen").hide();
   });
});

function afterNewSave(LastRefId) {
   alert("New Record " + LastRefId + " Succesfully Inserted");
   return false;
}
function afterDelete() {
   alert("Record Succesfully Deleted");
   //gotoscrn ("amsFileSetup", setAddURL());
   return false;
}
function afterEditSave(RefId){
   alert("Record " + RefId + " Succesfully Updated");
   //gotoscrn ("amsFileSetup", setAddURL());
   return false;
}
