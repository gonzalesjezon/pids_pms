$(document).off('click',".submit4").on('click',".submit4",function(){
		btn = $(this);
		$("#form4").ajaxForm({
			beforeSend:function(){
				
			},
			success:function(data){
				par  =  JSON.parse(data);
				if(par.status){	

					swal({  title: par.response,   
							text: '',   
							type: "success",   
							icon: 'success',
							
						}).then(function(){

							window.location.href = base_url+module_prefix+module;
							
						});

				}else{

					swal({  title: par.response,   
							text: '',   
							type: "error",   
							icon: 'error',
							
						});

				}

				btn.button('reset');
			},
			error:function(data){
				$error = data.responseJSON;
				/*reset popover*/	
				$('input[type="text"], select').popover('destroy');

				/*add popover*/
				block = 0;
				$(".error-msg").remove();
				$.each($error,function(k,v){
					var messages = v.join(', ');
					msg = '<div class="error-msg err-'+k+'" style="color:red;"><i class="fa fa-exclamation-triangle" style="color:rgb(255, 184, 0);"></i> '+messages+'</div>';
					$('input[name="'+k+'"], textarea[name="'+k+'"], select[name="'+k+'"]').after(msg).attr('data-content',messages);
					if(block == 0){
						$('html, body').animate({
					        scrollTop: $('.err-'+k).offset().top - 250
					    }, 500);
					    block++;
					}
				})
				$('.saving').replaceWith(btn);     
			},
			always:function(){
				setTimeout(function(){
						$('.saving').replaceWith(btn);     
					},300) 
			}
		}).submit();

});