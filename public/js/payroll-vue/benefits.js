class Errors{

    constructor(){
        this.errors = {};
    }

    get(field){
        if(this.errors[field]){
           return this.errors[field][0];
        }
    }

    has(field){
        
        return this.errors.hasOwnProperty(field);
    }

    record(errors){
        this.errors = errors;
    }

    clear(field){

      if(field) {
        delete this.errors[field];

        return;
      }

      this.errors = {};
       
    }

}

class Form{

    constructor(data){

        this.originalData = data;

        for(let field in data){
            this[field] = data[field];
        }

        this.errors = new Errors();

    }

    data(){

        let data = {};

        for(let property in this.originalData){
            data[property] = this[property];
        }

        return data;
    }

    reset(){

        for(let field in this.originalData){
            this[field] = '';
        }

    }

    // post(url){
    //     return this.submit('POST',url);
    // }

    submit(requestType, url){

        axios[requestType](url,this.data())
                .then(this.onSuccess.bind(this))
                .catch(this.onFail.bind(this))
                
    }


    onSuccess(response){

        alert(response.data.message);

        this.errors.clear();
        this.reset();
    }

    onFail(error){
        this.errors.record(error.response.data);
    }

}


var app = new Vue({

    el: '#app',

    data:{
        form: new Form({
            code: '',
            name: '',
            de_minimis_type: '',
            computation_type: '',
            amount: '',
            payroll_group: '',
            tax_type: '',
            itr_classification: '',
            include_computation: '',
            alphalist_classification: ''

        }),

        search: '',
        items: [],
        loading: false,
        order: 1,
        filterValue: '',
        ccn: null,
        currentPage: 0,
        itemsPerPage: 2,
        resultCount: 0,

    },
    mounted(){

            this.getRecords();
    },
    computed: {
        totalPages: function() {
          console.log(Math.ceil(this.resultCount / this.itemsPerPage) + " totalPages");
          return Math.ceil(this.resultCount / this.itemsPerPage);

        },

        // filteredAndSortedData() {
        //     // Apply filter first
        //     console.log(this.items);
        //     let result = this.items;
        //     if (this.filterValue) {
        //         result = result.filter(item => item.name.includes(this.filterValue));
        //     }

        //     // Sort the remaining values
        //     let ascDesc = this.sortAsc ? 1 : -1;
        //     return result.sort((a, b) => ascDesc * a.name.localeCompare(b.name));
        // }
        
        filteredItems() {
              return this.items.filter(item => {
                 return item.code.toLowerCase().indexOf(this.search.toLowerCase()) > -1
              })
        },

        
  
    },
    methods:{
        getRecords: function(){
            axios.get('benefits/show')
                .then(function(response){
                    app.items = response.data.data;
                })
                .catch(function(error){
                    console.log(error);
                });
        },
        
        onSubmit(){
            this.form.submit("post","benefits");
        },

        resetForm(){
            this.form.reset();
            this.form.errors.clear();
        },
        refresh: function() {
          this.$children[0].refresh();
        },

        sortvia: function (sortparam, order) {
            this.order = this.order * -1;
            this.sortparam = sortparam;
        },

        setPage: function(pageNumber) {
          this.currentPage = pageNumber;
          console.log(pageNumber);

        },



    },

    filters: {
        paginate: function(list) {

            this.resultCount = list.length;

            if (this.currentPage >= this.totalPages) {
              this.currentPage = Math.max(0, this.totalPages - 1);
            }

            var index = this.currentPage * this.itemsPerPage;

            return list.slice(index, index + this.itemsPerPage);
        }
        
    },

   
});



