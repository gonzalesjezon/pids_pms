function compute_grosspay(basicpay,allowance){
	basicpay = (basicpay) ? basicpay : 0;
	allowance = (allowance) ? allowance : 0;

	grosspay = (parseFloat(basicpay) + parseFloat(allowance));

	return grosspay;
}

function compute_total(tWorkingDays,tAbsence,tUndertime,tTardiness){
	tWorkingDays = (tWorkingDays) ? tWorkingDays : 0;
	tAbsence 	 = (tAbsence) ? tAbsence : 0;
	tUndertime 	 = (tUndertime) ? tUndertime : 0;
	tTardiness 	 = (tTardiness) ? tTardiness : 0;

	total = (parseFloat(tWorkingDays) - (parseFloat(tAbsence) + parseFloat(tUndertime) + parseFloat(tTardiness)));
	return total;
}

function compute_netpay(taxAmount,tLoan,tOtherDeductions,gTaxable,allowances){
	taxAmount 			  = (taxAmount) ? taxAmount : 0;
	tLoan 	  			  = (tLoan) ? tLoan : 0;
	tOtherDeductions 	  = (tOtherDeductions) ? tOtherDeductions : 0;
	gTaxable 	  		  = (gTaxable) ? gTaxable : 0;
	allowances 			  = (allowances) ? allowances : 0;
	if(gTaxable){

		netPay = ((parseFloat(gTaxable) + parseFloat(allowances)) - (parseFloat(taxAmount) + parseFloat(tLoan) + parseFloat(tOtherDeductions)));
	}else{
		netPay = 0;
	}

	return netPay;

}

function compute_totalworkdays(actualworkdays,adjustworkdays){
	totalworkdays = (actualworkdays + adjustworkdays);
	return totalworkdays;
}

function compute_workingdays(totalPay, actualDays){
	payPerDay = (totalPay * actualDays);
	return payPerDay;

}

function compute_basicpay(first,second){
	first = (first) ? first : 0;
	second = (second) ? second : 0;
	basicpay = (first + second);
	return basicpay;
}
function compute_lwop(first,second){
	first = (first) ? first : 0;
	second = (second) ? second : 0;
	lwop = (first + second);
	return lwop;
}
function compute_totalundertime(first,second){
	first = (first) ? first : 0;
	second = (second) ? second : 0;
	totalundertime = (first + second);
	return totalundertime;
}

function compute_totaltardines(first,second){
	first = (first) ? first : 0;
	second = (second) ? second : 0;
	total_tardines = (first + second);
	return total_tardines;
}
function compute_rate_per_minute(payPerHour, ratePerMinute){
	payPerMinute = ((payPerHour / 60) * (ratePerMinute));
	return payPerMinute;
}
function compute_basic_net(tBasic,tLwop,tUndertime,tTardiness){
	tBasic = (tBasic) ? tBasic : 0;
	tLwop 	 = (tLwop) ? tLwop : 0;
	tUndertime 	 = (tUndertime) ? tUndertime : 0;
	tTardiness 	 = (tTardiness) ? tTardiness : 0;

	total = (parseFloat(tBasic) - (parseFloat(tLwop) + parseFloat(tUndertime) + parseFloat(tTardiness)));
	return total;
}
function compute_cos_net_pay(gTaxable,taxAmount){
	gTaxable 		= (gTaxable) ? gTaxable : 0;
	taxAmount 	= (taxAmount) ? taxAmount : 0;

	total = (parseFloat(gTaxable) - parseFloat(taxAmount));
	return total;

}
function compute_gross_taxable(basicnet,totalContribution){
	basicnet = (basicnet) ? basicnet : 0;
	totalContribution 	 = (totalContribution) ? totalContribution : 0;
	grossTaxable = (parseFloat(basicnet) - parseFloat(totalContribution));
	return grossTaxable;
}