
(function(){
	$.ajaxSetup({
	    headers: {
	        // 'X-XSRF-TOKEN': $('input[name="_token"]').val()
	        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	    }
	});


	/*number only input on selected number*/
    $('input.numberinput').on('keypress', function (e) {
        return !(e.which != 8 && e.which != 0 &&
            (e.which < 48 || e.which > 57) && e.which != 46);
    });


})();
var _monthlyRate;
var btn;
/*serialize All form ON SUBMIT*/
$(document).off('click',".submit").on('click',".submit",function(){
		btn = $(this);

		$("#form").ajaxForm({
			beforeSend:function(){

			},
			success:function(data){
				par  =  JSON.parse(data);
				if(par.status){

					swal({  title: par.response,
							text: '',
							type: "success",
							icon: 'success',

						}).then(function(){

							window.location.href = base_url+module_prefix+module;
							// clear_form_elements('myform')

						});

				}else{

					swal({  title: par.response,
							text: '',
							type: "error",
							icon: 'error',

						});

				}

				btn.button('reset');
			},
			error:function(data){
				$error = data.responseJSON;
				/*reset popover*/
				$('input[type="text"], select').popover('destroy');

				/*add popover*/
				block = 0;
				$(".error-msg").remove();
				$.each($error,function(k,v){
					var messages = v.join(', ');
					msg = '<div class="error-msg err-'+k+'" style="color:red;"><i class="fa fa-exclamation-triangle" style="color:rgb(255, 184, 0);"></i> '+messages+'</div>';
					$('input[name="'+k+'"], textarea[name="'+k+'"], select[name="'+k+'"]').after(msg).attr('data-content',messages);
					if(block == 0){
						$('html, body').animate({
					        scrollTop: $('.err-'+k).offset().top - 250
					    }, 500);
					    block++;
					}
				})
				$('.saving').replaceWith(btn);
			},
			always:function(){
				setTimeout(function(){
						$('.saving').replaceWith(btn);
					},300)
			}
		}).submit();

});

function PrintElem(elem)
{
    var mywindow = window.open('', 'PRINT', 'height=700,width=600');

    mywindow.document.write('<html><head><title>' + document.title  + '</title>');
    mywindow.document.write('</head><body >');
    mywindow.document.write('<h1>' + document.title  + '</h1>');
    mywindow.document.write($('#'+elem).html());
    mywindow.document.write('</body></html>');

    mywindow.document.close(); // necessary for IE >= 10
    mywindow.focus(); // necessary for IE >= 10*/

    mywindow.print();
    mywindow.close();

    return true;
}

// Convert numbers to words
// copyright 25th July 2006, by Stephen Chapman http://javascript.about.com
// permission to use this Javascript on your web page is granted
// provided that all of the code (including this copyright notice) is
// used exactly as shown (you can change the numbering system if you wish)

// American Numbering System
var th = ['','thousand','million', 'billion','trillion'];
// uncomment this line for English Number System
// var th = ['','thousand','million', 'milliard','billion'];

var dg = ['zero','one','two','three','four', 'five','six','seven','eight','nine'];
var tn = ['ten','eleven','twelve','thirteen', 'fourteen','fifteen','sixteen', 'seventeen','eighteen','nineteen'];
var tw = ['twenty','thirty','forty','fifty', 'sixty','seventy','eighty','ninety'];
function toWords(s){
	s = s.toString();
	s = s.replace(/[\, ]/g,'');
	if (s != parseFloat(s))
		return 'not a number';
		var x = s.indexOf('.');
		if (x == -1) x = s.length;
		if (x > 15)
			return 'too big';
		var n = s.split('');
		var str = '';
		var sk = 0;
		for (var i=0; i < x; i++) {
			if ((x-i)%3==2) {
				if (n[i] == '1') {
					str += tn[Number(n[i+1])] + ' ';
					i++; sk=1;
				} else if (n[i]!=0) {
					str += tw[n[i]-2] + ' ';
					sk=1;
				}
			} else if (n[i]!=0) {
				str += dg[n[i]] +' ';
				if ((x-i)%3==0)
					str += 'hundred ';
				sk=1;
			} if ((x-i)%3==1) {
				if (sk) str += th[(x-i-1)/3] + ' ';
				sk=0;
			}
		}
		// if (x != s.length) {
		// 	var y = s.length;
		// 	str += 'point ';
		// 	for (var i=x+1; i<y; i++)
		// 		str += dg[n[i]] +' ';
		// }
		return str.replace(/\s+/g,' ');
}

function daysInMonth(month, year) {
    return new Date(year, month, 0).getDate();
}





/*edit*/
$(document).on('click','.edit',function(){
	action = $(this).data('action');
	path = base_url+action;
	window.location.href = path;
});



/*keycode delete*/
$(document).on('keydown',function(e){
	if(e.keyCode == 46){
		$('.delete').trigger('click');
	}
})


/*single delete*/
$(document).on('click','.single-delete',function(){
	url = base_url+module_prefix+module+'/destroy';
	self =$(this)
	idArr = [];
	idArr.push($(this).data('id'));

	if(confirm('You are about to delete a record!')){
		$.post(url,{data:idArr},function(data){
			console.log(data)
			if($.trim(data) == 'deleted'){
				self.parent().closest('tr').remove();
				// self.parent().removeClass('action-buttons')
				// self.parent().addClass('undo-button').html('<a class="btn btn-warning btn-xs undo"> <i class="fa fa-undo"></i> Undo</a>')
			}
		})
	}

})


/*restore*/
$(document).ready(function(){
	$(this).on('click','.restore',function(){
		url = base_url+module_prefix+module+'/restore';
		id = $(this).data('id');
		var self = $(this);
		if(confirm('You are about to restore this record. Click OK to Proceed')){
			$.post(url,{id:id},function(data){
				par = JSON.parse(data);
				if(par.status){
					self.parent().closest('tr').attr('style','')
				}
				alert(par.response)
			})
		}
	})
})


/*delete*/
$(document).on('click','.delete',function(){
	url = base_url+module_prefix+module+'/destroy';
	idArr = [];
	$('.chk-list').each(function(){
		if($(this).is(':checked')){

			//for items inside table
			if($(this).closest('tr').length > 0){
				id = $(this).closest('tr').attr('id');
				idArr.push(id);
			}

			//for items other than table, ex: div,span, or any container wrapper
			if($(this).closest('.entity-wrapper').length > 0){
				id = $(this).closest('.entity-wrapper').attr('id');
				idArr.push(id);
			}

		}
	})

	if(idArr.length > 0){
		if(confirm('You are about to delete the selected items')){
			$.post(url,{data:idArr},function(data){
				if($.trim(data) == 'deleted'){
					$.each(idArr,function(k,v){
						$("#"+v).remove();
					})
				}
			})
		}
	}

});
/*--------------------
| 	SEARCH
----------------------*/
var timer;
$(document).on('keyup','.search',function(){
	// $('input.search').addClass('searchSpinner');
	tools  = $('#tools-form').serialize()

	clearTimeout(timer);
	timer = setTimeout(
				function(){
					$.ajax({
					   type: "GET",
					   url: base_url+module_prefix+module+'/show',
					   data: {"q":$('.search').val(),'limit':$(".limit").val(), tools:tools },
					   success: function(res){
					      $(".sub-panel").html(res);
					      // $('input.search').removeClass('searchSpinner');
					   }
					});
				},500);
})


/*--------------------
| 	Paginate
----------------------*/

$(document).on('click','.pagination a',function(){
		$('.pagination li').removeClass('active');
		// $(this).parent().addClass('active');

		linkArr = $(this).attr("href").split('/');
		page = linkArr[linkArr.length - 1];
		console.log(page)
		// break;
		$.ajax({
		   type: "GET",
		   url: base_url+module_prefix+module+'/show'+page,
		   data: {"q":$('.search').val(),'limit':$(".limit").val() },
		   beforeSend:function(){
		      $(".sub-panel").html('<center>Please wait...</center>');
		   },
		   success: function(res){

		      $(".sub-panel").html(res);
		   }
		});
	return false;
});




/*--------------------
	filter
----------------------*/

$(document).ready(function(){
	$('.filter').change(function(){
		filter = $(this).val();
		$('.search').trigger('keyup');
	})
})

/*--------------------
	sort
----------------------*/

$(document).ready(function(){
	$('.sort').change(function(){
		filter = $(this).val();
		$('.search').trigger('keyup');
	})
})

/*--------------------
| 	LIMIT
----------------------*/
$(document).on('change','.limit',function(){
	clearTimeout(timer);
	timer = setTimeout(
				function(){
					$.ajax({
					   type: "GET",
					   url: base_url+module_prefix+module+'/datatable/1',
					   data: {"q":$('.search').val(),'limit':$(".limit").val() },
					   beforeSend:function(){
					      $(".sub-panel").html('<center>Please wait...</center>');
					   },
					   success: function(res){
					      $(".sub-panel").html(res);
					   }
					});
				},500);
})






/* file uploads
	@params
	gate  			-  Ex: #file-preview (photo thumbnail)
	targetCallback  -  Ex: #input
*/
$.uploadHandler = function(gate,targetCallback){

	$(gate).click(function(){
		$('.file-input').trigger('click');
	});

	$('.file-input').change(function(){
		$('#upload-form').ajaxForm(function(data){
			par = JSON.parse(data);
			if(par.status){
				$('#filename').val(par.response); // this will hold current filename
				$(targetCallback).val(par.response);
				$(gate).attr('src',base_url+'assets/uploads/photos_thumb/'+par.response);
			}else{ alert(par.response) }
		}).submit();
	})
}

$.uploadHandler('#file-preview','#photo');

$('.onlyNumber').keypress(function (event) {
    return isNumber(event, this)
});

function daysInMonth(month, year) {
    return new Date(year, month, 0).getDate();
}

function isNumber(evt, element) {

    var charCode = (evt.which) ? evt.which : event.keyCode

    if (
        (charCode != 45 || $(element).val().indexOf('-') != -1) &&      // “-” CHECK MINUS, AND ONLY ONE.
        (charCode != 46 || $(element).val().indexOf('.') != -1) &&      // “.” CHECK DOT, AND ONLY ONE.
        (charCode < 48 || charCode > 57))
        return false;

    return true;
}

function disabledInput(){
	$('.benefits-content :input').attr("disabled",true);
	$('.btn_new').on('click',function(){
		$('.benefits-content :input').attr("disabled",false);
		$('.btn_new').html('<i class="fa fa-save"></i> Save');
		$('.btn_edit').addClass('hidden');
	});
	$('.btn_edit').on('click',function(){
		$('.benefits-content :input').attr("disabled",false);
		$('.btn_edit').addClass('hidden');
		$('.btn_new').html('<i class="fa fa-save"></i> Save');
	});
	$('.btn_cancel').on('click',function(){
		$('.benefits-content :input').attr("disabled",true);
		$('.btn_new').html('<i class="fa fa-save"></i> New');
		$('.btn_edit').removeClass('hidden');
	});
}



function clear_form_elements(class_name) {
  $("."+class_name).find(':input').each(function() {

    switch(this.type) {
        case 'password':
        case 'text':
        case 'textarea':
        case 'file':
        case 'select-one':
        case 'select-multiple':
        case 'date':
        case 'number':
        case 'tel':
        case 'email':
            $(this).val('');
            break;
        case 'checkbox':
        case 'radio':
            this.checked = false;
            break;
    }
  });
}

$.commaSeparated  = function (x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function commaSeparateNumber(val){
        while (/(\d+)(\d{3})/.test(val.toString())){
          val = val.toString().replace(/(\d+)(\d{3})/, '$1'+','+'$2');
        }
        return val;
}



$(function(){
	$('.datepickeryear').datepicker({
	    changeMonth: false,
	    changeYear: true,
	    showButtonPanel: true,
	    yearRange: '1950:2013', // Optional Year Range
	    dateFormat: 'yy',
	    onClose: function(dateText, inst) {
	        var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
	        $(this).datepicker('setDate', new Date(year, 0, 1));
	    }});
	});

// TEXT FORMAT HH:MM:SS
$(".timeformat").focusin(function (evt) {
		id = $(this).attr('id');
        $(this).keypress(function () {
            content=$(this).val();
            content1 = content.replace(/\:/g, '');
            length=content1.length;

            if(((length % 2) == 0) && length < 10 && length > 1){
            	if(length != 6){
                	$('#'+id).val($('#'+id).val() + ':');
            	}
            }

        });
    });




