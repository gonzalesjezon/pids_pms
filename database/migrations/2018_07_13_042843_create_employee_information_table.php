<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeeInformationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('pms_employee_information')) {
            Schema::create('pms_employee_information', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('employee_id');
                $table->timestamp('hired_date')->nullable();
                $table->timestamp('assumption_date')->nullable();
                $table->timestamp('resigned_date')->nullable();
                $table->timestamp('rehired_date')->nullable();
                $table->timestamp('start_date')->nullable();
                $table->timestamp('end_date')->nullable();
                $table->integer('position_item_id');
                $table->integer('employee_status_id');
                $table->string('pay_period')->nullable();
                $table->integer('pay_rate_id');
                $table->integer('work_schedule_id');
                $table->integer('appointment_status_id');
                $table->integer('designation_id');
                $table->integer('created_by');
                $table->integer('updated_by')->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pms_employee_information');
    }
}
