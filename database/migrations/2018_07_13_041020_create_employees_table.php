<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('pms_employees')) {
            Schema::create('pms_employees', function (Blueprint $table) {
                $table->increments('id');                
                $table->integer('company_id')->nullable();
                $table->integer('branch_id')->nullable();
                $table->string('lastname')->nullable();
                $table->string('firstname')->nullable();
                $table->string('middlename')->nullable();
                $table->string('extension_name')->nullable();
                $table->string('nickname')->nullable();
                $table->string('contact_number')->nullable();
                $table->string('mobile_number')->nullable();
                $table->string('telephone_number')->nullable();
                $table->date('birthday')->nullable();
                $table->string('birth_place')->nullable();
                $table->string('email_address')->nullable();
                $table->string('gender')->nullable();
                $table->string('civil_status')->nullable();
                $table->integer('citizenship_id')->nullable();
                $table->integer('citizenship_country_id')->nullable();
                $table->boolean('filipino')->nullable();
                $table->boolean('naturalized')->nullable();
                $table->decimal('height',4,2)->nullable();
                $table->decimal('weight',4,2)->nullable();
                $table->string('blood_type')->nullable();
                $table->string('pagibig')->nullable();
                $table->string('gsis')->nullable();
                $table->string('philhealth');
                $table->string('tin')->nullable();
                $table->string('sss')->nullable();
                $table->string('govt_issued_id')->nullable();
                $table->string('govt_issued_id_number')->nullable();
                $table->string('govt_issued_id_place')->nullable();
                $table->date('govt_issued_id_date_issued')->nullable();
                $table->date('govt_issued_valid_until')->nullable();
                $table->string('agency_employee_number')->nullable();
                $table->string('biometrics')->nullable();
                $table->string('house_number')->nullable();
                $table->string('street')->nullable();
                $table->string('subdivision')->nullable();
                $table->string('brgy')->nullable();
                $table->string('city_id')->nullable();
                $table->string('province_id')->nullable();
                $table->integer('country_id')->nullable();
                $table->string('permanent_house_number')->nullable();
                $table->string('permanent_street' )->nullable();
                $table->string('permanent_subdivision')->nullable();
                $table->string('permanent_brgy')->nullable();
                $table->string('permanent_city_id')->nullable();
                $table->string('permanent_province_id')->nullable();
                $table->string('permanent_country_id' )->nullable();
                $table->string('permanent_telephone_number')->nullable();
                $table->string('image_path')->nullable();
                $table->integer('active')->nullable();
                $table->string('remarks')->nullable();
                $table->integer('created_by');
                $table->integer('updated_by')->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pms_employees');
    }
}
