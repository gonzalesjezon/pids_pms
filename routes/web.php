<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteSericeProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/','Auth\LoginController@showLoginForm');
Auth::routes();

Route::group(['middleware' => 'auth'],function(){

	Route::resource('dashboards', 'DashboardsController');


	Route::group(['prefix' => 'payrolls/admin/filemanagers'],function(){

		Route::get('benefits/getItem','BenefitsController@getItem');
		Route::resource('benefits','BenefitsController');

		Route::get('adjustments/getItem','AdjustmentsController@getItem');
		Route::resource('adjustments','AdjustmentsController');

		Route::get('deductions/getItem','DeductionsController@getItem');
		Route::resource('deductions','DeductionsController');

		Route::get('loans/getItem','LoansController@getItem');
		Route::resource('loans','LoansController');

		Route::get('responsibilitiescenter/getItem','ResponsibilitiesCenterController@getItem');
		Route::resource('responsibilitiescenter','ResponsibilitiesCenterController');

		Route::get('banks/getItem','BanksController@getItem');
		Route::resource('banks','BanksController');

		Route::get('bankbranches/getItem','BankBranchesController@getItem');
		Route::resource('bankbranches','BankBranchesController');

		Route::post('pagibig/storePolicy','PagibigController@storePolicy');
		Route::get('pagibig/showPolicy','PagibigController@showPolicy');
		Route::get('pagibig/getItem','PagibigController@getItem');
		Route::resource('pagibig','PagibigController');

		Route::post('philhealths/storePolicy','PhilhealthsController@storePolicy');
		Route::get('philhealths/showPolicy','PhilhealthsController@showPolicy');
		Route::get('philhealths/getItem','PhilhealthsController@getItem');
		Route::resource('philhealths','PhilhealthsController');

		Route::get('providentfundpolicies/getItem','ProvidentFundPoliciesController@getItem');
		Route::resource('providentfundpolicies','ProvidentFundPoliciesController');

		Route::get('salariesgrade/getSgstep','SalariesGradeController@getSgstep');
		Route::resource('salariesgrade','SalariesGradeController');

		Route::post('taxes/storeTaxstatus','TaxesController@storeTaxstatus');
		Route::get('taxes/showTaxstatus','TaxesController@showTaxstatus');
		Route::post('taxes/storeTaxstatusannual','TaxesController@storeTaxstatusannual');
		Route::get('taxes/showTaxstatusannual','TaxesController@showTaxstatusannual');
		Route::post('taxes/storeTaxpolicy','TaxesController@storeTaxpolicy');
		Route::get('taxes/showTaxpolicy','TaxesController@showTaxpolicy');

		Route::get('taxes/getTaxtable','TaxesController@getTaxtable');
		Route::get('taxes/getTaxstatus','TaxesController@getTaxstatus');
		Route::get('taxes/getTaxannual','TaxesController@getTaxannual');
		Route::get('taxes/getTaxpolicy','TaxesController@getTaxpolicy');

		Route::resource('taxes','TaxesController');

		Route::get('wagerates/getItem','WageRatesController@getItem');
		Route::resource('wagerates','WageRatesController');

		Route::get('gsis/getItem','GsisController@getItem');
		Route::resource('gsis','GsisController');

		Route::get('jobgrades/getJgstep','GsisController@getJgstep');
		Route::resource('jobgrades','JobGradesController');

		Route::resource('divisions','DivisionsController');
		Route::resource('offices','OfficesController');
		Route::resource('employeestatus','EmployeeStatusController');
		Route::resource('positions','PositionsController');
		Route::resource('position_items','PositionItemsController');
		Route::resource('rates','RatesController');
		Route::resource('travelrates','TravelRateController');
	});


	Route::group(['prefix'=>'payrolls/reports'],function(){
		Route::resource('generalpayroll','GeneralPayrollReportsController');
		Route::resource('payslips','PayslipsController');
		Route::resource('summaryadjustments','SummarAdjustmentReportsController');
		Route::resource('weeklypayroll','WeeklyPayrollReportsController');
		Route::resource('authoritydebit','AuthorityDebitReportsController');
		Route::resource('lwopadjustments','LWOPAdjustmentReportsController');
		Route::resource('stepincrementreports','StepIncrementReportsController');
		Route::resource('ratageneralpayroll','RATAGeneralPayrollReportsController');
		Route::resource('hdmfreports','HDMFReportsController');
		Route::resource('hdmftworeports','HDMFTwoReportsController');
		Route::resource('hdmfmplreports','HDMFMPLReportsController');
		Route::resource('philhealthreports','PhilhealthReportsController');
		Route::resource('providentfundreports','ProvidentFundReportsController');
		Route::resource('nhfmcreports','NHFMCReportsController');
	});

	Route::group(['prefix'=>'payrolls/otherpayrolls'],function(){

		Route::get('initialsalaries/getInitialSalary','InitialSalariesController@getInitialSalary');
		Route::resource('initialsalaries','InitialSalariesController');
		Route::get('lastsalaries/getLastSalary','LastSalariesController@getLastSalary');
		Route::resource('lastsalaries','LastSalariesController');
		Route::get('cancelsalaries/getCancelSalary','CancelSalariesController@getCancelSalary');
		Route::resource('cancelsalaries','CancelSalariesController');
		Route::get('rataadjustments/getRataAdjustment','RataAdjustmentsController@getRataAdjustment');
		Route::resource('rataadjustments','RataAdjustmentsController');

		// TRAVEL ALLOWANCE
		Route::get('leavemonetizations/getLeaveMonetization','LeaveMonetizationTransactionsController@getLeaveMonetization');
		Route::get('leavemonetizations/showLeaveMonetizationDatatable','LeaveMonetizationTransactionsController@showLeaveMonetizationDatatable');
		Route::post('leavemonetizations/processLeaveMonetization','LeaveMonetizationTransactionsController@processLeaveMonetization');
		Route::post('leavemonetizations/deleteLeaveMonetization','LeaveMonetizationTransactionsController@deleteLeaveMonetization');
		Route::resource('leavemonetizations','LeaveMonetizationTransactionsController');

		Route::get('lwop/getLWOP','LWOPController@getLWOP');
		Route::resource('lwop','LWOPController');

		Route::get('salaryadjustments/getSalaryAdjustment','SalaryAdjustmentsController@getSalaryAdjustment');
		Route::get('salaryadjustments/getDaysInAMonth','SalaryAdjustmentsController@getDaysInAMonth');
		Route::get('salaryadjustments/getCountedDays','SalaryAdjustmentsController@getCountedDays');
		Route::resource('salaryadjustments','SalaryAdjustmentsController');
		Route::get('stepincrements/getStepIncrement','StepIncrementsController@getStepIncrement');
		Route::resource('stepincrements','StepIncrementsController');

	});

	Route::group(['prefix' => 'payrolls'], function(){

		// EMPLOYEE FILE
		Route::get('admin/employees_payroll_informations/getJgstep','EmployeePayrollInformationsController@getJgstep');
		Route::get('admin/employees_payroll_informations/getItem','EmployeePayrollInformationsController@getItem');
		Route::get('admin/employees_payroll_informations/getSgstep','EmployeePayrollInformationsController@getSgstep');
		Route::get('admin/employees_payroll_informations/getEmployeesinfo','EmployeePayrollInformationsController@getEmployeesinfo');
		Route::get('admin/employees_payroll_informations/getSearchby','EmployeePayrollInformationsController@getSearchby');
		Route::get('admin/employees_payroll_informations/filter','EmployeePayrollInformationsController@filter');
		Route::get('admin/employees_payroll_informations/showBenefitinfo','EmployeePayrollInformationsController@showBenefitinfo');
		Route::get('admin/employees_payroll_informations/showSalaryinfo','EmployeePayrollInformationsController@showSalaryinfo');
		Route::get('admin/employees_payroll_informations/showDeductioninfo','EmployeePayrollInformationsController@showDeductioninfo');
		Route::get('admin/employees_payroll_informations/showLoaninfo','EmployeePayrollInformationsController@showLoaninfo');
		Route::post('admin/employees_payroll_informations/storeBenefitinfo','EmployeePayrollInformationsController@storeBenefitinfo');
		Route::post('admin/employees_payroll_informations/deleteBenefitinfo','EmployeePayrollInformationsController@deleteBenefitinfo');
		Route::post('admin/employees_payroll_informations/deleteLoanInfo','EmployeePayrollInformationsController@deleteLoanInfo');
		Route::post('admin/employees_payroll_informations/deleteDeductInfo','EmployeePayrollInformationsController@deleteDeductInfo');
		Route::post('admin/employees_payroll_informations/storeSalaryinfo','EmployeePayrollInformationsController@storeSalaryinfo');
		Route::post('admin/employees_payroll_informations/storeDeductioninfo','EmployeePayrollInformationsController@storeDeductioninfo');
		Route::post('admin/employees_payroll_informations/storeLoaninfo','EmployeePayrollInformationsController@storeLoaninfo');
		Route::post('admin/employees_payroll_informations/storeNonPlantilla','EmployeePayrollInformationsController@storeNonPlantilla');
		Route::resource('admin/employees_payroll_informations','EmployeePayrollInformationsController');

		Route::resource('admin/employeesetup','EmployeeSetupController');
		// RATA PAYROLL
		Route::get('specialpayrolls/ratatransactions/getSearchby','RataTransactionsController@getSearchby');
		Route::get('specialpayrolls/ratatransactions/getRataInfo','RataTransactionsController@getRataInfo');
		Route::post('specialpayrolls/ratatransactions/storeRata','RataTransactionsController@storeRata');
		Route::post('specialpayrolls/ratatransactions/processRata','RataTransactionsController@processRata');
		Route::post('specialpayrolls/ratatransactions/deleteRata','RataTransactionsController@deleteRata');
		Route::resource('specialpayrolls/ratatransactions/showRataDatatable','RataTransactionsController@showRataDatatable');
		Route::resource('specialpayrolls/ratatransactions/filter','RataTransactionsController@filter');
		Route::resource('specialpayrolls/ratatransactions','RataTransactionsController');

		// EME PAYROLL
		Route::get('specialpayrolls/emetransactions/getEmeInfo','EmeTransactionsController@getEmeInfo');
		Route::get('specialpayrolls/emetransactions/showEmeDatatable','EmeTransactionsController@showEmeDatatable');
		Route::post('specialpayrolls/emetransactions/processEme','EmeTransactionsController@processEme');
		Route::post('specialpayrolls/emetransactions/deleteEme','EmeTransactionsController@deleteEme');
		Route::resource('specialpayrolls/emetransactions','EmeTransactionsController');

		// COMMUNICATION ALLOTMENT
		Route::get('specialpayrolls/communicationtransactions/getCeaInfo','CommunicationTransactionsController@getCeaInfo');
		Route::get('specialpayrolls/communicationtransactions/showCeaDatatable','CommunicationTransactionsController@showCeaDatatable');
		Route::post('specialpayrolls/communicationtransactions/processCea','CommunicationTransactionsController@processCea');
		Route::post('specialpayrolls/communicationtransactions/deleteCea','CommunicationTransactionsController@deleteCea');
		Route::resource('specialpayrolls/communicationtransactions','CommunicationTransactionsController');

		// PERFORMANCE INCENTIVE
		Route::get('specialpayrolls/peitransactions/getPeiInfo','PerformanceIncentiveTransactionsController@getPeiInfo');
		Route::get('specialpayrolls/peitransactions/showPeiDatatable','PerformanceIncentiveTransactionsController@showPeiDatatable');
		Route::post('specialpayrolls/peitransactions/processPei','PerformanceIncentiveTransactionsController@processPei');
		Route::post('specialpayrolls/peitransactions/deletePei','PerformanceIncentiveTransactionsController@deletePei');
		Route::resource('specialpayrolls/peitransactions','PerformanceIncentiveTransactionsController');

		// CASH GIFT AND YEAR END BONUS
		Route::get('specialpayrolls/cgyetransactions/getCgyeInfo','CashGiftAndYearEndTransactionsController@getCgyeInfo');
		Route::get('specialpayrolls/cgyetransactions/showCgyeDatatable','CashGiftAndYearEndTransactionsController@showCgyeDatatable');
		Route::post('specialpayrolls/cgyetransactions/processCgye','CashGiftAndYearEndTransactionsController@processCgye');
		Route::post('specialpayrolls/cgyetransactions/deleteCgye','CashGiftAndYearEndTransactionsController@deleteCgye');
		Route::resource('specialpayrolls/cgyetransactions','CashGiftAndYearEndTransactionsController');

		// MID YEAR END BONUS
		Route::get('specialpayrolls/midyeartransactions/getMidYear','MidYearBonusTransactionsController@getMidYear');
		Route::get('specialpayrolls/midyeartransactions/showMidYearDatatable','MidYearBonusTransactionsController@showMidYearDatatable');
		Route::post('specialpayrolls/midyeartransactions/processMidYear','MidYearBonusTransactionsController@processMidYear');
		Route::post('specialpayrolls/midyeartransactions/deleteMidYear','MidYearBonusTransactionsController@deleteMidYear');
		Route::resource('specialpayrolls/midyeartransactions','MidYearBonusTransactionsController');

		// TRAVEL ALLOWANCE
		Route::get('specialpayrolls/traveltransactions/getTravelAllowance','TravelRateTransactionsController@getTravelAllowance');
		Route::get('specialpayrolls/traveltransactions/showTravelAllowanceDatatable','TravelRateTransactionsController@showTravelAllowanceDatatable');
		Route::post('specialpayrolls/traveltransactions/deleteTravelAllowance','TravelRateTransactionsController@deleteTravelAllowance');
		Route::resource('specialpayrolls/traveltransactions','TravelRateTransactionsController');

		// Performance Base Bonus
		Route::get('specialpayrolls/pbbtransactions/getPbbInfo','PerformanceBasicBonusTransactionsController@getPbbInfo');
		Route::get('specialpayrolls/pbbtransactions/showPbbDatatable','PerformanceBasicBonusTransactionsController@showPbbDatatable');
		Route::post('specialpayrolls/pbbtransactions/processPbb','PerformanceBasicBonusTransactionsController@processPbb');
		Route::post('specialpayrolls/pbbtransactions/deletePbb','PerformanceBasicBonusTransactionsController@deletePbb');
		Route::resource('specialpayrolls/pbbtransactions','PerformanceBasicBonusTransactionsController');

		Route::resource('otherpayrolls','OtherPayrollsController');
		Route::resource('filemanagers','FileManagersController');

		Route::post('transactions/deletePayroll','TransactionsController@deletePayroll');
		Route::post('transactions/processPayroll','TransactionsController@processPayroll');
		Route::post('transactions/storeBenefitInfoTransaction','TransactionsController@storeBenefitInfoTransaction');
		Route::post('transactions/storeDeductionInfoTransaction','TransactionsController@storeDeductionInfoTransaction');
		Route::get('transactions/showLoaninfo','TransactionsController@showLoaninfo');
		Route::get('transactions/showDeductioninfo','TransactionsController@showDeductioninfo');
		Route::get('transactions/showBenefitinfo','TransactionsController@showBenefitinfo');
		Route::get('transactions/getSearchby','TransactionsController@getSearchby');
		Route::get('transactions/searchName','TransactionsController@searchName');
		Route::get('transactions/filter','TransactionsController@filter');
		Route::get('transactions/getEmployeesinfo','TransactionsController@getEmployeesinfo');
		Route::post('transactions/deleteLoan','TransactionsController@deleteLoan');
		Route::post('transactions/deleteDeduction','TransactionsController@deleteDeduction');
		Route::post('transactions/deleteBenefit','TransactionsController@deleteBenefit');
		Route::resource('transactions','TransactionsController');

		Route::resource('reports','ReportsController');

		Route::resource('admin/payrollconfigurations','PayrollConfigurationsController');
		Route::get('admin/previousemployer/getPreviousEmployer','PreviousEmployerController@getPreviousEmployer');
		Route::resource('admin/previousemployer','PreviousEmployerController');
		Route::get('admin/beginningbalances/getBeginningBalances','BeginningBalancesController@getBeginningBalances');
		Route::resource('admin/beginningbalances','BeginningBalancesController');

		Route::get('overtimepay/getOvertimeInfo','OvertimePaysController@getOvertimeInfo');
		Route::post('overtimepay/deleteOvertime','OvertimePaysController@deleteOvertime');
		Route::post('overtimepay/storeOvertimeInfo','OvertimePaysController@storeOvertimeInfo');
		Route::resource('overtimepay','OvertimePaysController');
		Route::post('nonplantilla/processPayroll','NonPlantillaController@processPayroll');
		Route::get('nonplantilla/getEmployeesinfo','NonPlantillaController@getEmployeesinfo');
		Route::resource('nonplantilla','NonPlantillaController');



	});


});

