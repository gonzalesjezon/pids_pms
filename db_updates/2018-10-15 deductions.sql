-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.31-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table pids.pms_deductions
DROP TABLE IF EXISTS `pms_deductions`;
CREATE TABLE IF NOT EXISTS `pms_deductions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(225) DEFAULT NULL,
  `name` varchar(225) DEFAULT NULL,
  `payroll_group` varchar(225) DEFAULT NULL,
  `tax_type` varchar(225) DEFAULT NULL,
  `itr_classification` varchar(225) DEFAULT NULL,
  `alphalist_classification` varchar(225) DEFAULT NULL,
  `amount` decimal(9,2) DEFAULT NULL,
  `deduction_rate` decimal(9,8) DEFAULT NULL,
  `effective_year` varchar(225) DEFAULT NULL,
  `remarks` varchar(225) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

-- Dumping data for table pids.pms_deductions: ~9 rows (approximately)
/*!40000 ALTER TABLE `pms_deductions` DISABLE KEYS */;
INSERT INTO `pms_deductions` (`id`, `code`, `name`, `payroll_group`, `tax_type`, `itr_classification`, `alphalist_classification`, `amount`, `deduction_rate`, `effective_year`, `remarks`, `created_at`, `updated_at`, `updated_by`) VALUES
	(1, 'SSS CONT', 'SSS CONTRIBUTION', NULL, 'Taxable', NULL, NULL, 0.00, NULL, NULL, NULL, '2018-08-16 12:15:15', '2018-08-16 12:38:27', NULL),
	(2, 'MPCC', 'MULTI-PURPOSE COOPERATIVE CONTRIBUTION', NULL, 'Taxable', NULL, NULL, 0.00, NULL, NULL, NULL, '2018-08-16 12:16:30', '2018-08-16 12:17:27', NULL),
	(3, 'UD', 'UNION DUES', NULL, 'Taxable', NULL, NULL, 0.00, NULL, NULL, NULL, '2018-08-16 12:18:18', '2018-08-16 12:18:18', NULL),
	(4, 'WF', 'WELFARE FUND', NULL, 'Taxable', NULL, NULL, 0.00, NULL, NULL, NULL, '2018-08-16 15:55:43', '2018-08-16 15:55:43', NULL),
	(5, 'WD', 'WASSSLAI  Capital Contribution', NULL, 'Taxable', NULL, NULL, 0.00, NULL, NULL, NULL, '2018-08-17 04:08:59', '2018-08-17 04:08:59', NULL),
	(6, 'SB', 'SMART BILL', NULL, 'Taxable', NULL, NULL, 0.00, NULL, NULL, 'bill', '2018-09-11 14:00:39', '2018-09-11 14:00:39', NULL),
	(7, 'GB', 'GLOBE BILL', NULL, 'Taxable', NULL, NULL, 0.00, NULL, NULL, 'bill', '2018-09-11 14:00:58', '2018-09-11 14:00:58', NULL),
	(8, 'COOP', 'COOP', NULL, 'Taxable', NULL, NULL, 0.00, NULL, NULL, 'deductions', '2018-09-22 07:32:07', '2018-09-22 07:32:07', NULL),
	(9, 'PF', 'PROVIDENT FUND', NULL, 'Non Taxable', NULL, NULL, 0.00, 0.02739089, '2018', 'pf', '2018-09-22 10:59:10', '2018-10-15 08:44:51', NULL);
/*!40000 ALTER TABLE `pms_deductions` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
