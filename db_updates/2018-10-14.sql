-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.31-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table pids.pms_rata
CREATE TABLE IF NOT EXISTS `pms_rata` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) DEFAULT NULL,
  `position_item_id` int(11) DEFAULT NULL,
  `office_id` int(11) DEFAULT NULL,
  `department_id` int(11) DEFAULT NULL,
  `number_of_actual_work` int(11) DEFAULT NULL,
  `number_of_leave_field` int(11) DEFAULT NULL,
  `number_of_work_days` int(11) DEFAULT NULL,
  `number_of_used_vehicles` int(11) DEFAULT NULL,
  `percentage_of_rata` varchar(225) DEFAULT NULL,
  `percentage_of_rata_value` decimal(9,2) DEFAULT NULL,
  `adjustment_transpo_amount` decimal(9,2) DEFAULT NULL,
  `adjustment_number_work_days` int(11) DEFAULT NULL,
  `adjustment_rep_amount` decimal(9,2) DEFAULT NULL,
  `adjustment_percentage` decimal(9,2) DEFAULT NULL,
  `adjustment_percentage_amount` decimal(9,2) DEFAULT NULL,
  `adjustment_deduction_period` varchar(50) DEFAULT NULL,
  `transportation_amount` decimal(9,2) DEFAULT NULL,
  `representation_amount` decimal(9,2) DEFAULT NULL,
  `year` varchar(225) DEFAULT NULL,
  `month` varchar(225) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table pids.pms_rata: ~1 rows (approximately)
/*!40000 ALTER TABLE `pms_rata` DISABLE KEYS */;
INSERT INTO `pms_rata` (`id`, `employee_id`, `position_item_id`, `office_id`, `department_id`, `number_of_actual_work`, `number_of_leave_field`, `number_of_work_days`, `number_of_used_vehicles`, `percentage_of_rata`, `percentage_of_rata_value`, `adjustment_transpo_amount`, `adjustment_number_work_days`, `adjustment_rep_amount`, `adjustment_percentage`, `adjustment_percentage_amount`, `adjustment_deduction_period`, `transportation_amount`, `representation_amount`, `year`, `month`, `created_at`, `created_by`, `updated_at`, `updated_by`) VALUES
	(1, 1, NULL, NULL, 1, 22, NULL, 22, NULL, NULL, 1.00, 1250.00, 5, 1250.00, 0.25, 1250.00, 'secondweek', 5000.00, 5000.00, '2018', 'January', '2018-10-15 08:22:25', NULL, '2018-10-15 08:24:59', NULL);
/*!40000 ALTER TABLE `pms_rata` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
