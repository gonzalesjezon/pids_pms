-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.31-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table pids_hris.pms_adjustments
DROP TABLE IF EXISTS `pms_adjustments`;
CREATE TABLE IF NOT EXISTS `pms_adjustments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(225) DEFAULT NULL,
  `name` varchar(225) DEFAULT NULL,
  `payroll_group` varchar(225) DEFAULT NULL,
  `tax_type` varchar(225) DEFAULT NULL,
  `itr_classification` varchar(225) DEFAULT NULL,
  `alphalist_classification` varchar(225) DEFAULT NULL,
  `amount` decimal(9,2) DEFAULT NULL,
  `remarks` varchar(225) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table pids_hris.pms_adjustments: ~0 rows (approximately)
/*!40000 ALTER TABLE `pms_adjustments` DISABLE KEYS */;
/*!40000 ALTER TABLE `pms_adjustments` ENABLE KEYS */;

-- Dumping structure for table pids_hris.pms_attendance_info
DROP TABLE IF EXISTS `pms_attendance_info`;
CREATE TABLE IF NOT EXISTS `pms_attendance_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) DEFAULT NULL,
  `transaction_id` int(11) DEFAULT NULL,
  `actual_workdays` int(11) DEFAULT NULL,
  `adjust_workdays` int(11) DEFAULT NULL,
  `total_workdays` decimal(9,2) DEFAULT NULL,
  `actual_absence` int(11) DEFAULT NULL,
  `adjust_absence` int(11) DEFAULT NULL,
  `total_absence` decimal(9,2) DEFAULT NULL,
  `actual_tardines` int(11) DEFAULT NULL,
  `adjust_tardines` int(11) DEFAULT NULL,
  `total_tardines` decimal(9,2) DEFAULT NULL,
  `actual_undertime` int(11) DEFAULT NULL,
  `adjust_undertime` int(11) DEFAULT NULL,
  `total_undertime` decimal(9,2) DEFAULT NULL,
  `month` varchar(225) DEFAULT NULL,
  `year` varchar(225) DEFAULT NULL,
  `employee_status` varchar(225) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=100 DEFAULT CHARSET=latin1;

-- Dumping data for table pids_hris.pms_attendance_info: ~0 rows (approximately)
/*!40000 ALTER TABLE `pms_attendance_info` DISABLE KEYS */;
/*!40000 ALTER TABLE `pms_attendance_info` ENABLE KEYS */;

-- Dumping structure for table pids_hris.pms_bankbranches
DROP TABLE IF EXISTS `pms_bankbranches`;
CREATE TABLE IF NOT EXISTS `pms_bankbranches` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(225) DEFAULT NULL,
  `name` varchar(225) DEFAULT NULL,
  `address` varchar(225) DEFAULT NULL,
  `remarks` varchar(225) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- Dumping data for table pids_hris.pms_bankbranches: ~6 rows (approximately)
/*!40000 ALTER TABLE `pms_bankbranches` DISABLE KEYS */;
INSERT INTO `pms_bankbranches` (`id`, `code`, `name`, `address`, `remarks`, `created_at`, `updated_at`, `updated_by`) VALUES
	(1, 'MANILA', 'Manila', NULL, NULL, '2018-06-09 04:45:58', '2018-06-09 04:46:24', NULL),
	(2, 'QC', 'Quezon City', NULL, NULL, '2018-06-09 04:46:14', '2018-06-09 04:46:14', NULL),
	(3, 'CALOOCAN', 'Caloocan', NULL, NULL, '2018-06-09 04:46:48', '2018-06-09 04:46:48', NULL),
	(4, 'PASAY', 'Pasay', NULL, NULL, '2018-06-09 04:47:16', '2018-06-09 04:47:16', NULL),
	(5, 'PC', 'PASIG KAPITOLYO', NULL, NULL, '2018-06-21 04:30:31', '2018-06-21 04:30:31', NULL),
	(6, 'MY', 'Mandaluyong', NULL, NULL, '2018-06-21 07:56:18', '2018-06-21 07:56:18', NULL);
/*!40000 ALTER TABLE `pms_bankbranches` ENABLE KEYS */;

-- Dumping structure for table pids_hris.pms_banks
DROP TABLE IF EXISTS `pms_banks`;
CREATE TABLE IF NOT EXISTS `pms_banks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(225) DEFAULT NULL,
  `name` varchar(225) DEFAULT NULL,
  `branch_name` varchar(225) DEFAULT NULL,
  `bank_accountno` varchar(225) DEFAULT NULL,
  `remarks` varchar(225) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Dumping data for table pids_hris.pms_banks: ~5 rows (approximately)
/*!40000 ALTER TABLE `pms_banks` DISABLE KEYS */;
INSERT INTO `pms_banks` (`id`, `code`, `name`, `branch_name`, `bank_accountno`, `remarks`, `created_at`, `updated_at`, `updated_by`) VALUES
	(1, 'BDO', 'Banco De Oro', 'Manila', '234567', NULL, '2018-06-09 04:43:22', '2018-06-22 12:14:56', NULL),
	(2, 'BPI', 'Bank of Philippine Islands', 'Manila', '234567', NULL, '2018-06-09 04:44:42', '2018-06-22 12:14:47', NULL),
	(3, 'MB', 'Metro Bank', 'Manila', '345678', NULL, '2018-06-09 04:45:04', '2018-06-22 12:13:58', NULL),
	(4, 'BSP', 'Bangko Sentral ng Pilipinas', 'Manila', '456756789', NULL, '2018-06-09 04:45:43', '2018-06-22 12:14:35', NULL),
	(5, 'LBP', 'Land Bank of the Philippines', 'Manila', '35645678', NULL, '2018-06-21 04:30:06', '2018-06-22 12:14:19', NULL);
/*!40000 ALTER TABLE `pms_banks` ENABLE KEYS */;

-- Dumping structure for table pids_hris.pms_beginning_balances
DROP TABLE IF EXISTS `pms_beginning_balances`;
CREATE TABLE IF NOT EXISTS `pms_beginning_balances` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) DEFAULT NULL,
  `premium_amount` decimal(9,2) DEFAULT NULL,
  `tax_witheld` decimal(9,2) DEFAULT NULL,
  `basic_pay` decimal(9,2) DEFAULT NULL,
  `overtime_pay` decimal(9,2) DEFAULT NULL,
  `thirteen_month_pay` decimal(9,2) DEFAULT NULL,
  `deminimis` decimal(9,2) DEFAULT NULL,
  `other_salaries` decimal(9,2) DEFAULT NULL,
  `taxable_basic_pay` decimal(9,2) DEFAULT NULL,
  `taxable_overtime_pay` decimal(9,2) DEFAULT NULL,
  `taxable_thirteen_month_pay` decimal(9,2) DEFAULT NULL,
  `taxable_other_salaries` decimal(9,2) DEFAULT NULL,
  `as_of_date` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table pids_hris.pms_beginning_balances: ~1 rows (approximately)
/*!40000 ALTER TABLE `pms_beginning_balances` DISABLE KEYS */;
INSERT INTO `pms_beginning_balances` (`id`, `employee_id`, `premium_amount`, `tax_witheld`, `basic_pay`, `overtime_pay`, `thirteen_month_pay`, `deminimis`, `other_salaries`, `taxable_basic_pay`, `taxable_overtime_pay`, `taxable_thirteen_month_pay`, `taxable_other_salaries`, `as_of_date`, `created_at`, `updated_at`, `created_by`) VALUES
	(1, 92, 30000.00, 2131232.00, 321.00, 321.00, 312.00, 312.00, 321.00, 321.00, 321.00, 312.00, 321.00, '2018-07-24', '2018-07-17 13:31:41', '2018-07-17 14:25:25', NULL);
/*!40000 ALTER TABLE `pms_beginning_balances` ENABLE KEYS */;

-- Dumping structure for table pids_hris.pms_benefitinfo_transactions
DROP TABLE IF EXISTS `pms_benefitinfo_transactions`;
CREATE TABLE IF NOT EXISTS `pms_benefitinfo_transactions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) DEFAULT NULL,
  `transaction_id` int(11) DEFAULT NULL,
  `benefit_id` int(11) DEFAULT NULL,
  `benefit_info_id` int(11) DEFAULT NULL,
  `amount` decimal(9,2) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `year` varchar(50) DEFAULT NULL,
  `month` varchar(50) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=833 DEFAULT CHARSET=latin1;

-- Dumping data for table pids_hris.pms_benefitinfo_transactions: ~9 rows (approximately)
/*!40000 ALTER TABLE `pms_benefitinfo_transactions` DISABLE KEYS */;
INSERT INTO `pms_benefitinfo_transactions` (`id`, `employee_id`, `transaction_id`, `benefit_id`, `benefit_info_id`, `amount`, `status`, `year`, `month`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
	(744, 75, NULL, 4, 75, 2000.00, 'pera', '2018', 'January', 2, NULL, '2018-10-15 10:17:01', '2018-10-15 10:17:01'),
	(753, 76, NULL, 4, 76, 2000.00, 'pera', '2018', 'January', 2, NULL, '2018-10-15 10:17:02', '2018-10-15 10:17:02'),
	(761, 77, NULL, 4, 77, 2000.00, 'pera', '2018', 'January', 2, NULL, '2018-10-15 10:17:04', '2018-10-15 10:17:04'),
	(763, 78, NULL, 4, 78, 909.10, 'pera', '2018', 'January', 2, NULL, '2018-10-15 10:17:04', '2018-10-15 10:17:04'),
	(764, 79, NULL, 4, 79, 2000.00, 'pera', '2018', 'January', 2, NULL, '2018-10-15 10:17:04', '2018-10-15 10:17:04'),
	(767, 80, NULL, 4, 80, 2000.00, 'pera', '2018', 'January', 2, NULL, '2018-10-15 10:17:05', '2018-10-15 10:17:05'),
	(779, 81, NULL, 4, 81, 2000.00, 'pera', '2018', 'January', 2, NULL, '2018-10-15 10:17:07', '2018-10-15 10:17:07'),
	(782, 82, NULL, 4, 82, 2000.00, 'pera', '2018', 'January', 2, NULL, '2018-10-15 10:17:08', '2018-10-15 10:17:08'),
	(785, 83, NULL, 4, 83, 2000.00, 'pera', '2018', 'January', 2, NULL, '2018-10-15 10:17:08', '2018-10-15 10:17:08');
/*!40000 ALTER TABLE `pms_benefitinfo_transactions` ENABLE KEYS */;

-- Dumping structure for table pids_hris.pms_benefits
DROP TABLE IF EXISTS `pms_benefits`;
CREATE TABLE IF NOT EXISTS `pms_benefits` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(225) DEFAULT NULL,
  `name` varchar(225) DEFAULT NULL,
  `amount` decimal(9,2) DEFAULT NULL,
  `de_minimis_type` varchar(225) DEFAULT NULL,
  `computation_type` varchar(225) DEFAULT NULL,
  `payroll_group` varchar(225) DEFAULT NULL,
  `tax_type` varchar(225) DEFAULT NULL,
  `itr_classification` varchar(225) DEFAULT NULL,
  `alphalist_classification` varchar(225) DEFAULT NULL,
  `remarks` varchar(225) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

-- Dumping data for table pids_hris.pms_benefits: ~12 rows (approximately)
/*!40000 ALTER TABLE `pms_benefits` DISABLE KEYS */;
INSERT INTO `pms_benefits` (`id`, `code`, `name`, `amount`, `de_minimis_type`, `computation_type`, `payroll_group`, `tax_type`, `itr_classification`, `alphalist_classification`, `remarks`, `created_at`, `updated_at`, `updated_by`) VALUES
	(4, 'PERA', 'PERA', NULL, NULL, 'Based on Attendance', NULL, 'Taxable in excess of threshold', NULL, NULL, NULL, '2018-06-21 04:00:00', '2018-06-21 04:00:00', NULL),
	(5, 'REP', 'REPRESENTATION ALLOWANCE', NULL, NULL, 'Based on Attendance', NULL, 'Taxable in excess of threshold', NULL, NULL, NULL, '2018-06-21 04:01:53', '2018-06-21 04:01:53', NULL),
	(6, 'TRANSPO', 'TRANSPORTATION ALLOWANCE', NULL, NULL, 'Based on Attendance', NULL, 'Taxable in excess of threshold', NULL, NULL, NULL, '2018-06-21 04:02:43', '2018-06-21 04:02:43', NULL),
	(9, 'PEI', 'PERFORMANCE ENHANCEMENT INCENTIVE', 5000.00, NULL, 'Fixed Monthly', NULL, 'Non Taxable', NULL, NULL, NULL, '2018-07-12 01:38:03', '2018-08-01 06:51:47', NULL),
	(10, 'CEA', 'COMMUNICATIONS EXPENSE ALLOTMENT', NULL, NULL, 'Fixed Monthly', NULL, 'Non Taxable', NULL, NULL, NULL, '2018-07-12 05:08:03', '2018-07-12 05:08:03', NULL),
	(11, 'EE', 'EXTRAORDINARY EXPENSE', NULL, NULL, 'Fixed Monthly', NULL, 'Non Taxable', NULL, NULL, NULL, '2018-07-13 08:20:04', '2018-07-13 08:20:04', NULL),
	(12, 'ME', 'MISCELLANEOUS EXPENSE', NULL, NULL, 'Fixed Monthly', NULL, 'Non Taxable', NULL, NULL, NULL, '2018-07-13 08:20:29', '2018-07-13 08:20:29', NULL),
	(15, 'CG', 'CASH GIFT', 5000.00, NULL, 'Fixed Monthly', NULL, 'Non Taxable', NULL, NULL, NULL, '2018-07-14 12:04:38', '2018-07-24 09:58:04', NULL),
	(16, 'YEB', 'Year End Bonus', 0.00, NULL, 'Based on Attendance', NULL, 'Non Taxable', NULL, NULL, NULL, '2018-08-01 15:12:25', '2018-08-01 15:12:53', NULL),
	(17, 'MYB', 'Mid Year Bonus', NULL, NULL, 'Based on Attendance', NULL, 'Non Taxable', NULL, NULL, NULL, '2018-08-01 15:13:13', '2018-08-01 15:13:13', NULL),
	(18, 'PBB', 'Performance Base Bonus', 5000.00, NULL, 'Based on Attendance', NULL, 'Non Taxable', NULL, NULL, NULL, '2018-08-01 15:14:40', '2018-08-01 15:16:42', NULL),
	(19, 'TA', 'TRAVEL ALLOWANCE', 0.00, NULL, 'Fixed Monthly', NULL, 'Taxable', NULL, NULL, 'allowance', '2018-09-11 09:49:02', '2018-09-11 09:49:02', NULL);
/*!40000 ALTER TABLE `pms_benefits` ENABLE KEYS */;

-- Dumping structure for table pids_hris.pms_benefitsinfo
DROP TABLE IF EXISTS `pms_benefitsinfo`;
CREATE TABLE IF NOT EXISTS `pms_benefitsinfo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) DEFAULT NULL,
  `employeeinfo_id` int(11) DEFAULT NULL,
  `benefit_id` int(11) DEFAULT NULL,
  `benefit_description` varchar(225) DEFAULT NULL,
  `benefit_amount` decimal(9,2) DEFAULT NULL,
  `benefit_pay_period` varchar(225) DEFAULT NULL,
  `benefit_pay_sub` varchar(225) DEFAULT NULL,
  `benefit_effectivity_date` date DEFAULT NULL,
  `date_from` date DEFAULT NULL,
  `date_to` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=126 DEFAULT CHARSET=latin1;

-- Dumping data for table pids_hris.pms_benefitsinfo: ~125 rows (approximately)
/*!40000 ALTER TABLE `pms_benefitsinfo` DISABLE KEYS */;
INSERT INTO `pms_benefitsinfo` (`id`, `employee_id`, `employeeinfo_id`, `benefit_id`, `benefit_description`, `benefit_amount`, `benefit_pay_period`, `benefit_pay_sub`, `benefit_effectivity_date`, `date_from`, `date_to`, `created_at`, `updated_at`, `updated_by`) VALUES
	(1, 1, NULL, 4, NULL, 2000.00, NULL, NULL, '2018-09-20', NULL, NULL, NULL, NULL, NULL),
	(2, 2, NULL, 4, NULL, 2000.00, NULL, NULL, '2018-09-20', NULL, NULL, NULL, NULL, NULL),
	(3, 3, NULL, 4, NULL, 2000.00, NULL, NULL, '2018-09-20', NULL, NULL, NULL, NULL, NULL),
	(4, 4, NULL, 4, NULL, 2000.00, NULL, NULL, '2018-09-20', NULL, NULL, NULL, NULL, NULL),
	(5, 5, NULL, 4, NULL, 2000.00, NULL, NULL, '2018-09-20', NULL, NULL, NULL, NULL, NULL),
	(6, 6, NULL, 4, NULL, 2000.00, NULL, NULL, '2018-09-20', NULL, NULL, NULL, NULL, NULL),
	(7, 7, NULL, 4, NULL, 2000.00, NULL, NULL, '2018-09-20', NULL, NULL, NULL, NULL, NULL),
	(8, 8, NULL, 4, NULL, 2000.00, NULL, NULL, '2018-09-20', NULL, NULL, NULL, NULL, NULL),
	(9, 9, NULL, 4, NULL, 2000.00, NULL, NULL, '2018-09-20', NULL, NULL, NULL, NULL, NULL),
	(10, 10, NULL, 4, NULL, 2000.00, NULL, NULL, '2018-09-20', NULL, NULL, NULL, NULL, NULL),
	(11, 11, NULL, 4, NULL, 2000.00, NULL, NULL, '2018-09-20', NULL, NULL, NULL, NULL, NULL),
	(12, 12, NULL, 4, NULL, 2000.00, NULL, NULL, '2018-09-20', NULL, NULL, NULL, NULL, NULL),
	(13, 13, NULL, 4, NULL, 2000.00, NULL, NULL, '2018-09-20', NULL, NULL, NULL, NULL, NULL),
	(14, 14, NULL, 4, NULL, 2000.00, NULL, NULL, '2018-09-20', NULL, NULL, NULL, NULL, NULL),
	(15, 15, NULL, 4, NULL, 2000.00, NULL, NULL, '2018-09-20', NULL, NULL, NULL, NULL, NULL),
	(16, 16, NULL, 4, NULL, 2000.00, NULL, NULL, '2018-09-20', NULL, NULL, NULL, NULL, NULL),
	(17, 17, NULL, 4, NULL, 2000.00, NULL, NULL, '2018-09-20', NULL, NULL, NULL, NULL, NULL),
	(18, 18, NULL, 4, NULL, 2000.00, NULL, NULL, '2018-09-20', NULL, NULL, NULL, NULL, NULL),
	(19, 19, NULL, 4, NULL, 2000.00, NULL, NULL, '2018-09-20', NULL, NULL, NULL, NULL, NULL),
	(20, 20, NULL, 4, NULL, 2000.00, NULL, NULL, '2018-09-20', NULL, NULL, NULL, NULL, NULL),
	(21, 21, NULL, 4, NULL, 2000.00, NULL, NULL, '2018-09-20', NULL, NULL, NULL, NULL, NULL),
	(22, 22, NULL, 4, NULL, 2000.00, NULL, NULL, '2018-09-20', NULL, NULL, NULL, NULL, NULL),
	(23, 23, NULL, 4, NULL, 2000.00, NULL, NULL, '2018-09-20', NULL, NULL, NULL, NULL, NULL),
	(24, 24, NULL, 4, NULL, 2000.00, NULL, NULL, '2018-09-20', NULL, NULL, NULL, NULL, NULL),
	(25, 25, NULL, 4, NULL, 2000.00, NULL, NULL, '2018-09-20', NULL, NULL, NULL, NULL, NULL),
	(26, 26, NULL, 4, NULL, 2000.00, NULL, NULL, '2018-09-20', NULL, NULL, NULL, NULL, NULL),
	(27, 27, NULL, 4, NULL, 2000.00, NULL, NULL, '2018-09-20', NULL, NULL, NULL, NULL, NULL),
	(28, 28, NULL, 4, NULL, 2000.00, NULL, NULL, '2018-09-20', NULL, NULL, NULL, NULL, NULL),
	(29, 29, NULL, 4, NULL, 2000.00, NULL, NULL, '2018-09-20', NULL, NULL, NULL, NULL, NULL),
	(30, 30, NULL, 4, NULL, 2000.00, NULL, NULL, '2018-09-20', NULL, NULL, NULL, NULL, NULL),
	(31, 31, NULL, 4, NULL, 2000.00, NULL, NULL, '2018-09-20', NULL, NULL, NULL, NULL, NULL),
	(32, 32, NULL, 4, NULL, 2000.00, NULL, NULL, '2018-09-20', NULL, NULL, NULL, NULL, NULL),
	(33, 33, NULL, 4, NULL, 2000.00, NULL, NULL, '2018-09-20', NULL, NULL, NULL, NULL, NULL),
	(34, 34, NULL, 4, NULL, 2000.00, NULL, NULL, '2018-09-20', NULL, NULL, NULL, NULL, NULL),
	(35, 35, NULL, 4, NULL, 2000.00, NULL, NULL, '2018-09-20', NULL, NULL, NULL, NULL, NULL),
	(36, 36, NULL, 4, NULL, 2000.00, NULL, NULL, '2018-09-20', NULL, NULL, NULL, NULL, NULL),
	(37, 37, NULL, 4, NULL, 2000.00, NULL, NULL, '2018-09-20', NULL, NULL, NULL, NULL, NULL),
	(38, 38, NULL, 4, NULL, 2000.00, NULL, NULL, '2018-09-20', NULL, NULL, NULL, NULL, NULL),
	(39, 39, NULL, 4, NULL, 2000.00, NULL, NULL, '2018-09-20', NULL, NULL, NULL, NULL, NULL),
	(40, 40, NULL, 4, NULL, 2000.00, NULL, NULL, '2018-09-20', NULL, NULL, NULL, NULL, NULL),
	(41, 41, NULL, 4, NULL, 2000.00, NULL, NULL, '2018-09-20', NULL, NULL, NULL, NULL, NULL),
	(42, 42, NULL, 4, NULL, 2000.00, NULL, NULL, '2018-09-20', NULL, NULL, NULL, NULL, NULL),
	(43, 43, NULL, 4, NULL, 2000.00, NULL, NULL, '2018-09-20', NULL, NULL, NULL, NULL, NULL),
	(44, 44, NULL, 4, NULL, 2000.00, NULL, NULL, '2018-09-20', NULL, NULL, NULL, NULL, NULL),
	(45, 45, NULL, 4, NULL, 2000.00, NULL, NULL, '2018-09-20', NULL, NULL, NULL, NULL, NULL),
	(46, 46, NULL, 4, NULL, 2000.00, NULL, NULL, '2018-09-20', NULL, NULL, NULL, NULL, NULL),
	(47, 47, NULL, 4, NULL, 2000.00, NULL, NULL, '2018-09-20', NULL, NULL, NULL, NULL, NULL),
	(48, 48, NULL, 4, NULL, 2000.00, NULL, NULL, '2018-09-20', NULL, NULL, NULL, NULL, NULL),
	(49, 49, NULL, 4, NULL, 2000.00, NULL, NULL, '2018-09-20', NULL, NULL, NULL, NULL, NULL),
	(50, 50, NULL, 4, NULL, 2000.00, NULL, NULL, '2018-09-20', NULL, NULL, NULL, NULL, NULL),
	(51, 51, NULL, 4, NULL, 2000.00, NULL, NULL, '2018-09-20', NULL, NULL, NULL, NULL, NULL),
	(52, 52, NULL, 4, NULL, 2000.00, NULL, NULL, '2018-09-20', NULL, NULL, NULL, NULL, NULL),
	(53, 53, NULL, 4, NULL, 2000.00, NULL, NULL, '2018-09-20', NULL, NULL, NULL, NULL, NULL),
	(54, 54, NULL, 4, NULL, 2000.00, NULL, NULL, '2018-09-20', NULL, NULL, NULL, NULL, NULL),
	(55, 55, NULL, 4, NULL, 2000.00, NULL, NULL, '2018-09-20', NULL, NULL, NULL, NULL, NULL),
	(56, 56, NULL, 4, NULL, 2000.00, NULL, NULL, '2018-09-20', NULL, NULL, NULL, NULL, NULL),
	(57, 57, NULL, 4, NULL, 2000.00, NULL, NULL, '2018-09-20', NULL, NULL, NULL, NULL, NULL),
	(58, 58, NULL, 4, NULL, 2000.00, NULL, NULL, '2018-09-20', NULL, NULL, NULL, NULL, NULL),
	(59, 59, NULL, 4, NULL, 2000.00, NULL, NULL, '2018-09-20', NULL, NULL, NULL, NULL, NULL),
	(60, 60, NULL, 4, NULL, 2000.00, NULL, NULL, '2018-09-20', NULL, NULL, NULL, NULL, NULL),
	(61, 61, NULL, 4, NULL, 2000.00, NULL, NULL, '2018-09-20', NULL, NULL, NULL, NULL, NULL),
	(62, 62, NULL, 4, NULL, 2000.00, NULL, NULL, '2018-09-20', NULL, NULL, NULL, NULL, NULL),
	(63, 63, NULL, 4, NULL, 2000.00, NULL, NULL, '2018-09-20', NULL, NULL, NULL, NULL, NULL),
	(64, 64, NULL, 4, NULL, 2000.00, NULL, NULL, '2018-09-20', NULL, NULL, NULL, NULL, NULL),
	(65, 65, NULL, 4, NULL, 2000.00, NULL, NULL, '2018-09-20', NULL, NULL, NULL, NULL, NULL),
	(66, 66, NULL, 4, NULL, 2000.00, NULL, NULL, '2018-09-20', NULL, NULL, NULL, NULL, NULL),
	(67, 67, NULL, 4, NULL, 2000.00, NULL, NULL, '2018-09-20', NULL, NULL, NULL, NULL, NULL),
	(68, 68, NULL, 4, NULL, 2000.00, NULL, NULL, '2018-09-20', NULL, NULL, NULL, NULL, NULL),
	(69, 69, NULL, 4, NULL, 2000.00, NULL, NULL, '2018-09-20', NULL, NULL, NULL, NULL, NULL),
	(70, 70, NULL, 4, NULL, 1363.65, NULL, NULL, '2018-09-20', NULL, NULL, NULL, NULL, NULL),
	(71, 71, NULL, 4, NULL, 2000.00, NULL, NULL, '2018-09-20', NULL, NULL, NULL, NULL, NULL),
	(72, 72, NULL, 4, NULL, 2000.00, NULL, NULL, '2018-09-20', NULL, NULL, NULL, NULL, NULL),
	(73, 73, NULL, 4, NULL, 2000.00, NULL, NULL, '2018-09-20', NULL, NULL, NULL, NULL, NULL),
	(74, 74, NULL, 4, NULL, 2000.00, NULL, NULL, '2018-09-20', NULL, NULL, NULL, NULL, NULL),
	(75, 75, NULL, 4, NULL, 2000.00, NULL, NULL, '2018-09-20', NULL, NULL, NULL, NULL, NULL),
	(76, 76, NULL, 4, NULL, 2000.00, NULL, NULL, '2018-09-20', NULL, NULL, NULL, NULL, NULL),
	(77, 77, NULL, 4, NULL, 2000.00, NULL, NULL, '2018-09-20', NULL, NULL, NULL, NULL, NULL),
	(78, 78, NULL, 4, NULL, 909.10, NULL, NULL, '2018-09-20', NULL, NULL, NULL, NULL, NULL),
	(79, 79, NULL, 4, NULL, 2000.00, NULL, NULL, '2018-09-20', NULL, NULL, NULL, NULL, NULL),
	(80, 80, NULL, 4, NULL, 2000.00, NULL, NULL, '2018-09-20', NULL, NULL, NULL, NULL, NULL),
	(81, 81, NULL, 4, NULL, 2000.00, NULL, NULL, '2018-09-20', NULL, NULL, NULL, NULL, NULL),
	(82, 82, NULL, 4, NULL, 2000.00, NULL, NULL, '2018-09-20', NULL, NULL, NULL, NULL, NULL),
	(83, 83, NULL, 4, NULL, 2000.00, NULL, NULL, '2018-09-20', NULL, NULL, NULL, NULL, NULL),
	(84, 84, NULL, 4, NULL, 2000.00, NULL, NULL, '2018-09-20', NULL, NULL, NULL, NULL, NULL),
	(85, 85, NULL, 4, NULL, 2000.00, NULL, NULL, '2018-09-20', NULL, NULL, NULL, NULL, NULL),
	(86, 86, NULL, 4, NULL, 2000.00, NULL, NULL, '2018-09-20', NULL, NULL, NULL, NULL, NULL),
	(87, 87, NULL, 4, NULL, 2000.00, NULL, NULL, '2018-09-20', NULL, NULL, NULL, NULL, NULL),
	(88, 88, NULL, 4, NULL, 2000.00, NULL, NULL, '2018-09-20', NULL, NULL, NULL, NULL, NULL),
	(89, 89, NULL, 4, NULL, 2000.00, NULL, NULL, '2018-09-20', NULL, NULL, NULL, NULL, NULL),
	(90, 1, NULL, 5, NULL, 5000.00, NULL, NULL, '2018-09-20', NULL, NULL, NULL, NULL, NULL),
	(91, 2, NULL, 5, NULL, 7500.00, NULL, NULL, '2018-09-20', NULL, NULL, NULL, NULL, NULL),
	(92, 15, NULL, 5, NULL, 5000.00, NULL, NULL, '2018-09-20', NULL, NULL, NULL, NULL, NULL),
	(93, 16, NULL, 5, NULL, 5000.00, NULL, NULL, '2018-09-20', NULL, NULL, NULL, NULL, NULL),
	(94, 20, NULL, 5, NULL, 5000.00, NULL, NULL, '2018-09-20', NULL, NULL, NULL, NULL, NULL),
	(95, 23, NULL, 5, NULL, 7500.00, NULL, NULL, '2018-09-20', NULL, NULL, NULL, NULL, NULL),
	(96, 30, NULL, 5, NULL, 5000.00, NULL, NULL, '2018-09-20', NULL, NULL, NULL, NULL, NULL),
	(97, 38, NULL, 5, NULL, 7500.00, NULL, NULL, '2018-09-20', NULL, NULL, NULL, NULL, NULL),
	(98, 39, NULL, 5, NULL, 5000.00, NULL, NULL, '2018-09-20', NULL, NULL, NULL, NULL, NULL),
	(99, 42, NULL, 5, NULL, 11000.00, NULL, NULL, '2018-09-20', NULL, NULL, NULL, NULL, NULL),
	(100, 45, NULL, 5, NULL, 9000.00, NULL, NULL, '2018-09-20', NULL, NULL, NULL, NULL, NULL),
	(101, 46, NULL, 5, NULL, 5000.00, NULL, NULL, '2018-09-20', NULL, NULL, NULL, NULL, NULL),
	(102, 47, NULL, 5, NULL, 5000.00, NULL, NULL, '2018-09-20', NULL, NULL, NULL, NULL, NULL),
	(103, 50, NULL, 5, NULL, 5000.00, NULL, NULL, '2018-09-20', NULL, NULL, NULL, NULL, NULL),
	(104, 54, NULL, 5, NULL, 5000.00, NULL, NULL, '2018-09-20', NULL, NULL, NULL, NULL, NULL),
	(105, 55, NULL, 5, NULL, 5000.00, NULL, NULL, '2018-09-20', NULL, NULL, NULL, NULL, NULL),
	(106, 57, NULL, 5, NULL, 5000.00, NULL, NULL, '2018-09-20', NULL, NULL, NULL, NULL, NULL),
	(107, 61, NULL, 5, NULL, 5000.00, NULL, NULL, '2018-09-20', NULL, NULL, NULL, NULL, NULL),
	(108, 64, NULL, 5, NULL, 5000.00, NULL, NULL, '2018-09-20', NULL, NULL, NULL, NULL, NULL),
	(109, 66, NULL, 5, NULL, 5000.00, NULL, NULL, '2018-09-20', NULL, NULL, NULL, NULL, NULL),
	(110, 67, NULL, 5, NULL, 5000.00, NULL, NULL, '2018-09-20', NULL, NULL, NULL, NULL, NULL),
	(111, 1, NULL, 6, NULL, 5000.00, NULL, NULL, '2018-09-20', NULL, NULL, NULL, NULL, NULL),
	(112, 16, NULL, 6, NULL, 5000.00, NULL, NULL, '2018-09-20', NULL, NULL, NULL, NULL, NULL),
	(113, 20, NULL, 6, NULL, 5000.00, NULL, NULL, '2018-09-20', NULL, NULL, NULL, NULL, NULL),
	(114, 30, NULL, 6, NULL, 5000.00, NULL, NULL, '2018-09-20', NULL, NULL, NULL, NULL, NULL),
	(115, 39, NULL, 6, NULL, 5000.00, NULL, NULL, '2018-09-20', NULL, NULL, NULL, NULL, NULL),
	(116, 42, NULL, 6, NULL, 11000.00, NULL, NULL, '2018-09-20', NULL, NULL, NULL, NULL, NULL),
	(117, 46, NULL, 6, NULL, 5000.00, NULL, NULL, '2018-09-20', NULL, NULL, NULL, NULL, NULL),
	(118, 50, NULL, 6, NULL, 5000.00, NULL, NULL, '2018-09-20', NULL, NULL, NULL, NULL, NULL),
	(119, 54, NULL, 6, NULL, 5000.00, NULL, NULL, '2018-09-20', NULL, NULL, NULL, NULL, NULL),
	(120, 55, NULL, 6, NULL, 5000.00, NULL, NULL, '2018-09-20', NULL, NULL, NULL, NULL, NULL),
	(121, 57, NULL, 6, NULL, 5000.00, NULL, NULL, '2018-09-20', NULL, NULL, NULL, NULL, NULL),
	(122, 61, NULL, 6, NULL, 5000.00, NULL, NULL, '2018-09-20', NULL, NULL, NULL, NULL, NULL),
	(123, 64, NULL, 6, NULL, 5000.00, NULL, NULL, '2018-09-20', NULL, NULL, NULL, NULL, NULL),
	(124, 66, NULL, 6, NULL, 5000.00, NULL, NULL, '2018-09-20', NULL, NULL, NULL, NULL, NULL),
	(125, 67, NULL, 6, NULL, 5000.00, NULL, NULL, '2018-09-20', NULL, NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `pms_benefitsinfo` ENABLE KEYS */;

-- Dumping structure for table pids_hris.pms_deductioninfo
DROP TABLE IF EXISTS `pms_deductioninfo`;
CREATE TABLE IF NOT EXISTS `pms_deductioninfo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employeeinfo_id` int(11) DEFAULT NULL,
  `employee_id` int(11) DEFAULT NULL,
  `deduction_id` int(11) DEFAULT NULL,
  `deduct_pay_period` varchar(225) DEFAULT NULL,
  `deduction_rate` decimal(9,2) DEFAULT NULL,
  `deduct_amount` decimal(9,2) DEFAULT NULL,
  `deduct_date_start` date DEFAULT NULL,
  `deduct_date_end` date DEFAULT NULL,
  `deduct_date_terminated` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=138 DEFAULT CHARSET=latin1;

-- Dumping data for table pids_hris.pms_deductioninfo: ~137 rows (approximately)
/*!40000 ALTER TABLE `pms_deductioninfo` DISABLE KEYS */;
INSERT INTO `pms_deductioninfo` (`id`, `employeeinfo_id`, `employee_id`, `deduction_id`, `deduct_pay_period`, `deduction_rate`, `deduct_amount`, `deduct_date_start`, `deduct_date_end`, `deduct_date_terminated`, `created_at`, `updated_at`, `updated_by`) VALUES
	(1, NULL, 1, 8, NULL, NULL, 5375.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(2, NULL, 3, 8, NULL, NULL, 3500.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(3, NULL, 4, 8, NULL, NULL, 1000.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(4, NULL, 5, 8, NULL, NULL, 5863.75, NULL, NULL, NULL, NULL, NULL, NULL),
	(5, NULL, 6, 8, NULL, NULL, 10000.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(6, NULL, 7, 8, NULL, NULL, 4783.34, NULL, NULL, NULL, NULL, NULL, NULL),
	(7, NULL, 8, 8, NULL, NULL, 3933.33, NULL, NULL, NULL, NULL, NULL, NULL),
	(8, NULL, 9, 8, NULL, NULL, 4100.10, NULL, NULL, NULL, NULL, NULL, NULL),
	(9, NULL, 10, 8, NULL, NULL, 3600.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(10, NULL, 11, 8, NULL, NULL, 3200.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(11, NULL, 12, 8, NULL, NULL, 3650.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(12, NULL, 13, 8, NULL, NULL, 7337.58, NULL, NULL, NULL, NULL, NULL, NULL),
	(13, NULL, 16, 8, NULL, NULL, 14000.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(14, NULL, 18, 8, NULL, NULL, 500.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(15, NULL, 19, 8, NULL, NULL, 13000.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(16, NULL, 21, 8, NULL, NULL, 9613.05, NULL, NULL, NULL, NULL, NULL, NULL),
	(17, NULL, 22, 8, NULL, NULL, 12733.45, NULL, NULL, NULL, NULL, NULL, NULL),
	(18, NULL, 23, 8, NULL, NULL, 6833.33, NULL, NULL, NULL, NULL, NULL, NULL),
	(19, NULL, 24, 8, NULL, NULL, 4000.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(20, NULL, 25, 8, NULL, NULL, 6166.67, NULL, NULL, NULL, NULL, NULL, NULL),
	(21, NULL, 26, 8, NULL, NULL, 8516.67, NULL, NULL, NULL, NULL, NULL, NULL),
	(22, NULL, 27, 8, NULL, NULL, 10569.27, NULL, NULL, NULL, NULL, NULL, NULL),
	(23, NULL, 28, 8, NULL, NULL, 11797.56, NULL, NULL, NULL, NULL, NULL, NULL),
	(24, NULL, 29, 8, NULL, NULL, 12550.78, NULL, NULL, NULL, NULL, NULL, NULL),
	(25, NULL, 31, 8, NULL, NULL, 11002.77, NULL, NULL, NULL, NULL, NULL, NULL),
	(26, NULL, 32, 8, NULL, NULL, 500.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(27, NULL, 33, 8, NULL, NULL, 7285.46, NULL, NULL, NULL, NULL, NULL, NULL),
	(28, NULL, 34, 8, NULL, NULL, 1000.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(29, NULL, 35, 8, NULL, NULL, 14040.46, NULL, NULL, NULL, NULL, NULL, NULL),
	(30, NULL, 36, 8, NULL, NULL, 2966.67, NULL, NULL, NULL, NULL, NULL, NULL),
	(31, NULL, 39, 8, NULL, NULL, 2000.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(32, NULL, 40, 8, NULL, NULL, 10224.60, NULL, NULL, NULL, NULL, NULL, NULL),
	(33, NULL, 42, 8, NULL, NULL, 10000.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(34, NULL, 43, 8, NULL, NULL, 14620.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(35, NULL, 44, 8, NULL, NULL, 7315.83, NULL, NULL, NULL, NULL, NULL, NULL),
	(36, NULL, 46, 8, NULL, NULL, 1700.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(37, NULL, 48, 8, NULL, NULL, 2500.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(38, NULL, 49, 8, NULL, NULL, 3700.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(39, NULL, 50, 8, NULL, NULL, 3368.79, NULL, NULL, NULL, NULL, NULL, NULL),
	(40, NULL, 51, 8, NULL, NULL, 500.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(41, NULL, 52, 8, NULL, NULL, 8750.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(42, NULL, 53, 8, NULL, NULL, 5000.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(43, NULL, 54, 8, NULL, NULL, 4000.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(44, NULL, 55, 8, NULL, NULL, 7867.65, NULL, NULL, NULL, NULL, NULL, NULL),
	(45, NULL, 56, 8, NULL, NULL, 6045.78, NULL, NULL, NULL, NULL, NULL, NULL),
	(46, NULL, 57, 8, NULL, NULL, 2100.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(47, NULL, 58, 8, NULL, NULL, 3940.28, NULL, NULL, NULL, NULL, NULL, NULL),
	(48, NULL, 59, 8, NULL, NULL, 14913.23, NULL, NULL, NULL, NULL, NULL, NULL),
	(49, NULL, 61, 8, NULL, NULL, 8040.89, NULL, NULL, NULL, NULL, NULL, NULL),
	(50, NULL, 62, 8, NULL, NULL, 2933.45, NULL, NULL, NULL, NULL, NULL, NULL),
	(51, NULL, 63, 8, NULL, NULL, 4368.79, NULL, NULL, NULL, NULL, NULL, NULL),
	(52, NULL, 64, 8, NULL, NULL, 4670.47, NULL, NULL, NULL, NULL, NULL, NULL),
	(53, NULL, 65, 8, NULL, NULL, 1210.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(54, NULL, 68, 8, NULL, NULL, 14677.12, NULL, NULL, NULL, NULL, NULL, NULL),
	(55, NULL, 69, 8, NULL, NULL, 1220.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(56, NULL, 71, 8, NULL, NULL, 12493.33, NULL, NULL, NULL, NULL, NULL, NULL),
	(57, NULL, 72, 8, NULL, NULL, 5368.79, NULL, NULL, NULL, NULL, NULL, NULL),
	(58, NULL, 73, 8, NULL, NULL, 8966.66, NULL, NULL, NULL, NULL, NULL, NULL),
	(59, NULL, 74, 8, NULL, NULL, 4368.79, NULL, NULL, NULL, NULL, NULL, NULL),
	(60, NULL, 79, 8, NULL, NULL, 5033.45, NULL, NULL, NULL, NULL, NULL, NULL),
	(61, NULL, 80, 8, NULL, NULL, 4850.49, NULL, NULL, NULL, NULL, NULL, NULL),
	(62, NULL, 84, 8, NULL, NULL, 5650.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(63, NULL, 86, 8, NULL, NULL, 10000.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(64, NULL, 89, 8, NULL, NULL, 7033.35, NULL, NULL, NULL, NULL, NULL, NULL),
	(65, 2, 1, 9, NULL, 0.05, 3379.25, NULL, NULL, NULL, '2018-09-22 11:15:30', '2018-09-22 11:15:30', NULL),
	(66, NULL, 2, 9, NULL, 0.15, 15332.55, NULL, NULL, NULL, NULL, NULL, NULL),
	(67, NULL, 3, 9, NULL, 0.10, 1267.40, NULL, NULL, NULL, NULL, NULL, NULL),
	(68, NULL, 4, 9, NULL, 0.05, 2168.55, NULL, NULL, NULL, NULL, NULL, NULL),
	(69, NULL, 5, 9, NULL, 0.05, 2021.65, NULL, NULL, NULL, NULL, NULL, NULL),
	(70, NULL, 6, 9, NULL, 0.10, 3808.50, NULL, NULL, NULL, NULL, NULL, NULL),
	(71, NULL, 7, 9, NULL, 0.10, 1331.50, NULL, NULL, NULL, NULL, NULL, NULL),
	(72, NULL, 8, 9, NULL, 0.05, 649.50, NULL, NULL, NULL, NULL, NULL, NULL),
	(73, NULL, 9, 9, NULL, 0.05, 935.90, NULL, NULL, NULL, NULL, NULL, NULL),
	(74, NULL, 10, 9, NULL, 0.10, 1445.90, NULL, NULL, NULL, NULL, NULL, NULL),
	(75, NULL, 11, 9, NULL, 0.10, 1288.40, NULL, NULL, NULL, NULL, NULL, NULL),
	(76, NULL, 12, 9, NULL, 0.05, 633.70, NULL, NULL, NULL, NULL, NULL, NULL),
	(77, NULL, 13, 9, NULL, 0.02, 580.20, NULL, NULL, NULL, NULL, NULL, NULL),
	(78, NULL, 14, 9, NULL, 0.05, 1450.50, NULL, NULL, NULL, NULL, NULL, NULL),
	(79, NULL, 15, 9, NULL, 0.05, 3664.95, NULL, NULL, NULL, NULL, NULL, NULL),
	(80, NULL, 16, 9, NULL, 0.05, 3664.95, NULL, NULL, NULL, NULL, NULL, NULL),
	(81, NULL, 17, 9, NULL, 0.20, 4035.80, NULL, NULL, NULL, NULL, NULL, NULL),
	(82, NULL, 18, 9, NULL, 0.10, 1342.40, NULL, NULL, NULL, NULL, NULL, NULL),
	(83, NULL, 19, 9, NULL, 0.10, 3808.50, NULL, NULL, NULL, NULL, NULL, NULL),
	(84, NULL, 20, 9, NULL, 0.20, 13120.80, NULL, NULL, NULL, NULL, NULL, NULL),
	(85, NULL, 21, 9, NULL, 0.10, 2150.20, NULL, NULL, NULL, NULL, NULL, NULL),
	(86, NULL, 22, 9, NULL, 0.15, 6138.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(87, NULL, 23, 9, NULL, 0.15, 14023.20, NULL, NULL, NULL, NULL, NULL, NULL),
	(88, NULL, 24, 9, NULL, 0.02, 536.12, NULL, NULL, NULL, NULL, NULL, NULL),
	(89, NULL, 25, 9, NULL, 0.02, 770.86, NULL, NULL, NULL, NULL, NULL, NULL),
	(90, NULL, 26, 9, NULL, 0.12, 7151.64, NULL, NULL, NULL, NULL, NULL, NULL),
	(91, NULL, 27, 9, NULL, 0.05, 2201.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(92, NULL, 28, 9, NULL, 0.05, 2201.00, NULL, NULL, NULL, NULL, NULL, NULL),
	(93, NULL, 29, 9, NULL, 0.05, 2979.85, NULL, NULL, NULL, NULL, NULL, NULL),
	(94, NULL, 30, 9, NULL, 0.15, 9073.65, NULL, NULL, NULL, NULL, NULL, NULL),
	(95, NULL, 31, 9, NULL, 0.20, 11919.40, NULL, NULL, NULL, NULL, NULL, NULL),
	(96, NULL, 32, 9, NULL, 0.06, 1123.08, NULL, NULL, NULL, NULL, NULL, NULL),
	(97, NULL, 33, 9, NULL, 0.03, 880.77, NULL, NULL, NULL, NULL, NULL, NULL),
	(98, NULL, 34, 9, NULL, 0.10, 3808.50, NULL, NULL, NULL, NULL, NULL, NULL),
	(99, NULL, 35, 9, NULL, 0.15, 4351.50, NULL, NULL, NULL, NULL, NULL, NULL),
	(100, NULL, 36, 9, NULL, 0.20, 5751.80, NULL, NULL, NULL, NULL, NULL, NULL),
	(101, NULL, 37, 9, NULL, 0.05, 2021.65, NULL, NULL, NULL, NULL, NULL, NULL),
	(102, NULL, 38, 9, NULL, 0.20, 18697.60, NULL, NULL, NULL, NULL, NULL, NULL),
	(103, NULL, 39, 9, NULL, 0.08, 5248.32, NULL, NULL, NULL, NULL, NULL, NULL),
	(104, NULL, 40, 9, NULL, 0.10, 7329.90, NULL, NULL, NULL, NULL, NULL, NULL),
	(105, NULL, 41, 9, NULL, 0.10, 5871.70, NULL, NULL, NULL, NULL, NULL, NULL),
	(106, NULL, 42, 9, NULL, 0.20, 28706.80, NULL, NULL, NULL, NULL, NULL, NULL),
	(107, NULL, 43, 9, NULL, 0.05, 4007.15, NULL, NULL, NULL, NULL, NULL, NULL),
	(108, NULL, 44, 9, NULL, 0.10, 4774.20, NULL, NULL, NULL, NULL, NULL, NULL),
	(109, NULL, 45, 9, NULL, 0.20, 22996.20, NULL, NULL, NULL, NULL, NULL, NULL),
	(110, NULL, 46, 9, NULL, 0.05, 4121.95, NULL, NULL, NULL, NULL, NULL, NULL),
	(111, NULL, 47, 9, NULL, 0.20, 18697.60, NULL, NULL, NULL, NULL, NULL, NULL),
	(112, NULL, 48, 9, NULL, 0.05, 1588.25, NULL, NULL, NULL, NULL, NULL, NULL),
	(113, NULL, 49, 9, NULL, 0.05, 935.90, NULL, NULL, NULL, NULL, NULL, NULL),
	(114, NULL, 50, 9, NULL, 0.20, 19262.40, NULL, NULL, NULL, NULL, NULL, NULL),
	(115, NULL, 51, 9, NULL, 0.20, 4844.80, NULL, NULL, NULL, NULL, NULL, NULL),
	(116, NULL, 52, 9, NULL, 0.05, 935.90, NULL, NULL, NULL, NULL, NULL, NULL),
	(117, NULL, 53, 9, NULL, 0.20, 12279.40, NULL, NULL, NULL, NULL, NULL, NULL),
	(118, NULL, 54, 9, NULL, 0.20, 18421.60, NULL, NULL, NULL, NULL, NULL, NULL),
	(119, NULL, 55, 9, NULL, 0.05, 4605.40, NULL, NULL, NULL, NULL, NULL, NULL),
	(120, NULL, 56, 9, NULL, 0.02, 1174.34, NULL, NULL, NULL, NULL, NULL, NULL),
	(121, NULL, 57, 9, NULL, 0.20, 20443.40, NULL, NULL, NULL, NULL, NULL, NULL),
	(122, NULL, 58, 9, NULL, 0.05, 935.90, NULL, NULL, NULL, NULL, NULL, NULL),
	(123, NULL, 59, 9, NULL, 0.05, 2168.55, NULL, NULL, NULL, NULL, NULL, NULL),
	(124, NULL, 60, 9, NULL, 0.10, 2422.40, NULL, NULL, NULL, NULL, NULL, NULL),
	(125, NULL, 61, 9, NULL, 0.05, 5110.85, NULL, NULL, NULL, NULL, NULL, NULL),
	(126, NULL, 62, 9, NULL, 0.05, 2935.85, NULL, NULL, NULL, NULL, NULL, NULL),
	(127, NULL, 63, 9, NULL, 0.08, 3367.92, NULL, NULL, NULL, NULL, NULL, NULL),
	(128, NULL, 64, 9, NULL, 0.20, 16487.80, NULL, NULL, NULL, NULL, NULL, NULL),
	(129, NULL, 65, 9, NULL, 0.15, 9073.65, NULL, NULL, NULL, NULL, NULL, NULL),
	(130, NULL, 66, 9, NULL, 0.05, 4674.40, NULL, NULL, NULL, NULL, NULL, NULL),
	(131, NULL, 68, 9, NULL, 0.10, 6139.70, NULL, NULL, NULL, NULL, NULL, NULL),
	(132, NULL, 69, 9, NULL, 0.05, 1211.20, NULL, NULL, NULL, NULL, NULL, NULL),
	(133, NULL, 70, 9, NULL, 0.12, 2598.95, NULL, NULL, NULL, NULL, NULL, NULL),
	(134, NULL, 71, 9, NULL, 0.05, 1211.20, NULL, NULL, NULL, NULL, NULL, NULL),
	(135, NULL, 72, 9, NULL, 0.05, 1950.35, NULL, NULL, NULL, NULL, NULL, NULL),
	(136, NULL, 73, 9, NULL, 0.20, 12098.20, NULL, NULL, NULL, NULL, NULL, NULL),
	(137, NULL, 74, 9, NULL, 0.05, 1485.65, NULL, NULL, NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `pms_deductioninfo` ENABLE KEYS */;

-- Dumping structure for table pids_hris.pms_deductioninfo_transactions
DROP TABLE IF EXISTS `pms_deductioninfo_transactions`;
CREATE TABLE IF NOT EXISTS `pms_deductioninfo_transactions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) DEFAULT NULL,
  `transaction_id` int(11) DEFAULT NULL,
  `deduction_id` int(11) DEFAULT NULL,
  `deduction_info_id` int(11) DEFAULT NULL,
  `amount` decimal(9,2) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `year` varchar(50) DEFAULT NULL,
  `month` varchar(50) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=275 DEFAULT CHARSET=latin1;

-- Dumping data for table pids_hris.pms_deductioninfo_transactions: ~2 rows (approximately)
/*!40000 ALTER TABLE `pms_deductioninfo_transactions` DISABLE KEYS */;
INSERT INTO `pms_deductioninfo_transactions` (`id`, `employee_id`, `transaction_id`, `deduction_id`, `deduction_info_id`, `amount`, `status`, `year`, `month`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
	(163, 79, NULL, 8, 60, 5033.45, 'deductions', '2018', 'January', 2, NULL, '2018-10-15 10:17:04', '2018-10-15 10:17:04'),
	(168, 80, NULL, 8, 61, 4850.49, 'deductions', '2018', 'January', 2, NULL, '2018-10-15 10:17:05', '2018-10-15 10:17:05');
/*!40000 ALTER TABLE `pms_deductioninfo_transactions` ENABLE KEYS */;

-- Dumping structure for table pids_hris.pms_departments
DROP TABLE IF EXISTS `pms_departments`;
CREATE TABLE IF NOT EXISTS `pms_departments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(50) DEFAULT NULL,
  `name` varchar(225) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

-- Dumping data for table pids_hris.pms_departments: ~15 rows (approximately)
/*!40000 ALTER TABLE `pms_departments` DISABLE KEYS */;
INSERT INTO `pms_departments` (`id`, `code`, `name`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
	(1, NULL, 'Administrative & Finance Staff', NULL, NULL, NULL, NULL),
	(2, NULL, 'Project Services Dept. Staff', NULL, NULL, NULL, NULL),
	(3, NULL, 'Management & Information System Staff', NULL, NULL, NULL, NULL),
	(4, NULL, 'RIS - Publication', NULL, NULL, NULL, NULL),
	(5, NULL, 'RIS - Publication Affairs', NULL, NULL, NULL, NULL),
	(6, NULL, 'Research (OP)', NULL, NULL, NULL, NULL),
	(7, NULL, 'Research (OVP)', NULL, NULL, NULL, NULL),
	(8, NULL, 'Research Staff', NULL, NULL, NULL, NULL),
	(9, NULL, 'PASCN', NULL, NULL, NULL, NULL),
	(10, NULL, 'PIDS (in-house) Project', NULL, NULL, NULL, NULL),
	(11, NULL, 'PIDS - MECO Project', NULL, NULL, NULL, NULL),
	(12, NULL, 'PIDS - ACIAR Project', NULL, NULL, NULL, NULL),
	(13, NULL, 'PIDS  3iE "PWP.02.DSWD.IE  Project', NULL, NULL, NULL, NULL),
	(14, NULL, 'PIDS - DILG Project', NULL, NULL, NULL, NULL),
	(15, NULL, 'PIDS - DSWD Project', NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `pms_departments` ENABLE KEYS */;

-- Dumping structure for table pids_hris.pms_divisions
DROP TABLE IF EXISTS `pms_divisions`;
CREATE TABLE IF NOT EXISTS `pms_divisions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(50) DEFAULT NULL,
  `name` varchar(225) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=latin1;

-- Dumping data for table pids_hris.pms_divisions: ~35 rows (approximately)
/*!40000 ALTER TABLE `pms_divisions` DISABLE KEYS */;
INSERT INTO `pms_divisions` (`id`, `code`, `name`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
	(1, '', 'Office of Chair AMB', NULL, NULL, NULL, NULL),
	(2, '', 'Office of Comm JRB', NULL, NULL, NULL, NULL),
	(3, '', 'Office of Comm SAQ', NULL, NULL, NULL, NULL),
	(4, '', 'Office of the Director', NULL, NULL, NULL, NULL),
	(5, '', 'Agency-to-Agency', NULL, NULL, NULL, NULL),
	(6, '', 'Economic Investigation Division', NULL, NULL, NULL, NULL),
	(7, 'LID', 'Litigation Division', NULL, '2018-07-25 05:04:22', NULL, 2),
	(8, '', 'General Services Division', NULL, NULL, NULL, NULL),
	(9, '', 'Notification Division', NULL, NULL, NULL, NULL),
	(10, '', 'Monitoring and Investigation Division', NULL, NULL, NULL, NULL),
	(11, '', 'Corporate Planning and Management Division', NULL, NULL, NULL, NULL),
	(12, '', 'M & A Review Division', NULL, NULL, NULL, NULL),
	(13, 'IMD', 'Information Management Division', NULL, '2018-07-25 04:55:47', NULL, NULL),
	(14, '', 'Policy and Markets Division', NULL, NULL, NULL, NULL),
	(15, '', 'Human Resources Development Division', NULL, NULL, NULL, NULL),
	(16, '', 'Training and Advocacy Division', NULL, NULL, NULL, NULL),
	(17, '', 'CKMO', NULL, NULL, NULL, NULL),
	(18, '', 'Budget Division', NULL, NULL, NULL, NULL),
	(19, '', 'Internal Legal Services', NULL, NULL, NULL, NULL),
	(20, '', 'Office of Comm ERB', NULL, NULL, NULL, NULL),
	(21, '', 'Accounting Division', NULL, NULL, NULL, NULL),
	(22, '', 'Policy Research Division', NULL, NULL, NULL, NULL),
	(23, '', 'Information and Communications Technology Division', NULL, NULL, NULL, NULL),
	(24, '', 'Office of Comm ACA', NULL, NULL, NULL, NULL),
	(25, '', 'Office of the Executive Director', NULL, NULL, NULL, NULL),
	(26, '', 'ALO', NULL, NULL, NULL, NULL),
	(27, '', 'Office of the Chairman', NULL, NULL, NULL, NULL),
	(28, '', 'EO', NULL, NULL, NULL, NULL),
	(29, '', 'Training Division', NULL, NULL, NULL, NULL),
	(30, '', 'Policy Research and Development Division', NULL, NULL, NULL, NULL),
	(31, '', 'Adjudication Division', NULL, NULL, NULL, NULL),
	(32, '', 'Merger and Acquisition Division', NULL, NULL, NULL, NULL),
	(33, '', 'Knowledge Management Division', NULL, NULL, NULL, NULL),
	(34, '', 'Legal Division', NULL, NULL, NULL, NULL),
	(36, 'LD', 'Legal Division', '2018-07-25 05:06:09', '2018-07-25 05:06:09', 2, NULL);
/*!40000 ALTER TABLE `pms_divisions` ENABLE KEYS */;

-- Dumping structure for table pids_hris.pms_employees
DROP TABLE IF EXISTS `pms_employees`;
CREATE TABLE IF NOT EXISTS `pms_employees` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `employee_number` varchar(225) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `lastname` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `firstname` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `middlename` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `extension_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nickname` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telephone_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `birth_place` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gender` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `civil_status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `citizenship_id` int(11) DEFAULT NULL,
  `citizenship_country_id` int(11) DEFAULT NULL,
  `filipino` tinyint(1) DEFAULT NULL,
  `naturalized` tinyint(1) DEFAULT NULL,
  `height` decimal(4,2) DEFAULT NULL,
  `weight` decimal(4,2) DEFAULT NULL,
  `blood_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pagibig` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gsis` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `philhealth` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tin` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sss` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `govt_issued_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `govt_issued_id_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `govt_issued_id_place` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `govt_issued_id_date_issued` date DEFAULT NULL,
  `govt_issued_valid_until` date DEFAULT NULL,
  `agency_employee_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `biometrics` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `house_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `street` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subdivision` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `brgy` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `province_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  `permanent_house_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permanent_street` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permanent_subdivision` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permanent_brgy` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permanent_city_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permanent_province_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permanent_country_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permanent_telephone_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image_path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `active` int(11) DEFAULT NULL,
  `with_setup` int(11) DEFAULT NULL,
  `remarks` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=90 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table pids_hris.pms_employees: ~89 rows (approximately)
/*!40000 ALTER TABLE `pms_employees` DISABLE KEYS */;
INSERT INTO `pms_employees` (`id`, `employee_number`, `company_id`, `branch_id`, `lastname`, `firstname`, `middlename`, `extension_name`, `nickname`, `contact_number`, `mobile_number`, `telephone_number`, `birthday`, `birth_place`, `email_address`, `gender`, `civil_status`, `citizenship_id`, `citizenship_country_id`, `filipino`, `naturalized`, `height`, `weight`, `blood_type`, `pagibig`, `gsis`, `philhealth`, `tin`, `sss`, `govt_issued_id`, `govt_issued_id_number`, `govt_issued_id_place`, `govt_issued_id_date_issued`, `govt_issued_valid_until`, `agency_employee_number`, `biometrics`, `house_number`, `street`, `subdivision`, `brgy`, `city_id`, `province_id`, `country_id`, `permanent_house_number`, `permanent_street`, `permanent_subdivision`, `permanent_brgy`, `permanent_city_id`, `permanent_province_id`, `permanent_country_id`, `permanent_telephone_number`, `image_path`, `active`, `with_setup`, `remarks`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, '015539', NULL, NULL, 'ABOGADO', 'MARISA', 'S', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, '2018-09-22 04:32:18', NULL),
	(2, '010065', NULL, NULL, 'AGCAOILI', 'ANDREA', 'S', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, '2018-09-22 04:32:34', NULL),
	(3, '015608', NULL, NULL, 'ANTONIO', 'CHRISTIAN', 'E', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, '2018-09-22 04:31:37', NULL),
	(4, '015566', NULL, NULL, 'ARIOLA', 'CRISTINE', 'P', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, '2018-09-22 04:35:22', NULL),
	(5, '015481', NULL, NULL, 'ATIENZA', 'NORLITO', 'S', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, '2018-09-22 04:35:55', NULL),
	(6, '015599', NULL, NULL, 'CALLO', 'JAYMART RAMILE', 'L', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, '2018-09-22 04:44:57', NULL),
	(7, '010472', NULL, NULL, 'CAMONTOY', 'DANILO', 'M', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, '2018-09-22 04:45:15', NULL),
	(8, '015560', NULL, NULL, 'CATURAN', 'MICHAEL', 'A', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, '2018-09-22 04:45:29', NULL),
	(9, '015534', NULL, NULL, 'DEL PILAR', 'ROWEL', 'S', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, '2018-09-22 04:48:28', NULL),
	(10, NULL, NULL, NULL, 'FEDERICO', 'ASHER  JR', 'A', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, '2018-09-22 04:49:24', NULL),
	(11, '015572', NULL, NULL, 'GARCIA', 'LAMBERTO', 'F', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, '2018-09-22 04:53:50', NULL),
	(12, NULL, NULL, NULL, 'GONATICE', 'CRISTITO  JR', 'V', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, '2018-09-22 04:55:28', NULL),
	(13, '015662', NULL, NULL, 'LUNA', 'JOHN ZERNAN', 'B', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, '2018-09-22 04:59:53', NULL),
	(14, '015655', NULL, NULL, 'MAGALLONES', 'SHAROLD', 'B', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, '2018-09-22 05:00:29', NULL),
	(15, NULL, NULL, NULL, 'MIRABITE', 'JOSUE', 'O', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, '2018-09-22 05:05:49', NULL),
	(16, '015574', NULL, NULL, 'PATUAR', 'MA. DANA', 'E', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, '2018-09-22 05:14:52', NULL),
	(17, '015638', NULL, NULL, 'RAMOS', 'JOANNA THERESA', 'M', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, '2018-09-22 05:14:09', NULL),
	(18, '014605', NULL, NULL, 'SALAZAR', 'JESUS ARTHUR', 'O', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, '2018-09-22 05:12:08', NULL),
	(19, '011045', NULL, NULL, 'SALCEDO', 'NOVIE', 'D', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, '2018-09-22 05:11:54', NULL),
	(20, '015637', NULL, NULL, 'TAGANAS', 'LAYLORD XYRCHEZ', 'R', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, '2018-09-22 05:10:28', NULL),
	(21, '015434', NULL, NULL, 'UCOL', 'GRACE', 'B', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, '2018-09-22 05:07:26', NULL),
	(22, '015397', NULL, NULL, 'VICTORIA', 'MAILENE', 'C', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, '2018-09-22 05:06:58', NULL),
	(23, '015522', NULL, NULL, 'AJAYI', 'RENEE ANN JOLINA', 'C', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, '2018-09-22 04:32:49', NULL),
	(24, '015630', NULL, NULL, 'MIGUEL', 'VIVEKA', 'A', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, '2018-09-22 05:02:36', NULL),
	(25, '015628', NULL, NULL, 'OSEO', 'JAN MICHAEL', 'M', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, '2018-09-22 05:15:03', NULL),
	(26, '015547', NULL, NULL, 'SALAZAR', 'CHRISTINE RUTH', 'P', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, '2018-09-22 05:12:21', NULL),
	(27, '015565', NULL, NULL, 'FERNANDEZ', 'JOHN MARK', 'V', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, '2018-09-22 04:52:01', NULL),
	(28, '015485', NULL, NULL, 'GERIO', 'WINNIE', 'A', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, '2018-09-22 04:55:01', NULL),
	(29, '015591', NULL, NULL, 'JIMENEZ', 'DORIS', 'R', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, '2018-09-22 04:55:43', NULL),
	(30, '010090', NULL, NULL, 'ALCANTARA', 'JANE', 'C', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, '2018-09-22 04:33:17', NULL),
	(31, '013552', NULL, NULL, 'CLEOFAS', 'ROSSANA', 'P', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, '2018-09-22 04:46:19', NULL),
	(32, '015641', NULL, NULL, 'GARCIA', 'REYNALYN', 'A', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, '2018-09-22 04:54:45', NULL),
	(33, '015614', NULL, NULL, 'LAGORAS', 'CLARISSA', 'D', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, '2018-09-22 04:56:02', NULL),
	(34, '015652', NULL, NULL, 'SAN DIEGO', 'CARLA', 'P', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, '2018-09-22 05:11:28', NULL),
	(35, '015644', NULL, NULL, 'VALENCIA', 'REJINEL', 'G', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, '2018-09-22 05:07:12', NULL),
	(36, '010120', NULL, NULL, 'AQUINO', 'NECITA', 'Z', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, '2018-09-22 04:33:55', NULL),
	(37, '015486', NULL, NULL, 'MANUEL', 'MARIA GIZELLE', 'G', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, '2018-09-22 05:01:59', NULL),
	(38, '015472', NULL, NULL, 'SIAR', 'SHEILA', 'V', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, '2018-09-22 05:11:06', NULL),
	(39, '015651', NULL, NULL, 'TALIPING', 'ROWENA', 'T', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, '2018-09-22 05:10:14', NULL),
	(40, '015512', NULL, NULL, 'ASIS', 'RONINA ', 'D', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, '2018-09-22 04:35:37', NULL),
	(41, '015660', NULL, NULL, 'DABASOL', 'DHALIA ', 'O', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, '2018-09-22 04:47:03', NULL),
	(42, '014401', NULL, NULL, 'REYES', 'CELIA', 'M', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, '2018-09-22 05:13:56', NULL),
	(43, '015172', NULL, NULL, 'TOLENTINO', 'JUANITA', 'E', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, '2018-09-22 05:08:31', NULL),
	(44, '015462', NULL, NULL, 'ALMEDA', 'JOCELYN', 'P', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, '2018-09-22 04:33:30', NULL),
	(45, '012831', NULL, NULL, 'BALLESTEROS', 'MARIFE', 'M', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, '2018-09-22 04:37:13', NULL),
	(46, '015553', NULL, NULL, 'ABRIGO', 'MICHAEL RALPH', 'M', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, '2018-09-22 04:32:23', NULL),
	(47, '015546', NULL, NULL, 'ALBERT', 'JOSE RAMON', 'G', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, '2018-09-22 04:33:04', NULL),
	(48, '015636', NULL, NULL, 'ARBONEDA ', 'ARKIN', 'A', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, '2018-09-22 04:34:27', NULL),
	(49, '015590', NULL, NULL, 'BAYONA', 'JUNALYN', 'T', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, '2018-09-22 04:38:01', NULL),
	(50, '015551', NULL, NULL, 'BRIONES', 'ROEHLANO', 'M', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, '2018-09-22 04:44:27', NULL),
	(51, NULL, NULL, NULL, 'CALIZO', 'SYLWYN JR', 'C', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, '2018-09-22 04:44:43', NULL),
	(52, '010901', NULL, NULL, 'CINCO', 'EMMA', 'P', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, '2018-09-22 04:45:47', NULL),
	(53, '015430', NULL, NULL, 'CUENCA', 'JANET', 'S', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, '2018-09-22 04:46:48', NULL),
	(54, '015646', NULL, NULL, 'DACUYCUY', 'CONNIE', 'B', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, '2018-09-22 04:47:45', NULL),
	(55, '019500', NULL, NULL, 'DOMINGO', 'SONNY', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, '2018-09-22 04:48:41', NULL),
	(56, '015587', NULL, NULL, 'GALANG ', 'IVORY MYKA', 'R', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, '2018-09-22 04:54:06', NULL),
	(57, '013102', NULL, NULL, 'MANASAN', 'ROSARIO', 'G', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, '2018-09-22 05:01:07', NULL),
	(58, '013145', NULL, NULL, 'MELENDEZ', 'LUCITA', 'M', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, '2018-09-22 05:02:16', NULL),
	(59, NULL, NULL, NULL, 'MONDEZ', 'MARIA  BLESILA', 'D', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, '2018-09-22 05:06:03', NULL),
	(60, NULL, NULL, NULL, 'OLAGUERA', 'MA DIVINA', 'C', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, '2018-09-22 05:15:55', NULL),
	(61, NULL, NULL, NULL, 'ORBETA', 'ANICETO  JR', 'C', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, '2018-09-22 05:15:41', NULL),
	(62, '015564', NULL, NULL, 'ORTIZ', 'DANICA AISA', 'P', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, '2018-09-22 05:15:19', NULL),
	(63, NULL, NULL, NULL, 'ORTIZ', 'MA KRISTINA', 'P', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, '2018-09-22 05:15:30', NULL),
	(64, '015518', NULL, NULL, 'QUIMBA', 'FRANCIS MARK', 'A', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, '2018-09-22 05:14:27', NULL),
	(65, '015487', NULL, NULL, 'ROSELLON', 'MAUREEN ANE', 'D', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, '2018-09-22 05:12:39', NULL),
	(66, '015621', NULL, NULL, 'SERAFICA', 'RAMONETTE', 'B', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, '2018-09-22 05:11:17', NULL),
	(67, NULL, NULL, NULL, 'SICAT', 'CHARLOTTE JUSTINE', 'D', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, '2018-09-22 05:10:55', NULL),
	(68, '015470', NULL, NULL, 'TABUGA    ', 'AUBREY', 'D', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, '2018-09-22 05:10:41', NULL),
	(69, '015656', NULL, NULL, 'TAM', 'ZHANDRA', 'C', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, '2018-09-22 05:08:47', NULL),
	(70, '015622', NULL, NULL, 'TOLIN', 'LOVELY ANN', 'C', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, '2018-09-22 05:07:40', NULL),
	(71, '015647', NULL, NULL, 'VIZMANOS', 'JANA FLOR', 'C', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, '2018-09-22 05:00:49', NULL),
	(72, '015439', NULL, NULL, 'BELIZARIO', 'MILDRED', 'L', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, '2018-09-22 04:38:17', NULL),
	(73, '010880', NULL, NULL, 'MANTARING', 'MELALYN', 'C', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, '2018-09-22 05:01:42', NULL),
	(74, '013803', NULL, NULL, 'PIZARRO', 'SUSAN', 'I', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, '2018-09-22 05:14:39', NULL),
	(75, '015659', NULL, NULL, 'ABERILLA', 'JACHIN JANE ', 'O', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, '2018-09-22 04:32:14', NULL),
	(76, '015658', NULL, NULL, 'ANCHETA', 'JENICA', 'A', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, '2018-09-22 04:33:43', NULL),
	(77, '015649', NULL, NULL, 'BAJE', 'LORA KRYZ', 'C', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, '2018-09-22 04:36:56', NULL),
	(78, NULL, NULL, NULL, 'BARRAL', 'MARK ANTHONY', 'A', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, '2018-09-22 04:37:30', NULL),
	(79, '015627', NULL, NULL, 'BAUTISTA', 'JUN', 'A', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, '2018-09-22 04:37:43', NULL),
	(80, NULL, NULL, NULL, 'BORROMEO', 'NICOLI ARTHUR', 'B', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, '2018-09-22 04:42:43', NULL),
	(81, '015653', NULL, NULL, 'DE LA CRUZ', 'NEILLE GWEN', 'B', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, '2018-09-22 04:48:10', NULL),
	(82, NULL, NULL, NULL, 'ESCUETA', 'ROMEO  SR', 'J', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, '2018-09-22 04:49:09', NULL),
	(83, '015631', NULL, NULL, 'FLORES', 'GERWIN', 'V', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, '2018-09-22 04:53:32', NULL),
	(84, '015661', NULL, NULL, 'SALVANERA', 'ARJAN PAULO', 'S', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, '2018-09-22 05:11:40', NULL),
	(85, NULL, NULL, NULL, 'MANEJAR', 'ARVIE JOY', 'A', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, '2018-09-22 05:01:25', NULL),
	(86, '015645', NULL, NULL, 'CORPUS', 'JOHN PAUL', 'P', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, '2018-09-22 04:46:33', NULL),
	(87, '015642', NULL, NULL, 'ADARO', 'CATHARINE', 'E', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, '2018-09-22 04:32:28', NULL),
	(88, NULL, NULL, NULL, 'MADDAWIN', 'RICXIE', 'B', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, '2018-09-22 05:00:12', NULL),
	(89, '015664', NULL, NULL, 'ARAOS', 'NINA VICTORIA', 'V', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 0, NULL, NULL, '2018-09-22 04:34:10', NULL);
/*!40000 ALTER TABLE `pms_employees` ENABLE KEYS */;

-- Dumping structure for table pids_hris.pms_employeesbenefits
DROP TABLE IF EXISTS `pms_employeesbenefits`;
CREATE TABLE IF NOT EXISTS `pms_employeesbenefits` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `CompanyRefId` int(10) NOT NULL DEFAULT '0',
  `BranchRefId` int(10) NOT NULL DEFAULT '0',
  `EmployeesRefId` int(10) NOT NULL DEFAULT '0',
  `pms_EmployeesRefId` int(10) NOT NULL DEFAULT '0',
  `EffectivityDate` date DEFAULT NULL,
  `EndDate` date DEFAULT NULL,
  `withEndDate` int(1) DEFAULT NULL,
  `TaxTypeRefId` int(10) DEFAULT '0',
  `AllowanceRefId` int(10) DEFAULT '0',
  `Description` varchar(300) DEFAULT NULL,
  `PayPeriodRefId` int(10) DEFAULT '0',
  `PayrateRefId` int(10) DEFAULT '0',
  `Amount` decimal(10,2) DEFAULT '0.00',
  `Remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `RefId` (`RefId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table pids_hris.pms_employeesbenefits: ~0 rows (approximately)
/*!40000 ALTER TABLE `pms_employeesbenefits` DISABLE KEYS */;
/*!40000 ALTER TABLE `pms_employeesbenefits` ENABLE KEYS */;

-- Dumping structure for table pids_hris.pms_employeesbonus
DROP TABLE IF EXISTS `pms_employeesbonus`;
CREATE TABLE IF NOT EXISTS `pms_employeesbonus` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `CompanyRefId` int(10) NOT NULL DEFAULT '0',
  `BranchRefId` int(10) NOT NULL DEFAULT '0',
  `EmployeesRefId` int(10) NOT NULL DEFAULT '0',
  `pms_EmployeesRefId` int(10) NOT NULL DEFAULT '0',
  `CoveredDate` date DEFAULT NULL,
  `BonusName` varchar(300) DEFAULT NULL,
  `Amount` decimal(10,2) DEFAULT '0.00',
  `TaxTypeRefId` int(10) DEFAULT '0',
  `Remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `RefId` (`RefId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table pids_hris.pms_employeesbonus: ~0 rows (approximately)
/*!40000 ALTER TABLE `pms_employeesbonus` DISABLE KEYS */;
/*!40000 ALTER TABLE `pms_employeesbonus` ENABLE KEYS */;

-- Dumping structure for table pids_hris.pms_employeesdeductions
DROP TABLE IF EXISTS `pms_employeesdeductions`;
CREATE TABLE IF NOT EXISTS `pms_employeesdeductions` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `CompanyRefId` int(10) NOT NULL DEFAULT '0',
  `BranchRefId` int(10) NOT NULL DEFAULT '0',
  `EmployeesRefId` int(10) NOT NULL DEFAULT '0',
  `pms_EmployeesRefId` int(10) NOT NULL DEFAULT '0',
  `DeductionName` varchar(300) DEFAULT NULL,
  `Amount` decimal(10,2) DEFAULT '0.00',
  `PayPeriodRefId` int(10) DEFAULT '0',
  `PayRateRefId` int(10) DEFAULT '0',
  `StartDate` date DEFAULT NULL,
  `EndDate` date DEFAULT NULL,
  `TerminatedDate` date DEFAULT NULL,
  `isTerminate` int(1) DEFAULT NULL,
  `Remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `RefId` (`RefId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table pids_hris.pms_employeesdeductions: ~0 rows (approximately)
/*!40000 ALTER TABLE `pms_employeesdeductions` DISABLE KEYS */;
/*!40000 ALTER TABLE `pms_employeesdeductions` ENABLE KEYS */;

-- Dumping structure for table pids_hris.pms_employeesloans
DROP TABLE IF EXISTS `pms_employeesloans`;
CREATE TABLE IF NOT EXISTS `pms_employeesloans` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `CompanyRefId` int(10) NOT NULL DEFAULT '0',
  `BranchRefId` int(10) NOT NULL DEFAULT '0',
  `EmployeesRefId` int(10) NOT NULL DEFAULT '0',
  `pms_EmployeesRefId` int(10) NOT NULL DEFAULT '0',
  `LoanName` varchar(300) DEFAULT NULL,
  `EffectivityDate` date DEFAULT NULL,
  `GrantedDate` date DEFAULT NULL,
  `PayPeriodRefId` int(10) DEFAULT '0',
  `PayRateRefId` int(10) DEFAULT '0',
  `LoanAmount` decimal(10,2) DEFAULT '0.00',
  `LoanBalance` decimal(10,2) DEFAULT '0.00',
  `Amortization` int(10) DEFAULT '0',
  `StartDate` date DEFAULT NULL,
  `EndDate` date DEFAULT NULL,
  `TerminatedDate` date DEFAULT NULL,
  `isTerminate` int(1) DEFAULT NULL,
  `Remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `RefId` (`RefId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table pids_hris.pms_employeesloans: ~0 rows (approximately)
/*!40000 ALTER TABLE `pms_employeesloans` DISABLE KEYS */;
/*!40000 ALTER TABLE `pms_employeesloans` ENABLE KEYS */;

-- Dumping structure for table pids_hris.pms_employeessalary
DROP TABLE IF EXISTS `pms_employeessalary`;
CREATE TABLE IF NOT EXISTS `pms_employeessalary` (
  `RefId` bigint(50) NOT NULL AUTO_INCREMENT,
  `CompanyRefId` int(10) NOT NULL DEFAULT '0',
  `BranchRefId` int(10) NOT NULL DEFAULT '0',
  `EmployeesRefId` int(10) NOT NULL DEFAULT '0',
  `pms_EmployeesRefId` int(10) NOT NULL DEFAULT '0',
  `EffectivityDate` date DEFAULT NULL,
  `Description` varchar(300) DEFAULT NULL,
  `SalaryGradeRefId` int(10) DEFAULT '0',
  `JobGradeRefId` int(10) DEFAULT '0',
  `OldRate` decimal(10,2) DEFAULT '0.00',
  `NewRate` decimal(10,2) DEFAULT '0.00',
  `Adjustment` decimal(10,2) DEFAULT '0.00',
  `PayrateRefId` int(10) DEFAULT '0',
  `Remarks` varchar(300) DEFAULT NULL,
  `LastUpdateDate` date DEFAULT NULL,
  `LastUpdateTime` time DEFAULT NULL,
  `LastUpdateBy` varchar(50) DEFAULT NULL,
  `Data` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`RefId`),
  UNIQUE KEY `RefId` (`RefId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table pids_hris.pms_employeessalary: ~0 rows (approximately)
/*!40000 ALTER TABLE `pms_employeessalary` DISABLE KEYS */;
/*!40000 ALTER TABLE `pms_employeessalary` ENABLE KEYS */;

-- Dumping structure for table pids_hris.pms_employee_information
DROP TABLE IF EXISTS `pms_employee_information`;
CREATE TABLE IF NOT EXISTS `pms_employee_information` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `division_id` int(11) DEFAULT NULL,
  `department_id` int(11) DEFAULT NULL,
  `position_item_id` int(11) DEFAULT NULL,
  `office_id` int(11) DEFAULT NULL,
  `employee_status_id` int(11) DEFAULT NULL,
  `position_id` int(11) DEFAULT NULL,
  `hired_date` date DEFAULT NULL,
  `assumption_date` date DEFAULT NULL,
  `resigned_date` date DEFAULT NULL,
  `rehired_date` date DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `pay_period` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pay_rate_id` int(11) DEFAULT NULL,
  `work_schedule_id` int(11) DEFAULT NULL,
  `appointment_status_id` int(11) DEFAULT NULL,
  `designation_id` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=90 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table pids_hris.pms_employee_information: ~89 rows (approximately)
/*!40000 ALTER TABLE `pms_employee_information` DISABLE KEYS */;
INSERT INTO `pms_employee_information` (`id`, `employee_id`, `company_id`, `division_id`, `department_id`, `position_item_id`, `office_id`, `employee_status_id`, `position_id`, `hired_date`, `assumption_date`, `resigned_date`, `rehired_date`, `start_date`, `end_date`, `pay_period`, `pay_rate_id`, `work_schedule_id`, `appointment_status_id`, `designation_id`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 1, NULL, NULL, 1, NULL, NULL, 9, 113, NULL, '2018-01-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL),
	(2, 2, NULL, NULL, 1, NULL, NULL, 9, 129, NULL, '2018-01-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL),
	(3, 3, NULL, NULL, 1, NULL, NULL, 9, 213, NULL, '2018-01-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL),
	(4, 4, NULL, NULL, 1, NULL, NULL, 9, 30, NULL, '2018-01-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL),
	(5, 5, NULL, NULL, 1, NULL, NULL, 9, 30, NULL, '2018-02-10', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL),
	(6, 6, NULL, NULL, 1, NULL, NULL, 9, 175, NULL, '2018-01-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL),
	(7, 7, NULL, NULL, 1, NULL, NULL, 9, 170, NULL, '2018-01-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL),
	(8, 8, NULL, NULL, 1, NULL, NULL, 9, 170, NULL, '2018-01-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL),
	(9, 9, NULL, NULL, 1, NULL, NULL, 9, 73, NULL, '2018-01-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL),
	(10, 10, NULL, NULL, 1, NULL, NULL, 9, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(11, 11, NULL, NULL, 1, NULL, NULL, 9, 170, NULL, '2018-08-13', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL),
	(12, 12, NULL, NULL, 1, NULL, NULL, 9, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(13, 13, NULL, NULL, 1, 1, NULL, 9, 7, NULL, '2018-01-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL),
	(14, 14, NULL, NULL, 1, NULL, NULL, 9, 120, NULL, '2018-01-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL),
	(15, 15, NULL, NULL, 1, NULL, NULL, 9, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(16, 16, NULL, NULL, 1, NULL, NULL, 9, 132, NULL, '2018-01-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL),
	(17, 17, NULL, NULL, 1, NULL, NULL, 9, 459, NULL, '2018-09-22', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL),
	(18, 18, NULL, NULL, 1, NULL, NULL, 9, 452, NULL, '2018-01-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL),
	(19, 19, NULL, NULL, 1, NULL, NULL, 9, 30, NULL, '2018-01-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL),
	(20, 20, NULL, NULL, 1, NULL, NULL, 9, 461, NULL, '2018-08-04', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL),
	(21, 21, NULL, NULL, 1, NULL, NULL, 9, 152, NULL, '2018-01-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL),
	(22, 22, NULL, NULL, 1, NULL, NULL, 9, 33, NULL, '2018-01-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL),
	(23, 23, NULL, NULL, 2, NULL, NULL, 9, 133, NULL, '2018-01-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL),
	(24, 24, NULL, NULL, 2, NULL, NULL, 9, 167, NULL, '2018-03-17', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL),
	(25, 25, NULL, NULL, 2, NULL, NULL, 9, 183, NULL, '2018-01-13', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL),
	(26, 26, NULL, NULL, 2, NULL, NULL, 9, 47, NULL, '2018-01-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL),
	(27, 27, NULL, 1, 1, NULL, NULL, 9, 10, NULL, '2018-01-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL),
	(28, 28, NULL, NULL, 3, NULL, NULL, 9, 10, NULL, '2018-01-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL),
	(29, 29, NULL, NULL, 3, NULL, NULL, 9, 319, NULL, '2018-01-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL),
	(30, 30, NULL, NULL, 4, NULL, NULL, 9, 449, NULL, '2018-01-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL),
	(31, 31, NULL, NULL, 4, NULL, NULL, 9, 59, NULL, '2018-08-31', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL),
	(32, 32, NULL, NULL, 4, NULL, NULL, 9, 73, NULL, '2018-01-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL),
	(33, 33, NULL, NULL, 4, NULL, NULL, 9, 181, NULL, '2018-01-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL),
	(34, 34, NULL, NULL, 4, NULL, NULL, 9, 187, NULL, '2018-01-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL),
	(35, 35, NULL, NULL, 4, NULL, NULL, 9, 56, NULL, '2018-01-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL),
	(36, 36, NULL, NULL, 5, NULL, NULL, 9, 167, NULL, '2018-10-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL),
	(37, 37, NULL, NULL, 5, NULL, NULL, 9, 187, NULL, '2018-01-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL),
	(38, 38, NULL, NULL, 5, NULL, NULL, 9, 133, NULL, '2018-01-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL),
	(39, 39, NULL, NULL, 5, NULL, NULL, 9, 467, NULL, '2018-01-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL),
	(40, 40, NULL, NULL, 6, NULL, NULL, 9, 25, NULL, '2018-01-01', NULL, NULL, '2018-01-01', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL),
	(41, 41, NULL, NULL, 6, NULL, NULL, 9, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(42, 42, NULL, NULL, 6, NULL, NULL, 9, 40, NULL, '2018-03-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL),
	(43, 43, NULL, NULL, 6, NULL, NULL, 9, 226, NULL, '2018-06-05', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL),
	(44, 44, NULL, NULL, 7, NULL, NULL, 9, 104, NULL, '2018-01-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL),
	(45, 45, NULL, NULL, 7, NULL, NULL, 9, 204, NULL, '2018-01-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL),
	(46, 46, NULL, NULL, 8, NULL, NULL, 9, 38, NULL, '2018-01-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL),
	(47, 47, NULL, NULL, 8, NULL, NULL, 9, 18, NULL, '2018-01-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL),
	(48, 48, NULL, NULL, 8, NULL, NULL, 9, 16, NULL, '2018-03-19', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL),
	(49, 49, NULL, NULL, 8, NULL, NULL, 9, 73, NULL, '2018-01-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL),
	(50, 50, NULL, NULL, 8, NULL, NULL, 9, 18, NULL, '2018-03-20', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL),
	(51, 51, NULL, NULL, 8, NULL, NULL, 9, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(52, 52, NULL, NULL, 8, NULL, NULL, 9, 73, NULL, '2018-01-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL),
	(53, 53, NULL, NULL, 8, NULL, NULL, 9, 24, NULL, '2018-01-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL),
	(54, 54, NULL, NULL, 8, NULL, NULL, 9, 18, NULL, '2018-01-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL),
	(55, 55, NULL, NULL, 8, NULL, NULL, 9, 18, NULL, '2018-01-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL),
	(56, 56, NULL, NULL, 8, NULL, NULL, 9, 24, NULL, '2018-04-17', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL),
	(57, 57, NULL, NULL, 8, NULL, NULL, 9, 18, NULL, '2018-01-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL),
	(58, 58, NULL, NULL, 8, NULL, NULL, 9, 73, NULL, '2018-01-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL),
	(59, 59, NULL, NULL, 8, NULL, NULL, 9, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(60, 60, NULL, NULL, 8, NULL, NULL, 9, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(61, 61, NULL, NULL, 8, NULL, NULL, 9, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(62, 62, NULL, NULL, 8, NULL, NULL, 9, 24, NULL, '2018-04-17', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL),
	(63, 63, NULL, NULL, 8, NULL, NULL, 9, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(64, 64, NULL, NULL, 8, NULL, NULL, 9, 38, NULL, '2018-01-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL),
	(65, 65, NULL, NULL, 8, NULL, NULL, 9, 24, NULL, '2018-01-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL),
	(66, 66, NULL, NULL, 8, NULL, NULL, 9, 18, NULL, '2018-01-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL),
	(67, 67, NULL, NULL, 8, NULL, NULL, 9, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(68, 68, NULL, NULL, 8, NULL, NULL, 9, 24, NULL, '2018-01-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL),
	(69, 69, NULL, NULL, 8, NULL, NULL, 9, 15, NULL, '2018-01-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL),
	(70, 70, NULL, NULL, 8, NULL, NULL, 9, 16, NULL, '2018-07-20', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL),
	(71, 71, NULL, NULL, 8, NULL, NULL, 9, 15, NULL, '2018-01-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL),
	(72, 72, NULL, NULL, 9, NULL, NULL, 6, 183, NULL, '2018-01-01', NULL, NULL, '2018-01-01', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL),
	(73, 73, NULL, NULL, 9, NULL, NULL, 6, 253, NULL, '2018-01-01', NULL, NULL, '2018-01-01', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL),
	(74, 74, NULL, NULL, 9, NULL, NULL, 6, 62, NULL, '2018-01-01', NULL, NULL, '2018-01-01', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL),
	(75, 75, NULL, NULL, 4, 1, 3, 4, 25, NULL, '2018-01-01', NULL, NULL, '2018-01-01', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL),
	(76, 76, NULL, NULL, 10, NULL, NULL, 4, 15, NULL, '2018-01-01', NULL, NULL, '2018-01-01', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL),
	(77, 77, NULL, NULL, 10, NULL, NULL, 4, 15, NULL, '2018-01-01', NULL, NULL, '2018-01-01', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL),
	(78, 78, NULL, NULL, 10, NULL, NULL, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(79, 79, NULL, NULL, 10, NULL, NULL, 4, 322, NULL, '2018-01-01', NULL, NULL, '2018-01-01', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL),
	(80, 80, NULL, NULL, 10, NULL, NULL, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(81, 81, NULL, NULL, 10, NULL, NULL, 4, 56, NULL, '2018-01-01', NULL, NULL, '2018-01-01', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL),
	(82, 82, NULL, NULL, 10, NULL, NULL, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(83, 83, NULL, NULL, 10, NULL, NULL, 4, 170, NULL, '2018-01-01', NULL, NULL, '2018-01-01', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL),
	(84, 84, NULL, NULL, 11, NULL, NULL, 9, 15, NULL, '2018-01-01', NULL, NULL, '2018-01-01', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL),
	(85, 85, NULL, NULL, 12, NULL, NULL, 9, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(86, 86, NULL, NULL, 13, NULL, NULL, 9, 24, NULL, '2018-01-01', NULL, NULL, '2018-01-01', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL),
	(87, 87, NULL, NULL, 14, NULL, NULL, 9, 24, NULL, '2018-01-01', NULL, NULL, '2018-01-01', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL),
	(88, 88, NULL, NULL, 14, NULL, NULL, 9, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(89, 89, NULL, NULL, 15, NULL, NULL, 9, 15, NULL, '2018-01-15', NULL, NULL, '2018-01-15', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `pms_employee_information` ENABLE KEYS */;

-- Dumping structure for table pids_hris.pms_employee_status
DROP TABLE IF EXISTS `pms_employee_status`;
CREATE TABLE IF NOT EXISTS `pms_employee_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(50) DEFAULT NULL,
  `name` varchar(225) DEFAULT NULL,
  `category` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

-- Dumping data for table pids_hris.pms_employee_status: ~11 rows (approximately)
/*!40000 ALTER TABLE `pms_employee_status` DISABLE KEYS */;
INSERT INTO `pms_employee_status` (`id`, `code`, `name`, `category`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
	(1, 'P', 'Permanent', 1, NULL, '2018-08-16 08:59:50', NULL, 2),
	(2, 'T', 'Temporary', 1, NULL, '2018-08-16 09:00:22', NULL, 2),
	(3, 'J.O', 'Job Order', 0, NULL, '2018-08-16 09:01:16', NULL, 2),
	(4, 'CON', 'Contractual', 1, NULL, NULL, NULL, NULL),
	(5, 'CAS', 'Casual', 1, NULL, '2018-08-16 08:59:32', NULL, 2),
	(6, 'COT', 'Coterminous', 1, NULL, '2018-08-16 09:00:36', NULL, 2),
	(7, 'FT', 'Termer', NULL, NULL, '2018-08-10 02:27:44', NULL, 2),
	(8, 'PROB', 'Probationary', 1, NULL, '2018-08-16 09:00:49', NULL, 2),
	(9, 'R', 'Regular', 1, NULL, '2018-08-16 09:02:12', NULL, 2),
	(10, 'COS', 'Contract of Service', 0, NULL, '2018-08-16 09:01:36', NULL, 2),
	(11, 'GIP', 'GIP', 0, '2018-09-12 13:02:17', '2018-09-12 13:02:17', 2, NULL);
/*!40000 ALTER TABLE `pms_employee_status` ENABLE KEYS */;

-- Dumping structure for table pids_hris.pms_gsispolicy
DROP TABLE IF EXISTS `pms_gsispolicy`;
CREATE TABLE IF NOT EXISTS `pms_gsispolicy` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `policy_name` varchar(225) DEFAULT NULL,
  `pay_period` varchar(225) DEFAULT NULL,
  `deduction_period` varchar(225) DEFAULT NULL,
  `policy_type` varchar(225) DEFAULT NULL,
  `based_on` varchar(225) DEFAULT NULL,
  `computation` varchar(225) DEFAULT NULL,
  `ee_percentage` decimal(9,2) DEFAULT NULL,
  `er_percentage` decimal(9,2) DEFAULT NULL,
  `remarks` varchar(225) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table pids_hris.pms_gsispolicy: ~1 rows (approximately)
/*!40000 ALTER TABLE `pms_gsispolicy` DISABLE KEYS */;
INSERT INTO `pms_gsispolicy` (`id`, `policy_name`, `pay_period`, `deduction_period`, `policy_type`, `based_on`, `computation`, `ee_percentage`, `er_percentage`, `remarks`, `created_at`, `updated_at`, `updated_by`) VALUES
	(1, 'STANDARD POLICY', 'Monthly', 'Both', 'System Generated', 'Monthly Salary', '{\'EE\':\'.09\',\'ER\':\'.12\',\'EC\':\'.01\'}', 0.09, 0.12, NULL, '2018-06-09 10:25:03', '2018-06-21 03:14:34', NULL);
/*!40000 ALTER TABLE `pms_gsispolicy` ENABLE KEYS */;

-- Dumping structure for table pids_hris.pms_jobgrade
DROP TABLE IF EXISTS `pms_jobgrade`;
CREATE TABLE IF NOT EXISTS `pms_jobgrade` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `job_grade` varchar(225) DEFAULT NULL,
  `step1` decimal(9,2) DEFAULT NULL,
  `step2` decimal(9,2) DEFAULT NULL,
  `step3` decimal(9,2) DEFAULT NULL,
  `step4` decimal(9,2) DEFAULT NULL,
  `step5` decimal(9,2) DEFAULT NULL,
  `step6` decimal(9,2) DEFAULT NULL,
  `step7` decimal(9,2) DEFAULT NULL,
  `step8` decimal(9,2) DEFAULT NULL,
  `remarks` varchar(225) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

-- Dumping data for table pids_hris.pms_jobgrade: ~19 rows (approximately)
/*!40000 ALTER TABLE `pms_jobgrade` DISABLE KEYS */;
INSERT INTO `pms_jobgrade` (`id`, `job_grade`, `step1`, `step2`, `step3`, `step4`, `step5`, `step6`, `step7`, `step8`, `remarks`, `created_at`, `updated_at`, `updated_by`) VALUES
	(1, '1', 14087.00, 14242.00, 14399.00, 14543.00, 14688.00, 14835.00, 14897.00, 14930.00, NULL, NULL, NULL, NULL),
	(2, '2', 14936.00, 15160.00, 15387.00, 15618.00, 15853.00, 16090.00, 16332.00, 16577.00, NULL, NULL, NULL, NULL),
	(3, '3', 17975.00, 18245.00, 18518.00, 18796.00, 19078.00, 19364.00, 19665.00, 19949.00, NULL, NULL, NULL, NULL),
	(4, '4', 22532.00, 22870.00, 23213.00, 23561.00, 23915.00, 24273.00, 24637.00, 25007.00, NULL, NULL, NULL, NULL),
	(5, '5', 27414.00, 27825.00, 28243.00, 28666.00, 29096.00, 29504.00, 29917.00, 30335.00, NULL, NULL, NULL, NULL),
	(6, '6', 30351.00, 30806.00, 31268.00, 31737.00, 32213.00, 32697.00, 33187.00, 33685.00, NULL, NULL, NULL, NULL),
	(7, '7', 40169.00, 40772.00, 41383.00, 42004.00, 42634.00, 43273.00, 43923.00, 44581.00, NULL, NULL, NULL, NULL),
	(8, '8', 45269.00, 45948.00, 46637.00, 47337.00, 48047.00, 48768.00, 49499.00, 50242.00, NULL, NULL, NULL, NULL),
	(9, '9', 55600.00, 56434.00, 57224.00, 57968.00, 58664.00, 58694.00, 58723.00, 58752.00, NULL, NULL, NULL, NULL),
	(10, '10', 59100.00, 59987.00, 60886.00, 61800.00, 62727.00, 63667.00, 64622.00, 65592.00, NULL, NULL, NULL, NULL),
	(11, '11', 69499.00, 70194.00, 70896.00, 71605.00, 72321.00, 73044.00, 73775.00, 73811.00, NULL, NULL, NULL, NULL),
	(12, '12', 73900.00, 75009.00, 76134.00, 77276.00, 78435.00, 79611.00, 80805.00, 82018.00, NULL, NULL, NULL, NULL),
	(13, '13', 86899.00, 88202.00, 89526.00, 90868.00, 92231.00, 93615.00, 95019.00, 96444.00, NULL, NULL, NULL, NULL),
	(14, '14', 112500.00, 114188.00, 115900.00, 117523.00, 119168.00, 120717.00, 12216.00, 122227.00, NULL, NULL, NULL, NULL),
	(15, '15', 122500.00, 124338.00, 126203.00, 128096.00, 130017.00, 131967.00, 133947.00, 135956.00, NULL, NULL, NULL, NULL),
	(16, '16', 169300.00, 171840.00, 174417.00, 177033.00, 179689.00, 182384.00, 185120.00, 187897.00, NULL, NULL, NULL, NULL),
	(17, '17', 220200.00, 223503.00, 226856.00, 230258.00, 233712.00, 237218.00, 240776.00, 244388.00, NULL, NULL, NULL, NULL),
	(18, '18', 354312.00, 359627.00, 365021.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(19, '19', 531468.00, 539440.00, 547532.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `pms_jobgrade` ENABLE KEYS */;

-- Dumping structure for table pids_hris.pms_leave
DROP TABLE IF EXISTS `pms_leave`;
CREATE TABLE IF NOT EXISTS `pms_leave` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) DEFAULT NULL,
  `rata_id` int(11) DEFAULT NULL,
  `leave_type` varchar(225) DEFAULT NULL,
  `number_of_leave_field` int(11) DEFAULT NULL,
  `leave_date` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table pids_hris.pms_leave: ~0 rows (approximately)
/*!40000 ALTER TABLE `pms_leave` DISABLE KEYS */;
/*!40000 ALTER TABLE `pms_leave` ENABLE KEYS */;

-- Dumping structure for table pids_hris.pms_leave_monetization_transactions
DROP TABLE IF EXISTS `pms_leave_monetization_transactions`;
CREATE TABLE IF NOT EXISTS `pms_leave_monetization_transactions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) DEFAULT NULL,
  `salary_grade_id` int(11) DEFAULT NULL,
  `position_id` int(11) DEFAULT NULL,
  `salary_amount` decimal(9,2) DEFAULT NULL,
  `net_amount` decimal(9,2) DEFAULT NULL,
  `factor_rate` decimal(9,2) DEFAULT NULL,
  `number_of_days` int(11) DEFAULT NULL,
  `remarks` varchar(50) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table pids_hris.pms_leave_monetization_transactions: ~0 rows (approximately)
/*!40000 ALTER TABLE `pms_leave_monetization_transactions` DISABLE KEYS */;
/*!40000 ALTER TABLE `pms_leave_monetization_transactions` ENABLE KEYS */;

-- Dumping structure for table pids_hris.pms_loans
DROP TABLE IF EXISTS `pms_loans`;
CREATE TABLE IF NOT EXISTS `pms_loans` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(225) DEFAULT NULL,
  `name` varchar(225) DEFAULT NULL,
  `loan_type` varchar(225) DEFAULT NULL,
  `category` varchar(225) DEFAULT NULL,
  `gsis_excel_col` varchar(225) DEFAULT NULL,
  `amount` decimal(9,2) DEFAULT NULL,
  `remarks` varchar(225) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

-- Dumping data for table pids_hris.pms_loans: ~12 rows (approximately)
/*!40000 ALTER TABLE `pms_loans` DISABLE KEYS */;
INSERT INTO `pms_loans` (`id`, `code`, `name`, `loan_type`, `category`, `gsis_excel_col`, `amount`, `remarks`, `created_at`, `updated_at`, `updated_by`) VALUES
	(1, 'CONSOLOAN', 'CONSOLOAN', 'GSIS', NULL, NULL, NULL, NULL, '2018-06-19 17:32:01', '2018-06-19 17:32:01', NULL),
	(2, 'POLICY LOAN', 'POLICY LOAN', 'GSIS', NULL, NULL, NULL, NULL, '2018-06-19 17:32:37', '2018-06-19 17:35:22', NULL),
	(3, 'ER', 'EMERGENCY/CALAMITY LOAN', 'GSIS', NULL, NULL, NULL, NULL, '2018-06-19 17:33:50', '2018-06-19 17:33:50', NULL),
	(4, 'OPTIONAL LOAN', 'OPTIONAL LOAN', 'GSIS', NULL, NULL, NULL, NULL, '2018-06-19 17:34:11', '2018-09-22 05:48:20', NULL),
	(5, 'MPL', 'MULTI PURPOSE LOAN', 'Pagibig Loan', NULL, NULL, NULL, NULL, '2018-06-19 17:34:41', '2018-06-19 17:34:41', NULL),
	(6, 'CALAMITY LOAN', 'CALAMITY LOAN', 'Pagibig Loan', NULL, NULL, NULL, NULL, '2018-06-19 17:34:58', '2018-06-19 17:34:58', NULL),
	(7, 'MPLP', 'MULTI-PURPOSE COOPERATIVE LOAN', 'NULL', NULL, NULL, NULL, NULL, '2018-08-16 12:17:15', '2018-08-16 12:17:15', NULL),
	(8, 'LCH-DCS', 'LCH-DCS', 'GSIS', NULL, NULL, NULL, NULL, '2018-08-17 03:50:45', '2018-08-17 03:50:45', NULL),
	(9, 'EDUC LOAN', 'EDUCATION LOAN', 'GSIS', NULL, NULL, NULL, NULL, '2018-08-17 03:53:49', '2018-08-17 03:53:49', NULL),
	(10, 'WASSSLAI Loan', 'WASSSLAI Loan', NULL, NULL, NULL, NULL, NULL, '2018-08-17 04:09:54', '2018-08-17 04:09:54', NULL),
	(11, 'WFLOAN', 'WELFARE FUND LOAN', 'GSIS', NULL, NULL, NULL, NULL, '2018-08-17 06:51:16', '2018-08-17 06:51:16', NULL),
	(12, 'OPTIONAL CONTRI', 'OPTIONAL CONTRI', 'GSIS', NULL, NULL, NULL, NULL, '2018-09-22 05:48:32', '2018-09-22 05:48:32', NULL);
/*!40000 ALTER TABLE `pms_loans` ENABLE KEYS */;

-- Dumping structure for table pids_hris.pms_loansinfo
DROP TABLE IF EXISTS `pms_loansinfo`;
CREATE TABLE IF NOT EXISTS `pms_loansinfo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employeeinfo_id` int(11) DEFAULT NULL,
  `employee_id` int(11) DEFAULT NULL,
  `loan_id` int(11) DEFAULT NULL,
  `loan_totalamount` decimal(9,2) DEFAULT NULL,
  `loan_totalbalance` decimal(9,2) DEFAULT NULL,
  `loan_amortization` decimal(9,2) DEFAULT NULL,
  `loan_pay_period` varchar(225) DEFAULT NULL,
  `loan_date_granted` date DEFAULT NULL,
  `loan_date_started` date DEFAULT NULL,
  `loan_date_end` date DEFAULT NULL,
  `loan_date_terminated` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=84 DEFAULT CHARSET=latin1;

-- Dumping data for table pids_hris.pms_loansinfo: ~83 rows (approximately)
/*!40000 ALTER TABLE `pms_loansinfo` DISABLE KEYS */;
INSERT INTO `pms_loansinfo` (`id`, `employeeinfo_id`, `employee_id`, `loan_id`, `loan_totalamount`, `loan_totalbalance`, `loan_amortization`, `loan_pay_period`, `loan_date_granted`, `loan_date_started`, `loan_date_end`, `loan_date_terminated`, `created_at`, `updated_at`, `updated_by`) VALUES
	(1, NULL, 1, 2, NULL, NULL, 1000.00, NULL, '2017-04-24', NULL, '2042-07-05', NULL, NULL, NULL, NULL),
	(2, NULL, 2, 2, 140732.61, NULL, 1500.00, NULL, '2016-09-22', NULL, '2021-07-02', NULL, NULL, NULL, NULL),
	(3, NULL, 7, 2, NULL, NULL, 500.00, NULL, '2016-03-15', NULL, '2018-10-01', NULL, NULL, NULL, NULL),
	(4, NULL, 11, 2, 6933.05, NULL, 100.00, NULL, '2017-11-23', NULL, '2034-06-24', NULL, NULL, NULL, NULL),
	(5, NULL, 14, 2, 12012.34, NULL, 200.00, NULL, '2018-02-26', NULL, '2049-06-25', NULL, NULL, NULL, NULL),
	(6, NULL, 18, 2, NULL, NULL, 100.00, NULL, '2017-08-23', NULL, '2023-05-13', NULL, NULL, NULL, NULL),
	(7, NULL, 19, 2, 8755.55, NULL, 300.00, NULL, '2017-09-06', NULL, '2033-11-12', NULL, NULL, NULL, NULL),
	(8, NULL, 21, 2, 15000.00, NULL, 500.00, NULL, '2018-03-14', NULL, '2035-11-15', NULL, NULL, NULL, NULL),
	(9, NULL, 22, 2, 27014.43, NULL, 500.00, NULL, '2017-07-13', NULL, '1949-06-12', NULL, NULL, NULL, NULL),
	(10, NULL, 28, 2, 95939.93, NULL, 500.00, NULL, '2017-09-11', NULL, '2019-11-20', NULL, NULL, NULL, NULL),
	(11, NULL, 29, 2, 18495.40, NULL, 300.00, NULL, '2016-02-01', NULL, '2033-07-07', NULL, NULL, NULL, NULL),
	(12, NULL, 31, 2, 17000.00, NULL, 500.00, NULL, '2015-05-15', NULL, '2030-09-15', NULL, NULL, NULL, NULL),
	(13, NULL, 43, 2, 118001.52, NULL, 500.00, NULL, '2017-09-13', NULL, '2023-05-13', NULL, NULL, NULL, NULL),
	(14, NULL, 44, 2, NULL, NULL, 500.00, NULL, '2016-09-08', NULL, '2022-07-21', NULL, NULL, NULL, NULL),
	(15, NULL, 52, 2, 21652.41, NULL, 300.00, NULL, '2018-07-20', NULL, '2024-02-01', NULL, NULL, NULL, NULL),
	(16, NULL, 55, 2, 17675.29, NULL, 200.00, NULL, '2015-01-12', NULL, '2037-08-05', NULL, NULL, NULL, NULL),
	(17, NULL, 62, 2, NULL, NULL, 300.00, NULL, '2016-10-07', NULL, '2052-10-12', NULL, NULL, NULL, NULL),
	(18, NULL, 2, 2, 67758.55, NULL, 2000.00, NULL, '2015-10-16', NULL, '2020-10-01', NULL, NULL, NULL, NULL),
	(19, NULL, 74, 2, NULL, NULL, 200.00, NULL, '2014-07-28', NULL, '2022-07-02', NULL, NULL, NULL, NULL),
	(20, 5, 2, 4, 0.00, 0.00, 1500.00, NULL, NULL, NULL, NULL, NULL, '2018-09-22 05:46:48', '2018-09-22 05:46:48', NULL),
	(21, 5, 2, 12, 0.00, 0.00, 286.00, NULL, NULL, NULL, NULL, NULL, '2018-09-22 05:49:08', '2018-09-22 05:49:08', NULL),
	(22, 5, 2, 5, 140899.79, 0.00, 5870.82, NULL, '2017-08-07', '2017-10-01', '2017-09-30', NULL, NULL, '2018-09-22 06:57:13', NULL),
	(23, NULL, 3, 5, 31440.97, NULL, 1310.04, NULL, '2016-11-28', '2017-01-01', '2018-12-31', NULL, NULL, NULL, NULL),
	(24, NULL, 8, 5, 24530.54, NULL, 1022.11, NULL, '2017-04-18', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL),
	(25, NULL, 10, 5, 16011.84, NULL, 667.16, NULL, '2017-07-12', '2017-09-01', '2019-08-30', NULL, NULL, NULL, NULL),
	(26, NULL, 11, 5, 32808.44, NULL, 1367.02, NULL, '2017-08-22', '2017-10-01', '2019-09-30', NULL, NULL, NULL, NULL),
	(27, NULL, 12, 5, 10618.58, NULL, 442.44, NULL, '2017-07-21', '2017-09-01', '2019-08-30', NULL, NULL, NULL, NULL),
	(28, NULL, 18, 5, 77983.22, NULL, 3976.60, NULL, '2018-05-21', '2018-07-01', '2020-06-30', NULL, NULL, NULL, NULL),
	(29, NULL, 19, 5, 38450.06, NULL, 1952.44, NULL, '2018-02-28', '2018-04-01', '2018-03-31', NULL, NULL, NULL, NULL),
	(30, NULL, 21, 5, 84798.63, NULL, 3533.28, NULL, '2017-06-22', '2017-08-01', '2019-07-30', NULL, NULL, NULL, NULL),
	(31, NULL, 22, 5, 79413.75, NULL, 3308.91, NULL, '2016-10-17', '2016-12-01', '2018-11-30', NULL, NULL, NULL, NULL),
	(32, NULL, 28, 5, 48036.30, NULL, 2446.08, NULL, '2018-04-24', '2018-06-01', '2020-05-31', NULL, NULL, NULL, NULL),
	(33, NULL, 29, 5, 15369.83, NULL, 786.68, NULL, '2018-04-13', '2018-06-01', '2020-05-31', NULL, NULL, NULL, NULL),
	(34, NULL, 31, 5, 37582.13, NULL, 1565.92, NULL, '2016-11-10', '2017-01-01', '2018-12-31', NULL, NULL, NULL, NULL),
	(35, NULL, 43, 5, 41631.78, NULL, 1734.66, NULL, '2017-06-19', '2016-09-01', '2018-08-30', NULL, NULL, NULL, NULL),
	(36, NULL, 58, 5, NULL, NULL, 3961.66, NULL, '0000-00-00', '2018-04-01', '2019-04-30', NULL, NULL, NULL, NULL),
	(37, NULL, 82, 5, 24703.71, NULL, 1029.32, NULL, '2017-07-12', '2017-09-01', '2019-08-30', NULL, NULL, NULL, NULL),
	(38, NULL, 83, 5, 5658.99, NULL, 235.79, NULL, '2017-07-21', '2017-09-01', '2019-08-30', NULL, NULL, NULL, NULL),
	(39, NULL, 72, 5, 80612.11, NULL, 3358.84, NULL, '2016-09-21', '2016-11-01', '2018-10-31', NULL, NULL, NULL, NULL),
	(40, 41, 27, 3, 20000.00, 0.00, 655.56, NULL, '2017-10-05', '2018-01-10', '2020-12-10', NULL, '2018-09-22 07:02:31', '2018-09-22 07:02:31', NULL),
	(41, NULL, 11, 9, 10000.00, NULL, 216.67, NULL, '2017-07-27', '2017-09-10', '2022-08-10', NULL, NULL, NULL, NULL),
	(42, NULL, 18, 9, 10000.00, NULL, 216.67, NULL, '2017-07-31', '2017-09-10', '2022-08-10', NULL, NULL, NULL, NULL),
	(43, NULL, 19, 9, 10000.00, NULL, 216.67, NULL, '2017-08-02', '2017-09-10', '2022-08-10', NULL, NULL, NULL, NULL),
	(44, NULL, 27, 9, 10000.00, NULL, 216.67, NULL, '2017-07-19', '2017-08-10', '2022-07-10', NULL, NULL, NULL, NULL),
	(45, NULL, 28, 9, 10000.00, NULL, 216.67, NULL, '2017-07-19', '2017-08-10', '2022-07-10', NULL, NULL, NULL, NULL),
	(46, NULL, 43, 9, 10000.00, NULL, 216.67, NULL, '2017-07-31', '2017-09-10', '2022-08-10', NULL, NULL, NULL, NULL),
	(47, NULL, 44, 9, 10000.00, NULL, 216.67, NULL, '2017-08-02', '2017-09-10', '2022-08-10', NULL, NULL, NULL, NULL),
	(48, NULL, 2, 1, 750000.00, NULL, 10841.04, NULL, '2018-05-31', '2018-07-10', '2028-06-10', NULL, NULL, NULL, NULL),
	(49, NULL, 3, 1, 48620.00, NULL, 953.57, NULL, '2017-07-25', '2017-09-10', '2023-08-10', NULL, NULL, NULL, NULL),
	(50, NULL, 5, 1, 250000.00, NULL, 5573.92, NULL, '2017-06-16', '2017-07-10', '2022-06-10', NULL, NULL, NULL, NULL),
	(51, NULL, 7, 1, 147996.00, NULL, 2139.24, NULL, '2016-11-08', '2016-12-10', '2026-11-10', NULL, NULL, NULL, NULL),
	(52, NULL, 8, 1, 79044.00, NULL, 1593.95, NULL, '2014-05-19', '2014-08-10', '2020-07-10', NULL, NULL, NULL, NULL),
	(53, NULL, 9, 1, 105567.00, NULL, 2128.80, NULL, '2014-10-14', '2015-01-10', '2020-12-10', NULL, NULL, NULL, NULL),
	(54, NULL, 10, 1, NULL, NULL, 760.25, NULL, '2016-03-03', '2016-04-10', '2022-03-10', NULL, NULL, NULL, NULL),
	(55, NULL, 11, 1, 154608.00, NULL, 2234.81, NULL, '2018-03-01', '2018-04-10', '2028-03-10', NULL, NULL, NULL, NULL),
	(56, NULL, 14, 1, 203070.00, NULL, 3982.74, NULL, '2018-04-30', '2017-12-10', '2023-11-10', NULL, NULL, NULL, NULL),
	(57, NULL, 15, 1, NULL, NULL, 7407.90, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(58, NULL, 16, 1, 374521.00, NULL, 7345.35, NULL, '2017-05-04', '2017-06-10', '2023-05-10', NULL, NULL, NULL, NULL),
	(59, NULL, 18, 1, 187936.00, NULL, 2716.56, NULL, '2018-04-18', '2018-05-10', '2028-04-10', NULL, NULL, NULL, NULL),
	(60, NULL, 19, 1, 457020.00, NULL, 6606.09, NULL, '2018-03-16', '2018-04-10', '2028-03-10', NULL, NULL, NULL, NULL),
	(61, NULL, 22, 1, 400000.00, NULL, 5781.89, NULL, '2016-04-13', '2016-05-10', '2026-04-10', NULL, NULL, NULL, NULL),
	(62, NULL, 27, 1, 285432.00, NULL, 5598.02, NULL, '2018-01-15', '2018-02-10', '2024-01-10', NULL, NULL, NULL, NULL),
	(63, NULL, 28, 1, 528240.00, NULL, 7635.55, NULL, '2018-02-14', '2018-03-10', '2028-02-10', NULL, NULL, NULL, NULL),
	(64, NULL, 29, 1, 220000.00, NULL, 4314.78, NULL, '2017-07-17', '2017-08-10', '2023-07-10', NULL, NULL, NULL, NULL),
	(65, NULL, 31, 1, 268680.00, NULL, 5418.03, NULL, '2013-10-06', '2014-01-10', '2019-12-10', NULL, NULL, NULL, NULL),
	(66, NULL, 37, 1, 327530.00, NULL, 6604.76, NULL, '2013-12-16', '2014-03-10', '2020-02-10', NULL, NULL, NULL, NULL),
	(67, NULL, 40, 1, 180000.00, NULL, 4747.26, NULL, '2015-09-29', '2015-11-10', '2019-10-10', NULL, NULL, NULL, NULL),
	(68, NULL, 43, 1, 1105426.00, NULL, 15978.62, NULL, '2018-02-13', '2018-03-10', '2028-02-10', NULL, NULL, NULL, NULL),
	(69, NULL, 52, 1, 248920.00, NULL, 3598.07, NULL, '2017-06-15', '2017-07-10', '2027-06-10', NULL, NULL, NULL, NULL),
	(70, NULL, 55, 1, 410000.00, NULL, 8041.19, NULL, '2016-07-25', '2016-08-10', '2022-07-10', NULL, NULL, NULL, NULL),
	(71, NULL, 58, 1, 213360.00, NULL, 3084.06, NULL, '2017-06-15', '2017-07-10', '2027-06-10', NULL, NULL, NULL, NULL),
	(72, NULL, 59, 1, 416000.00, NULL, 6013.16, NULL, '2017-12-21', '2018-01-10', '2027-12-10', NULL, NULL, NULL, NULL),
	(73, NULL, 63, 1, 15000.00, NULL, 1332.78, NULL, '2016-10-28', '2016-12-10', '2017-11-10', NULL, NULL, NULL, NULL),
	(74, NULL, 67, 1, 130000.00, NULL, 6121.38, NULL, '2018-06-04', '2018-07-10', '2020-06-10', NULL, NULL, NULL, NULL),
	(75, NULL, 68, 1, 600000.00, NULL, 8672.83, NULL, '2017-09-25', '2017-10-10', '2027-09-10', NULL, NULL, NULL, NULL),
	(76, NULL, 72, 1, 100356.00, NULL, 4725.40, NULL, '2017-01-19', '2017-02-10', '2019-01-10', NULL, NULL, NULL, NULL),
	(77, NULL, 74, 1, 316960.00, NULL, 6391.60, NULL, '2013-06-17', '2013-09-10', '2019-08-10', NULL, NULL, NULL, NULL),
	(78, NULL, 79, 1, 352254.00, NULL, 6908.64, NULL, '2015-08-06', '2015-09-10', '2021-08-10', NULL, NULL, NULL, NULL),
	(79, NULL, 87, 1, 587170.00, NULL, 11515.95, NULL, '2018-02-15', '2018-03-10', '2024-02-18', NULL, NULL, NULL, NULL),
	(80, NULL, 72, 1, 100356.00, NULL, 4725.40, NULL, '2017-01-19', '2017-02-10', '2019-01-10', NULL, NULL, NULL, NULL),
	(81, NULL, 74, 1, 316960.00, NULL, 6391.60, NULL, '2013-06-17', '2013-09-10', '2019-08-10', NULL, NULL, NULL, NULL),
	(82, NULL, 79, 1, 352254.00, NULL, 6908.64, NULL, '2015-08-06', '2015-09-10', '2021-08-10', NULL, NULL, NULL, NULL),
	(83, NULL, 87, 1, 587170.00, NULL, 11515.95, NULL, '2018-02-15', '2018-03-10', '2024-02-18', NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `pms_loansinfo` ENABLE KEYS */;

-- Dumping structure for table pids_hris.pms_nonplantilla_employeeinfo
DROP TABLE IF EXISTS `pms_nonplantilla_employeeinfo`;
CREATE TABLE IF NOT EXISTS `pms_nonplantilla_employeeinfo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) DEFAULT NULL,
  `bank_id` int(11) DEFAULT NULL,
  `taxpolicy_id` int(11) DEFAULT NULL,
  `taxpolicy_two_id` int(11) DEFAULT NULL,
  `atm_no` int(11) DEFAULT NULL,
  `daily_rate_amount` decimal(9,2) DEFAULT NULL,
  `overtime_balance_amount` decimal(9,2) DEFAULT NULL,
  `tax_id_number` varchar(225) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table pids_hris.pms_nonplantilla_employeeinfo: ~0 rows (approximately)
/*!40000 ALTER TABLE `pms_nonplantilla_employeeinfo` DISABLE KEYS */;
/*!40000 ALTER TABLE `pms_nonplantilla_employeeinfo` ENABLE KEYS */;

-- Dumping structure for table pids_hris.pms_nonplantilla_transactions
DROP TABLE IF EXISTS `pms_nonplantilla_transactions`;
CREATE TABLE IF NOT EXISTS `pms_nonplantilla_transactions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) DEFAULT NULL,
  `salaryinfo_id` int(11) DEFAULT NULL,
  `division_id` int(11) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `position_item_id` int(11) DEFAULT NULL,
  `position_id` int(11) DEFAULT NULL,
  `office_id` int(11) DEFAULT NULL,
  `department_id` int(11) DEFAULT NULL,
  `employee_status_id` int(11) DEFAULT NULL,
  `employeeinfo_id` int(11) DEFAULT NULL,
  `actual_basicpay` decimal(9,2) DEFAULT NULL,
  `adjust_basicpay` decimal(9,2) DEFAULT NULL,
  `total_basicpay` decimal(9,2) DEFAULT NULL,
  `actual_absences` decimal(9,2) DEFAULT NULL,
  `adjust_absences` decimal(9,2) DEFAULT NULL,
  `total_absences` decimal(9,2) DEFAULT NULL,
  `actual_tardines` decimal(9,2) DEFAULT NULL,
  `adjust_tardines` decimal(9,2) DEFAULT NULL,
  `total_tardines` decimal(9,2) DEFAULT NULL,
  `actual_undertime` decimal(9,2) DEFAULT NULL,
  `adjust_undertime` decimal(9,2) DEFAULT NULL,
  `total_undertime` decimal(9,2) DEFAULT NULL,
  `monthly_rate_amount` decimal(9,2) DEFAULT NULL,
  `annual_rate_amount` decimal(9,2) DEFAULT NULL,
  `basic_net_pay` decimal(9,2) DEFAULT NULL,
  `actual_contribution` decimal(9,2) DEFAULT NULL,
  `adjust_contribution` decimal(9,2) DEFAULT NULL,
  `total_contribution` decimal(9,2) DEFAULT NULL,
  `actual_loan` decimal(9,2) DEFAULT NULL,
  `adjust_loan` decimal(9,2) DEFAULT NULL,
  `total_loan` decimal(9,2) DEFAULT NULL,
  `actual_otherdeduct` decimal(9,2) DEFAULT NULL,
  `adjust_otherdeduct` decimal(9,2) DEFAULT NULL,
  `total_otherdeduct` decimal(9,2) DEFAULT NULL,
  `net_deduction` decimal(9,2) DEFAULT NULL,
  `tax_rate_amount_one` decimal(9,2) DEFAULT NULL,
  `tax_rate_amount_two` decimal(9,2) DEFAULT NULL,
  `allowances` decimal(9,2) DEFAULT NULL,
  `gross_pay` decimal(9,2) DEFAULT NULL,
  `gross_taxable_pay` decimal(9,2) DEFAULT NULL,
  `net_pay` decimal(9,2) DEFAULT NULL,
  `pay_period` varchar(225) DEFAULT NULL,
  `sub_pay_period` varchar(225) DEFAULT NULL,
  `hold` int(11) DEFAULT NULL,
  `year` varchar(225) DEFAULT NULL,
  `month` varchar(225) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table pids_hris.pms_nonplantilla_transactions: ~0 rows (approximately)
/*!40000 ALTER TABLE `pms_nonplantilla_transactions` DISABLE KEYS */;
/*!40000 ALTER TABLE `pms_nonplantilla_transactions` ENABLE KEYS */;

-- Dumping structure for table pids_hris.pms_offices
DROP TABLE IF EXISTS `pms_offices`;
CREATE TABLE IF NOT EXISTS `pms_offices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(50) DEFAULT NULL,
  `name` varchar(225) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

-- Dumping data for table pids_hris.pms_offices: ~19 rows (approximately)
/*!40000 ALTER TABLE `pms_offices` DISABLE KEYS */;
INSERT INTO `pms_offices` (`id`, `code`, `name`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
	(1, NULL, 'Office of the Chairman', NULL, NULL, NULL, NULL),
	(2, NULL, 'Administrative Office', NULL, NULL, NULL, NULL),
	(3, NULL, 'Finance and Planning Management Office', NULL, NULL, NULL, NULL),
	(4, NULL, 'Competition Enforcement Office', NULL, NULL, NULL, NULL),
	(5, NULL, 'Mergers and Acquisitions Office', NULL, NULL, NULL, NULL),
	(6, 'EO', 'Economics Office', NULL, '2018-07-25 05:15:56', NULL, 2),
	(7, NULL, 'Communications and Knowledge Management Office', NULL, NULL, NULL, NULL),
	(8, NULL, 'Legal Office', NULL, NULL, NULL, NULL),
	(9, NULL, 'Office of the Executive Director', NULL, NULL, NULL, NULL),
	(10, NULL, 'Policy Research and Knowledge Management Office', NULL, NULL, NULL, NULL),
	(11, NULL, 'Administrative and Legal Offfice', NULL, NULL, NULL, NULL),
	(12, NULL, 'Financial&#44; Planning and Management Office', NULL, NULL, NULL, NULL),
	(13, NULL, 'Corporate Planning and Management Division', NULL, NULL, NULL, NULL),
	(14, NULL, 'Office of Comm JRB', NULL, NULL, NULL, NULL),
	(15, NULL, 'Office of Comm SAQ', NULL, NULL, NULL, NULL),
	(16, NULL, 'Office of Comm ERB', NULL, NULL, NULL, NULL),
	(17, NULL, 'Office of Comm ACA', NULL, NULL, NULL, NULL),
	(18, NULL, 'Office of Commissioner Stella Luz A. Quimbo', NULL, NULL, NULL, NULL),
	(19, NULL, 'Office of Commissioner Amabelle C. Asuncion', NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `pms_offices` ENABLE KEYS */;

-- Dumping structure for table pids_hris.pms_overtime
DROP TABLE IF EXISTS `pms_overtime`;
CREATE TABLE IF NOT EXISTS `pms_overtime` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) DEFAULT NULL,
  `previous_balance` decimal(9,2) DEFAULT NULL,
  `used_amount` decimal(9,2) DEFAULT NULL,
  `available_balance` decimal(9,2) DEFAULT NULL,
  `actual_regular_overtime` decimal(9,2) DEFAULT NULL,
  `adjust_regular_overtime` decimal(9,2) DEFAULT NULL,
  `total_regular_amount` decimal(9,2) DEFAULT NULL,
  `actual_special_overtime` decimal(9,2) DEFAULT NULL,
  `adjust_special_overtime` decimal(9,2) DEFAULT NULL,
  `total_special_amount` decimal(9,2) DEFAULT NULL,
  `actual_regular_holiday_overtime` decimal(9,2) DEFAULT NULL,
  `adjust_regular_holiday_overtime` decimal(9,2) DEFAULT NULL,
  `total_regular_holiday_amount` decimal(9,2) DEFAULT NULL,
  `total_overtime_amount` decimal(9,2) DEFAULT NULL,
  `year` varchar(50) DEFAULT NULL,
  `month` varchar(50) DEFAULT NULL,
  `pay_period` varchar(50) DEFAULT NULL,
  `sub_pay_period` varchar(50) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table pids_hris.pms_overtime: ~0 rows (approximately)
/*!40000 ALTER TABLE `pms_overtime` DISABLE KEYS */;
/*!40000 ALTER TABLE `pms_overtime` ENABLE KEYS */;

-- Dumping structure for table pids_hris.pms_pagibigpolicy
DROP TABLE IF EXISTS `pms_pagibigpolicy`;
CREATE TABLE IF NOT EXISTS `pms_pagibigpolicy` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `policy_name` varchar(225) DEFAULT NULL,
  `pay_period` varchar(225) DEFAULT NULL,
  `deduction_period` varchar(225) DEFAULT NULL,
  `policy_type` varchar(225) DEFAULT NULL,
  `based_on` varchar(225) DEFAULT NULL,
  `value` decimal(9,2) DEFAULT NULL,
  `remarks` varchar(225) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table pids_hris.pms_pagibigpolicy: ~1 rows (approximately)
/*!40000 ALTER TABLE `pms_pagibigpolicy` DISABLE KEYS */;
INSERT INTO `pms_pagibigpolicy` (`id`, `policy_name`, `pay_period`, `deduction_period`, `policy_type`, `based_on`, `value`, `remarks`, `created_at`, `updated_at`, `updated_by`) VALUES
	(2, 'STANDARD POLICY', NULL, 'Both', 'System Generated', 'Monthly Salary', NULL, NULL, '2018-06-21 03:12:28', '2018-06-21 03:12:28', NULL);
/*!40000 ALTER TABLE `pms_pagibigpolicy` ENABLE KEYS */;

-- Dumping structure for table pids_hris.pms_payroll_information
DROP TABLE IF EXISTS `pms_payroll_information`;
CREATE TABLE IF NOT EXISTS `pms_payroll_information` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bp_no` varchar(225) DEFAULT NULL,
  `employee_id` int(11) DEFAULT NULL,
  `gsispolicy_id` int(11) DEFAULT NULL,
  `pagibigpolicy_id` int(11) DEFAULT NULL,
  `philhealthpolicy_id` int(11) DEFAULT NULL,
  `taxpolicy_id` int(11) DEFAULT NULL,
  `bank_id` int(11) DEFAULT NULL,
  `bankbranch_id` int(11) DEFAULT NULL,
  `wagestatus_id` int(11) DEFAULT NULL,
  `daily_rate_amount` decimal(9,2) DEFAULT NULL,
  `monthly_rate_amount` decimal(9,2) DEFAULT NULL,
  `annual_rate_amount` decimal(9,2) DEFAULT NULL,
  `pagibig_contribution` decimal(9,2) DEFAULT NULL,
  `er_pagibig_share` decimal(9,2) DEFAULT NULL,
  `philhealth_contribution` decimal(9,2) DEFAULT NULL,
  `er_philhealth_share` decimal(9,2) DEFAULT NULL,
  `gsis_contribution` decimal(9,2) DEFAULT NULL,
  `er_gsis_share` decimal(9,2) DEFAULT NULL,
  `tax_contribution` decimal(9,2) DEFAULT NULL,
  `overtime_balance_amount` decimal(9,2) DEFAULT NULL,
  `atm_no` varchar(225) DEFAULT NULL,
  `pagibig2` decimal(9,2) DEFAULT NULL,
  `pagibig_personal` decimal(9,2) DEFAULT NULL,
  `no_ofdays_inayear` int(11) DEFAULT NULL,
  `no_ofdays_inamonth` int(11) DEFAULT NULL,
  `total_hours_inaday` int(11) DEFAULT NULL,
  `tax_payperiod` varchar(255) DEFAULT NULL,
  `tax_bracket` varchar(255) DEFAULT NULL,
  `tax_bracket_amount` decimal(9,2) DEFAULT NULL,
  `tax_inexcess` decimal(9,2) DEFAULT NULL,
  `union_dues` decimal(9,2) DEFAULT NULL,
  `mid_year_bonus` tinyint(4) DEFAULT NULL,
  `year_end_bonus` tinyint(4) DEFAULT NULL,
  `tax_id_number` varchar(225) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=90 DEFAULT CHARSET=latin1;

-- Dumping data for table pids_hris.pms_payroll_information: ~89 rows (approximately)
/*!40000 ALTER TABLE `pms_payroll_information` DISABLE KEYS */;
INSERT INTO `pms_payroll_information` (`id`, `bp_no`, `employee_id`, `gsispolicy_id`, `pagibigpolicy_id`, `philhealthpolicy_id`, `taxpolicy_id`, `bank_id`, `bankbranch_id`, `wagestatus_id`, `daily_rate_amount`, `monthly_rate_amount`, `annual_rate_amount`, `pagibig_contribution`, `er_pagibig_share`, `philhealth_contribution`, `er_philhealth_share`, `gsis_contribution`, `er_gsis_share`, `tax_contribution`, `overtime_balance_amount`, `atm_no`, `pagibig2`, `pagibig_personal`, `no_ofdays_inayear`, `no_ofdays_inamonth`, `total_hours_inaday`, `tax_payperiod`, `tax_bracket`, `tax_bracket_amount`, `tax_inexcess`, `union_dues`, `mid_year_bonus`, `year_end_bonus`, `tax_id_number`, `created_at`, `updated_at`, `updated_by`) VALUES
	(1, NULL, 75, 1, 2, 2, 2, NULL, NULL, NULL, 1913.59, 42099.00, 505188.00, 100.00, 100.00, 550.00, 550.00, 3788.91, 5051.88, 3581.77, 252594.00, NULL, 0.00, 0.00, NULL, 22, 8, NULL, NULL, 0.00, 0.00, NULL, NULL, NULL, NULL, '2018-09-22 04:19:39', '2018-09-22 04:32:14', NULL),
	(2, NULL, 1, 1, 2, 2, 2, NULL, NULL, NULL, 3072.05, 67585.00, 811020.00, 100.00, 100.00, 550.00, 550.00, 6082.65, 8110.20, 9379.84, 405510.00, NULL, 0.00, 0.00, NULL, 22, 8, NULL, NULL, 0.00, 0.00, NULL, NULL, NULL, NULL, '2018-09-22 04:20:09', '2018-09-22 10:10:51', NULL),
	(3, NULL, 46, 1, 2, 2, 2, NULL, NULL, NULL, 3747.23, 82439.00, 989268.00, 100.00, 100.00, 550.00, 550.00, 7419.51, 9892.68, 13144.08, 494634.00, NULL, 0.00, 0.00, NULL, 22, 8, NULL, NULL, 0.00, 0.00, NULL, NULL, NULL, NULL, '2018-09-22 04:21:57', '2018-09-22 04:32:23', NULL),
	(4, NULL, 87, 1, 2, 2, 2, NULL, NULL, NULL, 2668.95, 58717.00, 704604.00, 100.00, 100.00, 550.00, 550.00, 5284.53, 7046.04, 7362.37, 352302.00, NULL, 0.00, 0.00, NULL, 22, 8, NULL, NULL, 0.00, 0.00, NULL, NULL, NULL, NULL, '2018-09-22 04:22:10', '2018-09-22 04:32:28', NULL),
	(5, NULL, 2, 1, 2, 2, 2, NULL, NULL, NULL, 4646.23, 102217.00, 1226604.00, 100.00, 100.00, 550.00, 550.00, 9199.53, 12266.04, 18543.47, 613302.00, NULL, 0.00, 0.00, NULL, 22, 8, NULL, NULL, 0.00, 0.00, NULL, NULL, NULL, NULL, '2018-09-22 04:22:29', '2018-09-22 10:13:43', NULL),
	(6, NULL, 3, 1, 2, 2, 2, NULL, NULL, NULL, 576.09, 12674.00, 152088.00, 100.00, 100.00, 174.27, 174.27, 1140.66, 1520.88, 0.00, 76044.00, NULL, 0.00, 0.00, NULL, 22, 8, NULL, NULL, 0.00, 0.00, NULL, NULL, NULL, NULL, '2018-09-22 04:31:37', '2018-09-22 10:14:06', NULL),
	(7, NULL, 23, 1, 2, 2, 2, NULL, NULL, NULL, 4249.45, 93488.00, 1121856.00, 100.00, 100.00, 550.00, 550.00, 8413.92, 11218.56, 16160.45, 560928.00, NULL, 0.00, 0.00, NULL, 22, 8, NULL, NULL, 0.00, 0.00, NULL, NULL, NULL, NULL, '2018-09-22 04:32:49', '2018-09-22 10:23:25', NULL),
	(8, NULL, 47, 1, 2, 2, 2, NULL, NULL, NULL, 4249.45, 93488.00, 1121856.00, 100.00, 100.00, 550.00, 550.00, 8413.92, 11218.56, 16160.45, 560928.00, NULL, 0.00, 0.00, NULL, 22, 8, NULL, '66667', 10833.33, 17757.08, NULL, NULL, NULL, NULL, '2018-09-22 04:33:04', '2018-09-22 04:33:04', NULL),
	(9, NULL, 30, 1, 2, 2, 2, NULL, NULL, NULL, 2749.59, 60491.00, 725892.00, 100.00, 100.00, 550.00, 550.00, 5444.19, 7258.92, 7765.95, 362946.00, NULL, 0.00, 0.00, NULL, 22, 8, NULL, NULL, 0.00, 0.00, NULL, NULL, NULL, NULL, '2018-09-22 04:33:17', '2018-09-22 10:26:12', NULL),
	(10, NULL, 44, 1, 2, 2, 2, NULL, NULL, NULL, 2170.09, 47742.00, 572904.00, 100.00, 100.00, 550.00, 550.00, 4296.78, 5729.04, 4865.56, 286452.00, NULL, 0.00, 0.00, NULL, 22, 8, NULL, '33333', 2500.00, 9462.22, NULL, NULL, NULL, NULL, '2018-09-22 04:33:30', '2018-09-22 04:33:30', NULL),
	(11, NULL, 76, 1, 2, 2, 2, NULL, NULL, NULL, 1101.09, 24224.00, 290688.00, 100.00, 100.00, 333.08, 333.08, 2180.16, 2906.88, 155.55, 145344.00, NULL, 0.00, 0.00, NULL, 22, 8, NULL, '20833', 0.00, 777.76, NULL, NULL, NULL, NULL, '2018-09-22 04:33:43', '2018-09-22 04:33:43', NULL),
	(12, NULL, 36, 1, 2, 2, 2, NULL, NULL, NULL, 1307.23, 28759.00, 345108.00, 100.00, 100.00, 395.44, 395.44, 2588.31, 3451.08, 968.45, 172554.00, NULL, 0.00, 0.00, NULL, 22, 8, NULL, NULL, 0.00, 0.00, NULL, NULL, NULL, NULL, '2018-09-22 04:33:55', '2018-09-22 10:27:54', NULL),
	(13, NULL, 89, 1, 2, 2, 2, NULL, NULL, NULL, 1101.09, 24224.00, 290688.00, 100.00, 100.00, 333.08, 333.08, 2180.16, 2906.88, 155.55, 145344.00, NULL, 0.00, 0.00, NULL, 22, 8, NULL, '20833', 0.00, 777.76, NULL, NULL, NULL, NULL, '2018-09-22 04:34:10', '2018-09-22 04:34:10', NULL),
	(14, NULL, 48, 1, 2, 2, 2, NULL, NULL, NULL, 1443.86, 31765.00, 381180.00, 100.00, 100.00, 436.77, 436.77, 2858.85, 3811.80, 1507.28, 190590.00, NULL, 0.00, 0.00, NULL, 22, 8, NULL, '20833', 0.00, 7536.38, NULL, NULL, NULL, NULL, '2018-09-22 04:34:27', '2018-09-22 04:34:27', NULL),
	(15, NULL, 4, 1, 2, 2, 2, NULL, NULL, NULL, 1971.41, 43371.00, 520452.00, 100.00, 100.00, 550.00, 550.00, 3903.39, 5204.52, 3871.15, 260226.00, NULL, 0.00, 0.00, NULL, 22, 8, NULL, NULL, 0.00, 0.00, NULL, NULL, NULL, NULL, '2018-09-22 04:35:22', '2018-09-22 10:14:30', NULL),
	(16, NULL, 40, 1, 2, 2, 2, NULL, NULL, NULL, 3331.77, 73299.00, 879588.00, 100.00, 100.00, 550.00, 550.00, 6596.91, 8795.88, 10679.77, 439794.00, NULL, 0.00, 0.00, NULL, 22, 8, NULL, NULL, 0.00, 0.00, NULL, NULL, NULL, NULL, '2018-09-22 04:35:37', '2018-09-22 10:29:37', NULL),
	(17, NULL, 5, 1, 2, 2, 2, NULL, NULL, NULL, 1837.86, 40433.00, 485196.00, 100.00, 100.00, 555.95, 555.95, 3638.97, 4851.96, 3201.27, 242598.00, NULL, 0.00, 0.00, NULL, 22, 8, NULL, NULL, 0.00, 0.00, NULL, NULL, NULL, NULL, '2018-09-22 04:35:55', '2018-09-22 10:14:52', NULL),
	(18, NULL, 77, 1, 2, 2, 2, NULL, NULL, NULL, 1101.09, 24224.00, 290688.00, 100.00, 100.00, 333.08, 333.08, 2180.16, 2906.88, 155.55, 145344.00, NULL, 0.00, 0.00, NULL, 22, 8, NULL, '20833', 0.00, 777.76, NULL, NULL, NULL, NULL, '2018-09-22 04:36:56', '2018-09-22 04:36:56', NULL),
	(19, NULL, 45, 1, 2, 2, 2, NULL, NULL, NULL, 5226.41, 114981.00, 1379772.00, 100.00, 100.00, 550.00, 550.00, 10348.29, 13797.72, 22028.04, 689886.00, NULL, 0.00, 0.00, NULL, 22, 8, NULL, '66667', 10833.33, 37315.71, NULL, NULL, NULL, NULL, '2018-09-22 04:37:13', '2018-09-22 04:37:13', NULL),
	(20, NULL, 78, 1, 2, 2, 2, NULL, NULL, NULL, 1213.16, 26689.55, 320274.60, 100.00, 100.00, 366.98, 366.98, 2402.06, 3202.75, 597.50, 160137.30, NULL, 0.00, 0.00, NULL, 22, 8, NULL, '20833', 0.00, 2987.51, NULL, NULL, NULL, NULL, '2018-09-22 04:37:30', '2018-09-22 04:37:30', NULL),
	(21, NULL, 79, 1, 2, 2, 2, NULL, NULL, NULL, 1731.14, 38085.00, 457020.00, 100.00, 100.00, 523.67, 523.67, 3427.65, 4570.20, 2675.17, 228510.00, NULL, 0.00, 0.00, NULL, 22, 8, NULL, '33333', 2500.00, 700.68, NULL, NULL, NULL, NULL, '2018-09-22 04:37:43', '2018-09-22 04:37:43', NULL),
	(22, NULL, 49, 1, 2, 2, 2, NULL, NULL, NULL, 850.82, 18718.00, 224616.00, 100.00, 100.00, 257.37, 257.37, 1684.62, 2246.16, 0.00, 112308.00, NULL, 0.00, 0.00, NULL, 22, 8, NULL, '0', 0.00, 16676.01, NULL, NULL, NULL, NULL, '2018-09-22 04:38:01', '2018-09-22 04:38:01', NULL),
	(23, NULL, 72, 1, 2, 2, 2, NULL, NULL, NULL, 1773.05, 39007.00, 468084.00, 100.00, 100.00, 536.35, 536.35, 3510.63, 4680.84, 2881.76, 234042.00, NULL, 0.00, 0.00, NULL, 22, 8, NULL, '33333', 2500.00, 1527.02, NULL, NULL, NULL, NULL, '2018-09-22 04:38:17', '2018-09-22 04:38:17', NULL),
	(24, NULL, 80, 1, 2, 2, 2, NULL, NULL, NULL, 2668.95, 58717.00, 704604.00, 100.00, 100.00, 550.00, 550.00, 5284.53, 7046.04, 7362.37, 352302.00, NULL, 0.00, 0.00, NULL, 22, 8, NULL, '33333', 2500.00, 19449.47, NULL, NULL, NULL, NULL, '2018-09-22 04:42:43', '2018-09-22 04:42:43', NULL),
	(25, NULL, 50, 1, 2, 2, 2, NULL, NULL, NULL, 4377.82, 96312.00, 1155744.00, 100.00, 100.00, 550.00, 550.00, 8668.08, 11557.44, 16931.41, 577872.00, NULL, 0.00, 0.00, NULL, 22, 8, NULL, '66667', 10833.33, 20326.92, NULL, NULL, NULL, NULL, '2018-09-22 04:44:27', '2018-09-22 04:44:27', NULL),
	(26, NULL, 51, 1, 2, 2, 2, NULL, NULL, NULL, 1101.09, 24224.00, 290688.00, 100.00, 100.00, 333.08, 333.08, 2180.16, 2906.88, 155.55, 145344.00, NULL, 0.00, 0.00, NULL, 22, 8, NULL, '20833', 0.00, 777.76, NULL, NULL, NULL, NULL, '2018-09-22 04:44:43', '2018-09-22 04:44:43', NULL),
	(27, NULL, 6, 1, 2, 2, 2, NULL, NULL, NULL, 1731.14, 38085.00, 457020.00, 100.00, 100.00, 523.67, 523.67, 3427.65, 4570.20, 2675.17, 228510.00, NULL, 0.00, 0.00, NULL, 22, 8, NULL, NULL, 0.00, 0.00, NULL, NULL, NULL, NULL, '2018-09-22 04:44:57', '2018-09-22 10:15:50', NULL),
	(28, NULL, 7, 1, 2, 2, 2, NULL, NULL, NULL, 605.23, 13315.00, 159780.00, 100.00, 100.00, 183.08, 183.08, 1198.35, 1597.80, 0.00, 79890.00, NULL, 0.00, 0.00, NULL, 22, 8, NULL, NULL, 0.00, 0.00, NULL, NULL, NULL, NULL, '2018-09-22 04:45:15', '2018-09-22 10:16:19', NULL),
	(29, NULL, 8, 1, 2, 2, 2, NULL, NULL, NULL, 590.45, 12990.00, 155880.00, 100.00, 100.00, 178.61, 178.61, 1169.10, 1558.80, 0.00, 77940.00, NULL, 0.00, 0.00, NULL, 22, 8, NULL, NULL, 0.00, 0.00, NULL, NULL, NULL, NULL, '2018-09-22 04:45:29', '2018-09-22 10:16:34', NULL),
	(30, NULL, 52, 1, 2, 2, 2, NULL, NULL, NULL, 850.82, 18718.00, 224616.00, 100.00, 100.00, 257.37, 257.37, 1684.62, 2246.16, 0.00, 112308.00, NULL, 0.00, 0.00, NULL, 22, 8, NULL, '0', 0.00, 16676.01, NULL, NULL, NULL, NULL, '2018-09-22 04:45:46', '2018-09-22 04:45:46', NULL),
	(31, NULL, 31, 1, 2, 2, 2, NULL, NULL, NULL, 2708.95, 59597.00, 715164.00, 100.00, 100.00, 550.00, 550.00, 5363.73, 7151.64, 7562.57, 357582.00, NULL, 0.00, 0.00, NULL, 22, 8, NULL, NULL, 0.00, 0.00, NULL, NULL, NULL, NULL, '2018-09-22 04:46:19', '2018-09-22 10:26:27', NULL),
	(32, NULL, 86, 1, 2, 2, 2, NULL, NULL, NULL, 2668.95, 58717.00, 704604.00, 100.00, 100.00, 550.00, 550.00, 5284.53, 7046.04, 7362.37, 352302.00, NULL, 0.00, 0.00, NULL, 22, 8, NULL, '33333', 2500.00, 19449.47, NULL, NULL, NULL, NULL, '2018-09-22 04:46:33', '2018-09-22 04:46:33', NULL),
	(33, NULL, 53, 1, 2, 2, 2, NULL, NULL, NULL, 2790.77, 61397.00, 736764.00, 100.00, 100.00, 550.00, 550.00, 5525.73, 7367.64, 7972.07, 368382.00, NULL, 0.00, 0.00, NULL, 22, 8, NULL, '33333', 2500.00, 21888.27, NULL, NULL, NULL, NULL, '2018-09-22 04:46:48', '2018-09-22 04:46:48', NULL),
	(34, NULL, 41, 1, 2, 2, 2, NULL, NULL, NULL, 2668.95, 58717.00, 704604.00, 100.00, 100.00, 550.00, 550.00, 5284.53, 7046.04, 7362.37, 352302.00, NULL, 0.00, 0.00, NULL, 22, 8, NULL, NULL, 0.00, 0.00, NULL, NULL, NULL, NULL, '2018-09-22 04:47:03', '2018-09-22 10:29:52', NULL),
	(35, NULL, 54, 1, 2, 2, 2, NULL, NULL, NULL, 4186.73, 92108.00, 1105296.00, 100.00, 100.00, 550.00, 550.00, 8289.72, 11052.96, 15783.71, 552648.00, NULL, 0.00, 0.00, NULL, 22, 8, NULL, '66667', 10833.33, 16501.28, NULL, NULL, NULL, NULL, '2018-09-22 04:47:45', '2018-09-22 04:47:45', NULL),
	(36, NULL, 81, 1, 2, 2, 2, NULL, NULL, NULL, 1318.64, 29010.00, 348120.00, 100.00, 100.00, 398.89, 398.89, 2610.90, 3481.20, 1013.44, 174060.00, NULL, 0.00, 0.00, NULL, 22, 8, NULL, '20833', 0.00, 5067.21, NULL, NULL, NULL, NULL, '2018-09-22 04:48:10', '2018-09-22 04:48:10', NULL),
	(37, NULL, 9, 1, 2, 2, 2, NULL, NULL, NULL, 850.82, 18718.00, 224616.00, 100.00, 100.00, 257.37, 257.37, 1684.62, 2246.16, 0.00, 112308.00, NULL, 0.00, 0.00, NULL, 22, 8, NULL, NULL, 0.00, 0.00, NULL, NULL, NULL, NULL, '2018-09-22 04:48:28', '2018-09-22 10:16:53', NULL),
	(38, NULL, 55, 1, 2, 2, 2, NULL, NULL, NULL, 4186.73, 92108.00, 1105296.00, 100.00, 100.00, 550.00, 550.00, 8289.72, 11052.96, 15783.71, 552648.00, NULL, 0.00, 0.00, NULL, 22, 8, NULL, '66667', 10833.33, 16501.28, NULL, NULL, NULL, NULL, '2018-09-22 04:48:40', '2018-09-22 04:48:40', NULL),
	(39, NULL, 82, 1, 2, 2, 2, NULL, NULL, NULL, 541.55, 11914.00, 142968.00, 100.00, 100.00, 163.82, 163.82, 1072.26, 1429.68, 0.00, 71484.00, NULL, 0.00, 0.00, NULL, 22, 8, NULL, '0', 0.00, 10577.92, NULL, NULL, NULL, NULL, '2018-09-22 04:49:09', '2018-09-22 04:49:09', NULL),
	(40, NULL, 10, 1, 2, 2, 2, NULL, NULL, NULL, 657.23, 14459.00, 173508.00, 100.00, 100.00, 198.81, 198.81, 1301.31, 1735.08, 0.00, 86754.00, NULL, 0.00, 0.00, NULL, 22, 8, NULL, NULL, 0.00, 0.00, NULL, NULL, NULL, NULL, '2018-09-22 04:49:24', '2018-09-22 10:17:11', NULL),
	(41, NULL, 27, 1, 2, 2, 2, NULL, NULL, NULL, 2000.91, 44020.00, 528240.00, 100.00, 100.00, 550.00, 550.00, 3961.80, 5282.40, 4018.80, 264120.00, NULL, 0.00, 0.00, NULL, 22, 8, NULL, NULL, 0.00, 0.00, NULL, NULL, NULL, NULL, '2018-09-22 04:52:01', '2018-09-22 10:24:51', NULL),
	(42, NULL, 83, 1, 2, 2, 2, NULL, NULL, NULL, 576.09, 12674.00, 152088.00, 100.00, 100.00, 174.27, 174.27, 1140.66, 1520.88, 0.00, 76044.00, NULL, 0.00, 0.00, NULL, 22, 8, NULL, '0', 0.00, 11259.07, NULL, NULL, NULL, NULL, '2018-09-22 04:53:32', '2018-09-22 04:53:32', NULL),
	(43, NULL, 11, 1, 2, 2, 2, NULL, NULL, NULL, 585.64, 12884.00, 154608.00, 100.00, 100.00, 177.16, 177.16, 1159.56, 1546.08, 0.00, 77304.00, NULL, 0.00, 0.00, NULL, 22, 8, NULL, NULL, 0.00, 0.00, NULL, NULL, NULL, NULL, '2018-09-22 04:53:50', '2018-09-22 10:18:33', NULL),
	(44, NULL, 56, 1, 2, 2, 2, NULL, NULL, NULL, 2668.95, 58717.00, 704604.00, 100.00, 100.00, 550.00, 550.00, 5284.53, 7046.04, 7362.37, 352302.00, NULL, 0.00, 0.00, NULL, 22, 8, NULL, '33333', 2500.00, 19449.47, NULL, NULL, NULL, NULL, '2018-09-22 04:54:06', '2018-09-22 04:54:06', NULL),
	(45, NULL, 32, 1, 2, 2, 2, NULL, NULL, NULL, 850.82, 18718.00, 224616.00, 100.00, 100.00, 257.37, 257.37, 1684.62, 2246.16, 0.00, 112308.00, NULL, 0.00, 0.00, NULL, 22, 8, NULL, NULL, 0.00, 0.00, NULL, NULL, NULL, NULL, '2018-09-22 04:54:45', '2018-09-22 10:26:43', NULL),
	(46, NULL, 28, 1, 2, 2, 2, NULL, NULL, NULL, 2000.91, 44020.00, 528240.00, 100.00, 100.00, 550.00, 550.00, 3961.80, 5282.40, 4018.80, 264120.00, NULL, 0.00, 0.00, NULL, 22, 8, NULL, NULL, 0.00, 0.00, NULL, NULL, NULL, NULL, '2018-09-22 04:55:01', '2018-09-22 10:25:44', NULL),
	(47, NULL, 12, 1, 2, 2, 2, NULL, NULL, NULL, 576.09, 12674.00, 152088.00, 100.00, 100.00, 174.27, 174.27, 1140.66, 1520.88, 0.00, 76044.00, NULL, 0.00, 0.00, NULL, 22, 8, NULL, NULL, 0.00, 0.00, NULL, NULL, NULL, NULL, '2018-09-22 04:55:28', '2018-09-22 10:19:16', NULL),
	(48, NULL, 29, 1, 2, 2, 2, NULL, NULL, NULL, 2708.95, 59597.00, 715164.00, 100.00, 100.00, 550.00, 550.00, 5363.73, 7151.64, 7562.57, 357582.00, NULL, 0.00, 0.00, NULL, 22, 8, NULL, NULL, 0.00, 0.00, NULL, NULL, NULL, NULL, '2018-09-22 04:55:43', '2018-09-22 10:25:56', NULL),
	(49, NULL, 33, 1, 2, 2, 2, NULL, NULL, NULL, 1334.50, 29359.00, 352308.00, 100.00, 100.00, 403.69, 403.69, 2642.31, 3523.08, 1076.00, 176154.00, NULL, 0.00, 0.00, NULL, 22, 8, NULL, NULL, 0.00, 0.00, NULL, NULL, NULL, NULL, '2018-09-22 04:56:02', '2018-09-22 10:27:02', NULL),
	(50, NULL, 13, 1, 2, 2, 2, NULL, NULL, NULL, 1318.64, 29010.00, 348120.00, 100.00, 100.00, 398.89, 398.89, 2610.90, 3481.20, 1013.44, 174060.00, NULL, 0.00, 0.00, NULL, 22, 8, NULL, NULL, 0.00, 0.00, NULL, NULL, NULL, NULL, '2018-09-22 04:59:53', '2018-09-22 10:19:35', NULL),
	(51, NULL, 88, 1, 2, 2, 2, NULL, NULL, NULL, 1101.09, 24224.00, 290688.00, 100.00, 100.00, 333.08, 333.08, 2180.16, 2906.88, 155.55, 145344.00, NULL, 0.00, 0.00, NULL, 22, 8, NULL, '20833', 0.00, 777.76, NULL, NULL, NULL, NULL, '2018-09-22 05:00:12', '2018-09-22 05:00:12', NULL),
	(52, NULL, 14, 1, 2, 2, 2, NULL, NULL, NULL, 1318.64, 29010.00, 348120.00, 100.00, 100.00, 398.89, 398.89, 2610.90, 3481.20, 1013.44, 174060.00, NULL, 0.00, 0.00, NULL, 22, 8, NULL, NULL, 0.00, 0.00, NULL, NULL, NULL, NULL, '2018-09-22 05:00:29', '2018-09-22 10:19:55', NULL),
	(53, NULL, 71, 1, 2, 2, 2, NULL, NULL, NULL, 1101.09, 24224.00, 290688.00, 100.00, 100.00, 333.08, 333.08, 2180.16, 2906.88, 155.55, 145344.00, NULL, 0.00, 0.00, NULL, 22, 8, NULL, '20833', 0.00, 777.76, NULL, NULL, NULL, NULL, '2018-09-22 05:00:49', '2018-09-22 05:00:49', NULL),
	(54, NULL, 57, 1, 2, 2, 2, NULL, NULL, NULL, 4646.23, 102217.00, 1226604.00, 100.00, 100.00, 550.00, 550.00, 9199.53, 12266.04, 18543.47, 613302.00, NULL, 0.00, 0.00, NULL, 22, 8, NULL, '66667', 10833.33, 25700.47, NULL, NULL, NULL, NULL, '2018-09-22 05:01:07', '2018-09-22 05:01:07', NULL),
	(55, NULL, 85, 1, 2, 2, 2, NULL, NULL, NULL, 1101.09, 24224.00, 290688.00, 100.00, 100.00, 333.08, 333.08, 2180.16, 2906.88, 155.55, 145344.00, NULL, 0.00, 0.00, NULL, 22, 8, NULL, '20833', 0.00, 777.76, NULL, NULL, NULL, NULL, '2018-09-22 05:01:25', '2018-09-22 05:01:25', NULL),
	(56, NULL, 73, 1, 2, 2, 2, NULL, NULL, NULL, 2749.59, 60491.00, 725892.00, 100.00, 100.00, 550.00, 550.00, 5444.19, 7258.92, 7765.95, 362946.00, NULL, 0.00, 0.00, NULL, 22, 8, NULL, '33333', 2500.00, 21063.81, NULL, NULL, NULL, NULL, '2018-09-22 05:01:42', '2018-09-22 05:01:42', NULL),
	(57, NULL, 37, 1, 2, 2, 2, NULL, NULL, NULL, 1837.86, 40433.00, 485196.00, 100.00, 100.00, 555.95, 555.95, 3638.97, 4851.96, 3201.27, 242598.00, NULL, 0.00, 0.00, NULL, 22, 8, NULL, NULL, 0.00, 0.00, NULL, NULL, NULL, NULL, '2018-09-22 05:01:59', '2018-09-22 10:28:20', NULL),
	(58, NULL, 58, 1, 2, 2, 2, NULL, NULL, NULL, 850.82, 18718.00, 224616.00, 100.00, 100.00, 257.37, 257.37, 1684.62, 2246.16, 0.00, 112308.00, NULL, 0.00, 0.00, NULL, 22, 8, NULL, '0', 0.00, 16676.01, NULL, NULL, NULL, NULL, '2018-09-22 05:02:16', '2018-09-22 05:02:16', NULL),
	(59, NULL, 24, 1, 2, 2, 2, NULL, NULL, NULL, 1218.45, 26806.00, 321672.00, 100.00, 100.00, 368.58, 368.58, 2412.54, 3216.72, 618.38, 160836.00, NULL, 0.00, 0.00, NULL, 22, 8, NULL, NULL, 0.00, 0.00, NULL, NULL, NULL, NULL, '2018-09-22 05:02:36', '2018-09-22 10:23:47', NULL),
	(60, NULL, 15, 1, 2, 2, 2, NULL, NULL, NULL, 3331.77, 73299.00, 879588.00, 100.00, 100.00, 550.00, 550.00, 6596.91, 8795.88, 10679.77, 439794.00, NULL, 0.00, 0.00, NULL, 22, 8, NULL, NULL, 0.00, 0.00, NULL, NULL, NULL, NULL, '2018-09-22 05:05:48', '2018-09-22 10:20:16', NULL),
	(61, NULL, 59, 1, 2, 2, 2, NULL, NULL, NULL, 1971.41, 43371.00, 520452.00, 100.00, 100.00, 550.00, 550.00, 3903.39, 5204.52, 3871.15, 260226.00, NULL, 0.00, 0.00, NULL, 22, 8, NULL, '33333', 2500.00, 5484.61, NULL, NULL, NULL, NULL, '2018-09-22 05:06:03', '2018-09-22 05:06:03', NULL),
	(62, NULL, 22, 1, 2, 2, 2, NULL, NULL, NULL, 1860.00, 40920.00, 491040.00, 100.00, 100.00, 562.65, 562.65, 3682.80, 4910.40, 3310.39, 245520.00, NULL, 0.00, 1000.00, NULL, 22, 8, NULL, NULL, 0.00, 0.00, NULL, NULL, NULL, NULL, '2018-09-22 05:06:58', '2018-09-22 10:23:08', NULL),
	(63, NULL, 35, 1, 2, 2, 2, NULL, NULL, NULL, 1318.64, 29010.00, 348120.00, 100.00, 100.00, 398.89, 398.89, 2610.90, 3481.20, 1013.44, 174060.00, NULL, 0.00, 0.00, NULL, 22, 8, NULL, NULL, 0.00, 0.00, NULL, NULL, NULL, NULL, '2018-09-22 05:07:12', '2018-09-22 10:27:36', NULL),
	(64, NULL, 21, 1, 2, 2, 2, NULL, NULL, NULL, 977.36, 21502.00, 258024.00, 100.00, 100.00, 295.65, 295.65, 1935.18, 2580.24, 0.00, 129012.00, NULL, 0.00, 0.00, NULL, 22, 8, NULL, NULL, 0.00, 0.00, NULL, NULL, NULL, NULL, '2018-09-22 05:07:26', '2018-09-22 10:22:51', NULL),
	(65, NULL, 70, 1, 2, 2, 2, NULL, NULL, NULL, 984.45, 21657.95, 259895.40, 100.00, 100.00, 297.80, 297.80, 1949.22, 2598.95, 0.00, 129947.70, NULL, 0.00, 0.00, NULL, 22, 8, NULL, '0', 0.00, 19310.94, NULL, NULL, NULL, NULL, '2018-09-22 05:07:40', '2018-09-22 05:07:40', NULL),
	(66, NULL, 43, 1, 2, 2, 2, NULL, NULL, NULL, 3642.86, 80143.00, 961716.00, 100.00, 100.00, 550.00, 550.00, 7212.87, 9617.16, 12517.27, 480858.00, NULL, 0.00, 0.00, NULL, 22, 8, NULL, NULL, 0.00, 0.00, NULL, NULL, NULL, NULL, '2018-09-22 05:08:31', '2018-09-22 10:30:29', NULL),
	(67, NULL, 69, 1, 2, 2, 2, NULL, NULL, NULL, 1101.09, 24224.00, 290688.00, 100.00, 100.00, 333.08, 333.08, 2180.16, 2906.88, 155.55, 145344.00, NULL, 0.00, 1000.00, NULL, 22, 8, NULL, NULL, 0.00, 0.00, NULL, NULL, NULL, NULL, '2018-09-22 05:08:47', '2018-09-22 06:59:44', NULL),
	(68, NULL, 39, 1, 2, 2, 2, NULL, NULL, NULL, 2982.00, 65604.00, 787248.00, 100.00, 100.00, 550.00, 550.00, 5904.36, 7872.48, 8929.16, 393624.00, NULL, 0.00, 0.00, NULL, 22, 8, NULL, NULL, 0.00, 0.00, NULL, NULL, NULL, NULL, '2018-09-22 05:10:14', '2018-09-22 10:29:18', NULL),
	(69, NULL, 20, 1, 2, 2, 2, NULL, NULL, NULL, 2982.00, 65604.00, 787248.00, 100.00, 100.00, 550.00, 550.00, 5904.36, 7872.48, 8929.16, 393624.00, NULL, 0.00, 0.00, NULL, 22, 8, NULL, NULL, 0.00, 0.00, NULL, NULL, NULL, NULL, '2018-09-22 05:10:28', '2018-09-22 10:22:35', NULL),
	(70, NULL, 68, 1, 2, 2, 2, NULL, NULL, NULL, 2790.77, 61397.00, 736764.00, 100.00, 100.00, 550.00, 550.00, 5525.73, 7367.64, 7972.07, 368382.00, NULL, 0.00, 0.00, NULL, 22, 8, NULL, '33333', 2500.00, 21888.27, NULL, NULL, NULL, NULL, '2018-09-22 05:10:41', '2018-09-22 05:10:41', NULL),
	(71, NULL, 67, 1, 2, 2, 2, NULL, NULL, NULL, 3747.23, 82439.00, 989268.00, 100.00, 100.00, 550.00, 550.00, 7419.51, 9892.68, 13144.08, 494634.00, NULL, 0.00, 2000.00, NULL, 22, 8, NULL, NULL, 0.00, 0.00, NULL, NULL, NULL, NULL, '2018-09-22 05:10:55', '2018-09-22 06:59:30', NULL),
	(72, NULL, 38, 1, 2, 2, 2, NULL, NULL, NULL, 4249.45, 93488.00, 1121856.00, 100.00, 100.00, 550.00, 550.00, 8413.92, 11218.56, 16160.45, 560928.00, NULL, 0.00, 0.00, NULL, 22, 8, NULL, '66667', 10833.33, 17757.08, NULL, NULL, NULL, NULL, '2018-09-22 05:11:06', '2018-09-22 05:11:06', NULL),
	(73, NULL, 66, 1, 2, 2, 2, NULL, NULL, NULL, 4249.45, 93488.00, 1121856.00, 100.00, 100.00, 550.00, 550.00, 8413.92, 11218.56, 16160.45, 560928.00, NULL, 0.00, 0.00, NULL, 22, 8, NULL, '66667', 10833.33, 17757.08, NULL, NULL, NULL, NULL, '2018-09-22 05:11:17', '2018-09-22 05:11:17', NULL),
	(74, NULL, 34, 1, 2, 2, 2, NULL, NULL, NULL, 1731.14, 38085.00, 457020.00, 100.00, 100.00, 523.67, 523.67, 3427.65, 4570.20, 2675.17, 228510.00, NULL, 0.00, 0.00, NULL, 22, 8, NULL, NULL, 0.00, 0.00, NULL, NULL, NULL, NULL, '2018-09-22 05:11:28', '2018-09-22 10:27:20', NULL),
	(75, NULL, 84, 1, 2, 2, 2, NULL, NULL, NULL, 1101.09, 24224.00, 290688.00, 100.00, 100.00, 333.08, 333.08, 2180.16, 2906.88, 155.55, 145344.00, NULL, 0.00, 0.00, NULL, 22, 8, NULL, '20833', 0.00, 777.76, NULL, NULL, NULL, NULL, '2018-09-22 05:11:40', '2018-09-22 05:11:40', NULL),
	(76, NULL, 19, 1, 2, 2, 2, NULL, NULL, NULL, 1731.14, 38085.00, 457020.00, 100.00, 100.00, 523.67, 523.67, 3427.65, 4570.20, 2675.17, 228510.00, NULL, 0.00, 500.00, NULL, 22, 8, NULL, NULL, 0.00, 0.00, NULL, NULL, NULL, NULL, '2018-09-22 05:11:54', '2018-09-22 10:22:21', NULL),
	(77, NULL, 18, 1, 2, 2, 2, NULL, NULL, NULL, 610.18, 13424.00, 161088.00, 100.00, 100.00, 184.58, 184.58, 1208.16, 1610.88, 0.00, 80544.00, NULL, 0.00, 0.00, NULL, 22, 8, NULL, NULL, 0.00, 0.00, NULL, NULL, NULL, NULL, '2018-09-22 05:12:08', '2018-09-22 10:22:00', NULL),
	(78, NULL, 26, 1, 2, 2, 2, NULL, NULL, NULL, 2708.95, 59597.00, 715164.00, 100.00, 100.00, 550.00, 550.00, 5363.73, 7151.64, 7562.57, 357582.00, NULL, 0.00, 0.00, NULL, 22, 8, NULL, NULL, 0.00, 0.00, NULL, NULL, NULL, NULL, '2018-09-22 05:12:21', '2018-09-22 10:24:26', NULL),
	(79, NULL, 65, 1, 2, 2, 2, NULL, NULL, NULL, 2749.59, 60491.00, 725892.00, 100.00, 100.00, 550.00, 550.00, 5444.19, 7258.92, 7765.95, 362946.00, NULL, 0.00, 0.00, NULL, 22, 8, NULL, '33333', 2500.00, 21063.81, NULL, NULL, NULL, NULL, '2018-09-22 05:12:39', '2018-09-22 05:12:39', NULL),
	(80, NULL, 42, 1, 2, 2, 2, NULL, NULL, NULL, 6524.27, 143534.00, 1722408.00, 100.00, 100.00, 550.00, 550.00, 12918.06, 17224.08, 29823.01, 861204.00, NULL, 0.00, 0.00, NULL, 22, 8, NULL, NULL, 0.00, 0.00, NULL, NULL, NULL, NULL, '2018-09-22 05:13:56', '2018-09-22 10:30:08', NULL),
	(81, NULL, 17, 1, 2, 2, 2, NULL, NULL, NULL, 917.23, 20179.00, 242148.00, 100.00, 100.00, 277.46, 277.46, 1816.11, 2421.48, 0.00, 121074.00, NULL, 0.00, 500.00, NULL, 22, 8, NULL, NULL, 0.00, 0.00, NULL, NULL, NULL, NULL, '2018-09-22 05:14:08', '2018-09-22 10:21:25', NULL),
	(82, NULL, 64, 1, 2, 2, 2, NULL, NULL, NULL, 3747.23, 82439.00, 989268.00, 100.00, 100.00, 550.00, 550.00, 7419.51, 9892.68, 13144.08, 494634.00, NULL, 0.00, 0.00, NULL, 22, 8, NULL, '66667', 10833.33, 7702.49, NULL, NULL, NULL, NULL, '2018-09-22 05:14:26', '2018-09-22 05:14:26', NULL),
	(83, NULL, 74, 1, 2, 2, 2, NULL, NULL, NULL, 1350.59, 29713.00, 356556.00, 100.00, 100.00, 408.55, 408.55, 2674.17, 3565.56, 1139.46, 178278.00, NULL, 0.00, 0.00, NULL, 22, 8, NULL, '20833', 0.00, 5697.28, NULL, NULL, NULL, NULL, '2018-09-22 05:14:39', '2018-09-22 05:14:39', NULL),
	(84, NULL, 16, 1, 2, 2, 2, NULL, NULL, NULL, 3331.77, 73299.00, 879588.00, 100.00, 100.00, 550.00, 550.00, 6596.91, 8795.88, 10679.77, 439794.00, NULL, 0.00, 0.00, NULL, 22, 8, NULL, NULL, 0.00, 0.00, NULL, NULL, NULL, NULL, '2018-09-22 05:14:52', '2018-09-22 10:20:44', NULL),
	(85, NULL, 25, 1, 2, 2, 2, NULL, NULL, NULL, 1751.95, 38543.00, 462516.00, 100.00, 100.00, 529.97, 529.97, 3468.87, 4625.16, 2777.79, 231258.00, NULL, 0.00, 0.00, NULL, 22, 8, NULL, NULL, 0.00, 0.00, NULL, NULL, NULL, NULL, '2018-09-22 05:15:03', '2018-09-22 10:24:03', NULL),
	(86, NULL, 62, 1, 2, 2, 2, NULL, NULL, NULL, 2668.95, 58717.00, 704604.00, 100.00, 100.00, 550.00, 550.00, 5284.53, 7046.04, 7362.37, 352302.00, NULL, 0.00, 0.00, NULL, 22, 8, NULL, '33333', 2500.00, 19449.47, NULL, NULL, NULL, NULL, '2018-09-22 05:15:19', '2018-09-22 05:15:19', NULL),
	(87, NULL, 63, 1, 2, 2, 2, NULL, NULL, NULL, 1913.59, 42099.00, 505188.00, 100.00, 100.00, 550.00, 550.00, 3788.91, 5051.88, 3581.77, 252594.00, NULL, 0.00, 0.00, NULL, 22, 8, NULL, '33333', 2500.00, 4327.09, NULL, NULL, NULL, NULL, '2018-09-22 05:15:30', '2018-09-22 05:15:30', NULL),
	(88, NULL, 61, 1, 2, 2, 2, NULL, NULL, NULL, 4646.23, 102217.00, 1226604.00, 100.00, 100.00, 550.00, 550.00, 9199.53, 12266.04, 18543.47, 613302.00, NULL, 0.00, 0.00, NULL, 22, 8, NULL, '66667', 10833.33, 25700.47, NULL, NULL, NULL, NULL, '2018-09-22 05:15:41', '2018-09-22 05:15:41', NULL),
	(89, NULL, 60, 1, 2, 2, 2, NULL, NULL, NULL, 1101.09, 24224.00, 290688.00, 100.00, 100.00, 333.08, 333.08, 2180.16, 2906.88, 155.55, 145344.00, NULL, 0.00, 0.00, NULL, 22, 8, NULL, '20833', 0.00, 777.76, NULL, NULL, NULL, NULL, '2018-09-22 05:15:55', '2018-09-22 05:15:55', NULL);
/*!40000 ALTER TABLE `pms_payroll_information` ENABLE KEYS */;

-- Dumping structure for table pids_hris.pms_philhealthpolicy
DROP TABLE IF EXISTS `pms_philhealthpolicy`;
CREATE TABLE IF NOT EXISTS `pms_philhealthpolicy` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `policy_name` varchar(225) DEFAULT NULL,
  `pay_period` varchar(225) DEFAULT NULL,
  `deduction_period` varchar(225) DEFAULT NULL,
  `policy_type` varchar(225) DEFAULT NULL,
  `based_on` varchar(225) DEFAULT NULL,
  `below` decimal(9,4) DEFAULT NULL,
  `above` decimal(9,2) DEFAULT NULL,
  `remarks` varchar(225) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table pids_hris.pms_philhealthpolicy: ~1 rows (approximately)
/*!40000 ALTER TABLE `pms_philhealthpolicy` DISABLE KEYS */;
INSERT INTO `pms_philhealthpolicy` (`id`, `policy_name`, `pay_period`, `deduction_period`, `policy_type`, `based_on`, `below`, `above`, `remarks`, `created_at`, `updated_at`, `updated_by`) VALUES
	(2, 'STANDARD POLICY', 'Monthly', 'Both', 'System Generated', 'Monthly Salary', 0.0275, 1100.00, NULL, '2018-06-14 05:47:40', '2018-06-21 03:12:52', NULL);
/*!40000 ALTER TABLE `pms_philhealthpolicy` ENABLE KEYS */;

-- Dumping structure for table pids_hris.pms_positionitem_setup
DROP TABLE IF EXISTS `pms_positionitem_setup`;
CREATE TABLE IF NOT EXISTS `pms_positionitem_setup` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) DEFAULT NULL,
  `positionitem_id` int(11) DEFAULT NULL,
  `position_id` int(11) DEFAULT NULL,
  `salarygrade_id` int(11) DEFAULT NULL,
  `jobgrade_id` int(11) DEFAULT NULL,
  `step_inc` varchar(225) DEFAULT NULL,
  `amount` decimal(9,2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table pids_hris.pms_positionitem_setup: ~0 rows (approximately)
/*!40000 ALTER TABLE `pms_positionitem_setup` DISABLE KEYS */;
/*!40000 ALTER TABLE `pms_positionitem_setup` ENABLE KEYS */;

-- Dumping structure for table pids_hris.pms_positions
DROP TABLE IF EXISTS `pms_positions`;
CREATE TABLE IF NOT EXISTS `pms_positions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(50) DEFAULT NULL,
  `name` varchar(225) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table pids_hris.pms_positions: ~0 rows (approximately)
/*!40000 ALTER TABLE `pms_positions` DISABLE KEYS */;
/*!40000 ALTER TABLE `pms_positions` ENABLE KEYS */;

-- Dumping structure for table pids_hris.pms_position_items
DROP TABLE IF EXISTS `pms_position_items`;
CREATE TABLE IF NOT EXISTS `pms_position_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(50) DEFAULT NULL,
  `name` varchar(300) DEFAULT NULL,
  `position_id` int(11) DEFAULT NULL,
  `position_level_id` int(11) DEFAULT NULL,
  `position_classification_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table pids_hris.pms_position_items: ~0 rows (approximately)
/*!40000 ALTER TABLE `pms_position_items` DISABLE KEYS */;
/*!40000 ALTER TABLE `pms_position_items` ENABLE KEYS */;

-- Dumping structure for table pids_hris.pms_previous_employer
DROP TABLE IF EXISTS `pms_previous_employer`;
CREATE TABLE IF NOT EXISTS `pms_previous_employer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) DEFAULT NULL,
  `allowances` decimal(9,2) DEFAULT NULL,
  `thirteen_month_pay` decimal(9,2) DEFAULT NULL,
  `deminimis_amount` decimal(9,2) DEFAULT NULL,
  `tax_witheld` decimal(9,2) DEFAULT NULL,
  `premium_amount` decimal(9,2) DEFAULT NULL,
  `taxable_basic_pay` decimal(9,2) DEFAULT NULL,
  `taxable_thirteen_month_pay` decimal(9,2) DEFAULT NULL,
  `taxable_allowances` decimal(9,2) DEFAULT NULL,
  `as_of_date` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table pids_hris.pms_previous_employer: ~1 rows (approximately)
/*!40000 ALTER TABLE `pms_previous_employer` DISABLE KEYS */;
INSERT INTO `pms_previous_employer` (`id`, `employee_id`, `allowances`, `thirteen_month_pay`, `deminimis_amount`, `tax_witheld`, `premium_amount`, `taxable_basic_pay`, `taxable_thirteen_month_pay`, `taxable_allowances`, `as_of_date`, `created_at`, `updated_at`, `created_by`) VALUES
	(1, 92, 55555.00, 23456.00, 23456.00, 2345.00, 234.00, 34567.00, 23456.00, 23456.00, '2018-07-26', '2018-07-17 16:14:40', '2018-07-17 16:51:38', NULL);
/*!40000 ALTER TABLE `pms_previous_employer` ENABLE KEYS */;

-- Dumping structure for table pids_hris.pms_providentfund
DROP TABLE IF EXISTS `pms_providentfund`;
CREATE TABLE IF NOT EXISTS `pms_providentfund` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `policy_name` varchar(225) DEFAULT NULL,
  `pay_period` varchar(225) DEFAULT NULL,
  `deduction_period` varchar(225) DEFAULT NULL,
  `policy_type` varchar(225) DEFAULT NULL,
  `based_on` varchar(225) DEFAULT NULL,
  `ee_share` decimal(9,2) DEFAULT NULL,
  `er_share` decimal(9,2) DEFAULT NULL,
  `remarks` varchar(225) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

-- Dumping data for table pids_hris.pms_providentfund: ~9 rows (approximately)
/*!40000 ALTER TABLE `pms_providentfund` DISABLE KEYS */;
INSERT INTO `pms_providentfund` (`id`, `policy_name`, `pay_period`, `deduction_period`, `policy_type`, `based_on`, `ee_share`, `er_share`, `remarks`, `created_at`, `updated_at`, `updated_by`) VALUES
	(1, '2 %', 'Monthly', 'First Half', 'System Genearated', 'Fix Monthly Rate', 0.02, NULL, NULL, '2018-09-22 09:43:10', '2018-09-22 09:56:16', NULL),
	(2, '3 %', 'Monthly', 'First Half', 'System Genearated', 'Fix Monthly Rate', 0.03, NULL, NULL, '2018-09-22 09:43:25', '2018-09-22 09:56:29', NULL),
	(3, '5 %', 'Monthly', 'First Half', 'System Genearated', 'Fix Monthly Rate', 0.05, NULL, NULL, '2018-09-22 09:43:46', '2018-09-22 09:56:36', NULL),
	(4, '6 %', 'Monthly', 'First Half', 'System Genearated', 'Fix Monthly Rate', 0.06, NULL, NULL, '2018-09-22 09:44:53', '2018-09-22 09:56:43', NULL),
	(5, '8 %', 'Monthly', 'First Half', 'System Genearated', 'Fix Monthly Rate', 0.08, NULL, NULL, '2018-09-22 09:45:07', '2018-09-22 09:56:50', NULL),
	(6, '10 %', 'Monthly', 'First Half', 'System Genearated', 'Fix Monthly Rate', 0.10, NULL, NULL, '2018-09-22 09:45:32', '2018-09-22 09:57:02', NULL),
	(7, '12 %', 'Monthly', 'First Half', 'System Genearated', 'Fix Monthly Rate', 0.12, NULL, NULL, '2018-09-22 09:45:45', '2018-09-22 09:57:09', NULL),
	(8, '15 %', 'Monthly', 'First Half', 'System Genearated', 'Fix Monthly Rate', 0.15, NULL, NULL, '2018-09-22 09:46:05', '2018-09-22 09:57:16', NULL),
	(9, '20 %', 'Monthly', 'First Half', 'System Genearated', 'Fix Monthly Rate', 0.20, NULL, NULL, '2018-09-22 09:46:22', '2018-09-22 09:57:22', NULL);
/*!40000 ALTER TABLE `pms_providentfund` ENABLE KEYS */;

-- Dumping structure for table pids_hris.pms_rata
DROP TABLE IF EXISTS `pms_rata`;
CREATE TABLE IF NOT EXISTS `pms_rata` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) DEFAULT NULL,
  `position_item_id` int(11) DEFAULT NULL,
  `office_id` int(11) DEFAULT NULL,
  `department_id` int(11) DEFAULT NULL,
  `number_of_actual_work` int(11) DEFAULT NULL,
  `number_of_leave_field` int(11) DEFAULT NULL,
  `number_of_work_days` int(11) DEFAULT NULL,
  `number_of_used_vehicles` int(11) DEFAULT NULL,
  `percentage_of_rata` varchar(225) DEFAULT NULL,
  `percentage_of_rata_value` decimal(9,2) DEFAULT NULL,
  `adjustment_transpo_amount` decimal(9,2) DEFAULT NULL,
  `adjustment_number_work_days` int(11) DEFAULT NULL,
  `adjustment_rep_amount` decimal(9,2) DEFAULT NULL,
  `adjustment_percentage` decimal(9,2) DEFAULT NULL,
  `adjustment_percentage_amount` decimal(9,2) DEFAULT NULL,
  `adjustment_deduction_period` varchar(50) DEFAULT NULL,
  `transportation_amount` decimal(9,2) DEFAULT NULL,
  `representation_amount` decimal(9,2) DEFAULT NULL,
  `year` varchar(225) DEFAULT NULL,
  `month` varchar(225) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table pids_hris.pms_rata: ~21 rows (approximately)
/*!40000 ALTER TABLE `pms_rata` DISABLE KEYS */;
INSERT INTO `pms_rata` (`id`, `employee_id`, `position_item_id`, `office_id`, `department_id`, `number_of_actual_work`, `number_of_leave_field`, `number_of_work_days`, `number_of_used_vehicles`, `percentage_of_rata`, `percentage_of_rata_value`, `adjustment_transpo_amount`, `adjustment_number_work_days`, `adjustment_rep_amount`, `adjustment_percentage`, `adjustment_percentage_amount`, `adjustment_deduction_period`, `transportation_amount`, `representation_amount`, `year`, `month`, `created_at`, `created_by`, `updated_at`, `updated_by`) VALUES
	(2, 1, NULL, NULL, 1, 22, NULL, 22, NULL, NULL, 1.00, 1250.00, 5, 1250.00, 0.25, 1250.00, 'secondweek', 5000.00, 5000.00, '2018', 'January', '2018-10-15 11:02:40', NULL, '2018-10-15 11:02:54', NULL),
	(3, 46, NULL, NULL, 8, 22, NULL, 22, NULL, NULL, 1.00, 3750.00, 12, 3750.00, 0.75, 3750.00, 'secondweek', 5000.00, 5000.00, '2018', 'January', '2018-10-16 03:58:23', NULL, '2018-10-16 04:00:02', NULL),
	(4, 2, NULL, NULL, 1, 22, NULL, 22, NULL, NULL, 1.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 7500.00, '2018', 'January', '2018-10-16 06:12:04', NULL, '2018-10-16 06:12:04', NULL),
	(5, 23, NULL, NULL, 2, 22, NULL, 22, NULL, NULL, 1.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 7500.00, '2018', 'January', '2018-10-16 06:12:04', NULL, '2018-10-16 06:12:04', NULL),
	(6, 47, NULL, NULL, 8, 22, NULL, 22, NULL, NULL, 1.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5000.00, '2018', 'January', '2018-10-16 06:12:04', NULL, '2018-10-16 06:12:04', NULL),
	(7, 30, NULL, NULL, 4, 22, NULL, 22, NULL, NULL, 1.00, NULL, NULL, NULL, NULL, NULL, NULL, 5000.00, 5000.00, '2018', 'January', '2018-10-16 06:12:05', NULL, '2018-10-16 06:12:05', NULL),
	(8, 45, NULL, NULL, 7, 22, NULL, 22, NULL, NULL, 1.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9000.00, '2018', 'January', '2018-10-16 06:12:05', NULL, '2018-10-16 06:12:05', NULL),
	(9, 50, NULL, NULL, 8, 22, NULL, 22, NULL, NULL, 1.00, NULL, NULL, NULL, NULL, NULL, NULL, 5000.00, 5000.00, '2018', 'January', '2018-10-16 06:12:05', NULL, '2018-10-16 06:12:05', NULL),
	(10, 54, NULL, NULL, 8, 22, NULL, 22, NULL, NULL, 1.00, NULL, NULL, NULL, NULL, NULL, NULL, 5000.00, 5000.00, '2018', 'January', '2018-10-16 06:12:05', NULL, '2018-10-16 06:12:05', NULL),
	(11, 55, NULL, NULL, 8, 22, NULL, 22, NULL, NULL, 1.00, NULL, NULL, NULL, NULL, NULL, NULL, 5000.00, 5000.00, '2018', 'January', '2018-10-16 06:12:05', NULL, '2018-10-16 06:12:05', NULL),
	(12, 57, NULL, NULL, 8, 22, NULL, 22, NULL, NULL, 1.00, NULL, NULL, NULL, NULL, NULL, NULL, 5000.00, 5000.00, '2018', 'January', '2018-10-16 06:12:05', NULL, '2018-10-16 06:12:05', NULL),
	(13, 15, NULL, NULL, 1, 22, NULL, 22, NULL, NULL, 1.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5000.00, '2018', 'January', '2018-10-16 06:12:05', NULL, '2018-10-16 06:12:05', NULL),
	(14, 61, NULL, NULL, 8, 22, NULL, 22, NULL, NULL, 1.00, NULL, NULL, NULL, NULL, NULL, NULL, 5000.00, 5000.00, '2018', 'January', '2018-10-16 06:12:05', NULL, '2018-10-16 06:12:05', NULL),
	(15, 16, NULL, NULL, 1, 22, NULL, 22, NULL, NULL, 1.00, NULL, NULL, NULL, NULL, NULL, NULL, 5000.00, 5000.00, '2018', 'January', '2018-10-16 06:12:05', NULL, '2018-10-16 06:12:05', NULL),
	(16, 64, NULL, NULL, 8, 22, NULL, 22, NULL, NULL, 1.00, NULL, NULL, NULL, NULL, NULL, NULL, 5000.00, 5000.00, '2018', 'January', '2018-10-16 06:12:05', NULL, '2018-10-16 06:12:05', NULL),
	(17, 42, NULL, NULL, 6, 22, NULL, 22, NULL, NULL, 1.00, NULL, NULL, NULL, NULL, NULL, NULL, 11000.00, 11000.00, '2018', 'January', '2018-10-16 06:12:05', NULL, '2018-10-16 06:12:05', NULL),
	(18, 66, NULL, NULL, 8, 22, NULL, 22, NULL, NULL, 1.00, NULL, NULL, NULL, NULL, NULL, NULL, 5000.00, 5000.00, '2018', 'January', '2018-10-16 06:12:05', NULL, '2018-10-16 06:12:05', NULL),
	(19, 38, NULL, NULL, 5, 22, NULL, 22, NULL, NULL, 1.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 7500.00, '2018', 'January', '2018-10-16 06:12:05', NULL, '2018-10-16 06:12:05', NULL),
	(20, 67, NULL, NULL, 8, 22, NULL, 22, NULL, NULL, 1.00, NULL, NULL, NULL, NULL, NULL, NULL, 5000.00, 5000.00, '2018', 'January', '2018-10-16 06:12:05', NULL, '2018-10-16 06:12:05', NULL),
	(21, 20, NULL, NULL, 1, 22, NULL, 22, NULL, NULL, 1.00, NULL, NULL, NULL, NULL, NULL, NULL, 5000.00, 5000.00, '2018', 'January', '2018-10-16 06:12:05', NULL, '2018-10-16 06:12:05', NULL),
	(22, 39, NULL, NULL, 5, 22, NULL, 22, NULL, NULL, 1.00, NULL, NULL, NULL, NULL, NULL, NULL, 5000.00, 5000.00, '2018', 'January', '2018-10-16 06:12:06', NULL, '2018-10-16 06:12:06', NULL);
/*!40000 ALTER TABLE `pms_rata` ENABLE KEYS */;

-- Dumping structure for table pids_hris.pms_rates
DROP TABLE IF EXISTS `pms_rates`;
CREATE TABLE IF NOT EXISTS `pms_rates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rate` decimal(9,2) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;

-- Dumping data for table pids_hris.pms_rates: ~16 rows (approximately)
/*!40000 ALTER TABLE `pms_rates` DISABLE KEYS */;
INSERT INTO `pms_rates` (`id`, `rate`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
	(1, 0.15, 'hp', 2, 2, '2018-09-06 11:25:40', '2018-09-06 16:23:15'),
	(2, 0.50, 'hp', 2, NULL, '2018-09-06 11:25:56', '2018-09-06 11:25:56'),
	(3, 0.23, 'hp', 2, NULL, '2018-09-06 16:23:23', '2018-09-06 16:23:23'),
	(4, 0.30, 'hp', 2, NULL, '2018-09-06 16:23:34', '2018-09-06 16:23:34'),
	(15, 1.00, 'travelrate', 2, NULL, '2018-09-10 10:52:22', '2018-09-10 10:52:22'),
	(16, 0.75, 'travelrate', 2, NULL, '2018-09-10 10:53:41', '2018-09-10 10:53:41'),
	(17, 0.50, 'travelrate', 2, NULL, '2018-09-10 10:53:56', '2018-09-10 10:53:56'),
	(18, 0.02, 'pf', 2, NULL, '2018-09-22 09:26:20', '2018-09-22 09:26:20'),
	(19, 0.05, 'pf', 2, NULL, '2018-09-22 09:26:39', '2018-09-22 09:26:39'),
	(20, 0.10, 'pf', 2, NULL, '2018-09-22 09:26:47', '2018-09-22 09:26:47'),
	(21, 0.03, 'pf', 2, NULL, '2018-09-22 09:27:31', '2018-09-22 09:27:31'),
	(22, 0.06, 'pf', 2, NULL, '2018-09-22 09:27:38', '2018-09-22 09:27:38'),
	(23, 0.12, 'pf', 2, NULL, '2018-09-22 09:27:45', '2018-09-22 09:27:45'),
	(24, 0.15, 'pf', 2, NULL, '2018-09-22 09:28:25', '2018-09-22 09:28:25'),
	(25, 0.08, 'pf', 2, NULL, '2018-09-22 09:28:31', '2018-09-22 09:28:31'),
	(26, 0.20, 'pf', 2, NULL, '2018-09-22 09:28:38', '2018-09-22 09:28:38');
/*!40000 ALTER TABLE `pms_rates` ENABLE KEYS */;

-- Dumping structure for table pids_hris.pms_salarygrade
DROP TABLE IF EXISTS `pms_salarygrade`;
CREATE TABLE IF NOT EXISTS `pms_salarygrade` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `salary_grade` varchar(225) DEFAULT NULL,
  `step1` decimal(9,2) DEFAULT NULL,
  `step2` decimal(9,2) DEFAULT NULL,
  `step3` decimal(9,2) DEFAULT NULL,
  `step4` decimal(9,2) DEFAULT NULL,
  `step5` decimal(9,2) DEFAULT NULL,
  `step6` decimal(9,2) DEFAULT NULL,
  `step7` decimal(9,2) DEFAULT NULL,
  `step8` decimal(9,2) DEFAULT NULL,
  `remarks` varchar(225) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=latin1;

-- Dumping data for table pids_hris.pms_salarygrade: ~32 rows (approximately)
/*!40000 ALTER TABLE `pms_salarygrade` DISABLE KEYS */;
INSERT INTO `pms_salarygrade` (`id`, `salary_grade`, `step1`, `step2`, `step3`, `step4`, `step5`, `step6`, `step7`, `step8`, `remarks`, `created_at`, `updated_at`, `updated_by`) VALUES
	(1, '1', 10510.00, 10602.00, 10695.00, 10789.00, 10884.00, 10982.00, 11076.00, 11173.00, NULL, NULL, NULL, NULL),
	(2, '2', 11200.00, 11293.00, 11386.00, 11480.00, 11575.00, 11671.00, 11767.00, 11864.00, NULL, NULL, NULL, NULL),
	(3, '3', 11914.00, 12013.00, 12112.00, 11212.00, 12313.00, 12414.00, 12517.00, 12620.00, NULL, NULL, NULL, NULL),
	(4, '4', 12674.00, 12778.00, 12884.00, 12990.00, 13097.00, 13206.00, 13315.00, 13424.00, NULL, NULL, NULL, NULL),
	(5, '5', 13481.00, 13606.00, 13705.00, 13818.00, 13932.00, 14047.00, 14163.00, 14280.00, NULL, NULL, NULL, NULL),
	(6, '6', 14340.00, 14459.00, 14578.00, 14699.00, 14820.00, 14942.00, 15066.00, 15190.00, NULL, NULL, NULL, NULL),
	(7, '7', 15254.00, 15380.00, 15507.00, 15635.00, 15765.00, 15895.00, 16026.00, 16158.00, NULL, NULL, NULL, NULL),
	(8, '9', 17473.00, 17627.00, 17781.00, 17937.00, 18095.00, 18253.00, 18413.00, 18575.00, NULL, NULL, NULL, NULL),
	(9, '10', 18718.00, 18883.00, 19048.00, 19215.00, 19384.00, 19567.00, 19725.00, 19898.00, NULL, NULL, NULL, NULL),
	(10, '11', 20179.00, 20437.00, 20698.00, 20963.00, 21231.00, 21502.00, 21777.00, 22055.00, NULL, NULL, NULL, NULL),
	(11, '12', 22149.00, 22410.00, 22674.00, 22942.00, 23212.00, 23486.00, 23763.00, 24043.00, NULL, NULL, NULL, NULL),
	(12, '13', 24224.00, 24510.00, 24799.00, 25091.00, 25387.00, 25686.00, 25989.00, 26296.00, NULL, NULL, NULL, NULL),
	(13, '14', 26494.00, 26806.00, 27122.00, 27442.00, 27766.00, 28093.00, 28424.00, 28759.00, NULL, NULL, NULL, NULL),
	(14, '15', 29010.00, 29359.00, 29713.00, 30071.00, 30432.00, 30799.00, 31170.00, 31545.00, NULL, NULL, NULL, NULL),
	(15, '16', 31765.00, 32147.00, 32535.00, 32926.00, 33323.00, 33724.00, 34130.00, 34541.00, NULL, NULL, NULL, NULL),
	(16, '17', 34781.00, 35201.00, 35624.00, 36053.00, 36487.00, 36927.00, 37371.00, 37821.00, NULL, NULL, NULL, NULL),
	(17, '18', 38085.00, 38543.00, 39007.00, 39477.00, 39952.00, 40433.00, 40920.00, 41413.00, NULL, NULL, NULL, NULL),
	(18, '19', 42099.00, 42730.00, 43371.00, 44020.00, 44680.00, 45350.00, 46030.00, 46720.00, NULL, NULL, NULL, NULL),
	(19, '20', 47037.00, 47742.00, 48457.00, 49184.00, 49921.00, 50669.00, 57460.00, 52199.00, NULL, NULL, NULL, NULL),
	(20, '21', 52544.00, 53341.00, 54141.00, 54952.00, 55776.00, 56612.00, 57460.00, 58322.00, NULL, NULL, NULL, NULL),
	(21, '22', 58717.00, 59597.00, 60491.00, 61397.00, 62318.00, 63252.00, 64200.00, 65162.00, NULL, NULL, NULL, NULL),
	(22, '23', 65604.00, 66587.00, 67585.00, 68598.00, 69627.00, 70670.00, 71730.00, 72805.00, NULL, NULL, NULL, NULL),
	(23, '24', 73229.00, 74397.00, 75512.00, 76644.00, 77793.00, 78959.00, 80143.00, 81344.00, NULL, NULL, NULL, NULL),
	(24, '25', 82439.00, 83674.00, 84928.00, 86201.00, 87493.00, 88805.00, 90136.00, 91487.00, NULL, NULL, NULL, NULL),
	(25, '26', 92108.00, 93488.00, 94889.00, 96312.00, 97775.00, 99221.00, 100708.00, 102217.00, NULL, NULL, NULL, NULL),
	(26, '27', 102910.00, 104453.00, 106019.00, 107608.00, 109221.00, 110858.00, 112519.00, 114210.00, NULL, NULL, NULL, NULL),
	(27, '28', 114981.00, 116704.00, 118453.00, 120229.00, 122031.00, 123860.00, 125716.00, 127601.00, NULL, NULL, NULL, NULL),
	(28, '29', 128467.00, 130392.00, 132346.00, 134330.00, 136343.00, 138387.00, 140461.00, 142566.00, NULL, NULL, NULL, NULL),
	(29, '30', 143534.00, 145685.00, 147869.00, 150085.00, 152335.00, 154618.00, 156935.00, 159228.00, NULL, NULL, NULL, NULL),
	(30, '31', 198168.00, 201615.00, 205121.00, 208689.00, 212318.00, 216011.00, 219768.00, 223590.00, NULL, NULL, NULL, NULL),
	(31, '32', 233857.00, 238035.00, 242288.00, 246618.00, 251024.00, 255509.00, 260074.00, 264721.00, NULL, NULL, NULL, NULL),
	(32, '33', 289401.00, 298083.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `pms_salarygrade` ENABLE KEYS */;

-- Dumping structure for table pids_hris.pms_salaryinfo
DROP TABLE IF EXISTS `pms_salaryinfo`;
CREATE TABLE IF NOT EXISTS `pms_salaryinfo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) DEFAULT NULL,
  `salarygrade_id` int(11) DEFAULT NULL,
  `jobgrade_id` int(11) DEFAULT NULL,
  `positionitem_id` int(11) DEFAULT NULL,
  `position_id` int(11) DEFAULT NULL,
  `step_inc` varchar(225) DEFAULT NULL,
  `salary_description` varchar(225) DEFAULT NULL,
  `salary_old_rate` decimal(9,2) DEFAULT NULL,
  `salary_adjustment` decimal(9,2) DEFAULT NULL,
  `salary_new_rate` decimal(9,2) DEFAULT NULL,
  `salary_effectivity_date` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=90 DEFAULT CHARSET=latin1;

-- Dumping data for table pids_hris.pms_salaryinfo: ~89 rows (approximately)
/*!40000 ALTER TABLE `pms_salaryinfo` DISABLE KEYS */;
INSERT INTO `pms_salaryinfo` (`id`, `employee_id`, `salarygrade_id`, `jobgrade_id`, `positionitem_id`, `position_id`, `step_inc`, `salary_description`, `salary_old_rate`, `salary_adjustment`, `salary_new_rate`, `salary_effectivity_date`, `created_at`, `updated_at`, `updated_by`, `created_by`) VALUES
	(1, 1, 22, NULL, NULL, NULL, '4', 'Initial Salary', 0.00, 0.00, 67585.00, '2018-09-20', NULL, '2018-09-25 02:39:45', NULL, 0),
	(2, 2, 25, NULL, NULL, NULL, '8', NULL, NULL, NULL, 102217.00, '2018-09-20', NULL, NULL, NULL, 0),
	(3, 3, 4, NULL, NULL, NULL, '2', NULL, NULL, NULL, 12674.00, '2018-09-20', NULL, NULL, NULL, 0),
	(4, 4, 18, NULL, NULL, NULL, '4', NULL, NULL, NULL, 43371.00, '2018-09-20', NULL, NULL, NULL, 0),
	(5, 5, 17, NULL, NULL, NULL, '6', NULL, NULL, NULL, 40433.00, '2018-09-20', NULL, NULL, NULL, 0),
	(6, 6, 17, NULL, NULL, NULL, '2', NULL, NULL, NULL, 38085.00, '2018-09-20', NULL, NULL, NULL, 0),
	(7, 7, 4, NULL, NULL, NULL, '7', NULL, NULL, NULL, 13315.00, '2018-09-20', NULL, NULL, NULL, 0),
	(8, 8, 4, NULL, NULL, NULL, '1', NULL, NULL, NULL, 12990.00, '2018-09-20', NULL, NULL, NULL, 0),
	(9, 9, 9, NULL, NULL, NULL, '2', NULL, NULL, NULL, 18718.00, '2018-09-20', NULL, NULL, NULL, 0),
	(10, 10, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 14459.00, '2018-09-20', NULL, NULL, NULL, NULL),
	(11, 11, 4, NULL, NULL, NULL, '4', NULL, NULL, NULL, 12884.00, '2018-09-20', NULL, NULL, NULL, 0),
	(12, 12, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 12674.00, '2018-09-20', NULL, NULL, NULL, NULL),
	(13, 13, 14, NULL, NULL, NULL, '2', NULL, NULL, NULL, 29010.00, '2018-09-20', NULL, NULL, NULL, 0),
	(14, 14, 14, NULL, NULL, NULL, '2', NULL, NULL, NULL, 29010.00, '2018-09-20', NULL, NULL, NULL, 0),
	(15, 15, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 73299.00, '2018-09-20', NULL, NULL, NULL, NULL),
	(16, 16, 23, NULL, NULL, NULL, '2', NULL, NULL, NULL, 73299.00, '2018-09-20', NULL, NULL, NULL, 0),
	(17, 17, 10, NULL, NULL, NULL, '3', NULL, NULL, NULL, 20437.00, '2018-09-20', NULL, NULL, NULL, 0),
	(18, 18, 4, NULL, NULL, NULL, '8', NULL, NULL, NULL, 13424.00, '2018-09-20', NULL, NULL, NULL, 0),
	(19, 19, 17, NULL, NULL, NULL, '2', NULL, NULL, NULL, 38085.00, '2018-09-20', NULL, NULL, NULL, 0),
	(20, 20, 22, NULL, NULL, NULL, '3', NULL, NULL, NULL, 66587.00, '2018-09-20', NULL, NULL, NULL, 0),
	(21, 21, 10, NULL, NULL, NULL, '6', NULL, NULL, NULL, 21502.00, '2018-09-20', NULL, NULL, NULL, 0),
	(22, 22, 17, NULL, NULL, NULL, '7', NULL, NULL, NULL, 40920.00, '2018-09-20', NULL, NULL, NULL, 0),
	(23, 23, 25, NULL, NULL, NULL, '3', NULL, NULL, NULL, 93488.00, '2018-09-20', NULL, NULL, NULL, 0),
	(24, 24, 13, NULL, NULL, NULL, '3', NULL, NULL, NULL, 26806.00, '2018-09-20', NULL, NULL, NULL, 0),
	(25, 25, 17, NULL, NULL, NULL, '3', NULL, NULL, NULL, 38543.00, '2018-09-20', NULL, NULL, NULL, 0),
	(26, 26, 21, NULL, NULL, NULL, '3', NULL, NULL, NULL, 59597.00, '2018-09-20', NULL, NULL, NULL, 0),
	(27, 27, 18, NULL, NULL, NULL, '1', NULL, NULL, NULL, 44020.00, '2018-09-20', NULL, NULL, NULL, 0),
	(28, 28, 18, NULL, NULL, NULL, '1', NULL, NULL, NULL, 44020.00, '2018-09-20', NULL, NULL, NULL, 0),
	(29, 29, 21, NULL, NULL, NULL, '3', NULL, NULL, NULL, 59597.00, '2018-09-20', NULL, NULL, NULL, 0),
	(30, 30, 21, NULL, NULL, NULL, '4', NULL, NULL, NULL, 60491.00, '2018-09-20', NULL, NULL, NULL, 0),
	(31, 31, 21, NULL, NULL, NULL, '3', NULL, NULL, NULL, 59597.00, '2018-09-20', NULL, NULL, NULL, 0),
	(32, 32, 9, NULL, NULL, NULL, '2', NULL, NULL, NULL, 18718.00, '2018-09-20', NULL, NULL, NULL, 0),
	(33, 33, 14, NULL, NULL, NULL, '3', NULL, NULL, NULL, 29359.00, '2018-09-20', NULL, NULL, NULL, 0),
	(34, 34, 17, NULL, NULL, NULL, '2', NULL, NULL, NULL, 38085.00, '2018-09-20', NULL, NULL, NULL, 0),
	(35, 35, 14, NULL, NULL, NULL, '2', NULL, NULL, NULL, 29010.00, '2018-09-20', NULL, NULL, NULL, 0),
	(36, 36, 13, NULL, NULL, NULL, '8', NULL, NULL, NULL, 28759.00, '2018-09-20', NULL, NULL, NULL, 0),
	(37, 37, 17, NULL, NULL, NULL, '6', NULL, NULL, NULL, 40433.00, '2018-09-20', NULL, NULL, NULL, 0),
	(38, 38, 25, NULL, NULL, NULL, '3', NULL, NULL, NULL, 93488.00, '2018-09-20', NULL, NULL, NULL, 0),
	(39, 39, 22, NULL, NULL, NULL, '2', NULL, NULL, NULL, 65604.00, '2018-09-20', NULL, NULL, NULL, 0),
	(40, 40, 18, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 42099.00, '2018-09-20', NULL, NULL, NULL, 0),
	(41, 41, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 58717.00, '2018-09-20', NULL, NULL, NULL, NULL),
	(42, 42, 29, NULL, NULL, NULL, '2', NULL, NULL, NULL, 143534.00, '2018-09-20', NULL, NULL, NULL, 0),
	(43, 43, 23, NULL, NULL, NULL, '7', NULL, NULL, NULL, 80143.00, '2018-09-20', NULL, NULL, NULL, 0),
	(44, 44, 19, NULL, NULL, NULL, '3', NULL, NULL, NULL, 47742.00, '2018-09-20', NULL, NULL, NULL, 0),
	(45, 45, 27, NULL, NULL, NULL, '2', NULL, NULL, NULL, 114981.00, '2018-09-20', NULL, NULL, NULL, 0),
	(46, 46, 24, NULL, NULL, NULL, '2', NULL, NULL, NULL, 82439.00, '2018-09-20', NULL, NULL, NULL, 0),
	(47, 47, 25, NULL, NULL, NULL, '3', NULL, NULL, NULL, 94889.00, '2018-09-20', NULL, NULL, NULL, 0),
	(48, 48, 15, NULL, NULL, NULL, '2', NULL, NULL, NULL, 31765.00, '2018-09-20', NULL, NULL, NULL, 0),
	(49, 49, 9, NULL, NULL, NULL, '2', NULL, NULL, NULL, 18718.00, '2018-09-20', NULL, NULL, NULL, 0),
	(50, 50, 25, NULL, NULL, NULL, '1', NULL, NULL, NULL, 96312.00, '2018-09-20', NULL, NULL, NULL, 0),
	(51, 51, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 24224.00, '2018-09-20', NULL, NULL, NULL, NULL),
	(52, 52, 9, NULL, NULL, NULL, '2', NULL, NULL, NULL, 18718.00, '2018-09-20', NULL, NULL, NULL, 0),
	(53, 53, 21, NULL, NULL, NULL, '1', NULL, NULL, NULL, 61397.00, '2018-09-20', NULL, NULL, NULL, 0),
	(54, 54, 25, NULL, NULL, NULL, '2', NULL, NULL, NULL, 92108.00, '2018-09-20', NULL, NULL, NULL, 0),
	(55, 55, 25, NULL, NULL, NULL, '2', NULL, NULL, NULL, 92108.00, '2018-09-20', NULL, NULL, NULL, 0),
	(56, 56, 21, NULL, NULL, NULL, '2', NULL, NULL, NULL, 58717.00, '2018-09-20', NULL, NULL, NULL, 0),
	(57, 57, 25, NULL, NULL, NULL, '8', NULL, NULL, NULL, 102217.00, '2018-09-20', NULL, NULL, NULL, 0),
	(58, 58, 9, NULL, NULL, NULL, '2', NULL, NULL, NULL, 18718.00, '2018-09-20', NULL, NULL, NULL, 0),
	(59, 59, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 43371.00, '2018-09-20', NULL, NULL, NULL, NULL),
	(60, 60, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 24224.00, '2018-09-20', NULL, NULL, NULL, NULL),
	(61, 61, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 102217.00, '2018-09-20', NULL, NULL, NULL, NULL),
	(62, 62, 21, NULL, NULL, NULL, '2', NULL, NULL, NULL, 58717.00, '2018-09-20', NULL, NULL, NULL, 0),
	(63, 63, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 42099.00, '2018-09-20', NULL, NULL, NULL, NULL),
	(64, 64, 24, NULL, NULL, NULL, '2', NULL, NULL, NULL, 82439.00, '2018-09-20', NULL, NULL, NULL, 0),
	(65, 65, 21, NULL, NULL, NULL, '4', NULL, NULL, NULL, 60491.00, '2018-09-20', NULL, NULL, NULL, 0),
	(66, 66, 25, NULL, NULL, NULL, '3', NULL, NULL, NULL, 93488.00, '2018-09-20', NULL, NULL, NULL, 0),
	(67, 67, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 82439.00, '2018-09-20', NULL, NULL, NULL, NULL),
	(68, 68, 21, NULL, NULL, NULL, '1', NULL, NULL, NULL, 61397.00, '2018-09-20', NULL, NULL, NULL, 0),
	(69, 69, 12, NULL, NULL, NULL, '2', NULL, NULL, NULL, 24224.00, '2018-09-20', NULL, NULL, NULL, 0),
	(70, 70, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 31765.00, '2018-09-20', NULL, NULL, NULL, 0),
	(71, 71, 12, NULL, NULL, NULL, '2', NULL, NULL, NULL, 24224.00, '2018-09-20', NULL, NULL, NULL, 0),
	(72, 72, 17, NULL, NULL, NULL, '4', NULL, NULL, NULL, 39007.00, '2018-09-20', NULL, NULL, NULL, 0),
	(73, 73, 21, NULL, NULL, NULL, '4', NULL, NULL, NULL, 60491.00, '2018-09-20', NULL, NULL, NULL, 0),
	(74, 74, 14, NULL, NULL, NULL, '4', NULL, NULL, NULL, 29713.00, '2018-09-20', NULL, NULL, NULL, 0),
	(75, 75, 29, NULL, NULL, NULL, '3', 'Initial Salary', 0.00, 0.00, 42099.00, '2018-09-20', NULL, '2018-09-25 02:41:16', NULL, 0),
	(76, 76, 12, NULL, NULL, NULL, '2', NULL, NULL, NULL, 24224.00, '2018-09-20', NULL, NULL, NULL, 0),
	(77, 77, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 24224.00, '2018-09-20', NULL, NULL, NULL, 0),
	(78, 78, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 26689.55, '2018-09-20', NULL, NULL, NULL, NULL),
	(79, 79, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 38085.00, '2018-09-20', NULL, NULL, NULL, 0),
	(80, 80, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 58717.00, '2018-09-20', NULL, NULL, NULL, NULL),
	(81, 81, NULL, NULL, NULL, NULL, '2', NULL, NULL, NULL, 29010.00, '2018-09-20', NULL, NULL, NULL, 0),
	(82, 82, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 11914.00, '2018-09-20', NULL, NULL, NULL, NULL),
	(83, 83, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 12674.00, '2018-09-20', NULL, NULL, NULL, 0),
	(84, 84, 12, NULL, NULL, NULL, '2', NULL, NULL, NULL, 24224.00, '2018-09-20', NULL, NULL, NULL, 0),
	(85, 85, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 24224.00, '2018-09-20', NULL, NULL, NULL, NULL),
	(86, 86, 21, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 58717.00, '2018-09-20', NULL, NULL, NULL, 0),
	(87, 87, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 58717.00, '2018-09-20', NULL, NULL, NULL, 0),
	(88, 88, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 24224.00, '2018-09-20', NULL, NULL, NULL, NULL),
	(89, 89, NULL, NULL, NULL, NULL, '2', NULL, NULL, NULL, 24224.00, '2018-09-20', NULL, NULL, NULL, 0);
/*!40000 ALTER TABLE `pms_salaryinfo` ENABLE KEYS */;

-- Dumping structure for table pids_hris.pms_specialpayroll_transactions
DROP TABLE IF EXISTS `pms_specialpayroll_transactions`;
CREATE TABLE IF NOT EXISTS `pms_specialpayroll_transactions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) DEFAULT NULL,
  `benefit_info_id` int(11) DEFAULT NULL,
  `position_item_id` int(11) DEFAULT NULL,
  `position_id` int(11) DEFAULT NULL,
  `division_id` int(11) DEFAULT NULL,
  `office_id` int(11) DEFAULT NULL,
  `percentage` decimal(9,2) DEFAULT NULL,
  `cost_uniform_amount` decimal(9,2) DEFAULT NULL,
  `no_of_months_entitled` decimal(9,2) DEFAULT NULL,
  `cash_gift_amount` decimal(9,2) DEFAULT NULL,
  `cna_amount` decimal(9,2) DEFAULT NULL,
  `amount` decimal(9,2) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `year` varchar(50) DEFAULT NULL,
  `month` varchar(50) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table pids_hris.pms_specialpayroll_transactions: ~0 rows (approximately)
/*!40000 ALTER TABLE `pms_specialpayroll_transactions` DISABLE KEYS */;
/*!40000 ALTER TABLE `pms_specialpayroll_transactions` ENABLE KEYS */;

-- Dumping structure for table pids_hris.pms_taxannualtable
DROP TABLE IF EXISTS `pms_taxannualtable`;
CREATE TABLE IF NOT EXISTS `pms_taxannualtable` (
  `id` bigint(50) NOT NULL AUTO_INCREMENT,
  `code` varchar(10) DEFAULT NULL,
  `name` varchar(300) DEFAULT NULL,
  `below` decimal(6,2) DEFAULT NULL,
  `above` decimal(6,2) DEFAULT NULL,
  `rate` varchar(255) DEFAULT NULL,
  `effectivity_date` date DEFAULT NULL,
  `remarks` varchar(300) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table pids_hris.pms_taxannualtable: ~0 rows (approximately)
/*!40000 ALTER TABLE `pms_taxannualtable` DISABLE KEYS */;
/*!40000 ALTER TABLE `pms_taxannualtable` ENABLE KEYS */;

-- Dumping structure for table pids_hris.pms_taxespolicy
DROP TABLE IF EXISTS `pms_taxespolicy`;
CREATE TABLE IF NOT EXISTS `pms_taxespolicy` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `policy_name` varchar(225) DEFAULT NULL,
  `pay_period` varchar(225) DEFAULT NULL,
  `deduction_period` varchar(225) DEFAULT NULL,
  `policy_type` varchar(225) DEFAULT NULL,
  `based_on` varchar(225) DEFAULT NULL,
  `computation` varchar(225) DEFAULT NULL,
  `is_withholding` varchar(225) DEFAULT NULL,
  `job_grade_rate` decimal(9,2) DEFAULT NULL,
  `remarks` varchar(225) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

-- Dumping data for table pids_hris.pms_taxespolicy: ~6 rows (approximately)
/*!40000 ALTER TABLE `pms_taxespolicy` DISABLE KEYS */;
INSERT INTO `pms_taxespolicy` (`id`, `policy_name`, `pay_period`, `deduction_period`, `policy_type`, `based_on`, `computation`, `is_withholding`, `job_grade_rate`, `remarks`, `created_at`, `updated_at`, `updated_by`) VALUES
	(2, 'STANDARD POLICY', 'Monthly', 'Both', 'System Generated', 'Gross Salary', 'Gross Taxable', 'Monthly', NULL, NULL, '2018-06-19 17:24:26', '2018-06-21 04:31:17', NULL),
	(6, 'EWT 2 %', 'Monthly', 'Both', 'System Generated', 'Gross Salary', 'Gross Taxable', 'Annual', 0.02, NULL, '2018-08-07 06:01:11', '2018-08-09 09:45:31', NULL),
	(7, 'EWT 5 %', 'Monthly', 'Both', 'System Generated', 'Gross Salary', 'Gross Taxable', NULL, 0.05, NULL, '2018-08-07 06:01:49', '2018-08-07 06:01:49', NULL),
	(8, 'PWT 5 %', 'Monthly', 'Both', 'System Generated', 'Gross Salary', 'Gross Taxable', NULL, 0.05, NULL, '2018-08-07 06:02:10', '2018-08-07 06:02:10', NULL),
	(9, 'ITW 10 %', 'Monthly', 'Both', 'System Generated', 'Gross Salary', 'Gross Taxable', NULL, 0.10, NULL, '2018-08-09 09:46:08', '2018-08-09 09:46:08', NULL),
	(10, 'ITW 3 %', 'Monthly', 'Both', 'System Generated', 'Gross Salary', 'Gross Taxable', 'Annual', 0.03, NULL, '2018-08-09 09:46:49', '2018-08-09 09:48:15', NULL);
/*!40000 ALTER TABLE `pms_taxespolicy` ENABLE KEYS */;

-- Dumping structure for table pids_hris.pms_taxtable
DROP TABLE IF EXISTS `pms_taxtable`;
CREATE TABLE IF NOT EXISTS `pms_taxtable` (
  `id` bigint(50) NOT NULL AUTO_INCREMENT,
  `payperiod` varchar(225) DEFAULT NULL,
  `salary_bracket_level1` decimal(9,2) DEFAULT NULL,
  `salary_bracket_level2` decimal(9,2) DEFAULT NULL,
  `salary_bracket_level3` decimal(9,2) DEFAULT NULL,
  `salary_bracket_level4` decimal(9,2) DEFAULT NULL,
  `salary_bracket_level5` decimal(9,2) DEFAULT NULL,
  `salary_bracket_level6` decimal(9,2) DEFAULT NULL,
  `status` varchar(225) DEFAULT NULL,
  `effectivity_date` date DEFAULT NULL,
  `remarks` varchar(300) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Dumping data for table pids_hris.pms_taxtable: ~4 rows (approximately)
/*!40000 ALTER TABLE `pms_taxtable` DISABLE KEYS */;
INSERT INTO `pms_taxtable` (`id`, `payperiod`, `salary_bracket_level1`, `salary_bracket_level2`, `salary_bracket_level3`, `salary_bracket_level4`, `salary_bracket_level5`, `salary_bracket_level6`, `status`, `effectivity_date`, `remarks`, `created_at`, `updated_at`, `updated_by`) VALUES
	(1, 'Daily', 0.00, 0.00, 82.19, 356.16, 1342.47, 6602.74, NULL, '2018-06-12', NULL, '2018-06-11 09:52:22', '2018-06-11 09:52:22', NULL),
	(2, 'Monthly', 0.00, 0.00, 2500.00, 10833.33, 40833.33, 200833.33, NULL, '2018-06-12', NULL, '2018-06-11 09:52:22', '2018-06-11 09:52:22', NULL),
	(3, 'Semi Monthly', 0.00, 0.00, 1250.00, 5416.67, 20416.67, 100416.67, NULL, '2018-06-12', NULL, '2018-06-11 09:52:22', '2018-06-11 09:52:22', NULL),
	(4, 'Weekly', 0.00, 0.00, 576.92, 2500.00, 9423.08, 46346.15, NULL, '2018-06-12', NULL, '2018-06-11 09:52:22', '2018-06-11 09:52:22', NULL);
/*!40000 ALTER TABLE `pms_taxtable` ENABLE KEYS */;

-- Dumping structure for table pids_hris.pms_transactions
DROP TABLE IF EXISTS `pms_transactions`;
CREATE TABLE IF NOT EXISTS `pms_transactions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) DEFAULT NULL,
  `salaryinfo_id` int(11) DEFAULT NULL,
  `division_id` int(11) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `position_item_id` int(11) DEFAULT NULL,
  `position_id` int(11) DEFAULT NULL,
  `office_id` int(11) DEFAULT NULL,
  `department_id` int(11) DEFAULT NULL,
  `empstatus_id` int(11) DEFAULT NULL,
  `employeeinfo_id` int(11) DEFAULT NULL,
  `actual_workdays` int(11) DEFAULT NULL,
  `adjust_workdays` int(11) DEFAULT NULL,
  `actual_absences` int(11) DEFAULT NULL,
  `adjust_absences` int(11) DEFAULT NULL,
  `actual_tardiness` int(11) DEFAULT NULL,
  `adjust_tardiness` int(11) DEFAULT NULL,
  `actual_undertime` int(11) DEFAULT NULL,
  `adjust_undertime` int(11) DEFAULT NULL,
  `actual_basicpay_amount` decimal(9,2) DEFAULT NULL,
  `adjust_basicpay_amount` decimal(9,2) DEFAULT NULL,
  `total_basicpay_amount` decimal(9,2) DEFAULT NULL,
  `actual_absences_amount` decimal(9,2) DEFAULT NULL,
  `adjust_absences_amount` decimal(9,2) DEFAULT NULL,
  `total_absences_amount` decimal(9,2) DEFAULT NULL,
  `actual_tardines_amount` decimal(9,2) DEFAULT NULL,
  `adjust_tardines_amount` decimal(9,2) DEFAULT NULL,
  `total_tardines_amount` decimal(9,2) DEFAULT NULL,
  `actual_undertime_amount` decimal(9,2) DEFAULT NULL,
  `adjust_undertime_amount` decimal(9,2) DEFAULT NULL,
  `total_undertime_amount` decimal(9,2) DEFAULT NULL,
  `basic_net_pay` decimal(9,2) DEFAULT NULL,
  `actual_contribution` decimal(9,2) DEFAULT NULL,
  `adjust_contribution` decimal(9,2) DEFAULT NULL,
  `total_contribution` decimal(9,2) DEFAULT NULL,
  `actual_loan` decimal(9,2) DEFAULT NULL,
  `adjust_loan` decimal(9,2) DEFAULT NULL,
  `total_loan` decimal(9,2) DEFAULT NULL,
  `actual_otherdeduct` decimal(9,2) DEFAULT NULL,
  `adjust_otherdeduct` decimal(9,2) DEFAULT NULL,
  `total_otherdeduct` decimal(9,2) DEFAULT NULL,
  `net_deduction` decimal(9,2) DEFAULT NULL,
  `ecc_amount` decimal(9,2) DEFAULT NULL,
  `gross_pay` decimal(9,2) DEFAULT NULL,
  `gross_taxable_pay` decimal(9,2) DEFAULT NULL,
  `net_pay` decimal(9,2) DEFAULT NULL,
  `pay_period` varchar(225) DEFAULT NULL,
  `hold` int(11) DEFAULT NULL,
  `year` varchar(225) DEFAULT NULL,
  `month` varchar(225) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=268 DEFAULT CHARSET=latin1;

-- Dumping data for table pids_hris.pms_transactions: ~9 rows (approximately)
/*!40000 ALTER TABLE `pms_transactions` DISABLE KEYS */;
INSERT INTO `pms_transactions` (`id`, `employee_id`, `salaryinfo_id`, `division_id`, `company_id`, `position_item_id`, `position_id`, `office_id`, `department_id`, `empstatus_id`, `employeeinfo_id`, `actual_workdays`, `adjust_workdays`, `actual_absences`, `adjust_absences`, `actual_tardiness`, `adjust_tardiness`, `actual_undertime`, `adjust_undertime`, `actual_basicpay_amount`, `adjust_basicpay_amount`, `total_basicpay_amount`, `actual_absences_amount`, `adjust_absences_amount`, `total_absences_amount`, `actual_tardines_amount`, `adjust_tardines_amount`, `total_tardines_amount`, `actual_undertime_amount`, `adjust_undertime_amount`, `total_undertime_amount`, `basic_net_pay`, `actual_contribution`, `adjust_contribution`, `total_contribution`, `actual_loan`, `adjust_loan`, `total_loan`, `actual_otherdeduct`, `adjust_otherdeduct`, `total_otherdeduct`, `net_deduction`, `ecc_amount`, `gross_pay`, `gross_taxable_pay`, `net_pay`, `pay_period`, `hold`, `year`, `month`, `created_at`, `updated_at`, `updated_by`) VALUES
	(179, 75, 75, NULL, NULL, 1, 227, 3, 4, 4, 1, 22, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 147869.00, NULL, NULL, 4438.91, NULL, NULL, 0.00, NULL, NULL, 0.00, 4438.91, 100.00, 147869.00, 143430.09, 139848.32, NULL, NULL, '2018', 'January', '2018-10-15 10:17:01', '2018-10-15 10:17:01', NULL),
	(188, 76, 76, NULL, NULL, NULL, NULL, NULL, 10, 4, 11, 22, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 24224.00, NULL, NULL, 2613.24, NULL, NULL, 0.00, NULL, NULL, 0.00, 2613.24, 100.00, 24224.00, 21610.76, 21455.21, NULL, NULL, '2018', 'January', '2018-10-15 10:17:02', '2018-10-15 10:17:02', NULL),
	(196, 77, 77, NULL, NULL, NULL, NULL, NULL, 10, 4, 18, 22, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 24224.00, NULL, NULL, 2613.24, NULL, NULL, 0.00, NULL, NULL, 0.00, 2613.24, 100.00, 24224.00, 21610.76, 21455.21, NULL, NULL, '2018', 'January', '2018-10-15 10:17:04', '2018-10-15 10:17:04', NULL),
	(198, 78, 78, NULL, NULL, NULL, NULL, NULL, 10, 4, 20, 22, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 26689.55, NULL, NULL, 2869.04, NULL, NULL, 0.00, NULL, NULL, 0.00, 2869.04, 100.00, 26689.55, 23820.51, 23223.01, NULL, NULL, '2018', 'January', '2018-10-15 10:17:04', '2018-10-15 10:17:04', NULL),
	(199, 79, 79, NULL, NULL, NULL, NULL, NULL, 10, 4, 21, 22, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 38085.00, NULL, NULL, 4051.32, NULL, NULL, 13817.28, NULL, NULL, 5033.45, 22902.05, 100.00, 38085.00, 34033.68, 31358.51, NULL, NULL, '2018', 'January', '2018-10-15 10:17:04', '2018-10-15 10:17:04', NULL),
	(202, 80, 80, NULL, NULL, NULL, NULL, NULL, 10, 4, 24, 22, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 58717.00, NULL, NULL, 5934.53, NULL, NULL, 0.00, NULL, NULL, 4850.49, 10785.02, 100.00, 58717.00, 52782.47, 45420.10, NULL, NULL, '2018', 'January', '2018-10-15 10:17:05', '2018-10-15 10:17:05', NULL),
	(214, 81, 81, NULL, NULL, NULL, NULL, NULL, 10, 4, 36, 22, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 29010.00, NULL, NULL, 3109.79, NULL, NULL, 0.00, NULL, NULL, 0.00, 3109.79, 100.00, 29010.00, 25900.21, 24886.77, NULL, NULL, '2018', 'January', '2018-10-15 10:17:07', '2018-10-15 10:17:07', NULL),
	(217, 82, 82, NULL, NULL, NULL, NULL, NULL, 10, 4, 39, 22, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 11914.00, NULL, NULL, 1336.08, NULL, NULL, 1029.32, NULL, NULL, 0.00, 2365.40, 100.00, 11914.00, 10577.92, 10577.92, NULL, NULL, '2018', 'January', '2018-10-15 10:17:08', '2018-10-15 10:17:08', NULL),
	(220, 83, 83, NULL, NULL, NULL, NULL, NULL, 10, 4, 42, 22, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 12674.00, NULL, NULL, 1414.93, NULL, NULL, 235.79, NULL, NULL, 0.00, 1650.72, 100.00, 12674.00, 11259.07, 11259.07, NULL, NULL, '2018', 'January', '2018-10-15 10:17:08', '2018-10-15 10:17:08', NULL);
/*!40000 ALTER TABLE `pms_transactions` ENABLE KEYS */;

-- Dumping structure for table pids_hris.pms_travelrates
DROP TABLE IF EXISTS `pms_travelrates`;
CREATE TABLE IF NOT EXISTS `pms_travelrates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rate_amount` decimal(9,2) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table pids_hris.pms_travelrates: ~1 rows (approximately)
/*!40000 ALTER TABLE `pms_travelrates` DISABLE KEYS */;
INSERT INTO `pms_travelrates` (`id`, `rate_amount`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
	(2, 800.00, 'travelrate', 2, NULL, '2018-09-10 10:52:44', '2018-09-10 10:52:44');
/*!40000 ALTER TABLE `pms_travelrates` ENABLE KEYS */;

-- Dumping structure for table pids_hris.pms_travelrate_transactions
DROP TABLE IF EXISTS `pms_travelrate_transactions`;
CREATE TABLE IF NOT EXISTS `pms_travelrate_transactions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) DEFAULT NULL,
  `position_id` int(11) DEFAULT NULL,
  `travel_rate_id` int(11) DEFAULT NULL,
  `rate_id` int(11) DEFAULT NULL,
  `number_of_days` int(11) DEFAULT NULL,
  `travel_rate_amount` decimal(9,2) DEFAULT NULL,
  `net_amount` decimal(9,2) DEFAULT NULL,
  `remarks` varchar(50) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table pids_hris.pms_travelrate_transactions: ~0 rows (approximately)
/*!40000 ALTER TABLE `pms_travelrate_transactions` DISABLE KEYS */;
/*!40000 ALTER TABLE `pms_travelrate_transactions` ENABLE KEYS */;

-- Dumping structure for table pids_hris.pms_wagerates
DROP TABLE IF EXISTS `pms_wagerates`;
CREATE TABLE IF NOT EXISTS `pms_wagerates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wage_region` varchar(225) DEFAULT NULL,
  `wage_rate` decimal(9,2) DEFAULT NULL,
  `effectivity_date` date DEFAULT NULL,
  `based_on` varchar(225) DEFAULT NULL,
  `remarks` varchar(225) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table pids_hris.pms_wagerates: ~1 rows (approximately)
/*!40000 ALTER TABLE `pms_wagerates` DISABLE KEYS */;
INSERT INTO `pms_wagerates` (`id`, `wage_region`, `wage_rate`, `effectivity_date`, `based_on`, `remarks`, `created_at`, `updated_at`, `updated_by`) VALUES
	(1, 'National Capital Region', 200.00, '2018-06-19', NULL, NULL, '2018-06-12 03:34:42', '2018-06-12 03:34:42', NULL);
/*!40000 ALTER TABLE `pms_wagerates` ENABLE KEYS */;

-- Dumping structure for table pids_hris.users
DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table pids_hris.users: ~2 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
	(1, 'Jezon Gonzales', 'gonzales_jezon@yahoo.com', '$2y$10$o9TEoA6PVskFUo0cZ.0c/eHzcjnkBKYnN5FViptfAYPGbaX0rob8S', '6ugWfG2iGS1MyxKSpyo7GFnYUogTUJVOmCIANAne2mDVtNVqXPDhzE3jtb3z', '2018-07-13 08:52:26', '2018-07-13 08:52:26'),
	(2, 'Tester', 'admintester@gmail.com', '$2y$10$V5JxbFFe/k1xa2L0xh6.Cubom4q2l2A/9DEPC/jJVcVWORKFr.ycq', 'nGdusyX4f9Gzm8n0340Iesh51ItXoE3IckpR6r4ZDCNCeobZyVHTLRy5JwFm', '2018-07-15 02:12:16', '2018-07-15 02:12:16');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
